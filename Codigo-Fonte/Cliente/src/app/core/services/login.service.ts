import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { UsuarioService } from "../../features/usuario/usuario.service";
import { AppConstants } from "../settings/app-constants";

@Injectable({
  providedIn: "root",
})
export class LoginService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private usuarioService: UsuarioService
  ) {}

  ngOnInit() {}''

  login(usuario) {
    return this.http.post(AppConstants.baseLogin, JSON.stringify(usuario)).subscribe(
        (data) => {
          console.log(data)
          var token = JSON.parse(JSON.stringify(data)).Authorization.split(" ")[1];
          localStorage.setItem("token", token);
          var str = atob(localStorage.getItem("token").split(".")[1]);
          console.log(str)
          this.loginLocalStorage(usuario)


        },
        (error) => {
          console.error(error);
          alert("Acesso Negado!");
        }
      );
  }

  logout(){
    return this.http.get(AppConstants.baseLogout).subscribe(() => this.router.navigate(['login']))
  }

  loginLocalStorage(usuario) {
    return this.usuarioService
            .getByLogin(usuario.login)
            .subscribe(
              (usuarioL) => {
              localStorage.setItem("usuarioL", JSON.stringify(usuarioL));
              localStorage.setItem("user_role", JSON.stringify(usuarioL.authorities));
              console.log(localStorage.getItem('usuarioL'))
              console.log(localStorage.getItem('user_role'))
              this.router.navigate(['dashboard'])
              },
              error => console.error(error)
            );
  }
}
