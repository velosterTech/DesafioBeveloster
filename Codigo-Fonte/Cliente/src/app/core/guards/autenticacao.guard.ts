import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree,
} from "@angular/router";
import { Observable, of } from "rxjs";
import { UsuarioService } from "../../features/usuario/usuario.service";

@Injectable()
export class AutenticacaoGuard implements CanActivate, CanLoad {
  constructor(private userService: UsuarioService, private router: Router) {}

  usuarioAutenticado() {
    console.log('lendo Guard')
    if (this.userService.userAutenticado()) {
      return of(true)
    }
    this.router.navigate(['login'])
    return of(false);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {


    return this.usuarioAutenticado();
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {

    return true
  }
}
