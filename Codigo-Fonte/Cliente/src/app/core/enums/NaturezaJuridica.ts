export enum NaturezaJuridica {
  MEI = "MEI (Microempreendedor Individual)", EI = "EI (Empresário Individual)", LTDA = "LTADA (Sociedade Simples Limitada)", SA = "SA (Sociedade Anônima)", SLU = "SLU (Sociedade Limitada Unipessoal)"
}