import { INavData } from '@coreui/angular';

export const navItemsEsc: INavData[] = [
  {
    title: true,
    name: 'Cadastros'
  },
  {
    name: 'Usuário',
    url: '/usuarios',
    icon: 'icon-user'
  },
  {
    name: 'Empresa',
    url: '/empresas',
    icon: 'icon-briefcase'
  },
  {
    name: 'Departamentos',
    url: '/departamentos',
    icon: 'icon-briefcase',
  },

];

export const navItemsEmp: INavData[] = [
  {
    title: true,
    name: 'Cadastros'
  },
  {
    name: 'Usuário',
    url: '/usuarios',
    icon: 'icon-user'
  }
];


export const navItems: INavData[] = [
  {
    title: true,
    name: 'Cadastros'
  },
  {
    name: 'Usuário',
    url: '/usuarios',
    icon: 'icon-user'
  },
  {
    name: 'Escritório de contabilidade',
    url: '/escritorios',
    icon: 'icon-chart'
  },
  {
    name: 'Empresa',
    url: '/empresas',
    icon: 'icon-briefcase'
  },
  {
    name: 'Departamentos',
    url: '/departamentos',
    icon: 'icon-briefcase',
  },
  // {
  //   name: 'Categoria',
  //   url: '/categorias',
  //   icon: 'icon-briefcase',
  // }
];

