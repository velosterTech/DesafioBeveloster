export class AppConstants {

	public static get baseServidor(): string { return "http://localhost:8080/" }

	public static get baseLogin(): string { return this.baseServidor + "wk/login" }

  public static get baseLogout(): string { return this.baseServidor + "wk/logout" }

	public static get baseUrlUsuario(): string {return this.baseServidor + "wk/usuario/"}

	public static get baseUrlDocumento(): string {return this.baseServidor + "wk/documento/"}

	public static get baseUrlEscritorioContabil(): string {return this.baseServidor + "wk/escritorio/"}

	public static get baseUrlEmpresa(): string {return this.baseServidor + "wk/empresa/"}

	public static get baseUrlMensagem(): string {return this.baseServidor + "wk/mensagem/"}

	public static get baseUrlTipoDocumentos(): string {return this.baseServidor + "wk/tipodocumentos/"}

	public static get baseUrlCategoria(): string {return this.baseServidor + "wk/categoria/"}

	public static get baseUrlDepartamento(): string {return this.baseServidor + "wk/departamento/"}

	public static get baseUrlConfigurar(): string {return this.baseServidor + "wk/configuracao/"}

}
