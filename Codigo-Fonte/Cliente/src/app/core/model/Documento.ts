
import { Status } from "../enums/Status";
import { Usuario } from "./Usuario";

export class Documento {

    id: number;
    usuarioId: Usuario;
    competencia: string;
    valorVencimento: number;
    categoria: string;
    statusEnvio: Status;


}
