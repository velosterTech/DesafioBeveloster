import { EscritorioContabil } from "./EscritorioContabil";

export interface ITiposDocumentosDTO{
  nomeDocumento?: string | null;
  escritorioContabilId?: number | undefined;
  categoriaId?: number | undefined;
}

export class TiposDocumentosDTO implements ITiposDocumentosDTO{
  constructor(
    public nomeDocumento?: string | null,
    public escritorioContabilId?: number | undefined,
    public categoriaId?: number | undefined
  ){}
}
