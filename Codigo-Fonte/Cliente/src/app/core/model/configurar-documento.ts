import { Concluir } from "../enums/Concluir";

export interface ConfigurarDocumento{
  id: number;
  documentoId: number;
  concluir: Concluir;
  data: string
  regras: Array<string>
  file: any;
  documento: string;
  cnpjEscritorio: string;
}

