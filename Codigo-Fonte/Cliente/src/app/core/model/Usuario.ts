import { Endereco } from "./Endereco";
import { IRole } from "./Role";

export class Usuario {
  id: number;
  nome: string;
  rg: string;
  cpf: string;
  celular: string;
  email: string;
  endereco: Endereco;
  login: string;
  senha: string;
  authorities?: IRole[] | null;
}
