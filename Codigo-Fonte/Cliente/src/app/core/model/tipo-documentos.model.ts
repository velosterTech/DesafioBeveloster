import { ICategoria } from "./categoria.model";
import { EscritorioContabil } from "./EscritorioContabil";


export interface ITiposDocumentos{
  id?: number;
  nomeDocumento?: string | null;
  escritorioContabil?: EscritorioContabil | null;
  categoria?: ICategoria | null;
}

export class TiposDocumentos implements ITiposDocumentos{
  constructor(
    public id?: number,
    public nomeDocumento?: string | null,
    public escritorioContabil?: EscritorioContabil | null,
    public categoria?: ICategoria | null
  ){}
}

export function getRoleIdentifier(tiposDocumentos: ITiposDocumentos): number | undefined {
  return tiposDocumentos.id;
}
