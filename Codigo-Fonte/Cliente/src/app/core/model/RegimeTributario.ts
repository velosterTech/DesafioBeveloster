import { RegimeTributarioEnum } from "../enums/RegimeTributario";

export interface RegimeTributario {
  nome: RegimeTributarioEnum | null;
}
