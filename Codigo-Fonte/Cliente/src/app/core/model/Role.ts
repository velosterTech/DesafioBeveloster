export interface IRole{
  id?: number;
  nomeRole?: string | null;
}

export class Role implements IRole{
  constructor(
    public id?: number,
    public nomeRole?: string | null
  ){}
}

export function getRoleIdentifier(role: IRole): number | undefined {
  return role.id;
}