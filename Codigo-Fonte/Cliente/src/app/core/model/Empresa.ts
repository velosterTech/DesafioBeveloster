import { NaturezaJuridica } from "../enums/NaturezaJuridica";

import { EscritorioContabil } from "./EscritorioContabil";
import { RegimeTributario } from "./RegimeTributario";

export interface Empresa {

  id?: number;
  escritorio: string;
  cnpj: string;
  razaoSocial: string;
  nomeFantasia: string;
  inscricaoEstadual: string | undefined;
  cep: string;
  logradouro: string;
  bairro: string;
  numero: string
  complemento: string;
  contato: string
  email: string;
  codCnaePrimaria: string;
  atvdPrimaria: string;
  codCnaeSecundaria: string;
  atvdSecundaria: string;
  regimeTributario: RegimeTributario;
  naturezaJuridica: NaturezaJuridica | null;
  nomeSolicitante: string;
  cpf: string;
  emailSolicitante: string;
  celular: string;
}
