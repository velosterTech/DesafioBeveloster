import { EscritorioContabil } from "./EscritorioContabil";

export interface ICategoria{
  id?: number;
  nomeCategoria?: string | null;
}

export class Categoria implements ICategoria{
  constructor(
    public id?: number,
    public nomeCategoria?: string | null,
  ){}
}

export function getRoleIdentifier(categoria: ICategoria): number | undefined {
  return categoria.id;
}
