import { Endereco } from "./Endereco";

export interface EscritorioContabil {
  id?: number;
  cnpj: number;
  razaoSocial: string;
  nomeFantasia: string;
  endereco: Endereco
  telefoneFixo: number;
  email: string;
  codigoPrimariaCnae: number;
  atividadePrimariaCnae: string;
  codigoSecundariaCnae: number;
  atividadeSecundariaCnae: string;

  nomeSolicitante: string;
  cpfSolicitante: number;
  crc: string;
  telefoneCelular: string;
  emailResponsavel: string;
}
