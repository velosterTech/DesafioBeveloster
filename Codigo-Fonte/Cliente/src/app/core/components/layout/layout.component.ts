import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { MensagemService } from "../../../features/mensagem/mensagem.service";
import { UsuarioService } from "../../../features/usuario/usuario.service";
import { LoginService } from "../../services/login.service";

import { navItems, navItemsEmp, navItemsEsc } from "../../settings/_nav";

@Component({
  selector: "app-layout",
  templateUrl: "./layout.component.html",
  styleUrls: ["./layout.component.scss"],
})
export class LayoutComponent implements OnInit {
  idUsuario: number;
  url: string;
  nMensagens: number;
  usuarioL: any;

  admin: boolean;
  admin_escritorio: boolean;
  admin_empresa: boolean;

  constructor(
    private router: Router,
    private mensagemService: MensagemService,
    private usuarioService: UsuarioService,
    private loginService: LoginService
  ) {}

  ngOnInit() {

    const usuarioLogin = JSON.parse(localStorage.getItem('usuario'));
    this.usuarioService.getByLogin(usuarioLogin.login).subscribe(
      usuarioL => {
        const perfis = usuarioL.authorities;

        perfis.forEach((p) => {
          if (p.nome === "ROLE_ADMIN") {
            this.setAdmin();
          }
        });

        if (!this.admin) {
          perfis.forEach((p) => {
            if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
              this.setAdminEscritorio();
            } else if (p.nome === "ROLE_ADMIN_EMPRESA") {
              this.setAdminEmpresa();
              console.log(this.admin_empresa)
            }
          });
        }
      }
    )

    this.url = window.location.pathname;
    this.receberMensagensNLidas();
  }

  public sidebarMinimized = false;
  public navItems = navItems;
  public navItemsEsc = navItemsEsc;
  public navItemsEmp = navItemsEmp;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  // possuiRole(role: string) : boolean{
  //   return this.roles.find((roleUsuario) => roleUsuario === role)
  // }

  receberMensagensNLidas() {
    // usuarioL = JSON.parse(localStorage.getItem('usuario'));
    // this.usuarioService.getByLogin(usuarioL.login).subscribe(dados => {
    //   this.mensagemService.receberMensagem(dados.id).subscribe(data => {
    //     let msgNLida = [];
    //     data.forEach((item) => {
    //       if(item.status === false){
    //         msgNLida.push(item);
    //       }
    //     });
    //     this.setNMensagens(msgNLida.length);
    //   });
    // });
  }

  logout(){
    localStorage.clear();
    this.loginService.logout();
  }

  setIdUsuario(id: number) {
    this.idUsuario = id;
  }
  setNMensagens(n: number) {
    this.nMensagens = n;
  }
  setUsuarioL(value: any) {
    this.usuarioL = value;
  }
  setAdminEmpresa() {
    this.admin_empresa = true;
  }
  setAdminEscritorio() {
    this.admin_escritorio = true;
  }
  setAdmin() {
    this.admin = true;
  }
}
