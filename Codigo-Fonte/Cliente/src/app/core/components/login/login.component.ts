import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { UsuarioService } from '../../../features/usuario/usuario.service';
import { User } from '../../model/User';
import { LoginService } from '../../services/login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User = { login: '', senha: '' };
  radio : number = 0;
  senha2: string;


  constructor(
    private loginService: LoginService,
    private router: Router,
    private userService: UsuarioService,
    ) { }


  ngOnInit() {

  }

  public logar() {
    localStorage.setItem("usuario", JSON.stringify(this.user));
    this.loginService.login(this.user);
  }

  novo() {
    this.user = { login: '', senha: '' };
  }


  radio1clicado(){
      this.radio=1;
    }
    radio2clicado(){
     this.radio=2;
   }



}
