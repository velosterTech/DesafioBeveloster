import { Component, OnInit } from "@angular/core";
import { switchMap } from "rxjs/operators";
import { UsuarioService } from "../../../features/usuario/usuario.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
  admin: boolean;
  admin_escritorio: boolean;
  admin_empresa: boolean;
  roleComum: boolean = false;

  constructor(private usuarioService: UsuarioService) {}

  ngOnInit(): void {
    const perfis = JSON.parse(localStorage.getItem("user_role"));

    console.log(perfis)

    if (perfis === undefined || null) {
      const usuario = JSON.parse(localStorage.getItem("usuario"));
      this.usuarioService
        .getByLogin(usuario.login)
        .subscribe((u) => this.settingAdmin(u.perfis));
    } else {
      this.settingAdmin(perfis);
    }

    console.log(this.admin_escritorio)
  }

  settingAdmin(perfis) {
    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
      }
    });
    if (!this.admin) {
      perfis.forEach((p) => {
        if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
          this.setAdminEscritorio();
        } else if (p.nome === "ROLE_ADMIN_EMPRESA") {
          this.setAdminEmpresa();
          console.log(this.admin_empresa);
        } else {
          this.setRolecomum();
        }
      });
    }
  }

  setAdminEmpresa() {
    this.admin_empresa = true;
  }
  setAdminEscritorio() {
    this.admin_escritorio = true;
  }
  setAdmin() {
    this.admin = true;
  }

  setRolecomum() {
    this.roleComum = true;
  }
}
