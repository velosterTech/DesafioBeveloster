import { Component, OnInit } from "@angular/core";
import { RouterEvent, Event, Router, NavigationEnd } from "@angular/router";
import { DEFAULT_INTERRUPTSOURCES, Idle } from "@ng-idle/core";
import { filter } from "rxjs/operators";
import { Navigation } from "selenium-webdriver";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "Curso-Angular-REST";
  idleState = "Not started.";

  constructor(private idle: Idle, private router: Router) {
    this.setIdle(idle);
  }

  private setIdle(idle) {
    idle.setIdle(300);
    idle.setTimeout(600);
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => {
      this.idleState = "No longer idle.";
      console.log(this.idleState);
    });
    idle.onTimeout.subscribe(() => {
      localStorage.clear();
      this.idleState = "Timed out!";
      this.router.navigate(["/login"]);
    });

    this.router.events.pipe(
      filter((e: Event): e is NavigationEnd => e instanceof NavigationEnd)
    ).subscribe((e) => {
      if (e.urlAfterRedirects !== "/login"){
        console.log("teste")
        this.reset();
      }
    });

    idle.onIdleStart.subscribe(() => {
      this.idleState = "You've gone idle!";

    });
    idle.onTimeoutWarning.subscribe((countdown) => {
      this.idleState = "You will time out in " + countdown + " seconds!";
      console.log(this.idleState);
    });

  }

  reset() {
    this.idle.watch();
    this.idleState = "Started.";
  }

  ngOnInit(): void {
    this.setLoginExpiration();
  }

  public sair() {
    localStorage.clear();
    this.router.navigate(["login"]);
  }

  public esconderBarrar() {
    if (
      localStorage.getItem("token") !== null &&
      localStorage.getItem("token").toString().trim() !== null
    ) {
      return false;
    } else {
      return true;
    }
  }

  items = [{ label: "Sair", icon: "pi pi-fw pi-power-off" }];

  private setLoginExpiration() {
    let hours = 24; // Reset when storage in hours
    let now = new Date().getTime();
    let setupTime = localStorage.getItem("setupTime");
    if (setupTime == null) {
      localStorage.setItem("setupTime", JSON.stringify(now));
    } else {
      if (now - JSON.parse(setupTime) > hours * 60 * 60 * 1000) {
        localStorage.clear();
        localStorage.setItem("setupTime", JSON.stringify(now));
      }
    }
  }
}
