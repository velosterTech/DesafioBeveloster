import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Usuario } from '../../core/model/Usuario';
import { AppConstants } from '../../core/settings/app-constants';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
   })
};

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {


  constructor(private http: HttpClient) {
  }

  salvarUsuario(user, cnpj): Observable<any> {
    let params = new HttpParams();
    params = params.set('cnpj', cnpj);



    return this.http.post<any>(AppConstants.baseUrlUsuario + "cadastrar", user, {params: params});

  }

  getUsuarios(id, status, search): Observable<Usuario[]>{
    let params = new HttpParams();

    params = params.set('id', id);
    params = params.set('status', status);
    params = params.set('search', search);
    return this.http.get<Usuario[]>(AppConstants.baseUrlUsuario + "todos", {params: params});

  }
  getByLogin(login: string): Observable<any>{
    let params = new HttpParams();

    params = params.set('login',login);
    return this.http.get<Usuario>(AppConstants.baseUrlUsuario + "buscaLogin", {params: params});
  }

  getById(id){
    return this.http.get<any>(AppConstants.baseUrlUsuario + id);
  }

  atualizaUsuario(usuario, cnpj){
    let params = new HttpParams();
    params = params.set('cnpj', cnpj);
    return this.http.put(AppConstants.baseUrlUsuario + "update/" + usuario.id, usuario, {params: params});
  }

  statusUsuario(id){
    return this.http.put(AppConstants.baseUrlUsuario + "status", id);
  }

  userAutenticado() {
    console.log('lendo Service')
    let token = localStorage.getItem('token')
    console.log(token)
    if (token !== null && token.toString().trim() !== null){
      return true
    }
    return false;
  }

}
