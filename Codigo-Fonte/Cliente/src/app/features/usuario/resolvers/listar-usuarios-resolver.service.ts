import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from '../../../core/model/Usuario';
import { UsuarioService } from '../usuario.service';


@Injectable({
  providedIn: 'root'
})
export class ListarUsuariosResolverService implements Resolve<Usuario[]> {

  constructor(private usuarioService: UsuarioService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Usuario[] | Observable<Usuario[]> | Promise<Usuario[]> {

    console.log('Testando: resolver Usuario service')
    const usuario = JSON.parse(localStorage.getItem('usuarioL'))
    console.log(usuario)
    return this.usuarioService.getUsuarios(usuario.id, false, "")
  }
}
