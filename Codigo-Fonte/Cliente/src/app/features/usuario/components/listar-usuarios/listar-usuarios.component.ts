import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { EMPTY } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  map,
  mergeMap,
  switchMap,
  take,
  tap
} from "rxjs/operators";
import { Usuario } from "../../../../core/model/Usuario";
import { AvisoServiceService } from "../../../../shared/aviso-service.service";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { ConfirmacaoPopUpComponent } from "../../../../shared/components/confirmacao-pop-up/confirmacao-pop-up.component";
import { UsuarioService } from "../../usuario.service";

@Component({
  selector: "app-listar-usuarios",
  templateUrl: "./listar-usuarios.component.html",
  styleUrls: ["./listar-usuarios.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class ListarUsuariosComponent implements OnInit {
  campoPesquisa = new FormControl();
  usuarios: Usuario[] = [];
  /*usuarios: Usuario[] = []; /*= [/*{ nome : "Mateus Azevedo Gomes",
      rg: "XXX.XXX.XXX",
      cpf: "XXX.XXX.XXX-XX",
      celular: "(XX)9XXXX-XXXX",
      email: "usuario@dominio.com",
      endereco: {
        rua: 'Aeroporto de Congonhas',
        numero: 248,
        bairro: 'Emaús',
        cidade: 'Parnamirim',
        estado: 'RN',
        pais: 'BR',
        complemento: 'Casa',
        cep: '59149306'
      },
      senha: '',
      login: ''
    } ];*/

  retornoUsuarios = [];

  totalItems: number;
  currentPage: number = 1;
  smallnumPages: number = 0;

  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;

  currentPager: number = 1;

  buscarTodos = "";

  constructor(
    private usuarioService: UsuarioService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private avisoService: AvisoServiceService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {

    this.usuarios = this.route.snapshot.data.usuarios;
    console.log(this.usuarios)
    if (this.usuarios === undefined) {
      this.receberUsuarios(this.buscarTodos);
    } else {
      this.receberUsuarios(this.usuarios);
    }

    this.campoPesquisa.valueChanges
      .pipe(
        map((value) => value.trim()),
        debounceTime(150),
        distinctUntilChanged(),
        tap((value) => console.log(value)),
        switchMap((value: string) => {
          let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));
          return this.usuarioService.getUsuarios(usuarioL.id, false, value);
        })
      )
      .subscribe((data) => {
        this.usuarios = data;
        this.setTotalItems(data.length);
        this.retornoUsuarios = this.usuarios.slice(0, 5);
      });

  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.retornoUsuarios = this.usuarios.slice(startItem, endItem);
  }

  receberUsuarios(data) {
      const usuario = JSON.parse(localStorage.getItem("usuario"));
      this.usuarioService.getByLogin(usuario.login).pipe(
        tap(e => console.log(e)),
        mergeMap((usuario) => this.usuarioService.getUsuarios(usuario.id, false, "")),
        tap(e => console.log(e))
      )
        .subscribe((usuarios) => {
          this.usuarios = usuarios;
          console.log(usuario)
          this.totalItems = usuarios.length;
          this.retornoUsuarios = usuarios.slice(0, 5);
          if (usuarios.length == 0) {
            console.log("não há empresas cadastradas");
          }
        });

  }
  setTotalItems(lenght: number) {
    this.totalItems = lenght;
  }


  trocarStatus(usuario) {
    if (usuario.status) {
      const result$ = this.avisoService.showAviso('Confirmação', 'Deseja desativar ' + usuario.nome + '?');
      result$.asObservable().pipe(
        take(1),
        switchMap((result) => result ? this.usuarioService.statusUsuario(usuario.id) : EMPTY)).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberUsuarios("");
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Cadastro desativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar cadastro";
          }
        );
    } else {
      const result$ = this.avisoService.showAviso('Confirmação', 'Deseja ativar ' + usuario.nome + '?');
      result$.asObservable().pipe(
        take(1),
        switchMap((result) => result ? this.usuarioService.statusUsuario(usuario.id) : EMPTY)).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberUsuarios("");
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Cadastro ativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar cadastro";
          }
        );
    }
    this.currentPage = 1;

  }

  openDialog(usuario, id): void {
    const dialogRef = this.dialog.open(ConfirmacaoPopUpComponent, {
      width: "500px",
      height: "200px",

      data: { nome: usuario.nome, status: usuario.status },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result ?? 1) {
        this.usuarioService.statusUsuario(id).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberUsuarios("");
          },
          (error) => { console.log(error) }
        );
      }
    });
    this.currentPage = 1;
  }

  ativarDesativar(id) {
    this.usuarioService.statusUsuario(id).subscribe(
      (success) => {
        console.log("sucesso");
        this.receberUsuarios("");
      },
      (error) => { console.log(error) }
    );
    this.currentPage = 1;
  }
}
