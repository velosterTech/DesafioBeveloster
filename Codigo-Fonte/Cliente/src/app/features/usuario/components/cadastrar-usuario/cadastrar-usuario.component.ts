import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { NivelDeAcesso } from "../../../../core/enums/NivelDeAcesso";
import { Endereco } from "../../../../core/model/Endereco";
import { Usuario } from "../../../../core/model/Usuario";
import { CepService } from "../../../../core/services/cep.service";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { EmpresaService } from "../../../empresa/empresa.service";
import { EscritorioContabilService } from "../../../escritorio-contabil/escritorio-contabil.service";
import { UsuarioService } from "../../usuario.service";

@Component({
  selector: "app-cadastrar-usuario",
  templateUrl: "./cadastrar-usuario.component.html",
  styleUrls: ["./cadastrar-usuario.component.scss"],
})
export class CadastrarUsuarioComponent implements OnInit {
  usuario: Usuario;
  endereco: Endereco;
  editando: boolean = false;
  usuarioForm: FormGroup;
  empresas = [];
  escritorios = [];
  btnEmpresa: boolean = true;
  btnEscritorio: boolean = false;
  emp: boolean = false;
  esc: boolean = false;
  admin: boolean = false;
  admin_escritorio: boolean = false;
  admin_empresa: boolean = false;
  escritorioempresaId = 0;
  usuarioId = 0;
  nivelDeAcesso = NivelDeAcesso;
  niveis = [];
  departamentos = [];
  cpf: boolean = false;
  cpfEditando: string = "";
  multiplos: boolean = false;
  editandoAdminEsc: boolean = false;

  constructor(
    private rest: UsuarioService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private empresaService: EmpresaService,
    private escritorioService: EscritorioContabilService,
    private cepService: CepService,
    private http: HttpClient,
    private modalService: BsModalService
  ) {
    this.usuario = new Usuario();
    this.endereco = new Endereco();
    this.niveis = Object.keys(this.nivelDeAcesso);
  }

  ngOnInit(): void {
    let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));
    this.setUsuarioId(usuarioL.id);

    const perfis = JSON.parse(localStorage.getItem("user_role"));
    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
      }
    });

    if (!this.admin) {
      perfis.forEach((p) => {
        if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
          this.setAdminEscritorio();
        } else if (p.nome === "ROLE_ADMIN_EMPRESA") {
          this.setAdminEmpresa();
        }
      });
    }

    this.usuarioForm = this.fb.group({
      id: [null],
      nome: ["", [Validators.required]],
      rg: [""],
      cpf: [
        "",
        [
          Validators.required,
          Validators.minLength(11),
          Validators.maxLength(11),
        ],
      ],
      contato: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      departamento: null,
      cnpj: ["", Validators.required],
    });

    if (!this.editando) {
      if (this.admin_empresa) {

        if (usuarioL.empresas.length === 1) {
          this.usuarioForm.get(["cnpj"])!.setValue(usuarioL.empresas[0].cnpj);
        } else {
          this.setMultiplos();
          let empresa = [];
          usuarioL.empresas.forEach(function (item) {
            empresa.push(item);
          });
          this.empresas = empresa;
        }
        this.setBtnEmpresa();
      } else if (this.admin_escritorio) {

        this.setBtnEmpresa();
        if (usuarioL.escritorios.length === 1) {
          this.usuarioForm.get(["cnpj"])!.setValue(usuarioL.escritorios[0].cnpj);
          this.receberDepartamentos(usuarioL.escritorios[0].cnpj);
        } else {
          this.setMultiplos();
          let escritorio = [];
          usuarioL.escritorios.forEach(function (item) {
            escritorio.push(item);
          });
          this.escritorios = escritorio;
        }

      } else if (this.admin) {
        this.receberEmpresas("");
        this.receberEscritorios("");
      }
    }

    this.route.params
      .pipe(
        map((params: any) => params["id"]),
        switchMap((id) => {
          if (id !== undefined) {
            return this.rest.getById(id);
          } else {
            return new Observable<any>();
          }
        })
      )
      .subscribe((usuario) => {
        this.cpfEdit(usuario.cpf);
        if (usuario.escritorios.length > 0) {
          this.setBtnEmpresa();
          this.setBtnEscritorio();
          usuario.authorities.forEach((p) => {
            if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
              this.setEditandoAdminEsc();
            }
          });

          if (!this.editandoAdminEsc) {
            this.receberDepartamentos(usuario.escritorios[0].cnpj);
          }
        }
        this.updateForm(usuario);
        this.setEditando();
      });

    /*this.usuarioForm = this.fb.group({
      id: [null],
      nome: ["", [Validators.required]],
      rg: [""],
      cpf: ["", [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      contato: [""],
      email: ["", [Validators.required, Validators.email]],
      permissao: "",
      escritorio: 0,
      endereco: this.fb.group ({
        rua: [""],
        numero: [""],
        bairro: [""],
        cidade: [""],
        estado: [""],
        pais: [""],
        cep: [""],
        complemento: [""],
      }),
      // login: ["", [Validators.required]],
      // senha: ["", [Validators.required]],
      // confirmar_senha: ["", [Validators.required]]
    });
    */
  }

  cadastrar(form) {
    // this.usuario = form.value;

    this.rest.salvarUsuario(form.value, form.value.cnpj).subscribe(
      (teste) => {
        console.log(teste);
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Usuário cadastrado com sucesso";
        this.router.navigate(["dashboard"]);
      },
      (error) => {
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao cadastrar usuário";
        console.log(error);
      }
    );
  }

  updateForm(usuario) {
    //  if(usuario.escritorio !== null){
    //   this.usuarioForm.patchValue({
    //     id: usuario.id,
    //     nome: usuario.nome,
    //     rg: usuario.rg,
    //     cpf: usuario.cpf,
    //     contato: usuario.contato,
    //     email: usuario.email,
    //     cnpj: usuario.escritorio.cnpj

    //   });
    //  } else {
    //   this.usuarioForm.patchValue({
    //     id: usuario.id,
    //     nome: usuario.nome,
    //     rg: usuario.rg,
    //     cpf: usuario.cpf,
    //     contato: usuario.contato,
    //     email: usuario.email,
    //     cnpj: usuario.empresa.cnpj

    //   });
    //  }
    if (usuario.escritorios.length > 0) {
      this.usuarioForm.patchValue({
        id: usuario.id,
        nome: usuario.nome,
        rg: usuario.rg,
        cpf: usuario.cpf,
        contato: usuario.contato,
        email: usuario.email,
        cnpj: usuario.escritorios[0].cnpj,
      });
    } else if (usuario.empresas.length > 0) {
      this.usuarioForm.patchValue({
        id: usuario.id,
        nome: usuario.nome,
        rg: usuario.rg,
        cpf: usuario.cpf,
        contato: usuario.contato,
        email: usuario.email,
        cnpj: usuario.empresas[0].cnpj,
      });
    }

  }

  editar(usuario) {
    this.rest.atualizaUsuario(usuario.value, usuario.value.cnpj).subscribe(
      (success) => {
        console.log("Deu certo");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Usuário editado com sucesso";
        this.router.navigate(["dashboard"]);
      },
      (error) => {
        console.log("Falhou");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao editar usuário";
        console.log(usuario.value);
        console.log(usuario);
        console.log(error);
      }
    );
  }

  receberDepartamentos(cnpj) {
    this.escritorioService.findDepartamentos(cnpj).subscribe((data) => {
      let departamentos = [];
      data.forEach(function (item) {
        departamentos.push(item);
      });
      this.departamentos = departamentos;
      console.log(this.departamentos);
    });
  }

  receberEmpresas(busca: string) {
    this.empresaService
      .receberEmpresas(busca, this.usuarioId, true)
      .subscribe((data) => {
        let empresa = [];
        data.forEach(function (item) {
          empresa.push(item);
        });
        this.empresas = empresa;
      });
  }

  receberEscritorios(busca: string) {
    this.escritorioService.receberEscritorios(busca, true, this.usuarioId).subscribe((data) => {
      console.log(data);
      let escritorios = [];
      data.forEach(function (item) {
        escritorios.push(item);
      });
      this.escritorios = escritorios;
    });
  }
  mudarValorBtnEmpresa() {
    if (this.btnEmpresa === false) {
      this.btnEmpresa = true;
      this.btnEscritorio = false;
    }
  }

  mudarValorBtnEscritorio() {
    if (this.btnEscritorio === false) {
      this.btnEscritorio = true;
      this.btnEmpresa = false;
    }
  }

  setEscritorioempresaId(n) {
    this.escritorioempresaId = n;
  }

  consultaCep(event: any) {
    let cep: string = event;
    cep = cep.replace("-", "");
    console.log(this.usuarioForm);
    if (
      !(this.usuarioForm.get("endereco").get("cep").value === "") &&
      cep.length === 8
    ) {
      this.cepService.consulta(cep).subscribe(
        (success) => {
          if (success.erro) {
            console.error(success);
            this.usuarioForm
              .get("endereco")
              .get("cep")
              .setErrors({ cepInvalido: true });
          } else {
            this.onSuccess(success);
          }
        },
        (error) => {
          this.usuarioForm
            .get("endereco")
            .get("cep")
            .setErrors({ cepInvalido: true });
          console.error(error);
        }
      );
    }
  }

  onSuccess(response) {
    this.usuarioForm
      .get(["endereco"])
      .get(["rua"])!
      .setValue(response.logradouro);
    this.usuarioForm
      .get(["endereco"])
      .get(["bairro"])!
      .setValue(response.bairro);
    this.usuarioForm
      .get(["endereco"])
      .get(["cidade"])!
      .setValue(response.localidade);
    this.usuarioForm.get(["endereco"]).get(["estado"])!.setValue(response.uf);
  }

  setUsuarioId(id) {
    this.usuarioId = id;
  }
  setEditando() {
    this.editando = true;
  }
  setAdmin() {
    this.admin = true;
  }

  setAdminEscritorio() {
    this.admin_escritorio = true;
  }

  setAdminEmpresa() {
    this.admin_empresa = true;
  }
  onKey1() {
    this.emp = true;
  }
  onKey2(cnpj: string) {
    this.esc = true;
    cnpj = cnpj.replace(": ", "");
    while (cnpj.length != 14) {
      cnpj = cnpj.substring(1, cnpj.length);
    }

    this.receberDepartamentos(cnpj);
    this.usuarioForm.get(["departamento"])!.setValue(null);
  }

  cpfKeyPressEvent(event: any): void {
    let cpf: string = event;
    cpf = cpf.replace(".", "").replace(".", "").replace("-", "");
    console.log(this.usuarioForm);
    console.log(event);
    if (cpf !== "" && cpf.length === 11) {
      if (!this.testaCPF(cpf)) {
        this.usuarioForm.get("cpf").setErrors({ cpfInvalido: true });
      }
      this.http.get("/wk/usuario/cpf/" + cpf).subscribe(
        (success: any) => {
          console.log(success + " - nao tem no banco");
          if (success.length > 0) {
            if (this.editando && (success[0].cpf === this.cpfEditando)) {
            } else {
              this.usuarioForm.get("cpf").setErrors({ duplicado: true });
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
    console.log(this.usuarioForm);
  }

  testaCPF(cpf: string) {
    var Soma = 0;
    if (cpf === undefined) {
      this.setCpf(false);
      return false;
    }

    var strCPF = cpf.replace(".", "").replace(".", "").replace("-", "");
    if (
      strCPF === "00000000000" ||
      strCPF === "11111111111" ||
      strCPF === "22222222222" ||
      strCPF === "33333333333" ||
      strCPF === "44444444444" ||
      strCPF === "55555555555" ||
      strCPF === "66666666666" ||
      strCPF === "77777777777" ||
      strCPF === "88888888888" ||
      strCPF === "99999999999" ||
      strCPF.length !== 11
    ) {
      this.setCpf(false);
      //alert("CPF inválido");
      return false;
    }

    for (let i = 1; i <= 9; i++) {
      Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }

    var Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) {
      Resto = 0;
    }

    if (Resto !== parseInt(strCPF.substring(9, 10))) {
      this.setCpf(false);
      //alert("CPF inválido");
      return false;
    }

    Soma = 0;
    for (let k = 1; k <= 10; k++) {
      Soma = Soma + parseInt(strCPF.substring(k - 1, k)) * (12 - k);
    }

    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) {
      Resto = 0;
    }

    if (Resto !== parseInt(strCPF.substring(10, 11))) {
      this.setCpf(false);
      //alert("CPF inválido");
      return false;
    }

    this.setCpf(true);
    return true;
  }

  setCpf(cpf) {
    this.cpf = cpf;
  }
  setBtnEmpresa() {
    this.btnEmpresa = false;
  }
  setBtnEscritorio() {
    this.btnEscritorio = true;
  }

  cpfEdit(cpf) {
    this.cpfEditando = cpf;
  }

  setMultiplos() {
    this.multiplos = true;
  }
  setEditandoAdminEsc() {
    this.editandoAdminEsc = true;
  }
}
