import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { NivelDeAcesso } from '../../../../core/enums/NivelDeAcesso';
import { Endereco } from '../../../../core/model/Endereco';
import { Usuario } from '../../../../core/model/Usuario';
import { CepService } from '../../../../core/services/cep.service';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { UsuarioService } from '../../usuario.service';


@Component({
  selector: 'app-cadastrar-colaborador-escritorio',
  templateUrl: './cadastrar-colaborador-escritorio.component.html',
  styleUrls: ['./cadastrar-colaborador-escritorio.component.scss']
})
export class CadastrarColaboradorEscritorioComponent implements OnInit {

  admin: boolean = false;
  admin_escritorio: boolean = false;
  escritorios = [];
  usuario: Usuario;
  endereco: Endereco;
  editando: boolean = false;
  usuarioForm: FormGroup;
  usuarioId = 0;
  nivelDeAcesso = NivelDeAcesso;
  niveis = [];
  constructor(private rest: UsuarioService ,private fb: FormBuilder, private router: Router, private escritorioService: EscritorioContabilService, private cepService: CepService) {
    this.usuario = new Usuario();
    this.endereco = new Endereco();
    this.niveis = Object.keys(this.nivelDeAcesso);
  }

  ngOnInit(): void {
    this.receberEscritorios("");
    const perfis = JSON.parse(localStorage.getItem("user_role"));

    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
      }
    });

    if (!this.admin) {
      perfis.forEach((p) => {
        if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
          this.setAdminEscritorio();
        }
      });
    }

    if(this.admin){
      this.usuarioForm = this.fb.group({
        nome: ["", [Validators.required]],
        rg: [""],
        cpf: ["", [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
        celular: [""],
        email: ["", [Validators.required, Validators.email]],
        escritorio: ["", [Validators.required]],
        endereco: this.fb.group ({
          rua: [""],
          numero: [""],
          bairro: [""],
          cidade: [""],
          estado: [""],
          pais: [""],
          cep: [""],
          complemento: [""],
        }),
        // login: ["", [Validators.required]],
        // senha: ["", [Validators.required]],
        // confirmar_senha: ["", [Validators.required]]
      });
    }
    else if(this.admin_escritorio){
      let escritorioId = 0;
      let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
      console.log(usuarioL);
      this.setUsuarioId(usuarioL.id);
          this.usuarioForm = this.fb.group({
            nome: ["", [Validators.required]],
            rg: [""],
            cpf: ["", [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
            celular: [""],
            email: ["", [Validators.required, Validators.email]],
            endereco: this.fb.group ({
              rua: [""],
              numero: [""],
              bairro: [""],
              cidade: [""],
              estado: [""],
              pais: [""],
              cep: [""],
              complemento: [""],
            }),
            // login: ["", [Validators.required]],
            // senha: ["", [Validators.required]],
            // confirmar_senha: ["", [Validators.required]]
          });
    }
  }

  receberEscritorios(busca: string){
    // this.escritorioService.receberEscritorios(busca, true).subscribe(data => {
    //   console.log(data);
    //   let escritorios = [];
    //     data.forEach(function (item) {
    //     escritorios.push(item);
    //   });
    //   this.escritorios = escritorios;

    // });
  }

  setAdmin(){
    this.admin = true;
  }
  setAdminEscritorio(){
    this.admin_escritorio = true;
  }
  cadastrar(form) {
    this.usuario = form.value;
    console.log(form.value);
    if(this.admin){
      this.rest.salvarUsuario(this.usuario, form.value.escritorio).subscribe(teste => {console.log(teste);
      this.router.navigate(['dashboard']);});
    } else {
      this.rest.salvarUsuario(this.usuario, this.usuarioId ).subscribe(teste => {console.log(teste);
      this.router.navigate(['dashboard']);});
    }

  }
  consultaCep(cep: string){
    this.cepService.consulta(cep).subscribe(success => {
      if(success.erro){
        this.onError()
      } else{
        this.onSuccess(success);
      }
    }, error => this.onError());
  }

  onSuccess(response){
    this.usuarioForm.get(['endereco']).get(['rua'])!.setValue(response.logradouro);
    this.usuarioForm.get(['endereco']).get(['bairro'])!.setValue(response.bairro);
    this.usuarioForm.get(['endereco']).get(['cidade'])!.setValue(response.localidade);
    this.usuarioForm.get(['endereco']).get(['estado'])!.setValue(response.uf);
    this.usuarioForm.get(['endereco']).get(['complemento'])!.setValue(response.complemento);
    this.usuarioForm.get(['endereco']).get(['pais'])!.setValue("Brasil");
  }
  onError(){
    alert("Erro ao consultar CEP");
  }
  setUsuarioId(id){
    this.usuarioId = id;
  }
}
