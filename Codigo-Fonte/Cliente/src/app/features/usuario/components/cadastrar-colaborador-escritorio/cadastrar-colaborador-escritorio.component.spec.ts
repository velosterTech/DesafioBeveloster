import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarColaboradorEscritorioComponent } from './cadastrar-colaborador-escritorio.component';

describe('CadastrarColaboradorEscritorioComponent', () => {
  let component: CadastrarColaboradorEscritorioComponent;
  let fixture: ComponentFixture<CadastrarColaboradorEscritorioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastrarColaboradorEscritorioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarColaboradorEscritorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
