import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarColaboradorEmpresaComponent } from './cadastrar-colaborador-empresa.component';

describe('CadastrarColaboradorEmpresaComponent', () => {
  let component: CadastrarColaboradorEmpresaComponent;
  let fixture: ComponentFixture<CadastrarColaboradorEmpresaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastrarColaboradorEmpresaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarColaboradorEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
