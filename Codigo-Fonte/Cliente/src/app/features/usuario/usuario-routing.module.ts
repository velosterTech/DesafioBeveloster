



import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CadastrarColaboradorEmpresaComponent } from "./components/cadastrar-colaborador-empresa/cadastrar-colaborador-empresa.component";
import { CadastrarColaboradorEscritorioComponent } from "./components/cadastrar-colaborador-escritorio/cadastrar-colaborador-escritorio.component";
import { CadastrarUsuarioComponent } from "./components/cadastrar-usuario/cadastrar-usuario.component";
import { ListarUsuariosComponent } from "./components/listar-usuarios/listar-usuarios.component";

const routes: Routes = [
  {
    path: "",
    component: ListarUsuariosComponent,
  },
  {
    path: "editar/:id",
    component: CadastrarUsuarioComponent,
  },
  {
    path: "cadastrar",
    component: CadastrarUsuarioComponent,
  },
  {
    path: "cadastrar-colaborador-empresa",
    component: CadastrarColaboradorEmpresaComponent,
  },
  {
    path: "cadastrar-colaborador-escritorio",
    component: CadastrarColaboradorEscritorioComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsuarioRoutingModule {}
