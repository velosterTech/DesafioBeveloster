import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from "@angular/router";
import { Observable, of } from "rxjs";
import { filter } from "rxjs/operators";
import { UsuarioService } from "../usuario.service";

@Injectable()
export class UsuarioGuard implements CanActivate, CanLoad {
  constructor(private userService: UsuarioService, private router: Router) {}

  permittedAuthorities = ["ROLE_ADMIN_ESCRITORIO", "ROLE_ADMIN_EMPRESA", "ROLE_ADMIN"];

  isAutorizado() {
    const userRoles = JSON.parse(localStorage.getItem("user_role"));
    if (userRoles !== undefined) {
      return this.verificaAutorizacao(userRoles)
    } else {
      const usuario = JSON.parse(localStorage.getItem("usuario"));
      return this.verificaAutorizacao(this.userService.getByLogin(usuario.login).pipe(filter(u => u.perfis)))
    }
  }

  verificaAutorizacao(userRoles){
    let autorizado = false;

    userRoles.forEach((role) => {
      if (this.permittedAuthorities.includes(role.nome)) {
        autorizado = true;
      }
    });
    if (autorizado) {
      return of(true);
    }
    return of(false);
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
      return this.isAutorizado();
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    return this.isAutorizado();
  }
}
