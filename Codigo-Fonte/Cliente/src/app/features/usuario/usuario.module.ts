import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../../shared/shared.module';

import { CadastrarColaboradorEmpresaComponent } from './components/cadastrar-colaborador-empresa/cadastrar-colaborador-empresa.component';
import { CadastrarColaboradorEscritorioComponent } from './components/cadastrar-colaborador-escritorio/cadastrar-colaborador-escritorio.component';
import { CadastrarUsuarioComponent } from './components/cadastrar-usuario/cadastrar-usuario.component';
import { ListarUsuariosResolverService } from './resolvers/listar-usuarios-resolver.service';
import { UsuarioRoutingModule } from './usuario-routing.module';



@NgModule({
  declarations: [
    CadastrarColaboradorEmpresaComponent,
    CadastrarColaboradorEscritorioComponent,
    CadastrarUsuarioComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    UsuarioRoutingModule,
    NgxMaskModule.forChild()
  ],
  providers: [
    ListarUsuariosResolverService
  ]
})
export class UsuarioModule { }
