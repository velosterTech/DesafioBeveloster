import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastrarCategoriaComponent } from './components/cadastrar/cadastrar-categoria.component';
import { ListarCategoriaComponent } from './components/listar/listar-categoria.component';

const routes: Routes = [
  {
    path:"",
    component: ListarCategoriaComponent
  },
  {
    path:"cadastrar",
    component: CadastrarCategoriaComponent
  },
  {
    path:"editar/:id",
    component: CadastrarCategoriaComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriaRoutingModule { }
