import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CategoriaRoutingModule } from './categoria-routing.module';
import { CadastrarCategoriaComponent } from './components/cadastrar/cadastrar-categoria.component';
import { ListarCategoriaComponent } from './components/listar/listar-categoria.component';

@NgModule({
  declarations: [
    CadastrarCategoriaComponent,
    ListarCategoriaComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    CategoriaRoutingModule
  ]
})
export class CategoriaModule { }
