import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map, switchMap } from 'rxjs/operators';
import { CategoriaService } from '../../categoria.service';
import { ICategoria, Categoria } from '../../../../core/model/categoria.model';

@Component({
  selector: 'app-cadastrar-categoria',
  templateUrl: './cadastrar-categoria.component.html',
  styleUrls: ['./cadastrar-categoria.component.scss']
})
export class CadastrarCategoriaComponent implements OnInit {

  isUpdating = false;
  isSaving = false;
  categoriaForm = this.fb.group({
    id: [],
    nomeCategoria: []
  });

  constructor(
    protected fb: FormBuilder,
    protected route: ActivatedRoute,
    protected categoriaService: CategoriaService
    ) {}

  ngOnInit(): void {
    this.route.params.pipe(
      map((params: any) => params['id']),
      switchMap(id => {
        if(id !== undefined){ return this.categoriaService.findById(id); }
        else {return new Observable<ICategoria>();}
        })
    ).subscribe(categoria => {
      this.updateForm(categoria);
      categoria.id ? this.isUpdating = true : this.isUpdating = false;
    });
  }

  save(): void{
    this.isSaving = true;
    const categoria: ICategoria = this.createFromForm();
    console.log(categoria);
    // const tipoDocumentoDTO: ITiposDocumentosDTO = this.toDTO(tipoDocumento);
    if (categoria.id) {
      this.subscribeToSaveResponse(this.categoriaService.update(categoria, categoria.id));
    } else {
      this.subscribeToSaveResponse(this.categoriaService.create(categoria));
    }
  }

  previousState(): void {
    window.history.back();
  }

  protected subscribeToSaveResponse(result: Observable<ICategoria>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    window.scrollTo(0, 0);
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
    this.isUpdating = false;
  }

  trackCategoriaById(index: number, item: ICategoria): number {
    return item.id!;
  }

  protected createFromForm(): ICategoria {
    return {
      ...new Categoria(),
      id: this.categoriaForm.get(['id'])!.value,
      nomeCategoria: this.categoriaForm.get(['nomeCategoria'])!.value,
    };
  }

  // protected toDTO(tipoDocumento: ICategoria): ITiposDocumentosDTO {
  //   return {
  //     ...new TiposDocumentosDTO(),
  //     nomeDocumento: tipoDocumento.nomeDocumento,
  //     escritorioContabilId: tipoDocumento.escritorioContabil.id,
  //   };
  // }

  protected updateForm(categoria: ICategoria): void {
    this.categoriaForm.patchValue({
      id: categoria.id,
      nomeCategoria: categoria.nomeCategoria,
    });
  }
}
