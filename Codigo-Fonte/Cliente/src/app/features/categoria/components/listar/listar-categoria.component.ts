import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';

import { FormBuilder, FormControl } from '@angular/forms';
import { ICategoria } from '../../../../core/model/categoria.model';
import { CategoriaService } from '../../categoria.service';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { Usuario } from '../../../../core/model/Usuario';
import { ConfirmacaoPopUpComponent } from '../../../../shared/components/confirmacao-pop-up/confirmacao-pop-up.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria',
  templateUrl: './listar-categoria.component.html',
  styleUrls: ['./listar-categoria.component.scss']
})

export class ListarCategoriaComponent implements OnInit {
  currentPage: number = 1;
  totalItems: number;
  categorias: ICategoria[] = [];
  usuarioId = 0;
  role: string;

  categoriaPequisa = new FormControl();

  constructor(
    public dialog: MatDialog,
    protected categoriaService: CategoriaService,
    protected escritorioContabilService: EscritorioContabilService,
    protected fb: FormBuilder,
    protected router: Router
    ) {}

  ngOnInit(): void {
    let usuarioL: Usuario = JSON.parse(localStorage.getItem('usuarioL'));
    this.setUsuarioId(usuarioL.id);
    this.setRole(localStorage.getItem('user_role'));

    this.categoriaService.findAll().subscribe((categorias: ICategoria[]) => {
      this.categorias = categorias;
    });
  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.categorias = this.categorias.slice(startItem, endItem);
  }

  setTotalItems(lenght: number) {
    this.totalItems = lenght;
  }

  openDialog(tipoDocumento, id): void {
    const dialogRef = this.dialog.open(ConfirmacaoPopUpComponent, {
      width: '350px',
      data:{nome : tipoDocumento.nomeDocumento},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result ?? 1){
        this.categoriaService.delete(id).subscribe(success => {
          this.updateTable(null);
        });
      }
    });
  }

  onSearchEscritorioChange(event: any): void{
    this.updateTable(event.target.value);
  }

  protected updateTable(nomeCategoria: string){
    this.categorias = [];
    if(nomeCategoria === null){
      this.categoriaService.findAll().subscribe((categoria: ICategoria[]) => {
        categoria.forEach((categoria: ICategoria) => {
          this.categorias.push(categoria);
        });
      });
    }else{
      this.categoriaService.findByName(nomeCategoria).subscribe((categorias: ICategoria[]) => {
        categorias.forEach((categoria: ICategoria) => {
          this.categorias.push(categoria);
        });
      });
    }
  }
  setUsuarioId(id){
    this.usuarioId = id;
  }
  setRole(value: string) {
    this.role = value;
  }

  pesquisarCategoria(): void{
    const categoriaSearch = this.categoriaPequisa.value;

    if(categoriaSearch === '' || categoriaSearch === null || categoriaSearch === undefined){
      this.updateTable(null);
    }else{
      this.updateTable(categoriaSearch);
    }
  }
}
