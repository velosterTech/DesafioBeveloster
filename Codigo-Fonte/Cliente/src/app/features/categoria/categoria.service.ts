import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { ICategoria } from "../../core/model/categoria.model";
import { AppConstants } from "../../core/settings/app-constants";

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {


  constructor(private http: HttpClient) { }

  create(categoria: ICategoria): Observable<ICategoria>{
    return this.http.post<ICategoria>(`${AppConstants.baseUrlCategoria}novo`, categoria);
  }

  update(categoria: ICategoria, id: number): Observable<ICategoria>{
    return this.http.put<ICategoria>(`${AppConstants.baseUrlCategoria}update/${id}`, categoria);
  }

  delete(id: number): Observable<any>{
    return this.http.delete(`${AppConstants.baseUrlCategoria}delete/${id}`);
  }

  findById(id: number): Observable<ICategoria>{
    return this.http.get<ICategoria>(`${AppConstants.baseUrlCategoria}find/id/${id}`);
  }

  findByName(nomeCategoria: string): Observable<ICategoria[]>{
    return this.http.get<ICategoria[]>(`${AppConstants.baseUrlCategoria}find/name/${nomeCategoria}`);
  }

  findAll(): Observable<ICategoria[]>{
    return this.http.get<ICategoria[]>(`${AppConstants.baseUrlCategoria}find/all`);
  }

}
