import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SharedModule } from '../../shared/shared.module';
import { CadastrarTiposDocumentosComponent } from './components/cadastrar/cadastrar-tipos-documentos.component';
import { ListarTiposDocumentosComponent } from './components/listar/listar-tipos-documentos.component';
import { TipoDocumentosRoutingModule } from './tipo-documentos-routing.module';


@NgModule({
  declarations: [
    CadastrarTiposDocumentosComponent,
    ListarTiposDocumentosComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TipoDocumentosRoutingModule
  ]
})
export class TipoDocumentosModule { }
