import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map, switchMap } from 'rxjs/operators';
import { ICategoria } from '../../../../core/model/categoria.model';
import { CategoriaService } from '../../../categoria/categoria.service';
import { EscritorioContabil } from '../../../../core/model/EscritorioContabil';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { TiposDocumentosService } from '../../tipos-documentos.service';
import { ITiposDocumentosDTO, TiposDocumentosDTO } from '../../../../core/model/tipo-documentos-dto.model';
import { ITiposDocumentos, TiposDocumentos } from '../../../../core/model/tipo-documentos.model';


@Component({
  selector: 'app-cadastrar-tipos-documentos',
  templateUrl: './cadastrar-tipos-documentos.component.html',
  styleUrls: ['./cadastrar-tipos-documentos.component.scss']
})
export class CadastrarTiposDocumentosComponent implements OnInit {

  isUpdating = false;
  isSaving = false;
  escritoriosSharedCollection: EscritorioContabil[] =[];
  categoriasSharedCollection: ICategoria[] = [];
  tipoDocumentoForm = this.fb.group({
    id: [],
    nomeDocumento: [],
    escritorioContabil: [],
    categoria: []
  });

  constructor(
    protected fb: FormBuilder,
    protected escritorioContabilService: EscritorioContabilService,
    protected tiposDocumentosService: TiposDocumentosService,
    protected categoriaService: CategoriaService,
    protected route: ActivatedRoute
    ) {}

  ngOnInit(): void {
    this.escritorioContabilService.receberEscritorios('').subscribe((escritorios: EscritorioContabil[]) => {
      this.escritoriosSharedCollection = escritorios;
    });

    this.categoriaService.findAll().subscribe((categorias: ICategoria[]) => {
      this.categoriasSharedCollection = categorias;
    });

    this.route.params.pipe(
      map((params: any) => params['id']),
      switchMap(id => {
        if(id !== undefined){ return this.tiposDocumentosService.findById(id); }
        else {return new Observable<ITiposDocumentos>();}
        })
    ).subscribe(tipoDocumento => {
      this.updateForm(tipoDocumento);
      tipoDocumento.id ? this.isUpdating = true : this.isUpdating = false;
    });
  }

  save(): void{
    this.isSaving = true;
    const tipoDocumento: ITiposDocumentos = this.createFromForm();
    const tipoDocumentoDTO: ITiposDocumentosDTO = this.toDTO(tipoDocumento);
    if (tipoDocumento.id) {
      this.subscribeToSaveResponse(this.tiposDocumentosService.update(tipoDocumentoDTO, tipoDocumento.id));
    } else {
      this.subscribeToSaveResponse(this.tiposDocumentosService.create(tipoDocumentoDTO));
    }
  }

  previousState(): void {
    window.history.back();
  }

  protected subscribeToSaveResponse(result: Observable<ITiposDocumentos>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    window.scrollTo(0, 0);
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
    this.isUpdating = false;
  }

  trackTipoDocumentoById(index: number, item: ITiposDocumentos): number {
    return item.id!;
  }

  protected createFromForm(): ITiposDocumentos {
    return {
      ...new TiposDocumentos(),
      id: this.tipoDocumentoForm.get(['id'])!.value,
      nomeDocumento: this.tipoDocumentoForm.get(['nomeDocumento'])!.value,
      escritorioContabil: this.tipoDocumentoForm.get(['escritorioContabil'])!.value,
      categoria: this.tipoDocumentoForm.get(['categoria'])!.value
    };
  }

  protected toDTO(tipoDocumento: ITiposDocumentos): ITiposDocumentosDTO {
    return {
      ...new TiposDocumentosDTO(),
      nomeDocumento: tipoDocumento.nomeDocumento,
      escritorioContabilId: tipoDocumento.escritorioContabil.id,
      categoriaId: tipoDocumento.categoria.id
    };
  }

  protected updateForm(tipoDocumento: ITiposDocumentos): void {
    this.tipoDocumentoForm.patchValue({
      id: tipoDocumento.id,
      nomeDocumento: tipoDocumento.nomeDocumento,
      escritorioContabil: tipoDocumento.escritorioContabil,
      categoria: tipoDocumento.categoria
    });
  }
}
