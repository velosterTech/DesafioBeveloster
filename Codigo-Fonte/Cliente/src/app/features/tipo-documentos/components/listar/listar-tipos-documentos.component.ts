import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { EscritorioContabil } from '../../../../core/model/EscritorioContabil';
import { ITiposDocumentos } from '../../../../core/model/tipo-documentos.model';
import { Usuario } from '../../../../core/model/Usuario';
import { ConfirmacaoPopUpComponent } from '../../../../shared/components/confirmacao-pop-up/confirmacao-pop-up.component';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { TiposDocumentosService } from '../../tipos-documentos.service';


@Component({
  selector: 'app-tipos-documentos',
  templateUrl: './listar-tipos-documentos.component.html',
  styleUrls: ['./listar-tipos-documentos.component.scss']
})

export class ListarTiposDocumentosComponent implements OnInit {
  currentPage: number = 1;
  totalItems: number;
  tiposDocumentos: ITiposDocumentos[] = [];
  escritorios: EscritorioContabil[] =[];
  usuarioId = 0;
  role: string;
  constructor(
    public dialog: MatDialog,
    protected tiposDocumentosService: TiposDocumentosService,
    protected escritorioContabilService: EscritorioContabilService
    ) {}

  ngOnInit(): void {
    let usuarioL: Usuario = JSON.parse(localStorage.getItem('usuarioL'));
    this.setUsuarioId(usuarioL.id);
    this.setRole(localStorage.getItem('user_role'));

    this.tiposDocumentosService.findAll(this.usuarioId).subscribe((tiposDocumentos: ITiposDocumentos[]) => {
      this.tiposDocumentos = tiposDocumentos;
    });

    this.escritorioContabilService.receberEscritorios('').subscribe((escritorios: EscritorioContabil[]) => {
      this.escritorios = escritorios;
    });
  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.tiposDocumentos = this.tiposDocumentos.slice(startItem, endItem);
  }

  setTotalItems(lenght: number) {
    this.totalItems = lenght;
  }

  openDialog(tipoDocumento, id): void {
    const dialogRef = this.dialog.open(ConfirmacaoPopUpComponent, {
      width: '350px',
      data:{nome : tipoDocumento.nomeDocumento},
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result ?? 1){
        this.tiposDocumentosService.delete(id).subscribe(success => {
          this.updateTable('null');
        });
      }
    });
  }

  onSearchEscritorioChange(event: any): void{
    this.updateTable(event.target.value);
  }

  protected updateTable(idEscritorio: string){
    this.tiposDocumentos = [];
    if(idEscritorio !== 'null'){
      this.tiposDocumentosService.findAllByEscritorioId(+idEscritorio).subscribe((tiposDocumentos: ITiposDocumentos[]) => {
        tiposDocumentos.forEach((tipoDocumento: ITiposDocumentos) => {
          this.tiposDocumentos.push(tipoDocumento);
        });
      });
    }else{
      this.tiposDocumentosService.findAll(this.usuarioId).subscribe((tiposDocumentos: ITiposDocumentos[]) => {
        tiposDocumentos.forEach((tipoDocumento: ITiposDocumentos) => {
          this.tiposDocumentos.push(tipoDocumento);
        });
      });
    }
  }
  setUsuarioId(id){
    this.usuarioId = id;
  }
  setRole(value: string) {
    this.role = value;
  }
}
