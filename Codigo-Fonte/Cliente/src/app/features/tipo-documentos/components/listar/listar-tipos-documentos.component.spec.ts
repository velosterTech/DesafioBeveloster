import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListarTiposDocumentosComponent } from './listar-tipos-documentos.component';


describe('ListarTiposDocumentosComponent', () => {
  let component: ListarTiposDocumentosComponent;
  let fixture: ComponentFixture<ListarTiposDocumentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarTiposDocumentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarTiposDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
