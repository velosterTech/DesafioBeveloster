import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadastrarTiposDocumentosComponent } from './components/cadastrar/cadastrar-tipos-documentos.component';
import { ListarTiposDocumentosComponent } from './components/listar/listar-tipos-documentos.component';

const routes: Routes = [
  {
    path:"",
    component: ListarTiposDocumentosComponent
  },
  {
    path:"cadastrar",
    component: CadastrarTiposDocumentosComponent
  },
  {
    path:"editar/:id",
    component: CadastrarTiposDocumentosComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TipoDocumentosRoutingModule { }
