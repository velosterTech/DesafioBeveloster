import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ITiposDocumentosDTO } from "../../core/model/tipo-documentos-dto.model";
import { ITiposDocumentos } from "../../core/model/tipo-documentos.model";
import { AppConstants } from "../../core/settings/app-constants";



@Injectable({
  providedIn: 'root'
})
export class TiposDocumentosService {


  constructor(private http: HttpClient) { }

  create(tipoDocumento: ITiposDocumentosDTO): Observable<ITiposDocumentos>{
    return this.http.post<ITiposDocumentosDTO>(`${AppConstants.baseUrlTipoDocumentos}novo`, tipoDocumento);
  }

  update(tipoDocumento: ITiposDocumentosDTO, id: number): Observable<ITiposDocumentos>{
    return this.http.put(`${AppConstants.baseUrlTipoDocumentos}editar/${id}`, tipoDocumento);
  }

  delete(id: number): Observable<any>{
    return this.http.delete(`${AppConstants.baseUrlTipoDocumentos}delete/${id}`);
  }

  findById(id: number): Observable<ITiposDocumentos>{
    return this.http.get<ITiposDocumentos>(`${AppConstants.baseUrlTipoDocumentos}find/${id}`);
  }

  findAllByEscritorioId(id: number): Observable<ITiposDocumentos[]>{
    return this.http.get<ITiposDocumentos[]>(`${AppConstants.baseUrlTipoDocumentos}find/escritorio/${id}`);
  }

  findAllByUsuarioEscritorio(id: number): Observable<ITiposDocumentos[]>{
    return this.http.get<ITiposDocumentos[]>(`${AppConstants.baseUrlTipoDocumentos}find/usuario-escritorio/${id}`)
  }

  findAll(id): Observable<ITiposDocumentos[]>{
    let params = new HttpParams();

    params = params.set('id',id);
    return this.http.get<ITiposDocumentos[]>(`${AppConstants.baseUrlTipoDocumentos}find/all`, {params: params});
  }

  findAllByCategoria(id, categoriaId): Observable<ITiposDocumentos[]>{
    let params = new HttpParams();
    
    params = params.set('id',id);
    params = params.set('categoriaId', categoriaId);
    return this.http.get<ITiposDocumentos[]>(`${AppConstants.baseUrlTipoDocumentos}find/all/categoria`, {params: params});
  }
  
}
