import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { EscritorioContabil } from '../../../core/model/EscritorioContabil';
import { EscritorioContabilService } from '../../escritorio-contabil/escritorio-contabil.service';

@Injectable()
export class TipoDocumentoResolver implements Resolve<EscritorioContabil> {

  escritorioPage1: Observable<any>;

  constructor(private escritorioService: EscritorioContabilService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const buscarTodos = "";
    this.escritorioService.receberEscritorios(buscarTodos).subscribe(data => this.escritorioPage1 = data.slice(0,5));
    return this.escritorioPage1
  }
}
