import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { CadastrarDepartamentoComponent } from './components/cadastrar-departamento/cadastrar-departamento.component';
import { DepartamentoRoutingModule } from './departamento-routing.modules';


@NgModule({
  declarations: [
    CadastrarDepartamentoComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    DepartamentoRoutingModule,
  ],
  providers: [
  ]
})
export class DepartamentoModule { }