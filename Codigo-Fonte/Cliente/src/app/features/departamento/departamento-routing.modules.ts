import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CadastrarDepartamentoComponent } from "./components/cadastrar-departamento/cadastrar-departamento.component";

const routes: Routes = [
  // {
  //   path: "",
  //   component: ListarDocumentosComponent,
  // },
 
  {
    path: "",
    component: CadastrarDepartamentoComponent,
  },
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepartamentoRoutingModule {}