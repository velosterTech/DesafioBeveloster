import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AvisoConfirmacaoModalComponent } from '../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { DepartamentoService } from '../../departamento.service';

@Component({
  selector: 'app-cadastrar-departamento',
  templateUrl: './cadastrar-departamento.component.html',
  styleUrls: ['./cadastrar-departamento.component.scss']
})
export class CadastrarDepartamentoComponent implements OnInit {
  departamentoForm: FormGroup;
  admin: boolean = false;
  escritorios = [];
  multiplos: boolean = false;

  constructor(private fb: FormBuilder, private escritorioService: EscritorioContabilService, private departamentoService: DepartamentoService, private router: Router, private modalService: BsModalService) { }

  ngOnInit(): void {
    let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));
    const perfis = JSON.parse(localStorage.getItem("user_role"));

    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
        this.receberEscritorios("", usuarioL.id);
      }
    });

    if (this.admin) {
      this.departamentoForm = this.fb.group({
        id: [null],
        nome: ["", [Validators.required]],
        cnpj: ["", Validators.required],
      });
    } else if (usuarioL.escritorios.length > 1) {
      let escritorio = [];
      usuarioL.escritorios.forEach(function (item) {
        escritorio.push(item);
      });
      this.escritorios = escritorio;
      this.departamentoForm = this.fb.group({
        id: [null],
        nome: ["", [Validators.required]],
        cnpj: ["", Validators.required],
      });
      this.setMultiplos();


    }
    else {
      this.departamentoForm = this.fb.group({
        id: [null],
        nome: ["", [Validators.required]],
        cnpj: usuarioL.escritorios[0].cnpj,
      });
    }
  }

  setAdmin() {
    this.admin = true;
  }

  receberEscritorios(busca: string, id) {
    this.escritorioService.receberEscritorios(busca, true, id).subscribe((data) => {
      console.log(data);
      let escritorios = [];
      data.forEach(function (item) {
        escritorios.push(item);
      });
      this.escritorios = escritorios;
    });
  }

  cadastrar(form) {
    console.log(form.value)
    this.departamentoService
      .salvarDepartamento(form.value)
      .subscribe((teste) => {
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Departamento cadastrado com sucesso";
        console.log(teste);
        this.router.navigate(["dashboard"]);
      }, error => {
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao cadastrar departamento";
      });


  }
  setMultiplos(){
    this.multiplos = true;
  }
}
