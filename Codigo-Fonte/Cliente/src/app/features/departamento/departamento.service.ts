import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppConstants } from '../../core/settings/app-constants';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {

  constructor(private http: HttpClient) { }

  salvarDepartamento(departamento){
    return this.http.post<any>(AppConstants.baseUrlDepartamento + "salvar", departamento);
  }
}
