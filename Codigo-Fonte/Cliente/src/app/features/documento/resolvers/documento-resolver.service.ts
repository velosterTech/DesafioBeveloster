import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Empresa } from '../../../core/model/Empresa';
import { EmpresaService } from '../../empresa/empresa.service';

@Injectable({
  providedIn: 'root'
})
export class DocumentoResolverService implements Resolve<Empresa>{

  constructor(private router: Router, private empresaService: EmpresaService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Empresa | Observable<Empresa> | Promise<Empresa> {

    const usuario = JSON.parse(localStorage.getItem('usuarioL'));
    const buscarTodos = "";
    return this.empresaService.receberEmpresas(buscarTodos, usuario.id, true);
  }
}
