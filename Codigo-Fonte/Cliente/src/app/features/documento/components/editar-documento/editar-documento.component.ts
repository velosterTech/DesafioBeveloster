import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { DocumentoService } from '../../documento.service';

@Component({
  selector: 'app-editar-documento',
  templateUrl: './editar-documento.component.html',
  styleUrls: ['./editar-documento.component.scss']
})
export class EditarDocumentoComponent implements OnInit {

  documentoForm: FormGroup;

  constructor(private route: ActivatedRoute, private service: DocumentoService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.documentoForm = this.fb.group({
      id: [null],
      competencia: ["", Validators.required],
      valorVencimento: ["", Validators.min(0)],
      categoria: ["", Validators.required],
      //statusEnvio: ["", Validators.required],
      //usuarioId: ["", Validators.required],
    });

    // this.route.params.pipe(
    //   map((params: any) => params['id']),
    //   switchMap(id => this.service.receberDocumento(id))
    // ).subscribe(documento => this.atualizarFormulario(documento));
  }

  atualizarFormulario(documento){
    this.documentoForm.patchValue({
      id: documento.id,
      competencia: documento.competencia,
      valorVencimento: documento.valorVencimento,
      categoria: documento.categoria
    })
  }

  editar(documento){
    // this.service.editaDocumento(documento.value).subscribe(success => {
    //   console.log("Deu certo");
    //   this.router.navigate(['dashboard']);
    // }, error => {console.log("Falhou");
    //   console.log(error);
    // });
  }
}
