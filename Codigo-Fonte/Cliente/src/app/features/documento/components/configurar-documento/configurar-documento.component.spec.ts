import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurarDocumentoComponent } from './configurar-documento.component';

describe('ConfigurarDocumentoComponent', () => {
  let component: ConfigurarDocumentoComponent;
  let fixture: ComponentFixture<ConfigurarDocumentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigurarDocumentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurarDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
