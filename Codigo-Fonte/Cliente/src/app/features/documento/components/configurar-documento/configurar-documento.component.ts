import { HttpEvent, HttpResponse } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { Concluir } from "../../../../core/enums/Concluir";

import { FormatosDeData } from "../../../../core/enums/FormatosDeData";
import { ConfigurarDocumento } from "../../../../core/model/configurar-documento";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { EscritorioContabilService } from "../../../escritorio-contabil/escritorio-contabil.service";
import { DocumentoService } from "../../documento.service";

@Component({
  selector: "app-configurar-documento",
  templateUrl: "./configurar-documento.component.html",
  styleUrls: ["./configurar-documento.component.scss"],
})
export class ConfigurarDocumentoComponent implements OnInit {
  configuracaoForm: FormGroup;
  regraNome: string = "";
  formatos = [];
  formatosDeData = FormatosDeData;
  concluir = Concluir;
  concluirTarefa = [];
  arquivo: any;
  checkAposLeitura: boolean = true;
  checkAposEnvio: boolean = false;
  nomeArquivo = " Selecione arquivo..."
  escritorios = [];
  admin: boolean = false;
  admin_escritorio: boolean = false;
  usuarioId = 0;
  multiplos: boolean = false;
  gif: boolean = false;
  desativaBotao: boolean = false;
  editando: boolean = false;
  cnpjEscritorio: string = "";

  constructor(private fb: FormBuilder, private documentoService: DocumentoService, private escritorioService: EscritorioContabilService, private router: Router, private modalService: BsModalService,     private route: ActivatedRoute,
    ) {
    this.formatos = Object.keys(this.formatosDeData);
  }

  ngOnInit(): void {

    let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));
    console.log(usuarioL);
    this.setUsuarioId(usuarioL.id);

    this.route.params
      .pipe(
        map((params: any) => params["id"]),
        switchMap((id) => {
          if (id !== undefined) {
            return this.documentoService.getById(id);
          } else {
            return new Observable<any>();
          }
        })
      )
      .subscribe((documento) => {
        this.updateForm(documento);
        this.setEditando();
      });

    const perfis = JSON.parse(localStorage.getItem("user_role"));
    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
        this.receberEscritorios();
      }
    });

    if (!this.admin) {
      perfis.forEach((p) => {
        if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
          this.setAdminEscritorio();

        }
      });
    }

    this.configuracaoForm = this.fb.group({
      id: [null],
      documentoId: [null],
      regras: this.fb.array([], Validators.required),
      data: ["", Validators.required],
      documento: ["", Validators.required],
      cnpjEscritorio: [""]

    });
    if(!this.admin && !this.multiplos){
      this.setCnpjEscritorio(usuarioL.escritorios[0].cnpj);
    }

    if(usuarioL.escritorios.length > 1 ){
      this.setMultiplos();
      this.receberEscritorios();
    }

  }

  get regras() {
    return this.configuracaoForm.controls["regras"] as FormArray;
  }

  statusForm(data) {
    console.log(this.configuracaoForm.controls)
  }

  adicionarRegra() {
    if (this.regraNome.trim() === "") {
      console.error("valor vazio");
    } else {
      this.regras.push(this.fb.control(this.regraNome));
      this.regraNome = "";
    }
  }

  removeRegra(index) {
    this.regras.removeAt(index);
  }

  updateArquivo(file) {
    if (file[0].type !== "application/pdf") {
      console.error("envie um arquivo pdf");
    } else {
      const reader = new FileReader();
      const binaryString = reader.readAsDataURL(file[0]);
      reader.onload = (event: any) => {
        //Here you can do whatever you want with the base64 String

        var parts = event.target.result.split(";base64,");
        var contentType = parts[0].replace("data:", "");
        var base64 = parts[1];
        this.arquivo = base64;
      };
        this.nomeArquivo = file[0].name;
    }
  }


  mudarConcluirTarefa() {
    this.checkAposEnvio = !this.checkAposEnvio;
    this.checkAposLeitura = !this.checkAposLeitura;
  }

  cadastrar(configuracaoForm) {
    const configurarDocumento: ConfigurarDocumento = configuracaoForm.value;

    configurarDocumento.file = this.arquivo;

    if(this.checkAposLeitura){
      configurarDocumento.concluir = this.concluir.APOS_LEITURA;
    } else {
      configurarDocumento.concluir = this.concluir.APOS_ENVIO;
    }

     //console.log(configurarDocumento);
    this.gif = true;
    this.desativaBotao = true;
    console.log(configurarDocumento);
    this.documentoService.salvarConfiguracao(configurarDocumento).subscribe((data) => {
      console.log(data);
      this.gif = false;
      this.desativaBotao = false;
      if(data.length === 0){
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Configuração efetuada com succeso";
        this.router.navigate(["dashboard"]);
      } else {
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        let regrasNaoencontradas = "";
        data.forEach(function (item) {
          regrasNaoencontradas = regrasNaoencontradas + item + " ";
        });
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Falha ao efetuar configuração. Regras não encontradas: " + regrasNaoencontradas;
      }

    }, error => {
      const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao efetuar configuração";
        this.gif = false;
        this.desativaBotao = false;
     });
  }
  receberEscritorios(){
    this.escritorioService.receberEscritorios("", true, this.usuarioId).subscribe((data) => {
      let escritorios = [];
      data.forEach(function (item) {
        escritorios.push(item);
      });
      this.escritorios = escritorios;
    });
  }
  setMultiplos() {
    this.multiplos = true;
  }

  setAdmin() {
    this.admin = true;
  }

  setAdminEscritorio() {
    this.admin_escritorio = true;
  }

  setUsuarioId(id) {
    this.usuarioId = id;
  }
  setEditando() {
    this.editando = true;
  }

  updateForm(documento){
    console.log(documento.regras);
    this.configuracaoForm.patchValue({
      id: documento.id,
      documentoId: documento.documento.id,
      cnpjEscritorio: documento.documento.escritorio.cnpj,
      documento: documento.documento.nome,
      data: documento.data,
    });
    if(documento.concluir === "APOS_LEITURA"){
      this.checkAposEnvio = false;
      this.checkAposLeitura = true;
    } else {
      this.checkAposEnvio = true;
      this.checkAposLeitura = false;
    }
    for(let i = 0; i < documento.regras.length; i++){
      this.regras.push(this.fb.control(documento.regras[i].nome));
    }
  }
  editar(configuracaoForm){
    const configurarDocumento: ConfigurarDocumento = configuracaoForm.value;
    configurarDocumento.file = this.arquivo;
    // configurarDocumento.file = this.arquivo;
    if(this.checkAposLeitura){
      configurarDocumento.concluir = this.concluir.APOS_LEITURA;
    } else {
      configurarDocumento.concluir = this.concluir.APOS_ENVIO;
    }

     //console.log(configurarDocumento);
    this.gif = true;
    this.desativaBotao = true;
    this.documentoService.update(configurarDocumento).subscribe((data) => {
      console.log(data);
      this.gif = false;
      this.desativaBotao = false;
      if(data.length === 0){
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Edição efetuada com succeso";
        this.router.navigate(["dashboard"]);
      } else {
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        let regrasNaoencontradas = "";
        data.forEach(function (item) {
          regrasNaoencontradas = regrasNaoencontradas + item + " ";
        });
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Falha ao editar configuração. Regras não encontradas: " + regrasNaoencontradas;
      }

    }, error => {
      const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao editar configuração";
        this.gif = false;
        this.desativaBotao = false;
     });
  }

  setCnpjEscritorio(cnpj){
    this.cnpjEscritorio = cnpj;
  }
}
