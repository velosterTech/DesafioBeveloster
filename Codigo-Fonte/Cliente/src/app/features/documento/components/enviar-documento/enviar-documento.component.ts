import { HttpEvent, HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Status } from '../../../../core/enums/Status';
import { Documento } from '../../../../core/model/Documento';
import { Usuario } from '../../../../core/model/Usuario';
import { AvisoConfirmacaoModalComponent } from '../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { UsuarioService } from '../../../usuario/usuario.service';
import { DocumentoService } from '../../documento.service';



let usuarioL: Usuario;

@Component({
  selector: 'app-enviar-documento',
  templateUrl: './enviar-documento.component.html',
  styleUrls: ['./enviar-documento.component.scss']
})
export class EnviarDocumentoComponent implements OnInit {
  @ViewChild('fileDropRef', { static: false }) myFileInput: ElementRef;
  @ViewChild('checkBox', { static: false }) public checkBox: ElementRef;

  //documento: Documento;
  documentoForm: FormGroup;
  status = Status;
  enumKeys = [];
  usuarios: Array<Usuario> = [];
  files: any[] = [];
  usr: Usuario;
  uploadingFile = "none";
  uploadProgress = 0;
  desativado: boolean;
  mostrarAlertaSucesso: boolean = false;
  mostrarAlertaFalha: boolean = false;
  gif: boolean = false;
  idUsuario: number;

  mostrarDocumentos: boolean = false;
  private documentos = [];
  retornoDocumentos = [];
  totalItems: number;
  currentPage: number = 1;
  smallnumPages: number = 0;

  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;
  checkAll: boolean = false;
  botaoEnviar: boolean = false;
  admin: boolean = false;
  escritorios = [];
  esc: boolean = false;
  escritorioCnpj = "";

  currentPager: number = 1;

  constructor(private rest: DocumentoService, private fb: FormBuilder, private usuarioService: UsuarioService, private router: Router, private modalService: BsModalService, private escritorioService: EscritorioContabilService,) {
    this.enumKeys = Object.keys(this.status);
    this.usr = new Usuario();
  }

  ngOnInit(): void {
    let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));
    this.setIdUsuario(usuarioL.id);
    console.log(usuarioL[0]);
    const perfis = JSON.parse(localStorage.getItem("user_role"));
    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
        this.receberEscritorios("");
      }
    });

    if(!this.admin){
      this.setEscritorioCnpj(usuarioL.escritorios[0].cnpj);
      console.log(this.escritorioCnpj);
    }
    //this.receberUsuarios(this.usuarios, this.usr);
    //console.log(this.usuarios);
    /*this.documentoForm = this.fb.group({
      //competencia: ["", Validators.required],
      //valorVencimento: ["", Validators.min(0)],
      //categoria: ["", Validators.required],
      //statusEnvio: ["", Validators.required],
      //usuarioId: ["", Validators.required],
    });
    */
  }

  salvar() {
    /*this.documento = form.value;
    console.log(this.documento);
    this.rest.salvarDocumento(this.documento).subscribe(result => {});
    */
    this.enviarArquivos(this.files);
    //this.documentoForm.reset();

  }

  fecharAlertaSucesso() {
    this.mostrarAlertaSucesso = false;
  }

  fecharAlertaFalha() {
    this.mostrarAlertaFalha = false;
  }
  /*receberUsuarios(usuarios: Array<Usuario>, usr: Usuario) {
    this.usuarioService.getUsuarios().subscribe(data => {
      data.forEach(function (item) {
        usr.id = item.id;
        usr.nome = item.nome;
        usr.rg = item.rg;
        usr.cpf = item.cpf;
        usr.celular = item.celular;
        usr.email = item.email;
        usr.endereco = item.endereco;
        usuarios.push(usr);
      });
    });
  }
  */
  arquivosArrastados($event) {
    for (const item of $event) {
      if (item.type != "application/pdf") {
        this.mostrarAlertaFalha = true;
      }
    }
    if (!this.mostrarAlertaFalha) {
      this.receberArquivos($event);
    }
  }

  arquivosEscolhidos(files) {
    for (const item of files) {
      if (item.type != "application/pdf") {
        this.mostrarAlertaFalha = true;
      }
    }
    if (!this.mostrarAlertaFalha) {
      this.receberArquivos(files);
    }

  }
  receberArquivos(files: Array<any>) {
    if (!this.desativado) {
      this.fecharAlertaSucesso();
      for (const item of files) {
        this.files.push({ data: item, inProgress: false, progress: 0 });
      }
    }

  }

  /*
  prepararRequisicao(files: Array<any>){
    for (const file of files) {
      const formData = new FormData();
      formData.append('files', file.data);
      file.inProgress = true;
      this.enviarArquivos(formData);

    }

  }*/

  enviarArquivos(files: Array<any>) {

    const formData = new FormData();
    for (const file of files) {

      formData.append('files', file.data);
      file.inProgress = true;
    }
    this.rest.salvarUpload(formData, this.escritorioCnpj).subscribe((data: any) => { //(event: HttpEvent<Object>)
      console.log(data);

      if (data.type === 4) {
        this.uploadingFile = "none";
        this.uploadProgress = 0;
        this.files = [];
        this.myFileInput.nativeElement.value = "";
        this.desativado = false;
        this.gif = false;

        let mensagensDocumentoNull = "";
        let mensagensEmpresaNull = "";
        if (data.body === null) {
          const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
          bsModalRef.content.title = "Aviso!";
          bsModalRef.content.msg = "Não há nenhum documento configurado para este escritório!";
        } else {
          let documentos = [];
          data.body.forEach(function (item: any) {
            if (item.documento === null) {
              mensagensDocumentoNull += item.fileName + "\n";
            } else if (item.empresa === null) {
              mensagensEmpresaNull += item.fileName + "\n";
            } else {
              documentos.push(item);
            }

          });

          this.documentos = documentos;
          this.setTotalItems(documentos.length);
          if (documentos.length === files.length) {
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Todos os documentos foram lidos com sucesso!";
          } else if (mensagensDocumentoNull !== "" && mensagensEmpresaNull !== "") {
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = documentos.length + " documentos foram lidos com sucesso!\n Arquivos com documentos não identificados:\n" + mensagensDocumentoNull + "Verifique se estes estão cadastrados e configurados no sistema!\n Arquivos que possuem documentos com empresas não identificadas:\n" + mensagensEmpresaNull + "Verifique se estão cadastradas no sistema";
          } else if (mensagensDocumentoNull !== "") {
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = documentos.length + " documentos foram lidos com sucesso!\n Arquivos com documentos não identificados:\n" + mensagensDocumentoNull + "Verifique se estes estão cadastrados e configurados no sistema!";
          } else if (mensagensEmpresaNull !== "") {
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = documentos.length + " documentos foram lidos com sucesso!\n Arquivos que possuem documentos com empresas não identificadas:\n" + mensagensEmpresaNull + "Verifique se estão cadastradas no sistema";
          }
          if (documentos.length > 0) {
            this.setMostrar();
          }
        }

      } else if (data.type === 1) {
        this.uploadingFile = "block";
        const percentDone = Math.round((data.loaded * 100) / data.total);
        if (percentDone === 100) {
          this.gif = true;
        }
        this.uploadProgress = percentDone;
        this.desativado = true;

      }

      //   // }, error => {
      //   //   if(error.status === 424){
      //   //     alert("Erro ao identificar dados do documento");
      //   //   } else if(error.status === 417){
      //   //     alert("Tipo de documento não identificado")
      //   //   } else if(error.status === 409){
      //   //     alert("Empresa não encontrada")
      //   //   }else{
      //   //     alert("Erro ao enviar documentos");
      //   //   }
      //   //   this.gif = false;
      //   //   this.desativado = false;
      //   //   this.uploadingFile = "none";
      //   //   this.uploadProgress = 0;
      //   //   this.files = [];
      //   //   this.myFileInput.nativeElement.value = "";
      //   //   console.log(error.status);
    }, error => {
      const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
      bsModalRef.content.title = "Aviso!";
      bsModalRef.content.msg = "Ocorreu um problema ao ler os arquivos";
      this.gif = false;
      this.desativado = false;
      this.uploadingFile = "none";
      this.uploadProgress = 0;
      this.files = [];
      this.myFileInput.nativeElement.value = "";
      console.log(error.status);
    });
  }


  setIdUsuario(id: number) {
    this.idUsuario = id;
  }


  setTotalItems(lenght: number) {
    this.totalItems = lenght;
  }

  check() {
    this.checkAll = !this.checkAll;
    if (this.checkAll) {
      this.botaoEnviar = true;
      this.documentos.forEach(function (documento) {
        documento.check = true;
      });
    } else {
      this.botaoEnviar = false;
      this.documentos.forEach(function (documento) {
        documento.check = false;
      });
    }

  }

  checkSpecific(index: number) {
    this.documentos[index].check = !(this.documentos[index].check);
    if (!(this.documentos[index].check)) {
      this.setCheckAll();
      this.checkBox.nativeElement.checked = false;
    } else {
      let allChecked = true;
      for(let i =0; i< this.documentos.length; i++){
        if(!(this.documentos[index].check)){
          allChecked = false;
          break;
        }
      }
      if (allChecked) {
        this.setCheckAll();
        this.checkBox.nativeElement.checked = true;
      }
    }

    this.validarBotaoEnviar();
  }

  validarBotaoEnviar() {
    let n: number = 0;
    for (let i = 0; i < this.documentos.length; i++) {
      if ((this.documentos[i].check)) {
        this.botaoEnviar = true;
        break;
      } else {
        n++;
      }
    }
    if (n == this.documentos.length) {
      this.botaoEnviar = false;
    }
  }

  setMostrar() {
    this.mostrarDocumentos = true;
  }

  setOcultar() {
    this.mostrarDocumentos = false;
  }

  setCheckAll() {
    this.checkAll = false;
  }

  enviaDocumento(documento, i){
    if(this.documentos[i].enviado == false){
      console.log("requuisição");
      this.documentos[i].enviado = true;
      this.rest.enviarDocumento(documento).subscribe(data => {
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Documento enviado com sucesso";
        this.documentos.splice(this.documentos[i]);
        if(this.documentos.length == 0){
          this.setOcultar();
        }
      },
      error => {
        documento.enviado = false;
        const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Falha ao enviar documento";
      });
    }
  }


  enviaDocumentos(){

    for (let i = 0; i < this.documentos.length; i++) {
      if ((this.documentos[i].check)) {
        this.enviaDocumento(this.documentos[i], i);
      }
    }
  }

  receberEscritorios(busca: string) {
    this.escritorioService.receberEscritorios(busca, true, this.idUsuario).subscribe((data) => {
      console.log(data);
      let escritorios = [];
      data.forEach(function (item) {
        escritorios.push(item);
      });
      this.escritorios = escritorios;
    });
  }
  setAdmin(){
    this.admin = true;
  }
  onKey(cnpj: string) {
    this.setEsc();
    cnpj = cnpj.replace(": ", "");
    while (cnpj.length != 14) {
      cnpj = cnpj.substring(1, cnpj.length);
    }
    this.setEscritorioCnpj(cnpj);
  }

  setEsc(){
    this.esc = true;
  }

  setEscritorioCnpj(cnpj){
    this.escritorioCnpj = cnpj;
  }
}
