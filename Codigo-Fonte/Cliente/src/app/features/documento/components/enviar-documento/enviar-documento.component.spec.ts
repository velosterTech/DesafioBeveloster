import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EnviarDocumentoComponent } from './enviar-documento.component';

describe('FormDocumentoComponent', () => {
  let component: EnviarDocumentoComponent;
  let fixture: ComponentFixture<EnviarDocumentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EnviarDocumentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EnviarDocumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
