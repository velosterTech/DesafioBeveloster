import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { EMPTY } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap, take, tap } from 'rxjs/operators';
import { AvisoServiceService } from '../../../../shared/aviso-service.service';
import { AvisoConfirmacaoModalComponent } from '../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component';
import { DocumentoService } from '../../documento.service';

@Component({
  selector: 'app-listar-documentos',
  templateUrl: './listar-documentos.component.html',
  styleUrls: ['./listar-documentos.component.scss']
})
export class ListarDocumentosComponent implements OnInit {
  campoPesquisa = new FormControl();
  mostrarDocumentos: boolean = false;
  mostrar: string = "Mostrar Documentos +";
  private documentos = [];
  retornoDocumentos = [];
  totalItems: number;
  currentPage: number = 1;
  smallnumPages: number = 0;

  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;
  checkAll: boolean = false;
  usuarioId = 0;


  currentPager: number = 1;

  constructor(private documentoService: DocumentoService, private router: Router,  private avisoService: AvisoServiceService,
    private modalService: BsModalService) {

  }

  ngOnInit(): void {
    const usuario = JSON.parse(localStorage.getItem('usuarioL'))
    this.setUsuarioId(usuario.id);
    this.receberDocumentos("");
    this.campoPesquisa.valueChanges
      .pipe(
        map((value) => value.trim()),
        debounceTime(150),
        distinctUntilChanged(),
        tap((value) => console.log(value)),
        switchMap((value: string) => {
          return this.documentoService.receberDocumentosConfigurados(this.usuarioId, value);
        })
      )
      .subscribe((data) => {
        this.documentos = data;
        this.totalItems = data.lenght;
        this.retornoDocumentos = data.slice(0, 5);
      });
  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.retornoDocumentos = this.documentos.slice(startItem, endItem);
  }

  receberDocumentos(busca) {
    this.documentoService.receberDocumentosConfigurados(this.usuarioId, busca).subscribe(data => {
      console.log(data);
      let documentos = [];
      data.forEach(function (item) {
        documentos.push(item);
      });

      this.documentos = documentos;
      this.setTotalItems(documentos.length);
      this.retornoDocumentos = this.documentos.slice(0, 5);
    });
  }

  editar(id: number) {
    this.router.navigate(['editar-documento', id]);
  }

  setTotalItems(lenght: number) {
    this.totalItems = lenght;
  }

  check(){
    this.checkAll = !this.checkAll;
  }
  setUsuarioId(id){
    this.usuarioId = id;
  }

  trocarStatus(documento){
    if (documento.status) {
      const result$ = this.avisoService.showAviso('Confirmação', 'Deseja desativar ' + documento.documento.nome + '?');
      result$.asObservable().pipe(
        take(1),
        switchMap((result) => result ? this.documentoService.ativaDesativaStatus(documento.id) : EMPTY)).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberDocumentos("");
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Documento desativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar documento";
          }

        );
    } else {
      const result$ = this.avisoService.showAviso('Confirmação', 'Deseja ativar ' + documento.documento.nome + '?');
      result$.asObservable().pipe(
        take(1),
        switchMap((result) => result  ? this.documentoService.ativaDesativaStatus(documento.id) : EMPTY)).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberDocumentos("");
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Documento ativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar documento";
          }
        );
    }
    this.currentPage = 1;
  }

}
