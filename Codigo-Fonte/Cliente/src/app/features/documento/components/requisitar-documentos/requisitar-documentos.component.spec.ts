import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitarDocumentosComponent } from './requisitar-documentos.component';

describe('RequisitarDocumentosComponent', () => {
  let component: RequisitarDocumentosComponent;
  let fixture: ComponentFixture<RequisitarDocumentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequisitarDocumentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitarDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
