import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ICategoria } from '../../../../core/model/categoria.model';
import { ITiposDocumentos } from '../../../../core/model/tipo-documentos.model';
import { Usuario } from '../../../../core/model/Usuario';
import { CategoriaService } from '../../../categoria/categoria.service';
import { EmpresaService } from '../../../empresa/empresa.service';
import { MensagemService } from '../../../mensagem/mensagem.service';
import { TiposDocumentosService } from '../../../tipo-documentos/tipos-documentos.service';
import { UsuarioService } from '../../../usuario/usuario.service';

let usuarioL: Usuario;

@Component({
  selector: 'app-requisitar-documentos',
  templateUrl: './requisitar-documentos.component.html',
  styleUrls: ['./requisitar-documentos.component.scss']
})
export class RequisitarDocumentosComponent implements OnInit {
  empresas = [];
  requisicaoForm: FormGroup;
  idUsuario: number;
  categorias: ICategoria[] = [];
  tiposDocumentos: ITiposDocumentos[]= [];
  selecionado: Boolean = false;
  tiposSelecionados: number[]= [];

  constructor(private rest: EmpresaService, private fb: FormBuilder, private mensagemSerivce: MensagemService, private usuarioService: UsuarioService, private categoriaService: CategoriaService, private tipoDocumentoService: TiposDocumentosService) { }

  
  ngOnInit(): void {
    usuarioL = JSON.parse(localStorage.getItem('usuario'));
    this.categoriaService.findAll().subscribe((categorias: ICategoria[]) => {
      this.categorias = categorias;
    });
    this.usuarioService.getByLogin(usuarioL.login).subscribe(data => {
      this.setIdUsuario(data.id);
      this.receberEmpresas();
    });
    
    this.requisicaoForm = this.fb.group({
      usuarioDestino: ["", Validators.required],
      txtMensagem: ["", Validators.required],
      categoria: ["", Validators.required]
    })
  }

  receberEmpresas(){
    this.rest.receberEmpresas("", this.idUsuario, true).subscribe(data => {
      console.log(data);
      let empresas = [];
      data.forEach(function(item){
        empresas.push(item);
      });
      this.empresas = empresas;

    });
  }

  enviarRequisicao(form){
    console.log(this.tiposSelecionados);
    for(var i = 0; i < this.tiposSelecionados.length; i++){
      this.mensagemSerivce.novaSolicitacao(this.idUsuario, form.value, form.value.categoria, this.tiposSelecionados[i]).subscribe(success => console.log("ok"), error => console.log(error));
    }

  }
  setIdUsuario(id: number) {
    this.idUsuario = id;
  }
  findAllByCategoria(categoriaId){
    this.tipoDocumentoService.findAllByCategoria(this.idUsuario, categoriaId).subscribe((tiposDocumentos: ITiposDocumentos[]) => {
      this.tiposDocumentos = tiposDocumentos;
      
    });
  }

  onChangeCategoria(categoria){
    this.selecionado = true;
    this.findAllByCategoria(categoria);
    this.tiposSelecionados = [];
  }
  
  onChangeDocumentos(e){
    if(e.target.checked){
      this.tiposSelecionados.push(e.target.value);
    } else {
      this.tiposSelecionados.splice(this.tiposSelecionados.indexOf(e.target.value), 1);
    }
  }

}
