import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarDocumentosEnviadosComponent } from './listar-documentos-enviados.component';

describe('ListarDocumentosEnviadosComponent', () => {
  let component: ListarDocumentosEnviadosComponent;
  let fixture: ComponentFixture<ListarDocumentosEnviadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarDocumentosEnviadosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarDocumentosEnviadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
