import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route } from '@angular/router';
import { DocumentoService } from '../../documento.service';

@Component({
  selector: 'app-listar-documentos-enviados',
  templateUrl: './listar-documentos-enviados.component.html',
  styleUrls: ['./listar-documentos-enviados.component.scss']
})
export class ListarDocumentosEnviadosComponent implements OnInit {

  documentos = [];
  mostrarDocumentos: boolean = false;

  constructor(private route: ActivatedRoute, private documentoService: DocumentoService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const id = params["id"];
      if (id !== undefined) {
        this.receberDocumentos(id);
      }
    });
  }

  receberDocumentos(id){
    this.documentoService.getAllDocumentosEnviados(id).subscribe(data => {
      if(data.length > 0){
        console.log(data);
        let documentos = [];
        data.forEach(function (item) {
          documentos.push(item);
        });
        this.documentos = documentos;
      }
      this.setMostrar();
    });
  }
  setMostrar(){
    this.mostrarDocumentos = true;
  }
}
