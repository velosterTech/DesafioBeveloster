import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { EditarDocumentoComponent } from './components/editar-documento/editar-documento.component';
import { EnviarDocumentoComponent } from './components/enviar-documento/enviar-documento.component';
import { RequisitarDocumentosComponent } from './components/requisitar-documentos/requisitar-documentos.component';
import { BtnDropZoneDirective } from './directives/btn-drop-zone.directive';
import { DocumentoRoutingModule } from './documento-routing.module';
import { DocumentoResolverService } from './resolvers/documento-resolver.service';
import { ConfigurarDocumentoComponent } from './components/configurar-documento/configurar-documento.component';
import { ListarDocumentosEnviadosComponent } from './components/listar-documentos-enviados/listar-documentos-enviados.component';



@NgModule({
  declarations: [
    EditarDocumentoComponent,
    EnviarDocumentoComponent,
    RequisitarDocumentosComponent,
    BtnDropZoneDirective,
    ConfigurarDocumentoComponent,
    ListarDocumentosEnviadosComponent,

  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    DocumentoRoutingModule,
  ],
  providers: [
    DocumentoResolverService
  ]
})
export class DocumentoModule { }
