import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ConfigurarDocumentoComponent } from "./components/configurar-documento/configurar-documento.component";
import { EditarDocumentoComponent } from "./components/editar-documento/editar-documento.component";
import { EnviarDocumentoComponent } from "./components/enviar-documento/enviar-documento.component";
import { ListarDocumentosEnviadosComponent } from "./components/listar-documentos-enviados/listar-documentos-enviados.component";
import { ListarDocumentosComponent } from "./components/listar-documentos/listar-documentos.component";
import { RequisitarDocumentosComponent } from "./components/requisitar-documentos/requisitar-documentos.component";
import { DocumentoGuard } from "./guards/documento.guard";

const routes: Routes = [

  {
    path: "todos",
    component: ListarDocumentosComponent,
  },
  {
    path: "editar/:id",
    component: ConfigurarDocumentoComponent,
  },
  {
    path: "enviar",
    component: EnviarDocumentoComponent,
  },
  {
    path: "requisitar",
    component: RequisitarDocumentosComponent,
  },
  {
    path: "configurar",
    component: ConfigurarDocumentoComponent
  },
  {
    path: "",
    redirectTo: "configurar",
    pathMatch: "full"
  },
  {
    path:"documentos/:id",
    component: ListarDocumentosEnviadosComponent,
    canActivate: [DocumentoGuard],
    canLoad: [DocumentoGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentoRoutingModule {}
