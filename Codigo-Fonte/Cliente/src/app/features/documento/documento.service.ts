import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppConstants } from '../../core/settings/app-constants';


@Injectable({
  providedIn: 'root'
})
export class DocumentoService {


  constructor(private http: HttpClient) { }


  salvarUpload(formData, cnpj: string){
    return this.http.post<FormData>(AppConstants.baseUrlDocumento + "arquivos/"+ cnpj, formData, {
      reportProgress: true,
      observe: 'events',
    } ); /*{
      reportProgress: true,
      observe: 'events',
    }*/
  }

  salvarDocumento(documento){
    return this.http.post<any>(AppConstants.baseUrlDocumento + "salvar", documento);
  }

  salvarConfiguracao(configuracao){
    return this.http.post<any>(AppConstants.baseUrlConfigurar + "salvar", configuracao);
  }

  receberDocumentosConfigurados(id, busca){
    let params = new HttpParams();
    params = params.set('id', id);
    params = params.set('busca', busca);
    return this.http.get<any>(AppConstants.baseUrlConfigurar + "todos", {params: params});
  }

  receberDocumentos(cnpj): Observable<any> {
    let params = new HttpParams();
    params = params.set('cnpj', cnpj);
    return this.http.get<any>(AppConstants.baseUrlDocumento + "todos/cnpj", {params: params});
  }

  enviarDocumento(documento){
    return this.http.post<any>(AppConstants.baseUrlDocumento + "enviar-documento", documento);

  }

  getById(id){
    return this.http.get<any>(AppConstants.baseUrlConfigurar + id);
  }

  ativaDesativaStatus(id){
    return this.http.delete<any>(AppConstants.baseUrlConfigurar + id);
  }

  update(configuracao){
    return this.http.put<any>(AppConstants.baseUrlConfigurar + "update", configuracao);
  }

  getAllDocumentosEnviados(id){
    let params = new HttpParams();
    params = params.set('id', id);
    return this.http.get<any>(AppConstants.baseUrlDocumento + "documentos-enviados", {params: params});
  }
  // receberDocumentos(id): Observable<any>{
  //   let params = new HttpParams();
  //   params = params.set('id', id);
  //   return this.http.get<any>(AppConstants.baseUrlDocumento + "todos", {params: params});
  // }



  // receberDocumento(id){
  //   return this.http.get<any>(AppConstants.baseUrlDocumento + id);
  // }

  // editaDocumento(documento){
  //   return this.http.put(AppConstants.baseUrlDocumento + "editar/" + documento.id, documento);
  // }
}
