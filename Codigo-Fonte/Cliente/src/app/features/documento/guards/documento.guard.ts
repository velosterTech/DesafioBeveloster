import { Injectable } from "@angular/core";
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree,
} from "@angular/router";
import { Observable, of, Subject } from "rxjs";
import {
  filter,
  flatMap,
  map,
  mapTo,
  take,
  takeUntil,
  tap,
} from "rxjs/operators";
import { EmpresaService } from "../../empresa/empresa.service";

@Injectable({
  providedIn: "root",
})
export class DocumentoGuard implements CanActivate, CanLoad {
  permittedAuthorities = ["ROLE_ADMIN"];
  permittedAuthoritiesEscritorio = [
    "ROLE_ADMIN_ESCRITORIO",
    "ROLE_COLABORADOR_ESCRITORIO",
  ];
  permittedAuthoritiesEmpresa = [
    "ROLE_ADMIN_EMPRESA",
    "ROLE_COLABORADOR_EMPRESA",
  ];
  empresaId: number = 0;
  empresas = [];

  private route: ActivatedRouteSnapshot;
  constructor(private empresaService: EmpresaService) {}

  ngOnInit() {}

  // isAutorizado() {
  //   const usuario = JSON.parse(localStorage.getItem("usuarioL"));
  //   let userRoles = JSON.parse(localStorage.getItem("user_role"));
  //   if (userRoles !== undefined) {

  //     for (let i = 0; i < userRoles.length; i++) {
  //       if (this.permittedAuthorities.includes(userRoles[i].nome)) {
  //         return true;
  //       }
  //     }

  //     for (let i = 0; i < userRoles.length; i++) {
  //       if (this.permittedAuthoritiesEscritorio.includes(userRoles[i].nome)) {
  //         return "escritorio";
  //       }
  //     }

  //     return this.verificaAutorizacaoEmp(usuario);
  //   }
  //   return false;
  // }

  verificaAutorizacaoEmp(usuario) {
    let flag = false;
    usuario.empresas.forEach((empresa) => {
      if (empresa.id == this.empresaId) {
        flag = true;
      }
    });
    return flag;
  }

  public verificaAutorizacaoEsc(empresasId) {
    let flag = false;
    empresasId.forEach((id) => {
      if (id === this.empresaId) {
        flag = true;
      }
    });
    return flag;
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this.setActivedRoute(route);
    const usuario = JSON.parse(localStorage.getItem("usuarioL"));
    let userRoles = JSON.parse(localStorage.getItem("user_role"));
    this.setEmpresaId(parseInt(this.route.paramMap.get("id")));
    var permitted;

    userRoles.forEach((role) => {
      if (this.permittedAuthorities.includes(role.nome)) {
        permitted = of(true);
      }

      if (
        this.permittedAuthoritiesEmpresa.includes(role.nome) &&
        this.verificaAutorizacaoEmp(usuario)
      ) {
        permitted = of(true);
      }

      const authoritiesName = usuario.authorities.map((a) => a.nome);

      if (
        (authoritiesName.includes("ROLE_ADMIN_ESCRITORIO") ||
          authoritiesName.includes("ROLE_COLABORADOR_ESCRITORIO")) &&
        !authoritiesName.includes("ROLE_ADMIN")
      ) {
        permitted = this.empresaService
          .getEmpresasByEscritorioId(usuario.escritorios[0].id)
          .pipe(
            take(1),
            map((data) => {
              const empresasId = data.map((empresa) => empresa.id);
              if (
                this.permittedAuthoritiesEscritorio.includes(role.nome) &&
                this.verificaAutorizacaoEsc(empresasId)
              ) {
                return true;
              }
            })
          );
      }
    });

    return permitted;
  }

  // this.empresaService
  //   .getEmpresasByEscritorioId(usuario.escritorios[0].id)
  //   .subscribe((data: any) => {
  //     console.log(data);
  //     if (data.type === 4) {
  //       console.log(data.length)
  //       // for (let i = 0; i < data.body.length; i++) {
  //       //   if (parseInt(data.body[i].id) === this.empresaId) {
  //       //     return of(true);
  //       //   }
  //       // }
  //     } else {
  //     }
  //   });

  // this.empresaService
  //   .getEmpresasByEscritorioId(usuario.escritorios[0].id)
  //   .subscribe((data) => {

  //     for (let i = 0; i < data.length; i++) {
  //       if (parseInt(data[i].id) === this.empresaId) {
  //         return of(true);
  //       }
  //     }
  //   });

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return of(true);
  }

  setActivedRoute(route) {
    this.route = route;
  }
  setEmpresaId(id: number) {
    this.empresaId = id;
  }
}
