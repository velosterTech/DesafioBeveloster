import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { MensagemService } from '../../mensagem.service';

@Component({
  selector: 'app-listar-mensagens',
  templateUrl: './listar-mensagens.component.html',
  styleUrls: ['./listar-mensagens.component.scss']
})
export class ListarMensagensComponent implements OnInit {
  mensagens = [];
  retornoMensagens = [];
  totalItems: number;
  currentPage: number = 1;
  smallnumPages: number = 0;
  usuarioId: any;
  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;

  currentPager: number = 1;

  constructor(
    private mensagemService: MensagemService,
    private route: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.mensagens = this.route.snapshot.data.mensagens;
    this.receberMensagens(this.mensagens)
  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.retornoMensagens = this.mensagens.slice(startItem, endItem);
  }

  receberMensagens(mensagens) {
      this.totalItems = mensagens.length;
      this.retornoMensagens = mensagens.slice(0, 5);
      if (mensagens.length === 0) {
        this.totalItems = 0;
      }
  }

  checkboxAtt(mensagem){
    this.atualizaStatus(mensagem);
    this.receberMensagens(this.usuarioId);
  }
  atualizaStatus(mensagem){
    this.mensagemService.atualizaStatus(mensagem).subscribe();
  }
}
