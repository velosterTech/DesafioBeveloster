import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarMensagemComponent } from './cadastrar-mensagem.component';

describe('MensagemComponent', () => {
  let component: CadastrarMensagemComponent;
  let fixture: ComponentFixture<CadastrarMensagemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastrarMensagemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarMensagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
