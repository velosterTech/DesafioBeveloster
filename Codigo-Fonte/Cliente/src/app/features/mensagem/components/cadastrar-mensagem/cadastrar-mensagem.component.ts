import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { EmpresaService } from '../../../empresa/empresa.service';
import { EscritorioContabilService } from '../../../escritorio-contabil/escritorio-contabil.service';
import { MensagemService } from '../../mensagem.service';
import { UsuarioService } from '../../../usuario/usuario.service';

@Component({
  selector: 'app-cadastrar-mensagem',
  templateUrl: './cadastrar-mensagem.component.html',
  styleUrls: ['./cadastrar-mensagem.component.scss']
})
export class CadastrarMensagemComponent implements OnInit {
  empresas = [];
  escritorios = [];
  idUsuario: number;
  msgForm: FormGroup;

  constructor(private rest: MensagemService, private fb: FormBuilder, private usuarioService: UsuarioService, private empresaService: EmpresaService, private router: Router, private escritorioService: EscritorioContabilService) { }

  ngOnInit(): void {
    let usuarioL = JSON.parse(localStorage.getItem('usuario'));
    this.usuarioService.getByLogin(usuarioL.login).subscribe(data => this.setIdUsuario(data.id));
    this.receberEmpresas();
    this.receberEscritorios("");
    this.msgForm = this.fb.group({
      usuarioDestino: ["", Validators.required],
      txtMensagem: ["", Validators.required],
    })
  }

  receberEmpresas(){
    this.empresaService.receberEmpresas("", this.idUsuario).subscribe(data => {
      console.log(data);
      let empresas = [];
      data.forEach(function(item){
        empresas.push(item);
      });
      this.empresas = empresas;

    });
  }

  receberEscritorios(busca: string){
    this.escritorioService.receberEscritorios(busca).subscribe(data => {
      console.log(data);
      let escritorios = [];
        data.forEach(function (item) {
        escritorios.push(item);
      });
      this.escritorios = escritorios;
      
    });
  }
  enviar(msg){
    console.log(msg.value);
    this.rest.novaMensagem(this.idUsuario, msg.value).subscribe(success => {this.router.navigate(['listar-mensagens', this.idUsuario]);}, error => console.log("falha"));
  }

  setIdUsuario(id: number) {
    this.idUsuario = id;
  }


  voltar(){
    this.router.navigate(['listar-mensagens', this.idUsuario]);
  }

}
