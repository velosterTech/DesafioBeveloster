import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CadastrarMensagemComponent } from "./components/cadastrar-mensagem/cadastrar-mensagem.component";
import { ListarMensagensComponent } from "./components/listar-mensagens/listar-mensagens.component";
import { CadastrarMensagemResolverService } from "./resolvers/cadastrar-mensagem-resolver.service";


const routes: Routes = [
  {
    path: "",
    component: ListarMensagensComponent
  },
  {
    path: "cadastrar",
    component: CadastrarMensagemComponent,
    resolve: { mensagem: CadastrarMensagemResolverService }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MensagemRoutingModule {}
