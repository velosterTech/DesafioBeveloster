import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { ListarMensagensComponent } from "./components/listar-mensagens/listar-mensagens.component";
import { CadastrarMensagemComponent } from "./components/cadastrar-mensagem/cadastrar-mensagem.component";
import { MensagemRoutingModule } from "./mensagem-routing.module";
import { ListarMensagemResolverService } from "./resolvers/listar-mensagem-resolver.service";
import { CadastrarMensagemResolverService } from "./resolvers/cadastrar-mensagem-resolver.service";


@NgModule({
  declarations: [ListarMensagensComponent, CadastrarMensagemComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PaginationModule,
    MensagemRoutingModule,
  ],
  providers: [CadastrarMensagemResolverService, ListarMensagemResolverService],
})
export class MensagemModule {}
