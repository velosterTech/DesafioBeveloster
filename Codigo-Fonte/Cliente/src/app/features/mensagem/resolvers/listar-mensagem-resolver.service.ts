import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Mensagem } from '../../../core/model/Mensagem';
import { UsuarioService } from '../../usuario/usuario.service';
import { MensagemService } from '../mensagem.service';

@Injectable({
  providedIn: 'root'
})
export class ListarMensagemResolverService implements Resolve<Mensagem>{


  constructor(private usuarioService: UsuarioService, private router: Router, private mensagemService: MensagemService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Mensagem | Observable<Mensagem> | Promise<Mensagem> {

    const usuario = JSON.parse(localStorage.getItem('usuarioL'));

    return this.mensagemService.receberMensagem(usuario.id);
  }
}
