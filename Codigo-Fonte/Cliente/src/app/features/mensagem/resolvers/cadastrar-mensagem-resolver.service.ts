import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EmpresaService } from '../../empresa/empresa.service';
import { EscritorioContabilService } from '../../escritorio-contabil/escritorio-contabil.service';

@Injectable({
  providedIn: 'root'
})
export class CadastrarMensagemResolverService implements Resolve<any>{

  constructor(private empresaService: EmpresaService, private escritorioService: EscritorioContabilService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | Observable<any> | Promise<any> {
    const usuarioLogado = JSON.parse(localStorage.getItem('usuarioL'));
    console.log(usuarioLogado)
    const usuarioId = usuarioLogado.id;

    const testes$ = forkJoin({
      empresas : this.empresaService.receberEmpresas("", usuarioId)
      .pipe(catchError(error => {
        throw console.log(error.status + ' error ao receber empresas')
      })),
      escritorios : this.escritorioService.receberEscritorios("")
      .pipe(catchError(error => {
        throw console.log(error.status + ' error ao receber escritorios')
      }))
    })

    return testes$;
  }
}
