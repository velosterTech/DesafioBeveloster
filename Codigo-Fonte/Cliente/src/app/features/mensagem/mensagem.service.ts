import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppConstants } from "../../core/settings/app-constants";


@Injectable({
  providedIn: 'root'
})
export class MensagemService {


  constructor(private http: HttpClient) { }

  novaMensagem(id, mensagem): Observable<any> {
    return this.http.post<any>(AppConstants.baseUrlMensagem + "nova/" + id, mensagem);
  }
  novaSolicitacao(id, mensagem, categoriaId, tipoDocumentoId){
    let params = new HttpParams();
    
    params = params.set('categoriaId',categoriaId);
    params = params.set('tipoDocumentoId',tipoDocumentoId);
    return this.http.post<any>(AppConstants.baseUrlMensagem + "nova/solicitacao/" + id, mensagem, {params: params});
  }

  receberMensagem(id): Observable<any> {
    return this.http.get<any>(AppConstants.baseUrlMensagem + "todas/" + id);
  }

  atualizaStatus(mensagem){
    return this.http.put<any>(AppConstants.baseUrlMensagem + "atualizaStatus/" + mensagem.id, mensagem);
  }
}
