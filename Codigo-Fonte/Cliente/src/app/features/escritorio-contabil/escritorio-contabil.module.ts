import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../../shared/shared.module';

import { CadastrarEscritorioContabilComponent } from './components/cadastrar-escritorio-contabil/cadastrar-escritorio-contabil.component';
import { EscritorioContabilRoutingModule } from './escritorio-contabil-routing.module';
import { EscritorioResolver } from './resolvers/escritorio-resolver.service';


@NgModule({
  declarations: [
    CadastrarEscritorioContabilComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EscritorioContabilRoutingModule,
    SharedModule,
    NgxMaskModule.forChild(),
  ],
  providers: [
    
  ]
})
export class EscritorioContabilModule { }
