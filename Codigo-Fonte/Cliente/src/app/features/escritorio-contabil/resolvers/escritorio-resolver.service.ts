import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EscritorioContabil } from '../../../core/model/EscritorioContabil';
import { EscritorioContabilService } from '../escritorio-contabil.service';

@Injectable({
  providedIn: 'root'
})
export class EscritorioResolver implements Resolve<EscritorioContabil> {

  constructor(private escritorioService: EscritorioContabilService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): EscritorioContabil | Observable<EscritorioContabil> | Promise<EscritorioContabil> {
    
    const buscarTodos = "";
    const usuario = JSON.parse(localStorage.getItem('usuarioL'))
    const escritorio$ = this.escritorioService.receberEscritorios(buscarTodos, false, usuario.id);
    return escritorio$
  }
}
