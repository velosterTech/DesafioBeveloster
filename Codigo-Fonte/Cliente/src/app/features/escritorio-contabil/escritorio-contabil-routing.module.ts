import { NgModule } from "@angular/core";
import { PreloadAllModules, PreloadingStrategy, RouterModule, Routes } from "@angular/router";
import { CadastrarEscritorioContabilComponent } from "./components/cadastrar-escritorio-contabil/cadastrar-escritorio-contabil.component";
import { ListarEscritorioContabilComponent } from "./components/listar-escritorio-contabil/listar-escritorio-contabil.component";

const routes: Routes = [
  {
    path: "",
    component: ListarEscritorioContabilComponent
  },
  {
    path: "cadastrar",
    component: CadastrarEscritorioContabilComponent,
  },
  {
    path: "editar/:id",
    component: CadastrarEscritorioContabilComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EscritorioContabilRoutingModule {}
