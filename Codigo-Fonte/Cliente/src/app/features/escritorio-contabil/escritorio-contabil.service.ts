import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { take } from 'rxjs/operators';
import { EscritorioContabil } from "../../core/model/EscritorioContabil";
import { AppConstants } from "../../core/settings/app-constants";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
   })
};

@Injectable({
  providedIn: "root",
})
export class EscritorioContabilService {



  constructor(
    private http: HttpClient,
    private router: Router,

  ) {}

  criarFormulario(escritorio : EscritorioContabil): Observable<EscritorioContabil>{
    return this.http.post<EscritorioContabil>(AppConstants.baseUrlEscritorioContabil + `registrar`, escritorio, httpOptions)
                    .pipe(take(1))

  }
  receberEscritorios(busca: string, status, id){
    let params = new HttpParams();

    params = params.set('busca', busca);
    params = params.set('status', status);
    params = params.set('id', id);
    return this.http.get<any>(AppConstants.baseUrlEscritorioContabil + 'todos', {params: params});
  }

  getById(id){
    return this.http.get<any>(AppConstants.baseUrlEscritorioContabil + id);
  }

  editar(escritorio){
    return this.http.put<any>(AppConstants.baseUrlEscritorioContabil + "editar/" + escritorio.id, escritorio);
  }
  statusEscritorio(id){
    return this.http.put(AppConstants.baseUrlEscritorioContabil + "status", id);
  }

  findDepartamentos(cnpj){
    let params = new HttpParams();

    params = params.set('cnpj',cnpj);
    return this.http.get<any>(AppConstants.baseUrlEscritorioContabil + "departamento", {params: params});
  }
}
