import { absoluteFrom } from "@angular/compiler-cli/src/ngtsc/file_system";
import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { EMPTY } from "rxjs";
import { debounceTime, distinctUntilChanged, map, switchMap, take, tap } from "rxjs/operators";
import { AvisoServiceService } from "../../../../shared/aviso-service.service";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { ConfirmacaoPopUpComponent } from "../../../../shared/components/confirmacao-pop-up/confirmacao-pop-up.component";
import { ListarEmpresasComponent } from "../../../empresa/components/listar-empresas/listar-empresas.component";
import { ListarUsuariosComponent } from "../../../usuario/components/listar-usuarios/listar-usuarios.component";
import { EscritorioContabilService } from "../../escritorio-contabil.service";

@Component({
  selector: "app-listar-escritorio-contabil",
  templateUrl: "./listar-escritorio-contabil.component.html",
  styleUrls: ["./listar-escritorio-contabil.component.scss"],
})
export class ListarEscritorioContabilComponent implements OnInit {
  campoPesquisa = new FormControl();


  url: string;

  escritorios = [] || undefined;

  retorno_escritorios = [];
  usuarioId = 0;
  totalItems: number;
  currentPage: number = 1;
  smallnumPages: number = 0;

  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;

  currentPager: number = 1;
  buscarTodos = "";
  admin: boolean = false;
  admin_escritorio: boolean = false;

  constructor(
    private rest: EscritorioContabilService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private avisoService: AvisoServiceService,
    private modalService: BsModalService,
  ) {

  }

  ngOnInit(): void {
    const usuario = JSON.parse(localStorage.getItem('usuarioL'))
    this.setUsuarioId(usuario.id);
    this.escritorios = this.route.snapshot.data.escritorios;

    if (this.escritorios === undefined) {
      this.receberEscritorios(this.buscarTodos);
    } else {
      this.receberEscritorios(this.escritorios);
    }
    const perfis = JSON.parse(localStorage.getItem("user_role"));
    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
      }
    });

    if (!this.admin) {
      perfis.forEach((p) => {
        if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
          this.setAdminEscritorio();
        }
      });
    }

    this.campoPesquisa.valueChanges
      .pipe(
        map((value) => value.trim()),
        debounceTime(150),
        distinctUntilChanged(),
        tap((value) => console.log(value)),
        switchMap((value: string) => {
          return this.rest.receberEscritorios(value, false, this.usuarioId);
        })
      )
      .subscribe((data) => {
        this.escritorios = data;
        this.totalItems = data.lenght;
        this.retorno_escritorios = data.slice(0, 5);
      });

  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.retorno_escritorios = this.escritorios.slice(startItem, endItem);
  }

  receberEscritorios(data): void {
    if (data === "") {
      this.rest.receberEscritorios(data, false, this.usuarioId).subscribe((escritorios) => {
        this.escritorios = escritorios;
        this.totalItems = this.escritorios.length;
        this.retorno_escritorios = this.escritorios.slice(0, 5);
      });
    } else {
      if (data instanceof Array && data?.length === 0) {
        console.log("não há escritórios cadastrados");
      }
      this.totalItems = this.escritorios.length;
      this.retorno_escritorios = this.escritorios.slice(0, 5);
    }
  }

  openDialog(escritorio, id): void {
    const dialogRef = this.dialog.open(ConfirmacaoPopUpComponent, {
      width: "500px",
      height: "200px",
      data: { nome: escritorio.razaoSocial, status: escritorio.status },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result ?? 1) {
        this.rest.statusEscritorio(id).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberEscritorios("");
          },
          (error) => console.log("erro")
        );
      }
    });

  }
  trocarStatus(escritorio) {
    if (escritorio.status) {
      const result$ = this.avisoService.showAviso('Confirmação', 'Deseja desativar ' + escritorio.razaoSocial + '?');
      result$.asObservable().pipe(
        take(1),
        switchMap((result) => result ? this.rest.statusEscritorio(escritorio.id) : EMPTY)).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberEscritorios("");
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Cadastro desativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar cadastro";
          }

        );
    } else {
      const result$ = this.avisoService.showAviso('Confirmação', 'Deseja ativar ' + escritorio.razaoSocial + '?');
      result$.asObservable().pipe(
        take(1),
        switchMap((result) => result ? this.rest.statusEscritorio(escritorio.id) : EMPTY)).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberEscritorios("");
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Cadastro ativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(AvisoConfirmacaoModalComponent);
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar cadastro";
          }
        );
    }
    this.currentPage = 1;

  }

  setUsuarioId(id){
    this.usuarioId = id;
  }
  setAdmin() {
    this.admin = true;
  }

  setAdminEscritorio() {
    this.admin_escritorio = true;
  }
}

function teste(escritorios: any) {
  throw new Error("Function not implemented.");
}
