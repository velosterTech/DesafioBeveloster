import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarEscritorioContabilComponent } from './listar-escritorio-contabil.component';

describe('CadastrarClienteComponent', () => {
  let component: ListarEscritorioContabilComponent;
  let fixture: ComponentFixture<ListarEscritorioContabilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarEscritorioContabilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarEscritorioContabilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
