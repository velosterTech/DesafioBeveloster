import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarEscritorioContabilComponent } from './cadastrar-escritorio-contabil.component';

describe('CadastrarEscritorioContabilComponent', () => {
  let component: CadastrarEscritorioContabilComponent;
  let fixture: ComponentFixture<CadastrarEscritorioContabilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CadastrarEscritorioContabilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarEscritorioContabilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
