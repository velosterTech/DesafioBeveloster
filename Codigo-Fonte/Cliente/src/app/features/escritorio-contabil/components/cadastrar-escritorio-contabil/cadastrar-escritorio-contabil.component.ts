import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import {
  DefaultValueAccessor,
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Observable } from "rxjs";
import { map, switchMap } from "rxjs/operators";
import { EscritorioContabil } from "../../../../core/model/EscritorioContabil";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { EscritorioContabilService } from "../../escritorio-contabil.service";

@Component({
  selector: "app-cadastrar-escritorio-contabil",
  templateUrl: "./cadastrar-escritorio-contabil.component.html",
  styleUrls: ["./cadastrar-escritorio-contabil.component.scss"],
})
export class CadastrarEscritorioContabilComponent implements OnInit {
  escritorio: EscritorioContabil;
  escritorioForm: FormGroup;
  editando: boolean = false;
  cpfStatus: boolean;
  constructor(
    private escritorioContabilService: EscritorioContabilService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.route.params
      .pipe(
        map((params: any) => params["id"]),
        switchMap((id) => {
          if (id !== undefined) {
            return this.escritorioContabilService.getById(id);
          } else {
            return new Observable<any>();
          }
        })
      )
      .subscribe((escritorio) => {
        this.updateForm(escritorio);
        this.setEditando();
      });

    const original = DefaultValueAccessor.prototype.registerOnChange;

    DefaultValueAccessor.prototype.registerOnChange = function (fn) {
      return original.call(this, (value) => {
        const trimmed = typeof value === "string" ? value.trim() : value;
        return fn(trimmed);
      });
    };

    this.escritorioForm = this.fb.group({
      id: [null],
      cnpj: ["", Validators.required],
      razaoSocial: ["", Validators.required],
      nomeFantasia: "",
      endereco: ["", Validators.required],
      telefoneFixo: ["", [Validators.required]],
      email: ["", [Validators.required, Validators.email]],
      nomeSolicitante: ["", Validators.required],
      cpfSolicitante: ["", Validators.required],
      crc: ["", Validators.required],
      telefoneCelular: [""],
      emailResponsavel: ["", [Validators.required, Validators.email]],
      status: true,
    });
  }

  get controls() {
    return this.escritorioForm.controls;
  }

  get endereco() {
    return this.escritorioForm.controls.endereco;
  }

  onSubmit(escritorio) {
    this.escritorioContabilService.criarFormulario(escritorio.value).subscribe(
      (sucess) => {
        console.log("Enviado!");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Escritório cadastrado com sucesso";
        this.router.navigate(["dashboard"]);
      },
      (error) => {
        console.error("Deu ruim!");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );

        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao cadastrar escritório";
      },
      () => console.log("Request completo")
    );
  }

  setEditando() {
    this.editando = true;
  }

  updateForm(escritorio) {
    console.log(escritorio);
    this.escritorioForm.patchValue({
      id: escritorio.id,
      cnpj: escritorio.cnpj,
      razaoSocial: escritorio.razaoSocial,
      nomeFantasia: escritorio.nomeFantasia,
      endereco: escritorio.endereco,
      telefoneFixo: escritorio.contato,
      email: escritorio.email,
      nomeSolicitante: "a",
      cpfSolicitante: "71013898133",
      crc: "0SS-000000/S-0",
      telefoneCelular: "00000000000",
      emailResponsavel: "a@gmail.com",
    });
  }

  editar(escritorio) {
    this.escritorioContabilService.editar(escritorio.value).subscribe(
      (success) => {
        console.log("Deu certo");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso";
        bsModalRef.content.msg = "Escritório editado com sucesso";
        this.router.navigate(["dashboard"]);
      },
      (error) => {
        console.log("Falhou");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso";
        bsModalRef.content.msg = "Erro ao editar escritório";
        console.log(escritorio.value);
        console.log(error);
        this.router.navigate(["dashboard"]);
      }
    );
  }

  cnpjKeyPressEvent(event: any): void {
    if (!(this.escritorioForm.get("cnpj").value === "")) {
      let cnpj: string = event.target.value;
      cnpj = cnpj
        .replace(".", "")
        .replace(".", "")
        .replace("/", "")
        .replace("-", "");
      this.http.get("wk/empresa/cnpj/" + cnpj).subscribe(
        (success) => {
          console.log(success);
          if (success !== null) {
            this.escritorioForm.get("cnpj").setErrors({ duplicado: true });
          }
        },
        (error) => {
          console.log(error);
        }
      );
      this.http.get("/wk/escritorio/cnpj/" + cnpj).subscribe(
        (success) => {
          console.log(success + " - nao tem no banco");
          if (success !== null) {
            this.escritorioForm.get("cnpj").setErrors({ duplicado: true });
          }
        },
        (error) => {
          console.log(error);
        }
      );
      this.http.get("/receita/" + cnpj).subscribe(
        (success) => {
          const responseJson = JSON.parse(JSON.stringify(success));
          if (responseJson.message === "CNPJ inválido") {
            return this.escritorioForm
              .get("cnpj")
              .setErrors({ cnpjInvalido: true });
          }
          this.onSuccess(responseJson);
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  cpfKeyPressEvent(event: any): void {
    if (!(this.escritorioForm.get("cpfSolicitante").value === "")) {
      let cpfSolicitante: string = event.target.value;
      console.log(cpfSolicitante);
      cpfSolicitante = cpfSolicitante
        .replace(".", "")
        .replace(".", "")
        .replace("-", "");
      if (!this.testaCPF(cpfSolicitante)) {
        this.escritorioForm
          .get("cpfSolicitante")
          .setErrors({ cpfInvalido: true });
      }

      this.http.get<any>("/wk/usuario/cpf/" + cpfSolicitante).subscribe(
        (success) => {
          if (success.length > 1 && success[0].authorities.length > 1) {
            this.escritorioForm
              .get(["nomeSolicitante"])!
              .setValue(success[0].nome);
            this.escritorioForm.get(["crc"])!.setValue(success[0].crc);
            this.escritorioForm
              .get(["telefoneCelular"])!
              .setValue(success[0].contato);
            this.escritorioForm
              .get(["emailResponsavel"])!
              .setValue(success[0].email);
          } else if(success[0].authorities.length === 1){
            this.escritorioForm
              .get("cpfSolicitante")
              .setErrors({ duplicado: true });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  crcKeyPressEvent(event: any): void {
    let crc = event.target.value;
    crc = crc
        .replace("-", "")
        .replace("/", "")
        .replace("-", "");
    if (crc !== "") {
      this.http.get<any>("/wk/usuario/crc/" + crc).subscribe(
        (success) => {
          if (success !== null) {
            this.escritorioForm
              .get("crc")
              .setErrors({ duplicado: true });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  onSuccess(responseJson): void {
    console.log(responseJson);

    this.escritorioForm.get(["razaoSocial"])!.setValue(responseJson.nome);
    this.escritorioForm.get(["nomeFantasia"])!.setValue(responseJson.fantasia);
    this.escritorioForm
      .get(["endereco"])!
      .setValue(
        responseJson.logradouro +
          ", " +
          responseJson.numero +
          ", " +
          responseJson.bairro +
          ", " +
          responseJson.complemento
      );
    console.log(this.escritorioForm);
  }

  testaCPF(cpfSolicitante: string) {
    var Soma = 0;
    if (cpfSolicitante === undefined) {
      this.setCpf(false);
      //alert("cpfSolicitante inválido");
      return false;
    }

    var strCPF = cpfSolicitante
      .replace(".", "")
      .replace(".", "")
      .replace("-", "");
    if (
      strCPF === "00000000000" ||
      strCPF === "11111111111" ||
      strCPF === "22222222222" ||
      strCPF === "33333333333" ||
      strCPF === "44444444444" ||
      strCPF === "55555555555" ||
      strCPF === "66666666666" ||
      strCPF === "77777777777" ||
      strCPF === "88888888888" ||
      strCPF === "99999999999" ||
      strCPF.length !== 11
    ) {
      this.setCpf(false);
      //alert("cpfSolicitante inválido");
      return false;
    }

    for (let i = 1; i <= 9; i++) {
      Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }

    var Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) {
      Resto = 0;
    }

    if (Resto !== parseInt(strCPF.substring(9, 10))) {
      this.setCpf(false);
      //alert("cpfSolicitante inválido");
      return false;
    }

    Soma = 0;
    for (let k = 1; k <= 10; k++) {
      Soma = Soma + parseInt(strCPF.substring(k - 1, k)) * (12 - k);
    }

    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) {
      Resto = 0;
    }

    if (Resto !== parseInt(strCPF.substring(10, 11))) {
      this.setCpf(false);
      //alert("cpfSolicitante inválido");
      return false;
    }

    this.setCpf(true);
    return true;
  }

  setCpf(status) {
    this.cpfStatus = status;
  }
}
