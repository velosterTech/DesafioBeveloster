import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { transform } from 'typescript';
import { Empresa } from '../../../core/model/Empresa';
import { EmpresaService } from '../empresa.service';

@Injectable({
  providedIn: 'root'
})
export class ListarEmpresaResolverService implements Resolve<Empresa> {

  constructor(private empresaService: EmpresaService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Empresa | Observable<Empresa> | Promise<Empresa> {
    const usuario = JSON.parse(localStorage.getItem('usuarioL'))
    const todos = "";
    const empresas$ =  this.empresaService.receberEmpresas(todos, usuario.id, false)
    return empresas$
  }
}
