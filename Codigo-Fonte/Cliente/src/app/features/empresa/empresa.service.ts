import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AppConstants } from "../../core/settings/app-constants";


@Injectable({
  providedIn: 'root'
})
export class EmpresaService {


  constructor(private http: HttpClient) { }

  salvarEmpresa(empresa): Observable<any> {
    return this.http.post<any>(AppConstants.baseUrlEmpresa + "salva", empresa);
  }

  getById(id){
    return this.http.get(AppConstants.baseUrlEmpresa + id);
  }

  editarEmpresa(empresa){
    return this.http.put(AppConstants.baseUrlEmpresa  + "editar" , empresa);
  }
  statusEmpresa(id){
    return this.http.put(AppConstants.baseUrlEmpresa + "status", id);
  }
  receberEmpresas(busca: string, id, status): Observable<any> {
    let params = new HttpParams();

    params = params.set('busca', busca);
    params = params.set('id', id);
    params = params.set('status', status);

    return this.http.get<any>(AppConstants.baseUrlEmpresa + "todos", {params: params});
  }
  getEmpresasByEscritorioId(id){
    return this.http.get<any>(AppConstants.baseUrlEmpresa + "escritorio/" + id, {
      // reportProgress: true,
      // observe: 'events',
    });
  }

}
