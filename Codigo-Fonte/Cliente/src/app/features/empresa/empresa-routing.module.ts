import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CadastrarEmpresaComponent } from "./components/cadastrar-empresa/cadastrar-empresa.component";
import { ListarEmpresasComponent } from "./components/listar-empresas/listar-empresas.component";

const routes: Routes = [
  {
    path: "",
    component: ListarEmpresasComponent,
  },
  {
    path: "cadastrar",
    component: CadastrarEmpresaComponent,
  },
  {
    path: "editar/:id",
    component: CadastrarEmpresaComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmpresaRoutingModule {}
