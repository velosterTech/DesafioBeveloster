import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { NgxMaskModule } from "ngx-mask";
import { AvisoPopUp2Component } from "../../shared/components/aviso-pop-up2/aviso-pop-up2.component";
import { SharedModule } from "../../shared/shared.module";

import { AvisoPopUpComponent } from "./components/aviso-pop-up/aviso-pop-up.component";
import { CadastrarEmpresaComponent } from "./components/cadastrar-empresa/cadastrar-empresa.component";
import { EmpresaRoutingModule } from "./empresa-routing.module";
import { EmpresaGuard } from "./guards/empresa.guard";




@NgModule({
  declarations: [CadastrarEmpresaComponent, AvisoPopUpComponent, AvisoPopUp2Component],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    EmpresaRoutingModule,
    NgxMaskModule.forChild(),
  ],
  exports: [

  ],
  providers: []
})
export class EmpresaModule {}
