import { HttpClient } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { EMPTY } from "rxjs";
import { concatMap, tap } from "rxjs/operators";
import { NaturezaJuridica } from "../../../../core/enums/NaturezaJuridica";
import { RegimeTributarioEnum } from "../../../../core/enums/RegimeTributario";

import { Empresa } from "../../../../core/model/Empresa";
import { AppConstants } from "../../../../core/settings/app-constants";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";

import { EscritorioContabilService } from "../../../escritorio-contabil/escritorio-contabil.service";
import { EmpresaService } from "../../empresa.service";

declare var require: any;

@Component({
  selector: "app-cadastrar-empresa",
  templateUrl: "./cadastrar-empresa.component.html",
  styleUrls: ["./cadastrar-empresa.component.scss"],
})
export class CadastrarEmpresaComponent implements OnInit {
  editando: boolean = false;
  empresa: Empresa;
  empresaForm: FormGroup;
  escritorios = [];
  natureza = [];
  regime = [];
  regimeTributario = RegimeTributarioEnum;
  naturezaJuridica = NaturezaJuridica;
  url: string;
  statusInscricaoEstadual = true;

  bsModalRef: BsModalRef;
  cpfStatus: boolean;
  usuarioId = 0;
  admin;
  empresas: [];
  mensagemAviso: string;

  constructor(
    private fb: FormBuilder,
    private rest: EmpresaService,
    private router: Router,
    private route: ActivatedRoute,
    private escritorioService: EscritorioContabilService,
    public dialog: MatDialog,
    public http: HttpClient,
    private modalService: BsModalService
  ) {
    this.natureza = Object.keys(this.naturezaJuridica);
    this.regime = Object.keys(this.regimeTributario);
  }

  ngOnInit(): void {
    let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));

    this.setUsuarioId(usuarioL.id);

    this.empresas = this.route.snapshot.data.empresas;

    this.receberEscritorios("", usuarioL.id);

    this.setAdmin();
    this.empresaForm = this.fb.group({
      id: [null],
      escritorio: ["", [Validators.required]],
      cnpj: [
        "",
        [
          Validators.required,
          Validators.minLength(14),
          Validators.maxLength(18),
        ],
      ],
      razaoSocial: [""],
      nomeFantasia: [""],
      inscricaoEstadual: [""],
      cep: [""],
      logradouro: [""],
      bairro: [""],
      numero: [""],
      complemento: [""],
      contato: [
        "",
        [
          Validators.required
        ],
      ],
      email: ["", [Validators.required, Validators.email]],
      atvdPrimaria: [],
      codCnaePrimaria: [],
      atvdSecundaria: [],
      codCnaeSecundaria: [],
      naturezaJuridica: ["", [Validators.required]],
      regimeTributario: ["", [Validators.required]],

      nomeSolicitante: ["", [Validators.required]],
      cpf: [
        "",
        [
          Validators.required,
          Validators.minLength(11),
          Validators.maxLength(11),
        ],
      ],
      emailSolicitante: ["", [Validators.required, Validators.email]],
      celular: [
        "",
        [
          Validators.required,
          Validators.minLength(11),
          Validators.maxLength(14),
        ],
      ],
    });
    this.url = window.location.pathname;

    this.route.params.subscribe((params) => {
      const id = params["id"];
      console.log(id);
      if (id !== undefined) {
        const indexEmpresa = this.empresas.findIndex(
          (empresa: Empresa) => empresa.id === parseInt(id)
        );
        console.log(indexEmpresa);
        const empresa = this.empresas[indexEmpresa];
        this.setEditando();
        this.onSuccess(empresa);
      }
    });
  }

  cadastrar(empresaFormulario) {
    console.log(empresaFormulario.value);
    this.rest.salvarEmpresa(empresaFormulario.value).subscribe(
      (success) => {
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Empresa cadastrada com sucesso";
        console.log(success);
        this.router.navigate(["dashboard"]);
      },
      (error) => {
        console.error(error);
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao cadastrar empresa";
      }
    );
  }

  setEditando() {
    this.editando = true;
  }

  // updateForm(empresa) {
  //   console.log(empresa);
  //   this.empresaForm.patchValue({
  //     id: empresa.id,
  //     escritorio: empresa.escritorioId.razaoSocial,
  //     cnpj: empresa.pjId.cnpj,
  //     razaoSocial: empresa.pjId.razao_social,
  //     nomeFantasia: "",
  //     inscricaoEstadual:empresa.pjId.inscricao_estadual,
  //     cep:"",
  //     logradouro: empresa.pjId.endereco,
  //     bairro:"",
  //     numero:"",
  //     complemento:"",
  //     contato: empresa.pjId.contato,
  //     email: empresa.pjId.email,
  //     codCnaePrimaria: "",
  //     atvdPrimaria: "",
  //     codCnaeSecundaria: "",
  //     atvdSecundaria: "",
  //     naturezaJuridica: empresa.pjId.naturezaJuridica,
  //     regimeTributario: empresa.pjId.regimeTributario,

  //     nomeSolicitante: empresa.pjId.nome,
  //     cpf: empresa.responsavel.cpf,
  //     emailSolicitante: empresa.pjId.email,
  //     celular: empresa.pjId.celular,
  //   });
  // }

  editar(empresaForm) {
    console.log(empresaForm.value);

    this.rest.editarEmpresa(empresaForm.value).subscribe(
      (success) => {
        console.log(success);
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Empresa editada com sucesso";
        this.router.navigate(["dashboard"]);
      },
      (error) => {
        console.log("Falhou");
        const bsModalRef: BsModalRef = this.modalService.show(
          AvisoConfirmacaoModalComponent
        );
        bsModalRef.content.title = "Aviso!";
        bsModalRef.content.msg = "Erro ao editar empresa";
        console.log(this.empresa);
        console.log(error);
      }
    );
  }

  cnpjKeyPressEvent(event: any): void {
    if (!(this.empresaForm.get("cnpj").value === "")) {
      let cnpj: string = event.target.value;
      cnpj = cnpj
        .replace(".", "")
        .replace(".", "")
        .replace("/", "")
        .replace("-", "");

      let res : any;

      this.http.get("/receita/" + cnpj).pipe(
        tap((response) => {
          const responseJson = JSON.parse(JSON.stringify(response));
          if (responseJson.message === "CNPJ inválido") {
            console.log("cnpj invalido - receita")
            this.empresaForm
              .get("cnpj")
              .setErrors({ cnpjInvalido: true });
            return EMPTY
          } else {
            console.log(responseJson)
            res = responseJson;
          }
        }),
        concatMap(() => this.http.get(AppConstants.baseUrlEscritorioContabil + "cnpj/" + cnpj)),
        tap((response) => {
          console.log(response)
          if (response !== null) {
            console.log("escritorio ja existe")
            this.empresaForm.get("cnpj").setErrors({ duplicado: true });
            return EMPTY
          }

        }),
        concatMap(() => this.http.get(AppConstants.baseUrlEmpresa + "cnpj/" + cnpj)),
        tap((response) => {
          console.log(response)
          if (response !== null) {
            console.log("empresa ja existe")
            this.empresaForm.get("cnpj").setErrors({ duplicado: true });
          }

        })
      ).subscribe(
      next => {
        console.log(next)
        if (!(this.empresaForm.get('cnpj').errors?.duplicado || this.empresaForm.get('cnpj').errors?.cnpjInvalido)) {
          this.onSuccess(res)
        }
      },
      error => console.error(error)
      )
    }
  }

  cpfKeyPressEvent(event: any): void {
    if (!(this.empresaForm.get("cpf").value === "")) {
      let cpf: string = event.target.value;
      cpf = cpf.replace(".", "").replace(".", "").replace("-", "");
      if (!this.testaCPF(cpf)) {
        this.empresaForm.get("cpf").setErrors({ cpfInvalido: true });
      }

      this.http.get<any>("/wk/usuario/cpf/" + cpf).subscribe(
        success  => {
          if(success.length > 1 && success[0].authorities.length > 1){
            this.empresaForm.get(["nomeSolicitante"])!.setValue(success[0].nome);
            this.empresaForm.get(["celular"])!.setValue(success[0].contato);
            this.empresaForm.get(["emailSolicitante"])!.setValue(success[0].email);
          } else if (success[0].authorities.length === 1){
            this.empresaForm
              .get("cpf")
              .setErrors({ duplicado: true });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  inscricaoKeyPressEvent(event: any): void {
    console.log(this.empresaForm.value);
    if (!(this.empresaForm.get("inscricaoEstadual").value === "")) {
      let inscricao: string = event.target.value;
      this.http.get("/wk/empresa/inscricao-estadual/" + inscricao).subscribe(
        (success) => {
          console.log(success);
          if (success !== null) {
            this.empresaForm
              .get("inscricao_estadual")
              .setErrors({ duplicado: true });
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  previousState(): void {
    window.history.back();
  }

  onSuccess(responseJson): void {
    console.log(responseJson.value);
    if (this.editando === false) {
      this.empresaForm
        .get(["cnpj"])!
        .setValue(
          responseJson.cnpj
            .replace(".", "")
            .replace(".", "")
            .replace("/", "")
            .replace("-", "")
        );
      this.empresaForm.get(["id"])!.setValue(responseJson.id);
      this.empresaForm.get(["razaoSocial"])!.setValue(responseJson.nome);
      this.empresaForm.get(["nomeFantasia"])!.setValue(responseJson.fantasia);
      this.empresaForm
        .get(["cep"])
        .setValue(responseJson.cep.replace(".", "").replace("-", ""));
      this.empresaForm.get(["logradouro"])!.setValue(responseJson.logradouro);
      this.empresaForm.get(["bairro"])!.setValue(responseJson.bairro);
      this.empresaForm.get(["complemento"]).setValue(responseJson.complemento);
      this.empresaForm.get(["email"])!.setValue(responseJson.email);
      this.empresaForm
        .get(["atvdPrimaria"])!
        .setValue(responseJson.atividade_principal[0].text);
      this.empresaForm
        .get(["codCnaePrimaria"])!
        .setValue(responseJson.atividade_principal[0].code);
      this.empresaForm
        .get(["atvdSecundaria"])!
        .setValue(responseJson.atividades_secundarias[0].text);
      this.empresaForm
        .get(["codCnaeSecundaria"])!
        .setValue(responseJson.atividades_secundarias[0].code);
      this.empresaForm
        .get(["codCnaeSecundaria"])!
        .setValue(responseJson.atividades_secundarias[0].code);
    } else {
      this.empresaForm.get(["id"]).setValue(responseJson.id);
      this.empresaForm
        .get(["escritorio"])
        .setValue(responseJson.escritorio.cnpj);
      this.empresaForm.get(["cnpj"])!.setValue(responseJson.cnpj);
      this.empresaForm.get(["razaoSocial"])!.setValue(responseJson.razaoSocial);
      this.empresaForm
        .get(["nomeFantasia"])!
        .setValue(responseJson.nomeFantasia);
      this.empresaForm
        .get(["inscricaoEstadual"])!
        .setValue(responseJson.inscricaoEstadual);
      this.empresaForm
        .get(["cep"])
        .setValue(responseJson.cep.replace(".", "").replace("-", ""));
      this.empresaForm.get(["logradouro"])!.setValue(responseJson.logradouro);
      this.empresaForm.get(["bairro"]).setValue(responseJson.bairro);
      this.empresaForm.get(["numero"]).setValue(responseJson.numero);
      this.empresaForm.get(["complemento"]).setValue(responseJson.complemento);
      this.empresaForm.get(["contato"])!.setValue(responseJson.contato);
      this.empresaForm.get(["email"])!.setValue(responseJson.email);
      this.empresaForm
        .get(["atvdPrimaria"])!
        .setValue(responseJson.atvdPrimaria);
      this.empresaForm
        .get(["codCnaePrimaria"])!
        .setValue(responseJson.codCnaePrimaria);
      this.empresaForm
        .get(["atvdSecundaria"])!
        .setValue(responseJson.atvdSecundaria);
      this.empresaForm
        .get(["codCnaeSecundaria"])!
        .setValue(responseJson.codCnaeSecundaria);
      this.empresaForm
        .get(["regimeTributario"])!
        .setValue(responseJson.regimeTributario.nome);
      this.empresaForm
        .get(["naturezaJuridica"])!
        .setValue(responseJson.naturezaJuridica);
    }
  }

  onFinalize(): void {
    //finalize request
  }

  receberEscritorios(busca: string, id) {
    this.escritorioService.receberEscritorios(busca, true, id).subscribe((data) => {
      console.log(data);
      let escritorios = [];
      data.forEach(function (item) {
        escritorios.push(item);
      });
      this.escritorios = escritorios;
    });
  }

  // protected createFromForm(): Empresa {
  //   return {
  //     ...new Empresa(),
  //     id: this.empresaForm.get(["id"])!.value,
  //     escritorio: this.empresaForm.get(["escritorio"])!.value,
  //     cnpj: this.empresaForm.get(["cnpj"])!.value,
  //     razaoSocial: this.empresaForm.get(["razaoSocial"])!.value,
  //     nomeFantasia: this.empresaForm.get(["nomeFantasia"])!.value,
  //     inscricaoEstadual: this.empresaForm.get(["inscricaoEstadual"])!.value,
  //     cep: this.empresaForm.get(["cep"])!.value,
  //     logradouro: string;
  // bairro: string;
  // numero: string
  //     nomeSolicitante: this.empresaForm.get(["nomeSolicitante"])!.value,
  //     cpf: this.empresaForm.get(["cpf"])!.value,
  //     email: this.empresaForm.get(["email"])!.value,
  //     contato: this.empresaForm.get(["contato"])!.value,
  //     celular: this.empresaForm.get(["celular"])!.value,
  //     atvdPrimaria: this.empresaForm.get(["atvdPrimaria"])!.value,
  //     codCnaePrimaria: this.empresaForm.get(["codCnaePrimaria"])!.value,
  //     atvdSecundaria: this.empresaForm.get(["atvdSecundaria"])!.value,
  //     codCnaeSecundaria: this.empresaForm.get(["codCnaeSecundaria"])!.value,

  //     regimeTributario: this.empresaForm.get(["regimeTributario"])!.value,
  //     naturezaJuridica: this.empresaForm.get(["naturezaJuridica"])!.value,
  //   };
  // }

  testaCPF(cpf: string) {
    var Soma = 0;
    if (cpf === undefined) {
      this.setCpf(false);
      //alert("CPF inválido");
      return false;
    }

    var strCPF = cpf.replace(".", "").replace(".", "").replace("-", "");
    if (
      strCPF === "00000000000" ||
      strCPF === "11111111111" ||
      strCPF === "22222222222" ||
      strCPF === "33333333333" ||
      strCPF === "44444444444" ||
      strCPF === "55555555555" ||
      strCPF === "66666666666" ||
      strCPF === "77777777777" ||
      strCPF === "88888888888" ||
      strCPF === "99999999999" ||
      strCPF.length !== 11
    ) {
      this.setCpf(false);
      //alert("CPF inválido");
      return false;
    }

    for (let i = 1; i <= 9; i++) {
      Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    }

    var Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) {
      Resto = 0;
    }

    if (Resto !== parseInt(strCPF.substring(9, 10))) {
      this.setCpf(false);
      //alert("CPF inválido");
      return false;
    }

    Soma = 0;
    for (let k = 1; k <= 10; k++) {
      Soma = Soma + parseInt(strCPF.substring(k - 1, k)) * (12 - k);
    }

    Resto = (Soma * 10) % 11;
    if (Resto === 10 || Resto === 11) {
      Resto = 0;
    }

    if (Resto !== parseInt(strCPF.substring(10, 11))) {
      this.setCpf(false);

      // TODO: DIALOG mensagem de cpf inválido

      //alert("CPF inválido");
      return false;
    }

    this.setCpf(true);
    return true;
  }

  statusInscricaoMudou(event) {
    const inscricaoEstadualHTML = <HTMLInputElement>(
      document.getElementById("inscricaoEstadual")
    );
    this.statusInscricaoEstadual = !this.statusInscricaoEstadual;
    inscricaoEstadualHTML.disabled = !inscricaoEstadualHTML.disabled;
    if (!this.statusInscricaoEstadual) {
      this.empresaForm.get("inscricaoEstadual").clearValidators();
      this.empresaForm.get("inscricaoEstadual").setValue("");
      this.empresaForm.get("inscricaoEstadual").enabled
    } else {
      this.empresaForm
        .get("inscricaoEstadual")
        .setValidators([Validators.required]);
    }

  }

  setCpf(status) {
    this.cpfStatus = status;
  }

  setUsuarioId(id) {
    this.usuarioId = id;
  }
  setAdmin() {
    this.admin = true;
  }
}
