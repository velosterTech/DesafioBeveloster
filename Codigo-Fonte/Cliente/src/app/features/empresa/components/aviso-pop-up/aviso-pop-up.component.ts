import { Component } from '@angular/core';
import { MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-aviso-pop-up',
  templateUrl: './aviso-pop-up.component.html',
  styleUrls: ['./aviso-pop-up.component.scss']
})
export class AvisoPopUpComponent {

  constructor(
    public dialogRef: MatDialogRef<AvisoPopUpComponent>) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
