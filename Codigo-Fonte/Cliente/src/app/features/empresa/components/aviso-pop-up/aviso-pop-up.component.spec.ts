import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvisoPopUpComponent } from './aviso-pop-up.component';

describe('AvisoPopUpComponent', () => {
  let component: AvisoPopUpComponent;
  let fixture: ComponentFixture<AvisoPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvisoPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvisoPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
