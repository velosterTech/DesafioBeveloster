import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { EMPTY } from "rxjs";
import {
  debounceTime,
  distinctUntilChanged,
  map,
  switchMap,
  take,
  tap,
} from "rxjs/operators";
import { AvisoServiceService } from "../../../../shared/aviso-service.service";
import { AvisoConfirmacaoModalComponent } from "../../../../shared/components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { ConfirmacaoPopUpComponent } from "../../../../shared/components/confirmacao-pop-up/confirmacao-pop-up.component";
import { UsuarioService } from "../../../usuario/usuario.service";
import { EmpresaService } from "../../empresa.service";

@Component({
  selector: "app-listar-empresas",
  templateUrl: "./listar-empresas.component.html",
  styleUrls: ["./listar-empresas.component.scss"],
})
export class ListarEmpresasComponent implements OnInit {
  campoPesquisa = new FormControl();
  empresas: any = [];

  retorno_empresa = [];

  totalItems: number;
  currentPage: number = 1;
  smallnumPages: number = 0;

  maxSize: number = 5;
  bigTotalItems: number = 675;
  bigCurrentPage: number = 1;
  numPages: number = 0;

  buscarTodos = "";
  currentPager: number = 1;
  admin: boolean = false;
  admin_escritorio: boolean = false;
  admin_empresa: boolean = false;

  usuarioid;

  constructor(
    private rest: EmpresaService,
    private usuarioService: UsuarioService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private avisoService: AvisoServiceService,
    private modalService: BsModalService
  ) {}

  ngOnInit(): void {
    this.empresas = this.route.snapshot.data.empresas;
    console.log(this.empresas);
    if (this.empresas === undefined) {
      this.receberEmpresas(this.buscarTodos);
    } else {
      this.receberEmpresas(this.empresas);
    }

    const perfis = JSON.parse(localStorage.getItem("user_role"));
    perfis.forEach((p) => {
      if (p.nome === "ROLE_ADMIN") {
        this.setAdmin();
      }
    });

    if (!this.admin) {
      perfis.forEach((p) => {
        if (p.nome === "ROLE_ADMIN_ESCRITORIO") {
          this.setAdminEscritorio();
        } else if (p.nome === "ROLE_ADMIN_EMPRESA") {
          this.setAdminEmpresa();
        }
      });
    }

    this.campoPesquisa.valueChanges
      .pipe(
        map((value) => value.trim()),
        debounceTime(150),
        distinctUntilChanged(),
        tap((value) => console.log(value)),
        switchMap((value: string) => {
          let usuarioL = JSON.parse(localStorage.getItem("usuarioL"));
          return this.rest.receberEmpresas(value, usuarioL.id, false);
        })
      )
      .subscribe((data) => {
        this.empresas = data;
        this.setTotalItems(data.length);
        this.retorno_empresa = data.slice(0, 5);
      });
  }

  pageChanged(event: PageChangedEvent) {
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;
    this.retorno_empresa = this.empresas.slice(startItem, endItem);
  }

  receberEmpresas(data) {
    if (data === "") {
      const usuario = JSON.parse(localStorage.getItem("usuario"));
      console.log(usuario);
      this.usuarioService.getByLogin(usuario.login)
      .pipe(
          tap((u) => console.log(u)),
          switchMap((usuario) => {
            return this.rest.receberEmpresas(data, usuario.id, false);
          })
        )
        .subscribe((empresas) => {
          this.empresas = empresas;
          console.log(this.empresas);
          this.totalItems = this.empresas.length;
          this.retorno_empresa = this.empresas.slice(0, 5);
        });
    } else {
      if (data instanceof Array && data.length === 0) {
        console.log("não há empresas cadastrados");
      }
      this.totalItems = this.empresas?.length;
      this.retorno_empresa = this.empresas.slice(0, 5);
    }
  }

  setTotalItems(lenght: number) {
    this.totalItems = lenght;
  }
  openDialog(empresa, id): void {
    const dialogRef = this.dialog.open(ConfirmacaoPopUpComponent, {
      width: "500px",
      height: "200px",
      data: { nome: empresa.razaoSocial, status: empresa.status },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result ?? 1) {
        this.rest.statusEmpresa(id).subscribe(
          (success) => {
            console.log("sucesso");
            this.receberEmpresas("");
          },
          (error) => console.log("erro")
        );
      }
    });
  }

  trocarStatus(empresa) {
    if (empresa.status) {
      const result$ = this.avisoService.showAviso(
        "Confirmação",
        "Deseja desativar " + empresa.razaoSocial + "?"
      );
      result$
        .asObservable()
        .pipe(
          take(1),
          switchMap((result) =>
            result ? this.rest.statusEmpresa(empresa.id) : EMPTY
          )
        )
        .subscribe(
          (success) => {
            console.log("sucesso");
            this.receberEmpresas("");
            const bsModalRef: BsModalRef = this.modalService.show(
              AvisoConfirmacaoModalComponent
            );
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Cadastro desativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(
              AvisoConfirmacaoModalComponent
            );
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar cadastro";
          }
        );
    } else {
      const result$ = this.avisoService.showAviso(
        "Confirmação",
        "Deseja ativar " + empresa.razaoSocial + "?"
      );
      result$
        .asObservable()
        .pipe(
          take(1),
          switchMap((result) =>
            result ? this.rest.statusEmpresa(empresa.id) : EMPTY
          )
        )
        .subscribe(
          (success) => {
            console.log("sucesso");
            this.receberEmpresas("");
            const bsModalRef: BsModalRef = this.modalService.show(
              AvisoConfirmacaoModalComponent
            );
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Cadastro ativado!";
          },
          (error) => {
            console.log(error);
            const bsModalRef: BsModalRef = this.modalService.show(
              AvisoConfirmacaoModalComponent
            );
            bsModalRef.content.title = "Aviso!";
            bsModalRef.content.msg = "Erro ao desativar cadastro";
          }
        );
    }
    this.currentPage = 1;
  }

  setAdmin() {
    this.admin = true;
  }

  setAdminEscritorio() {
    this.admin_escritorio = true;
  }

  setAdminEmpresa() {
    this.admin_empresa = true;
  }
}
