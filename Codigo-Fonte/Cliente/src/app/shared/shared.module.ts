import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { PaginationModule } from "ngx-bootstrap/pagination";
import { ListarEmpresasComponent } from "../features/empresa/components/listar-empresas/listar-empresas.component";
import { ListarEscritorioContabilComponent } from "../features/escritorio-contabil/components/listar-escritorio-contabil/listar-escritorio-contabil.component";
import { ListarUsuariosComponent } from "../features/usuario/components/listar-usuarios/listar-usuarios.component";
import { AvisoConfirmacaoModalComponent } from "./components/aviso-confirmacao-modal/aviso-confirmacao-modal.component";
import { AvisoStatusModalComponent } from "./components/aviso-status-modal/aviso-status-modal.component";
import { ConfirmacaoPopUpComponent } from "./components/confirmacao-pop-up/confirmacao-pop-up.component";
import { FormDebugComponent } from "./components/form-debug/form-debug.component";

@NgModule({
  declarations: [
    ConfirmacaoPopUpComponent,
    FormDebugComponent,
    ListarEmpresasComponent,
    ListarUsuariosComponent,
    ListarEscritorioContabilComponent,
    AvisoConfirmacaoModalComponent,
    AvisoStatusModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    RouterModule,
  ],
  exports: [
    ConfirmacaoPopUpComponent,
    FormDebugComponent,
    ListarEmpresasComponent,
    ListarUsuariosComponent,
    ListarEscritorioContabilComponent,

  ],
  entryComponents: [ConfirmacaoPopUpComponent, AvisoConfirmacaoModalComponent],
})
export class SharedModule {}
