import { Injectable } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AvisoStatusModalComponent } from './components/aviso-status-modal/aviso-status-modal.component';

@Injectable({
  providedIn: 'root'
})
export class AvisoServiceService {

  constructor(private modalService: BsModalService) { }


  showAviso(title: string, msg: string){
    const bsModalRef: BsModalRef = this.modalService.show(AvisoStatusModalComponent);
    bsModalRef.content.title = title;
    bsModalRef.content.msg = msg;
    return (<AvisoStatusModalComponent>bsModalRef.content).confirmResult;
  }
}
