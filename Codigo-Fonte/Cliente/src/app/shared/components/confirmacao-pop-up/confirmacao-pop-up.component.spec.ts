import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmacaoPopUpComponent } from './confirmacao-pop-up.component';

describe('ConfirmacaoPopUpComponent', () => {
  let component: ConfirmacaoPopUpComponent;
  let fixture: ComponentFixture<ConfirmacaoPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmacaoPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmacaoPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
