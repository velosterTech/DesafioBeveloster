import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData{
  nome: string;
  status: boolean;
}

@Component({
  selector: 'app-confirmacao-pop-up',
  templateUrl: './confirmacao-pop-up.component.html',
  styleUrls: ['./confirmacao-pop-up.component.scss']
})
export class ConfirmacaoPopUpComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmacaoPopUpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {
      dialogRef.disableClose = true;
      //dialogRef.beforeClosed().subscribe(
        //result => dialogRef.close(0));
    }

  ngOnInit(): void {
  }
  onNoClick(){
    this.dialogRef.close(0);
  }
  onClick(){
    this.dialogRef.close(1);
  }

}
