import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/internal/Subject';

@Component({
  selector: 'app-aviso-status-modal',
  templateUrl: './aviso-status-modal.component.html',
  styleUrls: ['./aviso-status-modal.component.scss']
})
export class AvisoStatusModalComponent implements OnInit {
  @Input() title: string;
  @Input() msg: string;
  
  confirmResult: Subject<boolean>;
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
    this.confirmResult = new Subject();
  }

  confirm(){
    this.confirmResult.next(true);
    this.bsModalRef.hide();
  }
  close(){
    this.confirmResult.next(false);
    this.bsModalRef.hide();
  }
}
