import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvisoStatusModalComponent } from './aviso-status-modal.component';

describe('AvisoStatusModalComponent', () => {
  let component: AvisoStatusModalComponent;
  let fixture: ComponentFixture<AvisoStatusModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvisoStatusModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvisoStatusModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
