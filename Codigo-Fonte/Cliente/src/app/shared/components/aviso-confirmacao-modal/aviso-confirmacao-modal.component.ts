import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-aviso-confirmacao-modal',
  templateUrl: './aviso-confirmacao-modal.component.html',
  styleUrls: ['./aviso-confirmacao-modal.component.scss']
})
export class AvisoConfirmacaoModalComponent implements OnInit {
  @Input() title: string;
  @Input() msg: string;
  
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit(): void {
  }

  close(){
    this.bsModalRef.hide();
  }
}
