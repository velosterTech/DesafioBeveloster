import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvisoConfirmacaoModalComponent } from './aviso-confirmacao-modal.component';

describe('AvisoConfirmacaoModalComponent', () => {
  let component: AvisoConfirmacaoModalComponent;
  let fixture: ComponentFixture<AvisoConfirmacaoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvisoConfirmacaoModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvisoConfirmacaoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
