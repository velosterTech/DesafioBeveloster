import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-aviso-pop-up2',
  templateUrl: './aviso-pop-up2.component.html',
  styleUrls: ['./aviso-pop-up2.component.scss']
})
export class AvisoPopUp2Component {



  // @ViewChild('template', { static: true }) campoTemplate: TemplateRef<HTMLTemplateElement>;

  // bsModalRef: BsModalRef;

  // constructor(private modalService: BsModalService) { }

  // openModal(mensagem){
  //   this.mensagem = mensagem;
  //   this.bsModalRef = this.modalService.show(this.campoTemplate)
  // }

  // ngOnInit(): void {
  // }
    @Input() mensagem: string = "";
    @ViewChild('childModal', { static: false }) childModal?: ModalDirective;

    showChildModal(mensagem): void {
      this.mensagem = mensagem;
      console.log("teste")
      this.childModal?.show();
    }

    hideChildModal(): void {
      this.childModal?.hide();
    }


}
