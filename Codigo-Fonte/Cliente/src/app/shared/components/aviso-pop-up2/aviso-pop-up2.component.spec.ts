import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvisoPopUp2Component } from './aviso-pop-up2.component';

describe('AvisoPopUp2Component', () => {
  let component: AvisoPopUp2Component;
  let fixture: ComponentFixture<AvisoPopUp2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvisoPopUp2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvisoPopUp2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
