import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppAsideModule, AppHeaderModule, AppSidebarModule } from '@coreui/angular';
import { NgIdleModule, Idle } from '@ng-idle/core';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxMaskModule } from 'ngx-mask';
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './core/components/dashboard/dashboard.component';
import { LayoutComponent } from './core/components/layout/layout.component';
import { LoginComponent } from './core/components/login/login.component';
import { AutenticacaoGuard } from './core/guards/autenticacao.guard';
import { HttpInterceptorModule } from './core/interceptors/header-interceptor';
import { LoginService } from './core/services/login.service';
import { DepartamentoModule } from './features/departamento/departamento.module';
import { DocumentoModule } from './features/documento/documento.module';
import { EmpresaGuard } from './features/empresa/guards/empresa.guard';
import { EscritorioGuard } from './features/escritorio-contabil/guards/escritorio.guard';
import { UsuarioGuard } from './features/usuario/guards/usuario.guard';
import { AvisoPopUp2Component } from './shared/components/aviso-pop-up2/aviso-pop-up2.component';
import { SharedModule } from './shared/shared.module';
import { ListarDocumentosComponent } from './features/documento/components/listar-documentos/listar-documentos.component';



const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LayoutComponent,
    DashboardComponent,
    ListarDocumentosComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpInterceptorModule,
    BrowserAnimationsModule,
    AppHeaderModule,
    AppSidebarModule,
    AppAsideModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    ReactiveFormsModule,
    PaginationModule.forRoot(),
    NgxMaskModule.forRoot(),
    MatDialogModule,
    ModalModule.forRoot(),
    SharedModule,
    AppRoutingModule,
    DocumentoModule,
    DepartamentoModule,
    NgIdleModule.forRoot(),
  ],
  providers: [
    AvisoPopUp2Component,
    AutenticacaoGuard,
    LoginService,
    EmpresaGuard,
    EscritorioGuard,
    UsuarioGuard,
    Idle
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
