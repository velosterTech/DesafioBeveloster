import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./core/components/dashboard/dashboard.component";
import { LayoutComponent } from "./core/components/layout/layout.component";
import { LoginComponent } from "./core/components/login/login.component";
import { AutenticacaoGuard } from "./core/guards/autenticacao.guard";
import { EnviarDocumentoComponent } from "./features/documento/components/enviar-documento/enviar-documento.component";
import { ListarDocumentosComponent } from "./features/documento/components/listar-documentos/listar-documentos.component";
import { RequisitarDocumentosComponent } from "./features/documento/components/requisitar-documentos/requisitar-documentos.component";
import { DocumentoResolverService } from "./features/documento/resolvers/documento-resolver.service";
import { EmpresaGuard } from "./features/empresa/guards/empresa.guard";
import { ListarEmpresaResolverService } from "./features/empresa/resolvers/listar-empresa-resolver.service";
import { EscritorioGuard } from "./features/escritorio-contabil/guards/escritorio.guard";
import { EscritorioResolver } from "./features/escritorio-contabil/resolvers/escritorio-resolver.service";
import { ListarMensagemResolverService } from "./features/mensagem/resolvers/listar-mensagem-resolver.service";
import { UsuarioGuard } from "./features/usuario/guards/usuario.guard";
import { ListarUsuariosResolverService } from "./features/usuario/resolvers/listar-usuarios-resolver.service";


const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "",
    canActivate: [AutenticacaoGuard],
    children: [
      {
        path: "",
        component: LayoutComponent,
        children: [
          {
            path: "dashboard",
            component: DashboardComponent,
          },
          {
            path: "escritorios",
            canActivate: [EscritorioGuard],
            canLoad: [EscritorioGuard],
            loadChildren: () =>
              import(
                "./features/escritorio-contabil/escritorio-contabil.module"
              ).then((md) => md.EscritorioContabilModule),
            resolve: { escritorios: EscritorioResolver },
          },
          {
            path: "usuarios",
            canActivate: [UsuarioGuard],
            canLoad: [UsuarioGuard],
            loadChildren: () =>
              import("./features/usuario/usuario.module").then(
                (md) => md.UsuarioModule
              ),
            resolve: { usuarios: ListarUsuariosResolverService }
          },
          {
            path: "empresas",
            canActivate: [EmpresaGuard],
            canLoad: [EmpresaGuard],
            loadChildren: () =>
              import("./features/empresa/empresa.module").then(
                (md) => md.EmpresaModule
              ),
            resolve: { empresas: ListarEmpresaResolverService },
          },
          // {
          //   path: "mensagens",
          //   loadChildren: () =>
          //     import("./features/mensagem/mensagem.module").then(
          //       (md) => md.MensagemModule
          //     ),
          //   resolve: { mensagens: ListarMensagemResolverService },
          // },
          // {
          //   path: "tipo-documentos",
          //   loadChildren: () =>
          //     import("./features/tipo-documentos/tipo-documentos.module").then(
          //       (md) => md.TipoDocumentosModule
          //     ),
          // },
           {
            path: "departamentos",
            loadChildren: () =>
              import("./features/departamento/departamento.module").then(
                (md) => md.DepartamentoModule
              ),
           },
           {
            path: "documentos",
            loadChildren: () =>
              import("./features/documento/documento.module").then(
                (md) => md.DocumentoModule
              ),
           },
          // {
          //   path: "requisitar-documentos",
          //   children: [
          //     {
          //       path: "",
          //       component: RequisitarDocumentosComponent,
          //       resolve: { documentoRequisitado: DocumentoResolverService },
          //     },
          //   ],
          // },
          // {
          //   path: "enviar-documentos",
          //   children: [
          //     {
          //       path: "",
          //       component: EnviarDocumentoComponent,
          //     },
          //   ],
          // },
          {
            path: "",
            redirectTo: "/dashboard",
            pathMatch: "full"
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
