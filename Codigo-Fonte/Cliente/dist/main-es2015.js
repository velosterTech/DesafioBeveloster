(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+6xE":
/*!****************************************!*\
  !*** ./src/app/service/cep.service.ts ***!
  \****************************************/
/*! exports provided: CepService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CepService", function() { return CepService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");



let CepService = class CepService {
    constructor(http) {
        this.http = http;
    }
    consulta(cep) {
        cep = cep.replace(/\D/g, '');
        if (cep !== '') {
            const validaCep = /^[0-9]{8}$/;
            if (validaCep.test(cep)) {
                return this.http.get('/cep/' + cep + '/json');
            }
        }
    }
};
CepService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
CepService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], CepService);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Projetos\Beveloster2\DesafioBeveloster\Codigo-Fonte\Cliente\src\main.ts */"zUnb");


/***/ }),

/***/ "02KH":
/*!*********************************************!*\
  !*** ./src/app/service/mensagem.service.ts ***!
  \*********************************************/
/*! exports provided: MensagemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensagemService", function() { return MensagemService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app-constants */ "EKKv");




let MensagemService = class MensagemService {
    constructor(http) {
        this.http = http;
    }
    novaMensagem(id, mensagem) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlMensagem + "nova/" + id, mensagem);
    }
    receberMensagem(id) {
        console.log(id);
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlMensagem + "todas/" + id);
    }
    atualizaStatus(mensagem) {
        return this.http.put(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlMensagem + "atualizaStatus/" + mensagem.id, mensagem);
    }
};
MensagemService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
MensagemService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], MensagemService);



/***/ }),

/***/ "0LyG":
/*!********************************************************************!*\
  !*** ./src/app/cadastrar-empresa/cadastrar-empresa.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItZW1wcmVzYS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "14ci":
/*!***********************************!*\
  !*** ./src/app/model/Endereco.ts ***!
  \***********************************/
/*! exports provided: Endereco */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Endereco", function() { return Endereco; });
class Endereco {
}


/***/ }),

/***/ "2nF6":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/listar-mensagens/listar-mensagens.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"animated fadeIn mt-4\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Mensagens\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" routerLink=\"/mensagem\">\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>Remetente</th>\r\n                <th>Destinatário</th>\r\n                <th>Mensagem</th>\r\n                <th style=\"text-align: right;\">Lida</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let mensagem of retornoMensagens; let i = index\">\r\n                <th scope=\"row\">{{i + 1}}</th>\r\n                <td>{{mensagem.usuarioRemetente.nome}}</td>\r\n                <td>{{mensagem.usuarioDestino.nome}}</td>\r\n                <td> {{mensagem.txtMensagem}} </td>\r\n                <th style=\"text-align: right;\"> <input class=\"form-check-input\" type=\"checkbox\" value=\"true\" id=\"defaultCheck1\" [disabled]=\"(mensagem.status) == true\" [checked]=\"(mensagem.status) == true\" (change)=\"checkboxAtt(mensagem)\" > </th>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n            <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</div>\r\n");

/***/ }),

/***/ "3DPm":
/*!**********************************************!*\
  !*** ./src/app/categoria/categoria.model.ts ***!
  \**********************************************/
/*! exports provided: Categoria, getRoleIdentifier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Categoria", function() { return Categoria; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRoleIdentifier", function() { return getRoleIdentifier; });
class Categoria {
    constructor(id, nomeCategoria) {
        this.id = id;
        this.nomeCategoria = nomeCategoria;
    }
}
function getRoleIdentifier(categoria) {
    return categoria.id;
}


/***/ }),

/***/ "3QCp":
/*!**********************************************!*\
  !*** ./src/app/service/candidato.service.ts ***!
  \**********************************************/
/*! exports provided: CandidatoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CandidatoService", function() { return CandidatoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app-constants */ "EKKv");




let CandidatoService = class CandidatoService {
    constructor(http) {
        this.http = http;
    }
    getImcList() {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCandidato + 'imc');
    }
    getObesosList() {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCandidato + 'obesos');
    }
    getMediaIdadeList() {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCandidato + 'media-idade-tipo-sangue');
    }
    getQuantidadeDoadoresList() {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCandidato + 'quantidade-doadores');
    }
    getPorEstado() {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCandidato + 'por-estado');
    }
    salva(ar) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCandidato + 'salva-candidatos', ar);
    }
};
CandidatoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CandidatoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CandidatoService);



/***/ }),

/***/ "4a6u":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/mensagem/mensagem.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"animated fadeIn\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-md-8 align-items\">\r\n      <div class=\"card mt-4\">\r\n        <div class=\"card-header\">\r\n          <i class=\"icon-envelope\"></i><strong>Mensagem</strong>\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <form [formGroup]=\"msgForm\" class=\"form-horizontal\">\r\n            <div class=\"form-group row\">\r\n              <label class=\"col-md-3 col-form-label\">Remetente</label>\r\n            </div>\r\n            <hr>\r\n            <div class=\"form-group row\">\r\n              <label class=\"col-md-2 col-form-label\" for=\"text-input\">Destinatário</label>\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\" formControlName=\"destinatario\">\r\n                    <option value='' disabled selected>Empresa</option>\r\n                    <option *ngFor=\"let empresa of empresas\" [ngValue]=\"empresa.responsavel.id\"> {{empresa.pjId.razao_social}} </option>\r\n                    <option *ngFor=\"let escritorio of escritorios\" [ngValue]=\"escritorio.responsavel.id\"> {{escritorio.razaoSocial}} </option>\r\n                  </select>\r\n                </div>\r\n              \r\n              <!--div class=\"col-md-10\">\r\n                <input type=\"text\" id=\"text-input\" name=\"text-input\" class=\"form-control\"\r\n                placeholder=\"Destinatário\">\r\n                <span class=\"help-block\"></span>\r\n              </div-->\r\n            </div>\r\n            <div class=\"form-group row\">\r\n              <div class=\"col-md-12\">\r\n                <textarea id=\"textarea-input\" name=\"textarea-input\" rows=\"9\" class=\"form-control\"\r\n                placeholder=\"Digite aqui a mensagem\" formControlName=\"mensagem\"></textarea>\r\n              </div>\r\n            </div>\r\n            <div class=\"card-footer\">\r\n              <button type=\"button\" class=\"btn btn-sm btn-primary m-1\" (click)=\"enviar(msgForm)\"><i class=\"fa fa-send\" ></i> Enviar</button>\r\n              <button type=\"button\" class=\"btn btn-sm btn-danger\" (click)=\"voltar()\"><i class=\"fa fa-ban\"></i> Cancelar</button>\r\n            </div>\r\n          </form>\r\n        </div>\r\n      \r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "53o5":
/*!*******************************************************!*\
  !*** ./src/app/home-doador/home-doador.component.css ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card {\r\n    padding: 5%;\r\n}\r\n\r\ntable,\r\nth,\r\ntd,\r\ntr {\r\n    border: 1px solid #696969;\r\n    font-weight: bolder;\r\n    color: black;\r\n    padding-left: 10px;\r\n    padding-right: 10px;\r\n}\r\n\r\ntr:nth-child(2n+2) {\r\n    background: #ccc;\r\n}\r\n\r\ntd,\r\nth,\r\ntr {\r\n    text-align: center;\r\n    vertical-align: middle;\r\n}\r\n\r\nh3 {\r\n    font-weight: 100;\r\n    font-family: cursive;\r\n    color: #FF4500\r\n}\r\n\r\n.verde {\r\n    background-color: green;\r\n}\r\n\r\n.amarelo {\r\n    background-color: yellow;\r\n}\r\n\r\n.vermelho {\r\n    background-color: red;\r\n}\r\n\r\n.roxo {\r\n    background-color: purple;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUtZG9hZG9yLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0FBQ2Y7O0FBRUE7Ozs7SUFJSSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBOzs7SUFHSSxrQkFBa0I7SUFDbEIsc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQjtBQUNKOztBQUVBO0lBQ0ksdUJBQXVCO0FBQzNCOztBQUVBO0lBQ0ksd0JBQXdCO0FBQzVCOztBQUVBO0lBQ0kscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksd0JBQXdCO0FBQzVCIiwiZmlsZSI6ImhvbWUtZG9hZG9yLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FyZCB7XHJcbiAgICBwYWRkaW5nOiA1JTtcclxufVxyXG5cclxudGFibGUsXHJcbnRoLFxyXG50ZCxcclxudHIge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzY5Njk2OTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG50cjpudGgtY2hpbGQoMm4rMikge1xyXG4gICAgYmFja2dyb3VuZDogI2NjYztcclxufVxyXG5cclxudGQsXHJcbnRoLFxyXG50ciB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcblxyXG5oMyB7XHJcbiAgICBmb250LXdlaWdodDogMTAwO1xyXG4gICAgZm9udC1mYW1pbHk6IGN1cnNpdmU7XHJcbiAgICBjb2xvcjogI0ZGNDUwMFxyXG59XHJcblxyXG4udmVyZGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogZ3JlZW47XHJcbn1cclxuXHJcbi5hbWFyZWxvIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcclxufVxyXG5cclxuLnZlcm1lbGhvIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDtcclxufVxyXG5cclxuLnJveG8ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcHVycGxlO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "5H8X":
/*!******************************************************************!*\
  !*** ./src/app/cadastrar-empresa/cadastrar-empresa.component.ts ***!
  \******************************************************************/
/*! exports provided: CadastrarEmpresaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarEmpresaComponent", function() { return CadastrarEmpresaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_empresa_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-empresa.component.html */ "Ie/P");
/* harmony import */ var _cadastrar_empresa_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-empresa.component.scss */ "0LyG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _aviso_pop_up_aviso_pop_up_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../aviso-pop-up/aviso-pop-up.component */ "zb5I");
/* harmony import */ var _enum_NaturezaJuridica__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../enum/NaturezaJuridica */ "Pg0y");
/* harmony import */ var _enum_RegimeTributario__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../enum/RegimeTributario */ "P/kj");
/* harmony import */ var _modal_aviso_modal_aviso_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../modal-aviso/modal-aviso.component */ "KGs1");
/* harmony import */ var _model_Empresa__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../model/Empresa */ "ovI4");
/* harmony import */ var _model_Endereco__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../model/Endereco */ "14ci");
/* harmony import */ var _service_empresa_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../service/empresa.service */ "pFYR");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../service/escritorio-contabil.service */ "wjmZ");



















let CadastrarEmpresaComponent = class CadastrarEmpresaComponent {
    constructor(fb, rest, router, route, escritorioService, http, dialog, modalService) {
        this.fb = fb;
        this.rest = rest;
        this.router = router;
        this.route = route;
        this.escritorioService = escritorioService;
        this.http = http;
        this.dialog = dialog;
        this.modalService = modalService;
        this.editando = false;
        this.escritorios = [];
        this.natureza = [];
        this.regime = [];
        this.regimeTributario = _enum_RegimeTributario__WEBPACK_IMPORTED_MODULE_13__["RegimeTributario"];
        this.naturezaJuridica = _enum_NaturezaJuridica__WEBPACK_IMPORTED_MODULE_12__["NaturezaJuridica"];
        this.empresa = new _model_Empresa__WEBPACK_IMPORTED_MODULE_15__["Empresa"]();
        this.endereco = new _model_Endereco__WEBPACK_IMPORTED_MODULE_16__["Endereco"]();
        this.natureza = Object.keys(this.naturezaJuridica);
        this.regime = Object.keys(this.regimeTributario);
    }
    ngOnInit() {
        this.route.params
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["map"])((params) => params["id"]), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["switchMap"])((id) => {
            if (id !== undefined) {
                return this.rest.getById(id);
            }
            else {
                return new rxjs__WEBPACK_IMPORTED_MODULE_9__["Observable"]();
            }
        }))
            .subscribe((empresa) => {
            this.updateForm(empresa);
            this.setEditando();
        });
        this.receberEscritorios("");
        this.empresaForm = this.fb.group({
            id: [null],
            escritorio: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            cnpj: [
                "",
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(14),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(14),
                ],
            ],
            razao_social: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            inscricao_estadual: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            endereco: this.fb.group({
                rua: [""],
                numero: [""],
                bairro: [""],
                cidade: [""],
                estado: [""],
                pais: [""],
                cep: [
                    "",
                    [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(8),
                        _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(8),
                    ],
                ],
                complemento: [""],
            }),
            nome_fantasia: [],
            nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            cpf: [
                "",
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(11),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(11),
                ],
            ],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].email]],
            telefone: [
                "",
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(10),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(10),
                ],
            ],
            celular: [
                "",
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(11),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].maxLength(11),
                ],
            ],
            ativ_primar_cnae_: [],
            descricao_primar: [],
            ativ_secund_cnae_: [],
            descricao_secund: [],
            regime_tributario: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            natureza_juridica: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
            emailS: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].email]],
            termo_condicoes: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required]],
        });
        this.url = window.location.pathname;
    }
    cadastrar() {
        this.empresa = this.createFromForm();
        console.log(this.empresaForm);
        this.rest.salvarEmpresa(this.empresa).subscribe((success) => {
            console.log(success);
            //this.openAvisoDialog();
            //this.openModal();
            this.router.navigate(['dashboard']);
        }, (error) => {
            this.showInfo(error);
        });
    }
    showInfo(success) {
        console.log(success);
    }
    setEditando() {
        this.editando = true;
    }
    updateForm(empresa) {
        console.log(empresa);
        this.empresaForm.patchValue({
            id: empresa.id,
            escritorio: empresa.escritorioId.razaoSocial,
            cnpj: empresa.pjId.cnpj,
            razao_social: empresa.pjId.razao_social,
            nome_fantasia: "",
            nome: empresa.pjId.nome,
            cpf: empresa.responsavel.cpf,
            endereco: empresa.pjId.endereco,
            email: empresa.pjId.email,
            telefone: empresa.pjId.celular,
            celular: empresa.pjId.celular,
            ativ_primar_cnae_: "",
            descricao_primar: "",
            ativ_secund_cnae_: "",
            descricao_secund: "",
            inscricao_estadual: empresa.pjId.inscricao_estadual,
            regime_tributario: empresa.pjId.regimeTributario,
            natureza_juridica: empresa.pjId.naturezaJuridica,
            termo_condicoes: true,
        });
    }
    editar() {
        this.empresa = this.createFromForm();
        console.log(this.empresa);
        this.rest.editarEmpresa(this.empresa).subscribe((success) => {
            console.log("Deu certo");
            this.previousState();
        }, (error) => {
            console.log("Falhou");
            console.log(this.empresa);
            console.log(error);
        });
    }
    cnpjKeyPressEvent(event) {
        if (!(this.empresaForm.get("cnpj").value === "")) {
            let cnpj = event.target.value;
            cnpj = cnpj
                .replace(".", "")
                .replace(".", "")
                .replace("/", "")
                .replace("-", "");
            this.http.get("/wk/pessoajuridica/cnpj/" + cnpj).subscribe((success) => {
                console.log(success);
                /* Quando o banco corrigir os campo para Unique
                if (success !== null){
                  this.empresaForm.get("cnpj").setErrors({ duplicado: true });
                }
                */
            }, (error) => {
                console.log(error);
                this.empresaForm.get("cnpj").setErrors({ duplicado: true });
            });
            this.http.get("/receita" + cnpj).subscribe((success) => {
                this.onSuccess(success);
            }, (error) => {
                this.onError(error);
            });
        }
    }
    cpfKeyPressEvent(event) {
        if (!(this.empresaForm.get("cpf").value === "")) {
            let cpf = event.target.value;
            cpf = cpf.replace(".", "").replace(".", "").replace("-", "");
            this.http.get("/wk/usuario/cpf/" + cpf).subscribe((success) => {
                console.log(success);
                /* Quando o banco corrigir os campo para Unique
                if (success !== null){
                  this.empresaForm.get("cpf").setErrors({ duplicado: true });
                }
                */
            }, (error) => {
                console.log(error);
                this.empresaForm.get("cpf").setErrors({ duplicado: true });
            });
        }
    }
    inscricaoKeyPressEvent(event) {
        if (!(this.empresaForm.get("inscricao_estadual").value === "")) {
            let inscricao = event.target.value;
            this.http
                .get("/wk/pessoajuridica/inscricao-estadual/" + inscricao)
                .subscribe((success) => {
                console.log(success);
                /* Quando o banco corrigir os campo para Unique
              if (success !== null){
                this.empresaForm.get("inscricao_estadual").setErrors({ duplicado: true });
              }
              */
            }, (error) => {
                console.log(error);
                this.empresaForm
                    .get("inscricao_estadual")
                    .setErrors({ duplicado: true });
            });
        }
    }
    previousState() {
        window.history.back();
    }
    onSuccess(response) {
        const responseJson = JSON.parse(JSON.stringify(response));
        this.empresaForm.get(["razao_social"]).setValue(responseJson.nome);
        this.empresaForm.get(["nome_fantasia"]).setValue(responseJson.fantasia);
        this.empresaForm.get(["endereco"]).get(["cep"]).setValue(responseJson.cep);
        this.empresaForm
            .get(["endereco"])
            .get(["rua"])
            .setValue(responseJson.logradouro);
        this.empresaForm
            .get(["endereco"])
            .get(["bairro"])
            .setValue(responseJson.bairro);
        this.empresaForm
            .get(["endereco"])
            .get(["numero"])
            .setValue(responseJson.numero);
        this.empresaForm
            .get(["endereco"])
            .get(["complemento"])
            .setValue(responseJson.complemento);
        // this.empresaForm.get(['endereco'])!.setValue(responseJson.logradouro + ', ' + responseJson.bairro + ', '
        // + responseJson.municipio + ' - ' + responseJson.uf + ' (' + responseJson.cep + ') ');
        this.empresaForm
            .get(["ativ_primar_cnae_"])
            .setValue(responseJson.atividade_principal[0].code);
        this.empresaForm
            .get(["descricao_primar"])
            .setValue(responseJson.atividade_principal[0].text);
        this.empresaForm
            .get(["ativ_secund_cnae_"])
            .setValue(responseJson.atividades_secundarias[0].code);
        this.empresaForm
            .get(["descricao_secund"])
            .setValue(responseJson.atividades_secundarias[0].text);
    }
    onError(response) {
        const responseJson = JSON.parse(JSON.stringify(response));
        alert("Erro ao consultar o CNPJ. Detalhes: " + responseJson.statusText);
    }
    onFinalize() {
        //finalize request
    }
    receberEscritorios(busca) {
        this.escritorioService.receberEscritorios(busca).subscribe((data) => {
            console.log(data);
            let escritorios = [];
            data.forEach(function (item) {
                escritorios.push(item);
            });
            this.escritorios = escritorios;
        });
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new _model_Empresa__WEBPACK_IMPORTED_MODULE_15__["Empresa"]()), { id: this.empresaForm.get(["id"]).value, escritorio: this.empresaForm.get(["escritorio"]).value, cnpj: this.empresaForm.get(["cnpj"]).value, razao_social: this.empresaForm.get(["razao_social"]).value, nome_fantasia: this.empresaForm.get(["nome_fantasia"]).value, nome: this.empresaForm.get(["nome"]).value, cpf: this.empresaForm.get(["cpf"]).value, endereco: this.empresaForm.get(["endereco"]).value, email: this.empresaForm.get(["email"]).value, telefone: this.empresaForm.get(["telefone"]).value, celular: this.empresaForm.get(["celular"]).value, ativ_primar_cnae_: this.empresaForm.get(["ativ_primar_cnae_"]).value, descricao_primar: this.empresaForm.get(["descricao_primar"]).value, ativ_secund_cnae_: this.empresaForm.get(["ativ_secund_cnae_"]).value, inscricao_estadual: this.empresaForm.get(["inscricao_estadual"]).value, regime_tributario: this.empresaForm.get(["regime_tributario"]).value, natureza_juridica: this.empresaForm.get(["natureza_juridica"]).value });
    }
    openAvisoDialog() {
        console.log("1");
        const dialogRef = this.dialog.open(_aviso_pop_up_aviso_pop_up_component__WEBPACK_IMPORTED_MODULE_11__["AvisoPopUpComponent"], {
            width: "350px",
            data: {},
        });
        console.log("2");
        dialogRef.afterClosed().subscribe((result) => {
            this.previousState();
        });
    }
    openModal() {
        this.bsModalRef = this.modalService.show(_modal_aviso_modal_aviso_component__WEBPACK_IMPORTED_MODULE_14__["ModalAvisoComponent"]);
    }
};
CadastrarEmpresaComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _service_empresa_service__WEBPACK_IMPORTED_MODULE_17__["EmpresaService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_18__["EscritorioContabilService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"] },
    { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"] }
];
CadastrarEmpresaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: "app-cadastrar-empresa",
        template: _raw_loader_cadastrar_empresa_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_empresa_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
        _service_empresa_service__WEBPACK_IMPORTED_MODULE_17__["EmpresaService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
        _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_18__["EscritorioContabilService"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialog"],
        ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["BsModalService"]])
], CadastrarEmpresaComponent);



/***/ }),

/***/ "5hMc":
/*!****************************************************************!*\
  !*** ./src/app/listar-empresas/listar-empresas.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0YXItZW1wcmVzYXMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "67tn":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/form-debug/form-debug.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div style=\"margin-top: 20px\" *ngIf=\"form\">\r\n  <div>\r\n    Detalhes do Form\r\n  </div>\r\n  <pre>\r\n    Form válido: {{ form.valid }}\r\n  </pre>\r\n  <pre>\r\n    Form submetido: {{ form.submitted }}\r\n  </pre>\r\n  <pre>\r\n    Valores: <br> {{ form.value | json }}\r\n  </pre>\r\n</div>\r\n\r\n");

/***/ }),

/***/ "7glZ":
/*!**************************************************************************************************!*\
  !*** ./src/app/cadastrar-colaborador-escritorio/cadastrar-colaborador-escritorio.component.scss ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItY29sYWJvcmFkb3ItZXNjcml0b3Jpby5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "9AxR":
/*!******************************************************************!*\
  !*** ./src/app/listar-mensagens/listar-mensagens.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0YXItbWVuc2FnZW5zLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "9BWX":
/*!**************************************************!*\
  !*** ./src/app/mensagem/mensagem.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("textarea {\n  resize: none;\n}\n\ntextarea::-webkit-scrollbar {\n  width: 4px;\n}\n\ntextarea::-webkit-scrollbar-track {\n  background: #e4e4e4;\n  /* color of the tracking area */\n}\n\ntextarea::-webkit-scrollbar-thumb {\n  background-color: silver;\n  /* color of the scroll thumb */\n  border-radius: 20px;\n  /* roundness of the scroll thumb */\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXG1lbnNhZ2VtLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtBQUNKOztBQUVBO0VBQ0csbUJBQUE7RUFBdUMsK0JBQUE7QUFFMUM7O0FBQ0E7RUFDSSx3QkFBQTtFQUF5Qyw4QkFBQTtFQUN6QyxtQkFBQTtFQUEyQixrQ0FBQTtBQUkvQiIsImZpbGUiOiJtZW5zYWdlbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRleHRhcmVhIHtcclxuICAgIHJlc2l6ZTogbm9uZTtcclxufVxyXG5cclxudGV4dGFyZWE6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgIHdpZHRoOiA0cHg7XHJcbn1cclxuXHJcbnRleHRhcmVhOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgIGJhY2tncm91bmQ6IHJnYigyMjgsIDIyOCwgMjI4KTsgICAgICAgIC8qIGNvbG9yIG9mIHRoZSB0cmFja2luZyBhcmVhICovXHJcbn1cclxuICBcclxudGV4dGFyZWE6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxOTIsIDE5MiwgMTkyKTsgICAgLyogY29sb3Igb2YgdGhlIHNjcm9sbCB0aHVtYiAqL1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDsgICAgICAgLyogcm91bmRuZXNzIG9mIHRoZSBzY3JvbGwgdGh1bWIgKi9cclxufSJdfQ== */");

/***/ }),

/***/ "9CqS":
/*!********************************************************************!*\
  !*** ./src/app/confirmacao-pop-up/confirmacao-pop-up.component.ts ***!
  \********************************************************************/
/*! exports provided: ConfirmacaoPopUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmacaoPopUpComponent", function() { return ConfirmacaoPopUpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_confirmacao_pop_up_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./confirmacao-pop-up.component.html */ "Fd28");
/* harmony import */ var _confirmacao_pop_up_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./confirmacao-pop-up.component.scss */ "Y06A");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");





let ConfirmacaoPopUpComponent = class ConfirmacaoPopUpComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        dialogRef.disableClose = true;
        //dialogRef.beforeClosed().subscribe(
        //result => dialogRef.close(0));
    }
    ngOnInit() {
    }
    onNoClick() {
        this.dialogRef.close(0);
    }
    onClick() {
        this.dialogRef.close(1);
    }
};
ConfirmacaoPopUpComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Inject"], args: [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"],] }] }
];
ConfirmacaoPopUpComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-confirmacao-pop-up',
        template: _raw_loader_confirmacao_pop_up_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_confirmacao_pop_up_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"], Object])
], ConfirmacaoPopUpComponent);



/***/ }),

/***/ "9TuI":
/*!****************************************************************!*\
  !*** ./src/app/listar-mensagens/listar-mensagens.component.ts ***!
  \****************************************************************/
/*! exports provided: ListarMensagensComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarMensagensComponent", function() { return ListarMensagensComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_listar_mensagens_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./listar-mensagens.component.html */ "2nF6");
/* harmony import */ var _listar_mensagens_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listar-mensagens.component.scss */ "9AxR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _service_mensagem_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/mensagem.service */ "02KH");







let ListarMensagensComponent = class ListarMensagensComponent {
    constructor(mensagemService, route) {
        this.mensagemService = mensagemService;
        this.route = route;
        this.mensagens = [];
        this.retornoMensagens = [];
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.maxSize = 5;
        this.bigTotalItems = 675;
        this.bigCurrentPage = 1;
        this.numPages = 0;
        this.currentPager = 1;
    }
    ngOnInit() {
        this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((params) => params['id'])).subscribe(params => { this.setUsuarioId(params); this.receberMensagens(this.usuarioId); });
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.retornoMensagens = this.mensagens.slice(startItem, endItem);
    }
    receberMensagens(id) {
        this.mensagemService.receberMensagem(id).subscribe(data => {
            console.log(data);
            if (data !== null) {
                let mensagens = [];
                data.forEach(function (item) {
                    mensagens.push(item);
                });
                this.mensagens = mensagens;
                this.setTotalItems(mensagens.length);
                this.retornoMensagens = this.mensagens.slice(0, 5);
            }
            else {
                this.setTotalItems(0);
            }
        });
    }
    checkboxAtt(mensagem) {
        this.atualizaStatus(mensagem);
        this.receberMensagens(this.usuarioId);
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
    atualizaStatus(mensagem) {
        this.mensagemService.atualizaStatus(mensagem).subscribe();
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
};
ListarMensagensComponent.ctorParameters = () => [
    { type: _service_mensagem_service__WEBPACK_IMPORTED_MODULE_6__["MensagemService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
ListarMensagensComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-listar-mensagens',
        template: _raw_loader_listar_mensagens_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_listar_mensagens_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_mensagem_service__WEBPACK_IMPORTED_MODULE_6__["MensagemService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]])
], ListarMensagensComponent);



/***/ }),

/***/ "9j8H":
/*!****************************************************!*\
  !*** ./src/app/form-debug/form-debug.component.ts ***!
  \****************************************************/
/*! exports provided: FormDebugComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormDebugComponent", function() { return FormDebugComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_form_debug_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./form-debug.component.html */ "67tn");
/* harmony import */ var _form_debug_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form-debug.component.scss */ "LMm3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");




let FormDebugComponent = class FormDebugComponent {
    constructor() { }
    ngOnInit() {
    }
};
FormDebugComponent.ctorParameters = () => [];
FormDebugComponent.propDecorators = {
    form: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
FormDebugComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-form-debug',
        template: _raw_loader_form_debug_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_form_debug_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
], FormDebugComponent);



/***/ }),

/***/ "A3xY":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "AxdJ":
/*!********************************************!*\
  !*** ./src/app/service/usuario.service.ts ***!
  \********************************************/
/*! exports provided: UsuarioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioService", function() { return UsuarioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app-constants */ "EKKv");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
let UsuarioService = class UsuarioService {
    constructor(http) {
        this.http = http;
    }
    salvarUsuarioDoador(user, escritorio, colaborador, admin) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        params = params.set('colaborador', colaborador);
        params = params.set('id', escritorio);
        params = params.set('admin', admin);
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + "doador", user, { params: params });
    }
    salvarUsuarioAdmin(user) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + "admin", user);
    }
    getUsuarios(busca, id) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        params = params.set('busca', busca);
        params = params.set('id', id);
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + "todos", { params: params });
    }
    getByLogin(login) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();
        params = params.set('login', login);
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + "buscaLogin", { params: params });
    }
    getById(id) {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + id);
    }
    atualizaUsuario(usuario) {
        return this.http.put(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + "update/" + usuario.id, usuario);
    }
    deletarUsuario(id) {
        return this.http.delete(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlUsuario + "delete/" + id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1));
    }
    userAutenticado() {
        if (localStorage.getItem('token') !== null &&
            localStorage.getItem('token').toString().trim() !== null) {
            return true;
        }
        else {
            return false;
        }
    }
};
UsuarioService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UsuarioService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], UsuarioService);



/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "B9T5":
/*!******************************************************************************************!*\
  !*** ./src/app/cadastrar-colaborador-empresa/cadastrar-colaborador-empresa.component.ts ***!
  \******************************************************************************************/
/*! exports provided: CadastrarColaboradorEmpresaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarColaboradorEmpresaComponent", function() { return CadastrarColaboradorEmpresaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_colaborador_empresa_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-colaborador-empresa.component.html */ "L736");
/* harmony import */ var _cadastrar_colaborador_empresa_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-colaborador-empresa.component.scss */ "hMR8");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _enum_NivelDeAcesso__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../enum/NivelDeAcesso */ "aq7q");
/* harmony import */ var _model_Endereco__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../model/Endereco */ "14ci");
/* harmony import */ var _model_Usuario__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../model/Usuario */ "grlU");
/* harmony import */ var _service_cep_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/cep.service */ "+6xE");
/* harmony import */ var _service_empresa_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/empresa.service */ "pFYR");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");












let CadastrarColaboradorEmpresaComponent = class CadastrarColaboradorEmpresaComponent {
    constructor(rest, fb, router, empresaService, cepService) {
        this.rest = rest;
        this.fb = fb;
        this.router = router;
        this.empresaService = empresaService;
        this.cepService = cepService;
        this.admin = false;
        this.admin_escritorio = false;
        this.admin_empresa = false;
        this.escritorioId = null;
        this.empresas = [];
        this.editando = false;
        this.usuarioId = 0;
        this.nivelDeAcesso = _enum_NivelDeAcesso__WEBPACK_IMPORTED_MODULE_6__["NivelDeAcesso"];
        this.niveis = [];
        this.usuario = new _model_Usuario__WEBPACK_IMPORTED_MODULE_8__["Usuario"]();
        this.endereco = new _model_Endereco__WEBPACK_IMPORTED_MODULE_7__["Endereco"]();
        this.niveis = Object.keys(this.nivelDeAcesso);
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        console.log(usuarioL);
        this.setUsuarioId(usuarioL.id);
        console.log(localStorage.getItem('user_role'));
        if (localStorage.getItem('user_role') == "ROLE_ADMIN") {
            this.setAdmin();
        }
        else if (localStorage.getItem('user_role') == "ROLE_ADMIN_ESCRITORIO") {
            this.setAdminEscritorio();
        }
        else if (localStorage.getItem('user_role') == "ROLE_ADMIN_EMPRESA") {
            this.setAdminEmpresa();
        }
        if (this.admin) {
            this.usuarioForm = this.fb.group({
                nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                rg: [""],
                cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
                celular: [""],
                email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
                escritorio: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                endereco: this.fb.group({
                    rua: [""],
                    numero: [""],
                    bairro: [""],
                    cidade: [""],
                    estado: [""],
                    pais: [""],
                    cep: [""],
                    complemento: [""],
                }),
            });
        }
        else if (this.admin_empresa) {
            let empresaId = 0;
            this.usuarioForm = this.fb.group({
                nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                rg: [""],
                cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
                celular: [""],
                email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
                escritorio: empresaId,
                endereco: this.fb.group({
                    rua: [""],
                    numero: [""],
                    bairro: [""],
                    cidade: [""],
                    estado: [""],
                    pais: [""],
                    cep: [""],
                    complemento: [""],
                }),
                login: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                senha: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                confirmar_senha: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
            });
        }
        this.receberEmpresas("");
    }
    receberEmpresas(busca) {
        this.empresaService.receberEmpresas(busca, this.usuarioId).subscribe(data => {
            console.log(data);
            let empresa = [];
            data.forEach(function (item) {
                empresa.push(item);
            });
            this.empresas = empresa;
        });
    }
    setAdmin() {
        this.admin = true;
    }
    setAdminEscritorio() {
        this.admin_escritorio = true;
    }
    setAdminEmpresa() {
        this.admin_empresa = true;
    }
    setEscritorioId(id) {
        this.escritorioId = id;
    }
    cadastrar(form) {
        this.usuario = form.value;
        console.log(form.value);
        if (this.admin) {
            this.rest.salvarUsuarioDoador(this.usuario, form.value.escritorio, "empresa", "sim").subscribe(teste => {
                console.log(teste);
                this.router.navigate(['dashboard']);
            });
        }
        else {
            this.rest.salvarUsuarioDoador(this.usuario, this.usuarioId, "empresa", "nao").subscribe(teste => {
                console.log(teste);
                this.router.navigate(['dashboard']);
            });
        }
    }
    consultaCep(cep) {
        this.cepService.consulta(cep).subscribe(success => {
            if (success.erro) {
                this.onError();
            }
            else {
                this.onSuccess(success);
            }
        }, error => this.onError());
    }
    onSuccess(response) {
        this.usuarioForm.get(['endereco']).get(['rua']).setValue(response.logradouro);
        this.usuarioForm.get(['endereco']).get(['bairro']).setValue(response.bairro);
        this.usuarioForm.get(['endereco']).get(['cidade']).setValue(response.localidade);
        this.usuarioForm.get(['endereco']).get(['estado']).setValue(response.uf);
        this.usuarioForm.get(['endereco']).get(['complemento']).setValue(response.complemento);
        this.usuarioForm.get(['endereco']).get(['pais']).setValue("Brasil");
    }
    onError() {
        alert("Erro ao consultar CEP");
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
};
CadastrarColaboradorEmpresaComponent.ctorParameters = () => [
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__["UsuarioService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _service_empresa_service__WEBPACK_IMPORTED_MODULE_10__["EmpresaService"] },
    { type: _service_cep_service__WEBPACK_IMPORTED_MODULE_9__["CepService"] }
];
CadastrarColaboradorEmpresaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cadastrar-colaborador-empresa',
        template: _raw_loader_cadastrar_colaborador_empresa_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_colaborador_empresa_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_usuario_service__WEBPACK_IMPORTED_MODULE_11__["UsuarioService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _service_empresa_service__WEBPACK_IMPORTED_MODULE_10__["EmpresaService"], _service_cep_service__WEBPACK_IMPORTED_MODULE_9__["CepService"]])
], CadastrarColaboradorEmpresaComponent);



/***/ }),

/***/ "BZdJ":
/*!**************************************************************!*\
  !*** ./src/app/listar-usuarios/listar-usuarios.component.ts ***!
  \**************************************************************/
/*! exports provided: ListarUsuariosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarUsuariosComponent", function() { return ListarUsuariosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_listar_usuarios_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./listar-usuarios.component.html */ "rryG");
/* harmony import */ var _listar_usuarios_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listar-usuarios.component.scss */ "CtRF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../confirmacao-pop-up/confirmacao-pop-up.component */ "9CqS");
/* harmony import */ var _model_Usuario__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../model/Usuario */ "grlU");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");











let ListarUsuariosComponent = class ListarUsuariosComponent {
    constructor(usuarioService, router, dialog) {
        this.usuarioService = usuarioService;
        this.router = router;
        this.dialog = dialog;
        this.campoPesquisa = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.admin = false;
        this.admin_escritorio = false;
        this.admin_empresa = false;
        this.usuarios = [];
        /*usuarios: Usuario[] = []; /*= [/*{ nome : "Mateus Azevedo Gomes",
           rg: "XXX.XXX.XXX",
           cpf: "XXX.XXX.XXX-XX",
           celular: "(XX)9XXXX-XXXX",
           email: "usuario@dominio.com",
           endereco: {
             rua: 'Aeroporto de Congonhas',
             numero: 248,
             bairro: 'Emaús',
             cidade: 'Parnamirim',
             estado: 'RN',
             pais: 'BR',
             complemento: 'Casa',
             cep: '59149306'
           },
           senha: '',
           login: ''
         } ];*/
        this.retornoUsuarios = [];
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.maxSize = 5;
        this.bigTotalItems = 675;
        this.bigCurrentPage = 1;
        this.numPages = 0;
        this.currentPager = 1;
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuario'));
        console.log(usuarioL);
        this.usuarioService.getByLogin(usuarioL.login).subscribe(data => {
            console.log(data.authorities[0].nomeRole);
            if (data.authorities[0].nomeRole === "ROLE_ADMIN") {
                this.setAdmin();
                localStorage.setItem("user_role", "ROLE_ADMIN");
            }
            else if (data.authorities[0].nomeRole === "ROLE_ADMIN_EMPRESA") {
                this.setAdminEmpresa();
                localStorage.setItem("user_role", "ROLE_ADMIN_EMPRESA");
            }
            else if (data.authorities[0].nomeRole === "ROLE_ADMIN_ESCRITORIO") {
                this.setAdminEscritorio();
                localStorage.setItem("user_role", "ROLE_ADMIN_ESCRITORIO");
            }
            else {
                localStorage.setItem("user_role", "ROLE_SIMPLES");
            }
        });
        this.campoPesquisa.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(value => value.trim()), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["debounceTime"])(150), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["tap"])(value => console.log(value)), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])((value) => {
            let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
            return this.usuarioService.getUsuarios(value, usuarioL.id);
        })).subscribe(data => {
            let usuarios = [];
            data.forEach(function (item) {
                const usr = new _model_Usuario__WEBPACK_IMPORTED_MODULE_9__["Usuario"]();
                usr.id = item.id;
                usr.nome = item.nome;
                usr.rg = item.rg;
                usr.cpf = item.cpf;
                usr.celular = item.celular;
                usr.email = item.email;
                usr.login = item.login;
                usuarios.push(usr);
            });
            this.usuarios = usuarios;
            console.log(usuarios);
            this.setTotalItems(usuarios.length);
            this.retornoUsuarios = this.usuarios.slice(0, 5);
        });
        this.receberUsuarios("");
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.retornoUsuarios = this.usuarios.slice(startItem, endItem);
    }
    receberUsuarios(busca) {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        let usuarios = [];
        this.usuarioService.getUsuarios(busca, usuarioL.id).subscribe(data => {
            data.forEach(function (item) {
                const usr = new _model_Usuario__WEBPACK_IMPORTED_MODULE_9__["Usuario"]();
                usr.id = item.id;
                usr.nome = item.nome;
                usr.rg = item.rg;
                usr.cpf = item.cpf;
                usr.celular = item.celular;
                usr.email = item.email;
                usr.login = item.login;
                usuarios.push(usr);
            });
            this.usuarios = usuarios;
            console.log(this.usuarios);
            this.setTotalItems(usuarios.length);
            this.retornoUsuarios = this.usuarios.slice(0, 5);
        });
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
    editar(id) {
        this.router.navigate(['editar-usuario', id]);
    }
    openDialog(usuario, id) {
        const dialogRef = this.dialog.open(_confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_8__["ConfirmacaoPopUpComponent"], {
            width: '350px',
            data: { nome: usuario.nome },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result !== null && result !== void 0 ? result : 1) {
                this.usuarioService.deletarUsuario(id).subscribe(success => {
                    console.log("sucesso");
                    this.receberUsuarios("");
                }, error => console.log("erro"));
            }
        });
    }
    setAdmin() {
        this.admin = true;
    }
    setAdminEscritorio() {
        this.admin_escritorio = true;
    }
    setAdminEmpresa() {
        this.admin_empresa = true;
    }
};
ListarUsuariosComponent.ctorParameters = () => [
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_10__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] }
];
ListarUsuariosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-listar-usuarios',
        template: _raw_loader_listar_usuarios_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewEncapsulation"].None,
        styles: [_listar_usuarios_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_usuario_service__WEBPACK_IMPORTED_MODULE_10__["UsuarioService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"]])
], ListarUsuariosComponent);



/***/ }),

/***/ "C+bB":
/*!***********************************************************!*\
  !*** ./src/app/form-documento/btn-drop-zone.directive.ts ***!
  \***********************************************************/
/*! exports provided: BtnDropZoneDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BtnDropZoneDirective", function() { return BtnDropZoneDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");


let BtnDropZoneDirective = class BtnDropZoneDirective {
    constructor() {
        this.fileDropped = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    onDrgaOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = true;
    }
    onDrgaLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
    }
    ondrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.fileOver = false;
        const files = evt.dataTransfer.files;
        if (files.length > 0) {
            this.fileDropped.emit(files);
        }
    }
};
BtnDropZoneDirective.ctorParameters = () => [];
BtnDropZoneDirective.propDecorators = {
    fileOver: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"], args: ['class.fileover',] }],
    fileDropped: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"] }],
    onDrgaOver: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['dragover', ['$event'],] }],
    onDrgaLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['dragleave', ['$event'],] }],
    ondrop: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"], args: ['drop', ['$event'],] }]
};
BtnDropZoneDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appBtnDropZone]'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
], BtnDropZoneDirective);



/***/ }),

/***/ "C6Xi":
/*!**********************************************!*\
  !*** ./src/app/service/documento.service.ts ***!
  \**********************************************/
/*! exports provided: DocumentoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DocumentoService", function() { return DocumentoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app-constants */ "EKKv");




let DocumentoService = class DocumentoService {
    constructor(http) {
        this.http = http;
    }
    salvarUpload(formData, id) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlDocumento + "arquivos/" + id, formData, {
            reportProgress: true,
            observe: 'events',
        });
    }
    receberDocumentos(id) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        params = params.set('id', id);
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlDocumento + "todos", { params: params });
    }
    receberDocumento(id) {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlDocumento + id);
    }
    editaDocumento(documento) {
        return this.http.put(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlDocumento + "editar/" + documento.id, documento);
    }
};
DocumentoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
DocumentoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], DocumentoService);



/***/ }),

/***/ "CtRF":
/*!****************************************************************!*\
  !*** ./src/app/listar-usuarios/listar-usuarios.component.scss ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0YXItdXN1YXJpb3MuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "D9Ld":
/*!************************************************************************************************!*\
  !*** ./src/app/cadastrar-colaborador-escritorio/cadastrar-colaborador-escritorio.component.ts ***!
  \************************************************************************************************/
/*! exports provided: CadastrarColaboradorEscritorioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarColaboradorEscritorioComponent", function() { return CadastrarColaboradorEscritorioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_colaborador_escritorio_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-colaborador-escritorio.component.html */ "ODnz");
/* harmony import */ var _cadastrar_colaborador_escritorio_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-colaborador-escritorio.component.scss */ "7glZ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _enum_NivelDeAcesso__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../enum/NivelDeAcesso */ "aq7q");
/* harmony import */ var _model_Endereco__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../model/Endereco */ "14ci");
/* harmony import */ var _model_Usuario__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../model/Usuario */ "grlU");
/* harmony import */ var _service_cep_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/cep.service */ "+6xE");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/escritorio-contabil.service */ "wjmZ");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");












let CadastrarColaboradorEscritorioComponent = class CadastrarColaboradorEscritorioComponent {
    constructor(rest, fb, router, escritorioService, cepService) {
        this.rest = rest;
        this.fb = fb;
        this.router = router;
        this.escritorioService = escritorioService;
        this.cepService = cepService;
        this.admin = false;
        this.admin_escritorio = false;
        this.escritorios = [];
        this.editando = false;
        this.usuarioId = 0;
        this.nivelDeAcesso = _enum_NivelDeAcesso__WEBPACK_IMPORTED_MODULE_6__["NivelDeAcesso"];
        this.niveis = [];
        this.usuario = new _model_Usuario__WEBPACK_IMPORTED_MODULE_8__["Usuario"]();
        this.endereco = new _model_Endereco__WEBPACK_IMPORTED_MODULE_7__["Endereco"]();
        this.niveis = Object.keys(this.nivelDeAcesso);
    }
    ngOnInit() {
        this.receberEscritorios("");
        console.log(localStorage.getItem('user_role'));
        if (localStorage.getItem('user_role') == "ROLE_ADMIN") {
            this.setAdmin();
        }
        else if (localStorage.getItem('user_role') == "ROLE_ADMIN_ESCRITORIO") {
            this.setAdminEscritorio();
        }
        if (this.admin) {
            this.usuarioForm = this.fb.group({
                nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                rg: [""],
                cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
                celular: [""],
                email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
                escritorio: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                endereco: this.fb.group({
                    rua: [""],
                    numero: [""],
                    bairro: [""],
                    cidade: [""],
                    estado: [""],
                    pais: [""],
                    cep: [""],
                    complemento: [""],
                }),
            });
        }
        else if (this.admin_escritorio) {
            let escritorioId = 0;
            let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
            console.log(usuarioL);
            this.setUsuarioId(usuarioL.id);
            this.usuarioForm = this.fb.group({
                nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                rg: [""],
                cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
                celular: [""],
                email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
                endereco: this.fb.group({
                    rua: [""],
                    numero: [""],
                    bairro: [""],
                    cidade: [""],
                    estado: [""],
                    pais: [""],
                    cep: [""],
                    complemento: [""],
                }),
            });
        }
    }
    receberEscritorios(busca) {
        this.escritorioService.receberEscritorios(busca).subscribe(data => {
            console.log(data);
            let escritorios = [];
            data.forEach(function (item) {
                escritorios.push(item);
            });
            this.escritorios = escritorios;
        });
    }
    setAdmin() {
        this.admin = true;
    }
    setAdminEscritorio() {
        this.admin_escritorio = true;
    }
    cadastrar(form) {
        this.usuario = form.value;
        console.log(form.value);
        if (this.admin) {
            this.rest.salvarUsuarioDoador(this.usuario, form.value.escritorio, "escritorio", "sim").subscribe(teste => {
                console.log(teste);
                this.router.navigate(['dashboard']);
            });
        }
        else {
            this.rest.salvarUsuarioDoador(this.usuario, this.usuarioId, "escritorio", "nao").subscribe(teste => {
                console.log(teste);
                this.router.navigate(['dashboard']);
            });
        }
    }
    consultaCep(cep) {
        this.cepService.consulta(cep).subscribe(success => {
            if (success.erro) {
                this.onError();
            }
            else {
                this.onSuccess(success);
            }
        }, error => this.onError());
    }
    onSuccess(response) {
        this.usuarioForm.get(['endereco']).get(['rua']).setValue(response.logradouro);
        this.usuarioForm.get(['endereco']).get(['bairro']).setValue(response.bairro);
        this.usuarioForm.get(['endereco']).get(['cidade']).setValue(response.localidade);
        this.usuarioForm.get(['endereco']).get(['estado']).setValue(response.uf);
        this.usuarioForm.get(['endereco']).get(['complemento']).setValue(response.complemento);
        this.usuarioForm.get(['endereco']).get(['pais']).setValue("Brasil");
    }
    onError() {
        alert("Erro ao consultar CEP");
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
};
CadastrarColaboradorEscritorioComponent.ctorParameters = () => [
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__["UsuarioService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_10__["EscritorioContabilService"] },
    { type: _service_cep_service__WEBPACK_IMPORTED_MODULE_9__["CepService"] }
];
CadastrarColaboradorEscritorioComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cadastrar-colaborador-escritorio',
        template: _raw_loader_cadastrar_colaborador_escritorio_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_colaborador_escritorio_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_usuario_service__WEBPACK_IMPORTED_MODULE_11__["UsuarioService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_10__["EscritorioContabilService"], _service_cep_service__WEBPACK_IMPORTED_MODULE_9__["CepService"]])
], CadastrarColaboradorEscritorioComponent);



/***/ }),

/***/ "Dz3O":
/*!*******************************************************************************!*\
  !*** ./src/app/tipo-documentos/listar/listar-tipos-documentos.component.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0YXItdGlwb3MtZG9jdW1lbnRvcy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "EKKv":
/*!**********************************!*\
  !*** ./src/app/app-constants.ts ***!
  \**********************************/
/*! exports provided: AppConstants */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConstants", function() { return AppConstants; });
class AppConstants {
    static get baseServidor() { return "http://3.93.147.126:8080/"; }
    static get baseLogin() { return this.baseServidor + "wk/login"; }
    static get baseUrlUsuario() { return this.baseServidor + "wk/usuario/"; }
    static get baseUrlDocumento() { return this.baseServidor + "wk/documento/"; }
    static get baseUrlCandidato() { return this.baseServidor + "wk/candidato/"; }
    static get baseUrlEscritorioContabil() { return this.baseServidor + "wk/escritorio-contabil/"; }
    static get baseUrlEmpresaCliente() { return this.baseServidor + "wk/empresacliente/"; }
    static get baseUrlMensagem() { return this.baseServidor + "wk/mensagem/"; }
    static get baseUrlTipoDocumentos() { return this.baseServidor + "wk/tipodocumentos/"; }
    static get baseUrlCategoria() { return this.baseServidor + "wk/categoria/"; }
}


/***/ }),

/***/ "Edu+":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/listar-empresas/listar-empresas.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"animated fadeIn mt-4\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Lista de Empresas\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col col-8\">\r\n              <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" routerLink=\"/cadastrar-empresa\">\r\n            </div>\r\n            <div class=\"col col-4\">\r\n              <input type=\"text\" id=\"myInput\" class=\"form-control\" placeholder=\"Busque pela razão social\"\r\n                [formControl]=\"campoPesquisa\">\r\n\r\n            </div>\r\n          </div>\r\n\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>Escritório</th>\r\n                <th>CNPJ</th>\r\n                <th>Razão Social</th>\r\n                <th>Nome Fantasia</th>\r\n                <th>Proprietário</th>\r\n                <th>Contato</th>\r\n                <th></th>\r\n                <th></th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let empresa of retorno_empresa; let i = index\">\r\n                <th scope=\"row\">{{ i + 1 }}</th>\r\n                <td>{{ empresa.escritorioId.razaoSocial}}</td>\r\n                <td>{{ empresa.pjId.cnpj }}</td>\r\n                <td>{{ empresa.pjId.razao_social }}</td>\r\n                <td>{{ empresa.pjId.nome_fantasia }}</td>\r\n                <td>{{ empresa.responsavel.nome }}</td>\r\n                <td>{{empresa.pjId.email}}</td>\r\n                <td><input class=\"btn btn-info\" type=\"button\" value=\"Editar\" (click)=\"editar(empresa.id)\"></td>\r\n                <td><input class=\"btn btn-danger ms-3\" type=\"button\" (click)=\"openDialog(empresa, empresa.id)\" value=\"Excluir\"></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n            <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>");

/***/ }),

/***/ "FLUB":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tipo-documentos/listar/listar-tipos-documentos.component.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"animated fadeIn mt-4\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Lista de Tipos de Documentos\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col col-8\">\r\n              <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" routerLink=\"/cadastrar-tipos-documentos\">\r\n            </div>\r\n            <div class=\"col col-4\">\r\n\r\n              <select *ngIf=\"(role === 'ROLE_ADMIN')\"\r\n                class=\"form-control\" \r\n                id=\"field_escritorioBusca\" \r\n                (change)=\"onSearchEscritorioChange($event)\"\r\n                >\r\n                <option [value]=\"null\">Buscar por escritório</option>\r\n                <option [value]=\"escritorioOption.id\"\r\n                  *ngFor=\"let escritorioOption of escritorios\">\r\n                  {{ escritorioOption.nomeFantasia }}\r\n                </option>\r\n              </select>\r\n\r\n            </div>\r\n          </div>\r\n\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>Nome do documento</th>\r\n                <th>Escritório</th>\r\n                <th>Categoria</th>\r\n                <th></th>\r\n                <th></th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let tipoDocumento of tiposDocumentos; let i = index\">\r\n                <th scope=\"row\">{{ i + 1 }}</th>\r\n                <td>{{ tipoDocumento.nomeDocumento}}</td>\r\n                <td>{{ tipoDocumento.escritorioContabil.nomeFantasia }}</td>\r\n                <td>{{ tipoDocumento.categoria.nomeCategoria }}</td>\r\n                <td><input class=\"btn btn-info\" type=\"button\" value=\"Editar\" [routerLink]=\"['/editar-tipos-documentos', tipoDocumento.id]\"></td>\r\n                <td><input class=\"btn btn-danger ms-3\" type=\"button\" (click)=\"openDialog(tipoDocumento, tipoDocumento.id)\" value=\"Excluir\"></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n            <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>");

/***/ }),

/***/ "Fd28":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/confirmacao-pop-up/confirmacao-pop-up.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 mat-dialog-title>Confirmação de exclusão</h1>\r\n<div mat-dialog-content>\r\n  <p>Deseja excluir {{data.nome}} ? </p>\r\n</div>\r\n<div mat-dialog-actions class=\"align-items-right\">\r\n  <button mat-button class=\"btn btn-primary\" (click)=\"onClick()\" cdkFocusInitial>Sim</button>\r\n  <button mat-button class=\"btn btn-default\" (click)=\"onNoClick()\">Não</button>\r\n</div>\r\n");

/***/ }),

/***/ "GV3p":
/*!******************************************************************!*\
  !*** ./src/app/cadastrar-usuario/cadastrar-usuario.component.ts ***!
  \******************************************************************/
/*! exports provided: CadastrarUsuarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarUsuarioComponent", function() { return CadastrarUsuarioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_usuario_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-usuario.component.html */ "cc24");
/* harmony import */ var _cadastrar_usuario_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-usuario.component.scss */ "tpOW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _enum_NivelDeAcesso__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../enum/NivelDeAcesso */ "aq7q");
/* harmony import */ var _model_Endereco__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../model/Endereco */ "14ci");
/* harmony import */ var _model_Usuario__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../model/Usuario */ "grlU");
/* harmony import */ var _service_cep_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../service/cep.service */ "+6xE");
/* harmony import */ var _service_empresa_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../service/empresa.service */ "pFYR");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../service/escritorio-contabil.service */ "wjmZ");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");















let CadastrarUsuarioComponent = class CadastrarUsuarioComponent {
    constructor(rest, fb, route, router, empresaService, escritorioService, cepService) {
        this.rest = rest;
        this.fb = fb;
        this.route = route;
        this.router = router;
        this.empresaService = empresaService;
        this.escritorioService = escritorioService;
        this.cepService = cepService;
        this.editando = false;
        this.empresas = [];
        this.escritorios = [];
        this.btn1 = true;
        this.btn2 = false;
        this.emp = false;
        this.esc = false;
        this.admin = false;
        this.escritorioempresaId = 0;
        this.usuarioId = 0;
        this.nivelDeAcesso = _enum_NivelDeAcesso__WEBPACK_IMPORTED_MODULE_8__["NivelDeAcesso"];
        this.niveis = [];
        this.cpf = false;
        this.usuario = new _model_Usuario__WEBPACK_IMPORTED_MODULE_10__["Usuario"]();
        this.endereco = new _model_Endereco__WEBPACK_IMPORTED_MODULE_9__["Endereco"]();
        this.niveis = Object.keys(this.nivelDeAcesso);
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        console.log(usuarioL);
        this.setUsuarioId(usuarioL.id);
        this.setRole(localStorage.getItem('user_role'));
        if (localStorage.getItem('user_role') === "ROLE_ADMIN_EMPRESA" || localStorage.getItem('user_role') === "ROLE_ADMIN_ESCRITORIO") {
            this.usuarioForm = this.fb.group({
                id: [null],
                nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                rg: [""],
                cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
                celular: [""],
                email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
                escritorio: usuarioL.id,
                permissao: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                endereco: this.fb.group({
                    rua: [""],
                    numero: [""],
                    bairro: [""],
                    cidade: [""],
                    estado: [""],
                    cep: [""],
                }),
            });
        }
        else {
            this.setAdmin();
            this.receberEmpresas("");
            this.receberEscritorios("");
            this.usuarioForm = this.fb.group({
                id: [null],
                nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                rg: [""],
                cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
                celular: [""],
                email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
                nivelDeAcesso: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                escritorio: "",
                escritorio2: "",
                endereco: this.fb.group({
                    rua: [""],
                    numero: [""],
                    bairro: [""],
                    cidade: [""],
                    estado: [""],
                    cep: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
                }),
            });
        }
        this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((params) => params['id']), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])(id => {
            if (id !== undefined) {
                return this.rest.getById(id);
            }
            else {
                return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"]();
            }
        })).subscribe(usuario => {
            this.updateForm(usuario);
            this.setEditando();
        });
        /*this.usuarioForm = this.fb.group({
          id: [null],
          nome: ["", [Validators.required]],
          rg: [""],
          cpf: ["", [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
          celular: [""],
          email: ["", [Validators.required, Validators.email]],
          permissao: "",
          escritorio: 0,
          endereco: this.fb.group ({
            rua: [""],
            numero: [""],
            bairro: [""],
            cidade: [""],
            estado: [""],
            pais: [""],
            cep: [""],
            complemento: [""],
          }),
          // login: ["", [Validators.required]],
          // senha: ["", [Validators.required]],
          // confirmar_senha: ["", [Validators.required]]
        });
        */
    }
    cadastrar(form) {
        this.usuario = form.value;
        if (localStorage.getItem('user_role') === "ROLE_ADMIN_EMPRESA") {
            this.rest.salvarUsuarioDoador(this.usuario, this.usuarioId, "empresa", "nao").subscribe(teste => {
                console.log(teste);
                this.router.navigate(['dashboard']);
            });
        }
        else if (localStorage.getItem('user_role') === "ROLE_ADMIN_ESCRITORIO") {
            this.rest.salvarUsuarioDoador(this.usuario, this.usuarioId, "escritorio", "nao").subscribe(teste => {
                console.log(teste);
                this.router.navigate(['dashboard']);
            });
        }
        else {
            if (this.btn1) {
                this.rest.salvarUsuarioDoador(this.usuario, form.value.escritorio, "empresa", "sim").subscribe(teste => {
                    console.log(teste);
                    this.router.navigate(['dashboard']);
                });
            }
            else {
                this.rest.salvarUsuarioDoador(this.usuario, form.value.escritorio2, "escritorio", "sim").subscribe(teste => {
                    console.log(teste);
                    this.router.navigate(['dashboard']);
                });
            }
        }
    }
    updateForm(usuario) {
        if (usuario.endereco !== null) {
            console.log(this.nivelDeAcesso[usuario.nivelDeAcesso]);
            this.usuarioForm.patchValue({
                id: usuario.id,
                nome: usuario.nome,
                rg: usuario.rg,
                cpf: usuario.cpf,
                celular: usuario.celular,
                email: usuario.email,
                nivelDeAcesso: usuario.nivelDeAcesso,
                endereco: ({
                    rua: usuario.endereco.rua,
                    numero: usuario.endereco.numero,
                    bairro: usuario.endereco.bairro,
                    cidade: usuario.endereco.cidade,
                    estado: usuario.endereco.estado,
                    cep: usuario.endereco.cep,
                }),
            });
        }
        else {
            this.usuarioForm.patchValue({
                id: usuario.id,
                nome: usuario.nome,
                rg: usuario.rg,
                cpf: usuario.cpf,
                celular: usuario.celular,
                email: usuario.email,
                nivelDeAcesso: usuario.nivelDeAcesso,
                endereco: ({
                    rua: "",
                    numero: "",
                    bairro: "",
                    cidade: "",
                    estado: "",
                    cep: "",
                }),
            });
        }
    }
    editar(usuario) {
        this.rest.atualizaUsuario(usuario.value).subscribe(success => {
            console.log("Deu certo");
            this.router.navigate(['dashboard']);
        }, error => {
            console.log("Falhou");
            console.log(usuario.value);
            console.log(error);
        });
    }
    setRole(value) {
        this.role = value;
    }
    receberEmpresas(busca) {
        this.empresaService.receberEmpresas(busca, this.usuarioId).subscribe(data => {
            let empresa = [];
            data.forEach(function (item) {
                empresa.push(item);
            });
            this.empresas = empresa;
        });
    }
    receberEscritorios(busca) {
        this.escritorioService.receberEscritorios(busca).subscribe(data => {
            console.log(data);
            let escritorios = [];
            data.forEach(function (item) {
                escritorios.push(item);
            });
            this.escritorios = escritorios;
        });
    }
    mudarValorBtn1() {
        if (this.btn1 === false) {
            this.btn1 = true;
            this.btn2 = false;
        }
    }
    mudarValorBtn2() {
        if (this.btn2 === false) {
            this.btn2 = true;
            this.btn1 = false;
        }
    }
    setEscritorioempresaId(n) {
        this.escritorioempresaId = n;
    }
    consultaCep(cep) {
        this.cepService.consulta(cep).subscribe(success => {
            if (success.erro) {
                this.onError();
            }
            else {
                this.onSuccess(success);
            }
        }, error => this.onError());
    }
    onSuccess(response) {
        this.usuarioForm.get(['endereco']).get(['rua']).setValue(response.logradouro);
        this.usuarioForm.get(['endereco']).get(['bairro']).setValue(response.bairro);
        this.usuarioForm.get(['endereco']).get(['cidade']).setValue(response.localidade);
        this.usuarioForm.get(['endereco']).get(['estado']).setValue(response.uf);
    }
    onError() {
        alert("Erro ao consultar CEP");
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
    setEditando() {
        this.editando = true;
    }
    setAdmin() {
        this.admin = true;
    }
    onKey1() { this.emp = true; }
    onKey2() { this.esc = true; }
    testaCPF(cpf) {
        var Soma = 0;
        if (cpf === undefined) {
            this.setCpf(false);
            //alert("CPF inválido");
            return false;
        }
        var strCPF = cpf.replace('.', '').replace('.', '').replace('-', '');
        if (strCPF === '00000000000' || strCPF === '11111111111' || strCPF === '22222222222' || strCPF === '33333333333' ||
            strCPF === '44444444444' || strCPF === '55555555555' || strCPF === '66666666666' || strCPF === '77777777777' || strCPF === '88888888888' ||
            strCPF === '99999999999' || strCPF.length !== 11) {
            this.setCpf(false);
            //alert("CPF inválido");
            return false;
        }
        for (let i = 1; i <= 9; i++) {
            Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        }
        var Resto = (Soma * 10) % 11;
        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(9, 10))) {
            this.setCpf(false);
            //alert("CPF inválido");
            return false;
        }
        Soma = 0;
        for (let k = 1; k <= 10; k++) {
            Soma = Soma + parseInt(strCPF.substring(k - 1, k)) * (12 - k);
        }
        Resto = (Soma * 10) % 11;
        if ((Resto === 10) || (Resto === 11)) {
            Resto = 0;
        }
        if (Resto !== parseInt(strCPF.substring(10, 11))) {
            this.setCpf(false);
            //alert("CPF inválido");
            return false;
        }
        this.setCpf(true);
        return true;
    }
    setCpf(cpf) {
        this.cpf = cpf;
    }
};
CadastrarUsuarioComponent.ctorParameters = () => [
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_14__["UsuarioService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _service_empresa_service__WEBPACK_IMPORTED_MODULE_12__["EmpresaService"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_13__["EscritorioContabilService"] },
    { type: _service_cep_service__WEBPACK_IMPORTED_MODULE_11__["CepService"] }
];
CadastrarUsuarioComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cadastrar-usuario',
        template: _raw_loader_cadastrar_usuario_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_usuario_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_usuario_service__WEBPACK_IMPORTED_MODULE_14__["UsuarioService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _service_empresa_service__WEBPACK_IMPORTED_MODULE_12__["EmpresaService"], _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_13__["EscritorioContabilService"], _service_cep_service__WEBPACK_IMPORTED_MODULE_11__["CepService"]])
], CadastrarUsuarioComponent);



/***/ }),

/***/ "H/d9":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/dashboard/dashboard.component.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-header\r\n  [navbarBrandRouterLink]=\"['/dashboard']\"\r\n  [fixed]=\"true\"\r\n  [navbarBrandFull]=\"{src: '../../assets/img/SAFFOVERDE.png', width: 83, height: 54, alt: 'CoreUI Logo'}\"\r\n  [navbarBrandMinimized]=\"{src: '../../assets/img/SAFFOVERDE.png', width: 50, height: 34, alt: 'CoreUI Logo'}\"\r\n  [sidebarToggler]=\"'lg'\"\r\n  [asideMenuToggler]=\"'lg'\">\r\n  <ul class=\"nav navbar-nav d-md-down-none\">\r\n    <li class=\"nav-item px-3\">\r\n      <a class=\"nav-link\" href=\"#\">Index</a>\r\n    </li>\r\n    <li class=\"nav-item px-3\">\r\n      <a class=\"nav-link\" href=\"#\">Usuários</a>\r\n    </li>\r\n    \r\n    <li class=\"nav-item px-3 dropdown\" dropdown>\r\n      <a class=\"nav-link\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle (click)=\"false\">Documentos</a>\r\n      <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\" *dropdownMenu aria-labelledby=\"simple-dropdown\">\r\n        <a class=\"dropdown-item\" routerLink='/form-documento' routerLinkActive=\"router-link-active\">Enviar Documentos</a>\r\n        <a *ngIf=\"(role == 'ROLE_ADMIN' || role == 'ROLE_ADMIN_ESCRITORIO')\" class=\"dropdown-item\" routerLink='/requisitar-documento' routerLinkActive=\"router-link-active\">Requisitar Documentos</a>\r\n      </div>\r\n    </li>\r\n    <li class=\"nav-item px-3\">\r\n      <a class=\"nav-link\" href=\"#\">Configurações</a>\r\n    </li>\r\n    <li class=\"nav-item px-3\" id=\"msg\" style=\"color: rgb(115, 129, 143);\">\r\n      <a class=\"nav-link\" role=\"button\" (click)=\"mensagem()\" routerLinkActive=\"router-link-active\">Mensagens</a>\r\n    </li>\r\n  </ul>\r\n  <ul class=\"nav navbar-nav ml-auto\">\r\n    <li class=\"nav-item dropdown\" dropdown placement=\"bottom right\">\r\n      <a class=\"nav-link\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle (click)=\"false\">\r\n        <img src=\"\" class=\"img-avatar\" alt=\"Perfil\"/>\r\n        <span *ngIf=\"(nMensagens) > 0\" class=\"badge badge-danger\">!</span>\r\n      </a>\r\n      <div class=\"dropdown-menu dropdown-menu-right\" *dropdownMenu aria-labelledby=\"simple-dropdown\">\r\n        <div class=\"dropdown-header text-center\"><strong>Conta</strong></div>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-bell-o\"></i> Atualizações<span class=\"badge badge-info\">42</span></a>\r\n        <a class=\"dropdown-item\"  role=\"button\" (click)=\"mensagem()\" routerLinkActive=\"router-link-active\" style=\"width: 250px;\"><i class=\"fa fa-envelope-o\"></i> Menssagens não lidas <span class=\"badge badge-danger\">{{nMensagens}}</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-tasks\"></i> Tarefas<span class=\"badge badge-danger\">42</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-comments\"></i> Comment<span class=\"badge badge-warning\">42</span></a>\r\n        <div class=\"dropdown-header text-center\"><strong>Configurações</strong></div>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-user\"></i> Perfil</a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-wrench\"></i> Configurações</a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-usd\"></i> Pagamentos<span class=\"badge badge-dark\">42</span></a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-file\"></i> Projetos<span class=\"badge badge-primary\">42</span></a>\r\n        <div class=\"divider\"></div>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-shield\"></i> Lock account</a>\r\n        <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-lock\"></i> Sair</a>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</app-header>\r\n<div class=\"app-body\">\r\n  <app-sidebar  #appSidebar [fixed]=\"true\" [display]=\"'lg'\" [minimized]=\"sidebarMinimized\" (minimizedChange)=\"toggleMinimize($event)\">\r\n    <app-sidebar-nav *ngIf=\"(role == 'ROLE_ADMIN')\" [navItems]=\"navItems\" [perfectScrollbar] [disabled]=\"appSidebar.minimized\"></app-sidebar-nav>\r\n    <app-sidebar-nav *ngIf=\"(role == 'ROLE_ADMIN_EMPRESA')\" [navItems]=\"navItemsEmp\" [perfectScrollbar] [disabled]=\"appSidebar.minimized\"></app-sidebar-nav>\r\n    <app-sidebar-nav *ngIf=\"(role == 'ROLE_ADMIN_ESCRITORIO')\"[navItems]=\"navItemsEsc\" [perfectScrollbar] [disabled]=\"appSidebar.minimized\"></app-sidebar-nav>\r\n    <app-sidebar-minimizer></app-sidebar-minimizer>\r\n  </app-sidebar>\r\n\r\n\r\n  <main class=\"main\">\r\n    <div class=\"container-fluid\">\r\n      <app-cadastrar-cliente *ngIf=\"url == '/dashboard' && (role == 'ROLE_ADMIN') \"></app-cadastrar-cliente>\r\n      <app-listar-empresas *ngIf=\"url == '/dashboard' && (role == 'ROLE_ADMIN' || role == 'ROLE_ADMIN_ESCRITORIO') \"></app-listar-empresas>\r\n      <app-listar-usuarios *ngIf=\"url == '/dashboard'\"></app-listar-usuarios>\r\n\r\n      \r\n      <app-listar-documentos *ngIf=\"url == '/dashboard'&& (role == 'ROLE_ADMIN' || role == 'ROLE_ADMIN_EMPRESA')\" ></app-listar-documentos>\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </main>\r\n  <!--<app-aside [fixed]=\"true\" [display]=\"false\" [ngClass]=\"'test'\">\r\n    <tabset>\r\n      <tab>\r\n        <ng-template tabHeading><i class=\"icon-list\"></i></ng-template>\r\n        <div class=\"list-group list-group-accent\">\r\n          <div class=\"list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small\">Today</div>\r\n          <div class=\"list-group-item list-group-item-accent-warning list-group-item-divider\">\r\n            <div class=\"avatar float-right\">\r\n              <img class=\"img-avatar\" src=\"assets/img/avatars/7.jpg\" alt=\"\">\r\n            </div>\r\n            <div>Meeting with\r\n              <strong>Lucas</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  1 - 3pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-location-pin\"></i>  Palo Alto, CA</small>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-info\">\r\n            <div class=\"avatar float-right\">\r\n              <img class=\"img-avatar\" src=\"assets/img/avatars/4.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n            </div>\r\n            <div>Skype with\r\n              <strong>Megan</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  4 - 5pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-social-skype\"></i>  On-line</small>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small\">Tomorrow</div>\r\n          <div class=\"list-group-item list-group-item-accent-danger list-group-item-divider\">\r\n            <div>New UI Project -\r\n              <strong>deadline</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  10 - 11pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-home\"></i>  creativeLabs HQ</small>\r\n            <div class=\"avatars-stack mt-2\">\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/2.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/3.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/4.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/5.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/6.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-success list-group-item-divider\">\r\n            <div>\r\n              <strong>#10 Startups.Garden</strong> Meetup</div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  1 - 3pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-location-pin\"></i>  Palo Alto, CA</small>\r\n          </div>\r\n          <div class=\"list-group-item list-group-item-accent-primary list-group-item-divider\">\r\n            <div>\r\n              <strong>Team meeting</strong>\r\n            </div>\r\n            <small class=\"text-muted mr-3\">\r\n              <i class=\"icon-calendar\"></i>  4 - 6pm</small>\r\n            <small class=\"text-muted\">\r\n              <i class=\"icon-home\"></i>  creativeLabs HQ</small>\r\n            <div class=\"avatars-stack mt-2\">\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/2.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/3.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/4.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/5.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/6.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/7.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n              <div class=\"avatar avatar-xs\">\r\n                <img class=\"img-avatar\" src=\"assets/img/avatars/8.jpg\" alt=\"admin@bootstrapmaster.com\">\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </tab>\r\n      <tab>\r\n        <ng-template tabHeading><i class=\"icon-speech\"></i></ng-template>\r\n        <div class=\"p-3\">\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n          <hr>\r\n          <div class=\"message\">\r\n            <div class=\"py-3 pb-5 mr-3 float-left\">\r\n              <div class=\"avatar\">\r\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\r\n                <span class=\"avatar-status badge-success\"></span>\r\n              </div>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lukasz Holeczek</small>\r\n              <small class=\"text-muted float-right mt-1\">1:52 PM</small>\r\n            </div>\r\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\r\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\r\n          </div>\r\n        </div>\r\n      </tab>\r\n      <tab>\r\n        <ng-template tabHeading><i class=\"icon-settings\"></i></ng-template>\r\n        <div class=\"p-3\">\r\n          <h6>Settings</h6>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-4\">\r\n              <small><b>Option 1</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\" checked>\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n              </label>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>\r\n            </div>\r\n          </div>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-3\">\r\n              <small><b>Option 2</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\">\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n              </label>\r\n            </div>\r\n            <div>\r\n              <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>\r\n            </div>\r\n          </div>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-3\">\r\n              <small><b>Option 3</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\">\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n                <span class=\"switch-handle\"></span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <div class=\"aside-options\">\r\n            <div class=\"clearfix mt-3\">\r\n              <small><b>Option 4</b></small>\r\n              <label class=\"switch switch-label switch-pill switch-success switch-sm float-right\">\r\n                <input type=\"checkbox\" class=\"switch-input\" checked>\r\n                <span class=\"switch-slider\" data-checked=\"On\" data-unchecked=\"Off\"></span>\r\n              </label>\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <h6>System Utilization</h6>\r\n          <div class=\"text-uppercase mb-1 mt-4\"><small><b>CPU Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-info\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">348 Processes. 1/4 Cores.</small>\r\n          <div class=\"text-uppercase mb-1 mt-2\"><small><b>Memory Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-warning\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">11444GB/16384MB</small>\r\n          <div class=\"text-uppercase mb-1 mt-2\"><small><b>SSD 1 Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-danger\" role=\"progressbar\" style=\"width: 95%\" aria-valuenow=\"95\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">243GB/256GB</small>\r\n          <div class=\"text-uppercase mb-1 mt-2\"><small><b>SSD 2 Usage</b></small></div>\r\n          <div class=\"progress progress-xs\">\r\n            <div class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 10%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n          </div>\r\n          <small class=\"text-muted\">25GB/256GB</small>\r\n        </div>\r\n      </tab>\r\n    </tabset>\r\n  </app-aside>-->\r\n</div>\r\n");

/***/ }),

/***/ "HTR/":
/*!************************************************************!*\
  !*** ./src/app/form-documento/form-documento.component.ts ***!
  \************************************************************/
/*! exports provided: FormDocumentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormDocumentoComponent", function() { return FormDocumentoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_form_documento_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./form-documento.component.html */ "R9d+");
/* harmony import */ var _form_documento_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./form-documento.component.scss */ "P+xf");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _enum_Status__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../enum/Status */ "PN/7");
/* harmony import */ var _model_Documento__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../model/Documento */ "tTeE");
/* harmony import */ var _model_Usuario__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../model/Usuario */ "grlU");
/* harmony import */ var _service_documento_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/documento.service */ "C6Xi");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");












let usuarioL;
let FormDocumentoComponent = class FormDocumentoComponent {
    constructor(rest, fb, usuarioService, router) {
        this.rest = rest;
        this.fb = fb;
        this.usuarioService = usuarioService;
        this.router = router;
        this.status = _enum_Status__WEBPACK_IMPORTED_MODULE_7__["Status"];
        this.enumKeys = [];
        this.usuarios = [];
        this.files = [];
        this.uploadingFile = "none";
        this.uploadProgress = 0;
        this.mostrarAlertaSucesso = false;
        this.mostrarAlertaFalha = false;
        this.gif = false;
        this.documento = new _model_Documento__WEBPACK_IMPORTED_MODULE_8__["Documento"]();
        this.enumKeys = Object.keys(this.status);
        this.usr = new _model_Usuario__WEBPACK_IMPORTED_MODULE_9__["Usuario"]();
    }
    ngOnInit() {
        usuarioL = JSON.parse(localStorage.getItem('usuario'));
        console.log(JSON.parse(localStorage.getItem('usuario')));
        //this.receberUsuarios(this.usuarios, this.usr);
        //console.log(this.usuarios);
        /*this.documentoForm = this.fb.group({
          //competencia: ["", Validators.required],
          //valorVencimento: ["", Validators.min(0)],
          //categoria: ["", Validators.required],
          //statusEnvio: ["", Validators.required],
          //usuarioId: ["", Validators.required],
        });
        */
    }
    salvar() {
        /*this.documento = form.value;
        console.log(this.documento);
        this.rest.salvarDocumento(this.documento).subscribe(result => {});
        */
        this.enviarArquivos(this.files);
        //this.documentoForm.reset();
    }
    fecharAlertaSucesso() {
        this.mostrarAlertaSucesso = false;
    }
    fecharAlertaFalha() {
        this.mostrarAlertaFalha = false;
    }
    /*receberUsuarios(usuarios: Array<Usuario>, usr: Usuario) {
      this.usuarioService.getUsuarios().subscribe(data => {
        data.forEach(function (item) {
          usr.id = item.id;
          usr.nome = item.nome;
          usr.rg = item.rg;
          usr.cpf = item.cpf;
          usr.celular = item.celular;
          usr.email = item.email;
          usr.endereco = item.endereco;
          usuarios.push(usr);
        });
      });
    }
    */
    arquivosArrastados($event) {
        for (const item of $event) {
            if (item.type != "application/pdf") {
                this.mostrarAlertaFalha = true;
            }
        }
        if (!this.mostrarAlertaFalha) {
            this.receberArquivos($event);
        }
    }
    arquivosEscolhidos(files) {
        for (const item of files) {
            if (item.type != "application/pdf") {
                this.mostrarAlertaFalha = true;
            }
        }
        if (!this.mostrarAlertaFalha) {
            this.receberArquivos(files);
        }
    }
    receberArquivos(files) {
        if (usuarioL === undefined) {
            console.log("Faça o login");
            this.router.navigate(['login']);
        }
        else {
            console.log(usuarioL);
            this.usuarioService.getByLogin(usuarioL.login).subscribe(data => this.setIdUsuario(data.id));
        }
        if (!this.desativado) {
            this.fecharAlertaSucesso();
            for (const item of files) {
                this.files.push({ data: item, inProgress: false, progress: 0 });
            }
        }
    }
    enviarArquivos(files) {
        const formData = new FormData();
        for (const file of files) {
            formData.append('files', file.data);
            file.inProgress = true;
        }
        this.rest.salvarUpload(formData, this.idUsuario).subscribe((event) => {
            console.log(event);
            if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].Response) {
                this.uploadingFile = "none";
                this.uploadProgress = 0;
                this.files = [];
                this.myFileInput.nativeElement.value = "";
                this.desativado = false;
                this.mostrarAlertaSucesso = true;
                this.gif = false;
            }
            else if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].UploadProgress) {
                this.uploadingFile = "block";
                const percentDone = Math.round((event.loaded * 100) / event.total);
                if (percentDone === 100) {
                    this.gif = true;
                }
                this.uploadProgress = percentDone;
                this.desativado = true;
            }
        }, error => {
            alert("Erro ao enviar documentos");
            this.gif = false;
            this.desativado = false;
            this.uploadingFile = "none";
            this.uploadProgress = 0;
            this.files = [];
            this.myFileInput.nativeElement.value = "";
            console.log(error);
        });
    }
    setIdUsuario(id) {
        this.idUsuario = id;
    }
};
FormDocumentoComponent.ctorParameters = () => [
    { type: _service_documento_service__WEBPACK_IMPORTED_MODULE_10__["DocumentoService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] }
];
FormDocumentoComponent.propDecorators = {
    myFileInput: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["ViewChild"], args: ['fileDropRef', { static: false },] }]
};
FormDocumentoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-form-documento',
        template: _raw_loader_form_documento_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_form_documento_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_documento_service__WEBPACK_IMPORTED_MODULE_10__["DocumentoService"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"], _service_usuario_service__WEBPACK_IMPORTED_MODULE_11__["UsuarioService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
], FormDocumentoComponent);



/***/ }),

/***/ "IEbr":
/*!********************************************************!*\
  !*** ./src/app/modal-aviso/modal-aviso.component.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtb2RhbC1hdmlzby5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "IOBM":
/*!**********************************************************!*\
  !*** ./src/app/tipo-documentos/tipo-documentos.model.ts ***!
  \**********************************************************/
/*! exports provided: TiposDocumentos, getRoleIdentifier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TiposDocumentos", function() { return TiposDocumentos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRoleIdentifier", function() { return getRoleIdentifier; });
class TiposDocumentos {
    constructor(id, nomeDocumento, escritorioContabil, categoria) {
        this.id = id;
        this.nomeDocumento = nomeDocumento;
        this.escritorioContabil = escritorioContabil;
        this.categoria = categoria;
    }
}
function getRoleIdentifier(tiposDocumentos) {
    return tiposDocumentos.id;
}


/***/ }),

/***/ "Ie/P":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastrar-empresa/cadastrar-empresa.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div>\r\n  <button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n  <main class=\"main d-flex align-items-center mt-4\">\r\n\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 mx-auto\">\r\n          <div class=\"card mx-4\">\r\n            <div class=\"card-body p-4\">\r\n              <form [formGroup]=\"empresaForm\">\r\n                <h1 *ngIf=\"(!editando)\" >Cadastrar Empresa</h1>\r\n                <h1 *ngIf=\"(editando)\" > Editar Empresa</h1>\r\n                <p *ngIf=\"(!editando)\" class=\"text-muted\">Crie a empresa</p>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <select *ngIf=\"(!editando)\"  class=\"form-control\" name=\"escritorio\"\r\n                  [class.is-invalid]=\"empresaForm.get('escritorio').invalid && (empresaForm.get('escritorio').dirty || empresaForm.get('escritorio').touched)\" formControlName=\"escritorio\">\r\n                    <option value=\"\" disabled selected> CNPJ do Escritório </option>\r\n                    <option *ngFor=\"let escritorio of escritorios\" [ngValue]=\"escritorio.razaoSocial\"> {{escritorio.cnpj}} </option>\r\n                  </select>\r\n                  <div class=\"invalid-feedback\">\r\n                    Selecione um escritorio.\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cnpj\"\r\n                    [class.is-invalid]=\"empresaForm.get('cnpj').invalid && (empresaForm.get('cnpj').dirty || empresaForm.get('cnpj').touched)\"\r\n                    placeholder=\"CNPJ\" formControlName=\"cnpj\" (blur)=\"cnpjKeyPressEvent($event)\" mask=\"00.000.000/0000-00\">\r\n                  <div class=\"invalid-feedback\">\r\n                    <div *ngIf=\"empresaForm.get('cnpj').errors?.required\">  O campo CNPJ é obrigatório. Deve ter 11 dígitos. </div>\r\n                    <div *ngIf=\"empresaForm.get('cnpj').errors?.duplicado && empresaForm.get('cnpj').dirty\">  CNPJ já existe, por favor insira outro. </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"razao_social\" placeholder=\"Razão Social da empresa\"\r\n                    formControlName=\"razao_social\">\r\n                </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"nome_fantasia\" placeholder=\"Nome Fantasia da empresa\"\r\n                    formControlName=\"nome_fantasia\" disabled>\r\n                </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"inscricao_estadual\" placeholder=\"Inscrição Estadual\" formControlName=\"inscricao_estadual\"\r\n                  [class.is-invalid]=\"empresaForm.get('inscricao_estadual').invalid && (empresaForm.get('inscricao_estadual').dirty || empresaForm.get('inscricao_estadual').touched)\" (blur)=\"inscricaoKeyPressEvent($event)\"\r\n                  mask=\"0000000000000\">\r\n                  <div class=\"invalid-feedback\">\r\n                    <div *ngIf=\"empresaForm.get('inscricao_estadual').errors?.required\">O campo Inscrição Estadual é obrigatório.</div>\r\n                    <div *ngIf=\"empresaForm.get('inscricao_estadual').errors?.duplicado && empresaForm.get('inscricao_estadual').dirty\">Inscrição Estadual já existe, por favor insira outro.</div>\r\n                  </div>\r\n                </div>\r\n\r\n\r\n                <div formGroupName=\"endereco\" style=\"border: none;\">\r\n                  <div class=\"input-group mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"cep\" placeholder=\"CEP\" [class.is-invalid]=\"empresaForm.get('endereco.cep').invalid && (empresaForm.get('endereco.cep').dirty || empresaForm.get('endereco.cep').touched)\" formControlName=\"cep\"  mask=\"00000-000\" >\r\n                    <div class=\"invalid-feedback\">\r\n                      O campo CEP é obrigatório. Deve ter 8 dígitos.\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"input-group mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"rua\" placeholder=\"Rua\" formControlName=\"rua\">\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"input-group col-8 mb-3\">\r\n                      <input type=\"text\" class=\"form-control\" name=\"bairro\" placeholder=\"Bairro\" formControlName=\"bairro\">\r\n                    </div>\r\n                    <div class=\"input-group col mb-3\">\r\n                      <input type=\"number\" class=\"form-control\" name=\"numero\" placeholder=\"Número\" formControlName=\"numero\">\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"input-group mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"complemento\" placeholder=\"Complemento\" formControlName=\"complemento\">\r\n                  </div>\r\n                  </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"telefone\" placeholder=\"Contato\"\r\n                    [class.is-invalid]=\"empresaForm.get('telefone').invalid && (empresaForm.get('telefone').dirty || empresaForm.get('telefone').touched)\" formControlName=\"telefone\" mask=\"(00) 0000-0000\">\r\n                    <div class=\"invalid-feedback\">\r\n                      O campo Contato é obrigatório.\r\n                    </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon\">@</i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Email\"\r\n                  [class.is-invalid]=\"empresaForm.get('email').invalid && (empresaForm.get('email').dirty || empresaForm.get('email').touched)\" formControlName=\"email\">\r\n                  <div class=\"invalid-feedback\">\r\n                    Insira um email válido.\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"row\">\r\n                  <div class=\"input-group mb-3 col-7\">\r\n                      <input type=\"text\" class=\"form-control\" name=\"descricao_primar\" placeholder=\"Atividade Primária\"\r\n                      formControlName=\"descricao_primar\" disabled>\r\n                  </div>\r\n                  <div class=\"input-group mb-3 col-5\">\r\n                      <input type=\"text\" class=\"form-control\" name=\"ativ_primar_cnae_\" placeholder=\"Código CNAE\"\r\n                      formControlName=\"ativ_primar_cnae_\" disabled>\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"input-group mb-3 col-7\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"descricao_secund\" placeholder=\"Atividade Secundária\"\r\n                    formControlName=\"descricao_secund\" disabled>\r\n                  </div>\r\n                  <div class=\"input-group mb-3 col-5\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"ativ_secund_cnae_\"\r\n                    placeholder=\"Código CNAE\" formControlName=\"ativ_secund_cnae_\" disabled>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <select class=\"form-control\" nome=\"regime_tributario\" [class.is-invalid]=\"empresaForm.get('regime_tributario').invalid && (empresaForm.get('regime_tributario').dirty || empresaForm.get('regime_tributario').touched)\" formControlName=\"regime_tributario\">\r\n                    <option value=\"\" disabled selected> Regime Tributário</option>\r\n                    <option *ngFor=\"let r of regime\" [value]=\"r\"> {{regimeTributario[r]}} </option>\r\n                </select>\r\n                  <div class=\"invalid-feedback\">\r\n                    Selecione o regime tributário.\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <select class=\"form-control\" nome=\"natureza_juridica\" [class.is-invalid]=\"empresaForm.get('natureza_juridica').invalid && (empresaForm.get('natureza_juridica').dirty || empresaForm.get('natureza_juridica').touched)\" formControlName=\"natureza_juridica\">\r\n                    <option value=\"\" disabled selected> Natureza Jurídica </option>\r\n                    <option *ngFor=\"let n of natureza\" [value]=\"n\"> {{naturezaJuridica[n]}} </option>\r\n                  </select>\r\n                  <div class=\"invalid-feedback\">\r\n                    Selecione a natureza jurídica.\r\n                  </div>\r\n                </div>\r\n\r\n                <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" name=\"nome\"\r\n                    [class.is-invalid]=\"empresaForm.get('nome').invalid && (empresaForm.get('nome').dirty || empresaForm.get('nome').touched)\"\r\n                    placeholder=\"Nome do Solicitante\" formControlName=\"nome\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Nome é obrigatório.\r\n                  </div>\r\n                </div>\r\n                <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cpf\" placeholder=\"CPF do Solicitante\" [class.is-invalid]=\"empresaForm.get('cpf').invalid && (empresaForm.get('cpf').dirty || empresaForm.get('cpf').touched)\"\r\n                    formControlName=\"cpf\" (blur)=\"cpfKeyPressEvent($event)\"  mask=\"000.000.000-00\">\r\n                    <div class=\"invalid-feedback\">\r\n                      <div *ngIf=\"empresaForm.get('cpf').errors?.required\"> O campo CPF é obrigatório.</div>\r\n                      <div *ngIf=\"empresaForm.get('cpf').errors?.duplicado && empresaForm.get('cpf').dirty\"> CPF já existe, por favor insira outro.</div>\r\n                    </div>\r\n                </div>\r\n                <!--div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"endereco\" placeholder=\"Endereço da empresa\"\r\n                    formControlName=\"endereco\" disabled>\r\n                </div-->\r\n\r\n                <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon\">@</i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Email do Solicitante\"\r\n                  [class.is-invalid]=\"empresaForm.get('emailS').invalid && (empresaForm.get('emailS').dirty || empresaForm.get('emailS').touched)\" formControlName=\"emailS\">\r\n                    <div class=\"invalid-feedback\">\r\n                      Insira um email válido.\r\n                    </div>\r\n                </div>\r\n\r\n                <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"celular\" placeholder=\"Celular do Solicitante\" [class.is-invalid]=\"empresaForm.get('celular').invalid && (empresaForm.get('celular').dirty || empresaForm.get('celular').touched)\"\r\n                    formControlName=\"celular\" mask=\"(00) 00000-0000\">\r\n                    <div class=\"invalid-feedback\">\r\n                      O campo Celular é obrigatório.\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"(!editando)\" class=\"form-check mb-3\">\r\n                  <input class=\"form-check-input\" type=\"checkbox\" value=\"aceito\" id=\"checkbox1\"\r\n                    formControlName=\"termo_condicoes\">\r\n                  <label class=\"form-check-label\" for=\"checkbox1\">\r\n                    Aceito os Termos e as Condições.\r\n                  </label>\r\n                </div>\r\n                <button *ngIf=\"(!editando)\" type=\"button\" class=\"btn btn-block btn-success\"\r\n                  (click)=\"cadastrar()\" [disabled]=\"empresaForm.invalid\">Cadastrar</button>\r\n                <button *ngIf=\"(editando)\" type=\"button\" class=\"btn btn-block btn-success\"\r\n                (click)=\"editar()\">Editar</button>\r\n              </form>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n");

/***/ }),

/***/ "KGs1":
/*!******************************************************!*\
  !*** ./src/app/modal-aviso/modal-aviso.component.ts ***!
  \******************************************************/
/*! exports provided: ModalAvisoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalAvisoComponent", function() { return ModalAvisoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_modal_aviso_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./modal-aviso.component.html */ "UoEy");
/* harmony import */ var _modal_aviso_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal-aviso.component.scss */ "IEbr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");





let ModalAvisoComponent = class ModalAvisoComponent {
    constructor(modalService) {
        this.modalService = modalService;
    }
};
ModalAvisoComponent.ctorParameters = () => [
    { type: ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalService"] }
];
ModalAvisoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-modal-aviso',
        template: _raw_loader_modal_aviso_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_modal_aviso_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_4__["BsModalService"]])
], ModalAvisoComponent);



/***/ }),

/***/ "Ko81":
/*!********************************************************************!*\
  !*** ./src/app/listar-documentos/listar-documentos.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mostrar {\n  margin-bottom: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGxpc3Rhci1kb2N1bWVudG9zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQUE7QUFDRiIsImZpbGUiOiJsaXN0YXItZG9jdW1lbnRvcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tb3N0cmFye1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "L736":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastrar-colaborador-empresa/cadastrar-colaborador-empresa.component.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n<main class=\"main d-flex align-items-center mt-4\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"usuarioForm\">\r\n              <h1 >Cadastrar Usuário</h1>\r\n              <p  class=\"text-muted\">Crie o usuário</p>\r\n              <div *ngIf=\"(admin) || (admin_escritorio)\" class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\" formControlName=\"escritorio\">\r\n                    <option value='' disabled selected>Empresa</option>\r\n                    <option *ngFor=\"let empresa of empresas\" [ngValue]=\"empresa.id\"> {{empresa.pjId.cnpj}} </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"nome\" [class.is-invalid]=\"usuarioForm.get('nome').invalid && (usuarioForm.get('nome').dirty || usuarioForm.get('nome').touched)\" placeholder=\"Nome Completo\" formControlName=\"nome\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Nome é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"rg\" placeholder=\"RG\" formControlName=\"rg\">\r\n                </div>\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cpf\" [class.is-invalid]=\"usuarioForm.get('cpf').invalid && (usuarioForm.get('cpf').dirty || usuarioForm.get('cpf').touched)\" placeholder=\"CPF\" formControlName=\"cpf\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo CPF é obrigatório.\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-phone\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"celular\" placeholder=\"Celular\" formControlName=\"celular\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\">@</span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"email\" [class.is-invalid]=\"usuarioForm.get('email').invalid && (usuarioForm.get('email').dirty || usuarioForm.get('email').touched)\" placeholder=\"Email\" formControlName=\"email\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Email é obrigatório.\r\n                </div>\r\n              </div>\r\n              \r\n              <div formGroupName=\"endereco\" style=\"border: none;\">\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cep\" placeholder=\"CEP\" formControlName=\"cep\" (blur)=\"consultaCep($event.target.value)\" [class.is-invalid]=\"usuarioForm.get('endereco').get('cep').invalid && (usuarioForm.get('endereco').get('cep').dirty || usuarioForm.get('endereco').get('cep').touched)\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo CEP é obrigatório.\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"input-group col-8 mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"rua\" placeholder=\"Rua\" formControlName=\"rua\">\r\n                  </div>\r\n                  <div class=\"input-group col mb-3\">\r\n                    <input type=\"number\" class=\"form-control\" name=\"numero\" placeholder=\"Número\" formControlName=\"numero\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"bairro\" placeholder=\"Bairro\" formControlName=\"bairro\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cidade\" placeholder=\"Cidade\" formControlName=\"cidade\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"estado\" placeholder=\"Estado\" formControlName=\"estado\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"pais\" placeholder=\"Pais\" formControlName=\"pais\">\r\n                </div>\r\n                \r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"complemento\" placeholder=\"Complemento\" formControlName=\"complemento\">\r\n                </div>\r\n              </div>\r\n                <!-- <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" name=\"login\" [class.is-invalid]=\"usuarioForm.get('login').invalid && (usuarioForm.get('login').dirty || usuarioForm.get('login').touched)\" placeholder=\"Login\" formControlName=\"login\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Login é obrigatório.\r\n                  </div>\r\n                </div> -->\r\n                <!-- <div  class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" name=\"senha\" [class.is-invalid]=\"usuarioForm.get('senha').invalid && (usuarioForm.get('senha').dirty || usuarioForm.get('senha').touched)\" placeholder=\"Senha\" formControlName=\"senha\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Senha é obrigatório.\r\n                  </div>\r\n                </div> -->\r\n                <!-- <div  class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" name=\"confirmar_senha\" [class.is-invalid]=\"usuarioForm.get('confirmar_senha').value != usuarioForm.get('senha').value\" placeholder=\"Confirmar Senha\" formControlName=\"confirmar_senha\">\r\n                  <div class=\"invalid-feedback\">\r\n                    As senhas digitadas não coincidem\r\n                  </div>\r\n                </div> -->\r\n                <div class=\"row\">\r\n                  <div class=\"input-group col mb-3\">\r\n                    <select class=\"form-control\">\r\n                      <option value='' disabled selected>Nível de Acesso</option>\r\n                      <option *ngFor=\"let nivel of niveis\" [value]=\"nivel\"> {{nivelDeAcesso[nivel]}} </option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n                <!-- <button  type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(usuarioForm)\" [disabled]=\"usuarioForm.invalid || usuarioForm.get('confirmar_senha').value != usuarioForm.get('senha').value\">Cadastrar</button> -->\r\n                <button  type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(usuarioForm)\" [disabled]=\"usuarioForm.invalid\">Cadastrar</button>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n\r\n");

/***/ }),

/***/ "L9FF":
/*!*************************************************************************************!*\
  !*** ./src/app/tipo-documentos/cadastrar/cadastrar-tipos-documentos.component.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItdGlwb3MtZG9jdW1lbnRvcy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "LMm3":
/*!******************************************************!*\
  !*** ./src/app/form-debug/form-debug.component.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb3JtLWRlYnVnLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "MbNM":
/*!************************************************************************!*\
  !*** ./src/app/categoria/cadastrar/cadastrar-categoria.component.scss ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItY2F0ZWdvcmlhLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "Nv4Z":
/*!*********************************************************************!*\
  !*** ./src/app/tipo-documentos/service/tipos-documentos.service.ts ***!
  \*********************************************************************/
/*! exports provided: TiposDocumentosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TiposDocumentosService", function() { return TiposDocumentosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app-constants */ "EKKv");




let TiposDocumentosService = class TiposDocumentosService {
    constructor(http) {
        this.http = http;
    }
    create(tipoDocumento) {
        return this.http.post(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}novo`, tipoDocumento);
    }
    update(tipoDocumento, id) {
        return this.http.put(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}editar/${id}`, tipoDocumento);
    }
    delete(id) {
        return this.http.delete(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}delete/${id}`);
    }
    findById(id) {
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}find/${id}`);
    }
    findAllByEscritorioId(id) {
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}find/escritorio/${id}`);
    }
    findAllByUsuarioEscritorio(id) {
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}find/usuario-escritorio/${id}`);
    }
    findAll(id) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        params = params.set('id', id);
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlTipoDocumentos}find/all`, { params: params });
    }
};
TiposDocumentosService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
TiposDocumentosService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], TiposDocumentosService);



/***/ }),

/***/ "ODnz":
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastrar-colaborador-escritorio/cadastrar-colaborador-escritorio.component.html ***!
  \****************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n<main class=\"main d-flex align-items-center mt-4\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"usuarioForm\">\r\n              <h1 *ngIf=\"(!editando)\">Cadastrar Usuário</h1>\r\n              <h1 *ngIf=\"(editando)\"> Editar Usuário </h1>\r\n              <p *ngIf=\"(!editando)\" class=\"text-muted\">Crie o usuário</p>\r\n              <div *ngIf=\"(admin)\" class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\" formControlName=\"escritorio\">\r\n                    <option value='' disabled selected>Escritório</option>\r\n                    <option *ngFor=\"let escritorio of escritorios\" [ngValue]=\"escritorio.id\"> {{escritorio.cnpj}} </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"nome\" [class.is-invalid]=\"usuarioForm.get('nome').invalid && (usuarioForm.get('nome').dirty || usuarioForm.get('nome').touched)\" placeholder=\"Nome Completo\" formControlName=\"nome\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Nome é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"rg\" placeholder=\"RG\" formControlName=\"rg\">\r\n                </div>\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cpf\" [class.is-invalid]=\"usuarioForm.get('cpf').invalid && (usuarioForm.get('cpf').dirty || usuarioForm.get('cpf').touched)\" placeholder=\"CPF\" formControlName=\"cpf\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo CPF é obrigatório. Deve ter 11 dígitos.\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-phone\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"celular\" placeholder=\"Celular\" formControlName=\"celular\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\">@</span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"email\" [class.is-invalid]=\"usuarioForm.get('email').invalid && (usuarioForm.get('email').dirty || usuarioForm.get('email').touched)\" placeholder=\"Email\" formControlName=\"email\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Email é obrigatório.\r\n                </div>\r\n              </div>\r\n              \r\n              <div formGroupName=\"endereco\" style=\"border: none;\">\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cep\" placeholder=\"CEP\" formControlName=\"cep\" (blur)=\"consultaCep($event.target.value)\" [class.is-invalid]=\"usuarioForm.get('endereco').get('cep').invalid && (usuarioForm.get('endereco').get('cep').dirty || usuarioForm.get('endereco').get('cep').touched)\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo CEP é obrigatório.\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"input-group col-8 mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"rua\" placeholder=\"Rua\" formControlName=\"rua\">\r\n                  </div>\r\n                  <div class=\"input-group col mb-3\">\r\n                    <input type=\"number\" class=\"form-control\" name=\"numero\" placeholder=\"Número\" formControlName=\"numero\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"bairro\" placeholder=\"Bairro\" formControlName=\"bairro\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cidade\" placeholder=\"Cidade\" formControlName=\"cidade\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"estado\" placeholder=\"Estado\" formControlName=\"estado\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"pais\" placeholder=\"Pais\" formControlName=\"pais\">\r\n                </div>\r\n                \r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"complemento\" placeholder=\"Complemento\" formControlName=\"complemento\">\r\n                </div>\r\n              </div>\r\n                <!-- <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" name=\"login\" [class.is-invalid]=\"usuarioForm.get('login').invalid && (usuarioForm.get('login').dirty || usuarioForm.get('login').touched)\" placeholder=\"Login\" formControlName=\"login\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Login é obrigatório.\r\n                  </div>\r\n                </div> -->\r\n                <!-- <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" name=\"senha\" [class.is-invalid]=\"usuarioForm.get('senha').invalid && (usuarioForm.get('senha').dirty || usuarioForm.get('senha').touched)\" placeholder=\"Senha\" formControlName=\"senha\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Senha é obrigatório.\r\n                  </div>\r\n                </div> -->\r\n                <!-- <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" name=\"confirmar_senha\" [class.is-invalid]=\"usuarioForm.get('confirmar_senha').value != usuarioForm.get('senha').value\" placeholder=\"Confirmar Senha\" formControlName=\"confirmar_senha\">\r\n                  <div class=\"invalid-feedback\">\r\n                    As senhas digitadas não coincidem\r\n                  </div>\r\n                </div> -->\r\n                <div class=\"row\">\r\n                  <div class=\"input-group col mb-3\">\r\n                    <select class=\"form-control\">\r\n                      <option value='' disabled selected>Nível de Acesso</option>\r\n                      <option *ngFor=\"let nivel of niveis\" [value]=\"nivel\"> {{nivelDeAcesso[nivel]}} </option>\r\n                    </select>\r\n                  </div>\r\n                </div>\r\n                <!-- <button *ngIf=\"(!editando)\" type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(usuarioForm)\" [disabled]=\"usuarioForm.invalid || usuarioForm.get('confirmar_senha').value != usuarioForm.get('senha').value\">Cadastrar</button> -->\r\n                <button *ngIf=\"(!editando)\" type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(usuarioForm)\" [disabled]=\"usuarioForm.invalid\">Cadastrar</button>\r\n                <button *ngIf=\"(editando)\" type=\"button\" class=\"btn btn-block btn-success\" (click)=\"editar(usuarioForm)\" [disabled]=\"usuarioForm.invalid\">Editar</button>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n");

/***/ }),

/***/ "P+xf":
/*!**************************************************************!*\
  !*** ./src/app/form-documento/form-documento.component.scss ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("main {\n  margin-top: 5%;\n}\n\n.dropzone {\n  min-width: 100px;\n  min-height: 250px;\n  padding: 25% 0 25% 0;\n  text-align: center;\n  border: dashed 3px #979797;\n  position: relative;\n  margin: 5% auto 0 auto;\n}\n\n.dropzone input {\n  opacity: 0;\n  position: absolute;\n  z-index: 2;\n  width: 100%;\n  height: 100%;\n  top: 0;\n  left: 0;\n  cursor: pointer;\n}\n\n.dropzone label {\n  color: white;\n  min-width: 100px;\n  height: 44px;\n  border-radius: 21.5px;\n  background-color: #db202f;\n  padding: 8px 16px;\n}\n\n.dropzone h3 {\n  font-size: 20px;\n  font-weight: 600;\n  color: #38424c;\n}\n\n.fileover {\n  -webkit-animation: shake 1s;\n          animation: shake 1s;\n  -webkit-animation-iteration-count: infinite;\n          animation-iteration-count: infinite;\n}\n\n@-webkit-keyframes shake {\n  0% {\n    transform: translate(1px, 1px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  10% {\n    transform: translate(-1px, -2px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n  20% {\n    transform: translate(-3px, 0px) rotate(1deg);\n    border: solid 3px #979797;\n  }\n  30% {\n    transform: translate(3px, 2px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  40% {\n    transform: translate(1px, -1px) rotate(1deg);\n    border: solid 3px #979797;\n  }\n  50% {\n    transform: translate(-1px, 2px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n  60% {\n    transform: translate(-3px, 1px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  70% {\n    transform: translate(3px, 1px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n  80% {\n    transform: translate(-1px, -1px) rotate(1deg);\n    border: solid 3px #979797;\n  }\n  90% {\n    transform: translate(1px, 2px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  100% {\n    transform: translate(1px, -2px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n}\n\n@keyframes shake {\n  0% {\n    transform: translate(1px, 1px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  10% {\n    transform: translate(-1px, -2px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n  20% {\n    transform: translate(-3px, 0px) rotate(1deg);\n    border: solid 3px #979797;\n  }\n  30% {\n    transform: translate(3px, 2px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  40% {\n    transform: translate(1px, -1px) rotate(1deg);\n    border: solid 3px #979797;\n  }\n  50% {\n    transform: translate(-1px, 2px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n  60% {\n    transform: translate(-3px, 1px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  70% {\n    transform: translate(3px, 1px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n  80% {\n    transform: translate(-1px, -1px) rotate(1deg);\n    border: solid 3px #979797;\n  }\n  90% {\n    transform: translate(1px, 2px) rotate(0deg);\n    border: solid 3px #979797;\n  }\n  100% {\n    transform: translate(1px, -2px) rotate(-1deg);\n    border: solid 3px #979797;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGZvcm0tZG9jdW1lbnRvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtBQUNGOztBQUNBO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUFFRjs7QUFBRTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsZUFBQTtBQUVKOztBQUNFO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUdFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQURKOztBQU9BO0VBQ0UsMkJBQUE7VUFBQSxtQkFBQTtFQUNBLDJDQUFBO1VBQUEsbUNBQUE7QUFKRjs7QUFPQTtFQUVFO0lBQ0UsMkNBQUE7SUFDQSx5QkFBQTtFQUxGO0VBUUE7SUFDRSw4Q0FBQTtJQUNBLHlCQUFBO0VBTkY7RUFTQTtJQUNFLDRDQUFBO0lBQ0EseUJBQUE7RUFQRjtFQVVBO0lBQ0UsMkNBQUE7SUFDQSx5QkFBQTtFQVJGO0VBV0E7SUFDRSw0Q0FBQTtJQUNBLHlCQUFBO0VBVEY7RUFZQTtJQUNFLDZDQUFBO0lBQ0EseUJBQUE7RUFWRjtFQWFBO0lBQ0UsNENBQUE7SUFDQSx5QkFBQTtFQVhGO0VBY0E7SUFDRSw0Q0FBQTtJQUNBLHlCQUFBO0VBWkY7RUFlQTtJQUNFLDZDQUFBO0lBQ0EseUJBQUE7RUFiRjtFQWdCQTtJQUNFLDJDQUFBO0lBQ0EseUJBQUE7RUFkRjtFQWlCQTtJQUNFLDZDQUFBO0lBQ0EseUJBQUE7RUFmRjtBQUNGOztBQXhDQTtFQUVFO0lBQ0UsMkNBQUE7SUFDQSx5QkFBQTtFQUxGO0VBUUE7SUFDRSw4Q0FBQTtJQUNBLHlCQUFBO0VBTkY7RUFTQTtJQUNFLDRDQUFBO0lBQ0EseUJBQUE7RUFQRjtFQVVBO0lBQ0UsMkNBQUE7SUFDQSx5QkFBQTtFQVJGO0VBV0E7SUFDRSw0Q0FBQTtJQUNBLHlCQUFBO0VBVEY7RUFZQTtJQUNFLDZDQUFBO0lBQ0EseUJBQUE7RUFWRjtFQWFBO0lBQ0UsNENBQUE7SUFDQSx5QkFBQTtFQVhGO0VBY0E7SUFDRSw0Q0FBQTtJQUNBLHlCQUFBO0VBWkY7RUFlQTtJQUNFLDZDQUFBO0lBQ0EseUJBQUE7RUFiRjtFQWdCQTtJQUNFLDJDQUFBO0lBQ0EseUJBQUE7RUFkRjtFQWlCQTtJQUNFLDZDQUFBO0lBQ0EseUJBQUE7RUFmRjtBQUNGIiwiZmlsZSI6ImZvcm0tZG9jdW1lbnRvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWFpbntcclxuICBtYXJnaW4tdG9wOiA1JTtcclxufVxyXG4uZHJvcHpvbmV7XHJcbiAgbWluLXdpZHRoOiAxMDBweDtcclxuICBtaW4taGVpZ2h0OiAyNTBweDtcclxuICBwYWRkaW5nOiAyNSUgMCAyNSUgMDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgYm9yZGVyOiBkYXNoZWQgM3B4ICM5Nzk3OTc7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIG1hcmdpbjogNSUgYXV0byAwIGF1dG87XHJcblxyXG4gIGlucHV0IHtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuXHJcbiAgbGFiZWwge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWluLXdpZHRoOiAxMDBweDtcclxuICAgIGhlaWdodDogNDRweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIxLjVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkYjIwMmY7XHJcbiAgICBwYWRkaW5nOiA4cHggMTZweDtcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgaDMge1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGNvbG9yOiAjMzg0MjRjO1xyXG4gIH1cclxuXHJcbiAgICBcclxufVxyXG5cclxuLmZpbGVvdmVyIHtcclxuICBhbmltYXRpb246IHNoYWtlIDFzO1xyXG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xyXG59XHJcblxyXG5Aa2V5ZnJhbWVzIHNoYWtlIHtcclxuICBcclxuICAwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxcHgsIDFweCkgcm90YXRlKDBkZWcpO1xyXG4gICAgYm9yZGVyOiBzb2xpZCAzcHggIzk3OTc5NztcclxuICB9XHJcblxyXG4gIDEwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMXB4LCAtMnB4KSByb3RhdGUoLTFkZWcpO1xyXG4gICAgYm9yZGVyOiBzb2xpZCAzcHggIzk3OTc5NztcclxuICB9XHJcblxyXG4gIDIwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtM3B4LCAwcHgpIHJvdGF0ZSgxZGVnKTtcclxuICAgIGJvcmRlcjogc29saWQgM3B4ICM5Nzk3OTc7XHJcbiAgfVxyXG5cclxuICAzMCUge1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoM3B4LCAycHgpIHJvdGF0ZSgwZGVnKTtcclxuICAgIGJvcmRlcjogc29saWQgM3B4ICM5Nzk3OTc7XHJcbiAgfVxyXG5cclxuICA0MCUge1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMXB4LCAtMXB4KSByb3RhdGUoMWRlZyk7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjOTc5Nzk3O1xyXG4gIH1cclxuXHJcbiAgNTAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC0xcHgsIDJweCkgcm90YXRlKC0xZGVnKTtcclxuICAgIGJvcmRlcjogc29saWQgM3B4ICM5Nzk3OTc7XHJcbiAgfVxyXG5cclxuICA2MCUge1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTNweCwgMXB4KSByb3RhdGUoMGRlZyk7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjOTc5Nzk3O1xyXG4gIH1cclxuXHJcbiAgNzAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDNweCwgMXB4KSByb3RhdGUoLTFkZWcpO1xyXG4gICAgYm9yZGVyOiBzb2xpZCAzcHggIzk3OTc5NztcclxuICB9XHJcblxyXG4gIDgwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMXB4LCAtMXB4KSByb3RhdGUoMWRlZyk7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjOTc5Nzk3O1xyXG4gIH1cclxuXHJcbiAgOTAlIHtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKDFweCwgMnB4KSByb3RhdGUoMGRlZyk7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjOTc5Nzk3O1xyXG4gIH1cclxuXHJcbiAgMTAwJSB7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxcHgsIC0ycHgpIHJvdGF0ZSgtMWRlZyk7XHJcbiAgICBib3JkZXI6IHNvbGlkIDNweCAjOTc5Nzk3O1xyXG4gIH1cclxufSJdfQ== */");

/***/ }),

/***/ "P/kj":
/*!******************************************!*\
  !*** ./src/app/enum/RegimeTributario.ts ***!
  \******************************************/
/*! exports provided: RegimeTributario */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegimeTributario", function() { return RegimeTributario; });
var RegimeTributario;
(function (RegimeTributario) {
    RegimeTributario["SIMPLES"] = "Simples Nacional";
    RegimeTributario["SIMPLESM"] = "Simples Nacional (MEI)";
    RegimeTributario["LUCRO"] = "Lucro Real";
    RegimeTributario["LUCROP"] = "Lucro Presumido";
})(RegimeTributario || (RegimeTributario = {}));


/***/ }),

/***/ "PN/7":
/*!********************************!*\
  !*** ./src/app/enum/Status.ts ***!
  \********************************/
/*! exports provided: Status */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return Status; });
var Status;
(function (Status) {
    Status["AGUARDANDO"] = "Aguardando";
    Status["ENVIADO"] = "Enviado";
})(Status || (Status = {}));


/***/ }),

/***/ "Pg0y":
/*!******************************************!*\
  !*** ./src/app/enum/NaturezaJuridica.ts ***!
  \******************************************/
/*! exports provided: NaturezaJuridica */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NaturezaJuridica", function() { return NaturezaJuridica; });
var NaturezaJuridica;
(function (NaturezaJuridica) {
    NaturezaJuridica["MEI"] = "MEI (Microempreendedor Individual)";
    NaturezaJuridica["EI"] = "EI (Empres\u00E1rio Individual)";
    NaturezaJuridica["LTDA"] = "LTADA (Sociedade Simples Limitada)";
    NaturezaJuridica["SA"] = "SA (Sociedade An\u00F4nima)";
    NaturezaJuridica["SLU"] = "SLU (Sociedade Limitada Unipessoal)";
})(NaturezaJuridica || (NaturezaJuridica = {}));


/***/ }),

/***/ "QX6l":
/*!**************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.ts ***!
  \**************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_dashboard_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./dashboard.component.html */ "H/d9");
/* harmony import */ var _dashboard_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.component.css */ "VKVo");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _service_mensagem_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/mensagem.service */ "02KH");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");
/* harmony import */ var _nav__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_nav */ "c2Qq");








let usuarioL = null;
let DashboardComponent = class DashboardComponent {
    constructor(router, mensagemService, usuarioService) {
        this.router = router;
        this.mensagemService = mensagemService;
        this.usuarioService = usuarioService;
        this.sidebarMinimized = false;
        this.navItems = _nav__WEBPACK_IMPORTED_MODULE_7__["navItems"];
        this.navItemsEsc = _nav__WEBPACK_IMPORTED_MODULE_7__["navItemsEsc"];
        this.navItemsEmp = _nav__WEBPACK_IMPORTED_MODULE_7__["navItemsEmp"];
    }
    ngOnInit() {
        this.setUsuarioL(JSON.parse(localStorage.getItem('usuarioL')));
        this.setRole(localStorage.getItem('user_role'));
        this.url = window.location.pathname;
        this.receberMensagens();
    }
    toggleMinimize(e) {
        this.sidebarMinimized = e;
    }
    receberMensagens() {
        usuarioL = JSON.parse(localStorage.getItem('usuario'));
        this.usuarioService.getByLogin(usuarioL.login).subscribe(dados => {
            this.mensagemService.receberMensagem(dados.id).subscribe(data => {
                console.log(data);
                let msgNLida = [];
                data.forEach((item) => {
                    if (item.status === false) {
                        msgNLida.push(item);
                    }
                });
                this.setNMensagens(msgNLida.length);
            });
        });
    }
    mensagem() {
        usuarioL = JSON.parse(localStorage.getItem('usuario'));
        console.log(usuarioL);
        this.usuarioService.getByLogin(usuarioL.login).subscribe(data => {
            console.log(data);
            this.setIdUsuario(data.id);
            this.router.navigate(['listar-mensagens', this.idUsuario]);
        });
    }
    setIdUsuario(id) {
        this.idUsuario = id;
    }
    setNMensagens(n) {
        this.nMensagens = n;
    }
    setRole(value) {
        this.role = value;
    }
    setUsuarioL(value) {
        this.usuarioL = value;
    }
};
DashboardComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _service_mensagem_service__WEBPACK_IMPORTED_MODULE_5__["MensagemService"] },
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"] }
];
DashboardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-dashboard',
        template: _raw_loader_dashboard_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_dashboard_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _service_mensagem_service__WEBPACK_IMPORTED_MODULE_5__["MensagemService"], _service_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"]])
], DashboardComponent);



/***/ }),

/***/ "R9d+":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/form-documento/form-documento.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main class=\"main d-flex align-items-center\">\r\n      <div class=\"container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-md-6 mx-auto\">\r\n            <div class=\"card mx-4\">\r\n              <div class=\"card-body p-4\">\r\n                <form>\r\n                  <h1>Documentos</h1>\r\n                  <!--div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('competencia').invalid && (documentoForm.get('competencia').dirty || documentoForm.get('competencia').touched)\">\r\n                      <span>O campo competência é obrigatório.</span>\r\n                  </div>\r\n                  <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('valorVencimento').invalid && (documentoForm.get('valorVencimento').dirty || documentoForm.get('valorVencimento').touched)\">\r\n                      <span>O campo valor de vencimento não pode ser vazio ou menor que 0.</span>\r\n                  </div>\r\n                  <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('categoria').invalid && (documentoForm.get('categoria').dirty || documentoForm.get('categoria').touched)\">\r\n                    <span>O campo categoria é obrigatório.</span>\r\n                  </div>\r\n                  <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('statusEnvio').invalid && (documentoForm.get('statusEnvio').dirty || documentoForm.get('statusEnvio').touched)\">\r\n                    <span>O campo status de envio é obrigatório.</span>\r\n                  </div>\r\n                  <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('usuarioId').invalid && (documentoForm.get('usuarioId').dirty || documentoForm.get('usuarioId').touched)\">\r\n                    <span>O campo usuário é obrigatório.</span>\r\n                  </div>     \r\n                  <div class=\"input-group mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"competencia\" formControlName=\"competencia\" placeholder=\"Competência\" required>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"input-group col mb-3\">\r\n                      <input type=\"number\" class=\"form-control\" formControlName=\"valorVencimento\" name=\"valor_vencimento\" placeholder=\"Valor de Vencimento\" value=\"{{0}}\" required>\r\n                    </div>\r\n                    <div class=\"input-group col mb-3\">\r\n                      <input type=\"text\" class=\"form-control\" formControlName=\"categoria\" name=\"categoria\" placeholder=\"Categoria\" required>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"input-group col mb-3\">\r\n                      <select class=\"form-control\" formControlName=\"statusEnvio\" name=\"statusEnvio\">\r\n                        <option value='' disabled selected>Status de envio</option>\r\n                        <option *ngFor=\"let name of enumKeys\" [value]=\"name\"> {{status[name]}} </option>\r\n                      </select>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"input-group col mb-3\">\r\n                      <select class=\"form-control\"  formControlName=\"usuarioId\" name=\"usuarioId\">\r\n                        <option value='' disabled selected>Usuario</option>\r\n                        <option *ngFor=\"let usuario of usuarios\" [ngValue]=\"usuario\"> {{usuario.nome}} </option>\r\n                      </select>\r\n                    </div>\r\n                  </div-->\r\n                  <div class=\"row\">\r\n                    <div class =\"iput-group col mb-3\" >\r\n                      \r\n                      <div class=\"dropzone\" appBtnDropZone (fileDropped)= \"arquivosArrastados($event)\">\r\n                        <input type=\"file\" #fileDropRef id=\"fileDropRef\"  multiple (change)=\"arquivosEscolhidos($event.target.files)\" accept=\".pdf\"/>\r\n                        <h3>Arraste e solte os arquivos aqui</h3>\r\n                        <h3>ou</h3>\r\n                        <label for=\"fileDropRef\">Busque por eles</label>\r\n                      </div>\r\n                      <div class=\"progress\" style=\"margin-top: 5px;\" [style.display]=\"uploadingFile\">\r\n                        <div class=\"progress-bar\" role=\"progressbar\" style=\"background-color: green;\" [style.width]=\"uploadProgress + '%'\"> {{uploadProgress}}%</div>\r\n                      </div>\r\n                      <div *ngIf=\"mostrarAlertaSucesso\" class=\"alert alert-success\" role=\"alert\" style=\"margin-top: 10px;\">\r\n                        Documentos enviados com sucesso!\r\n                        <button  type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"fecharAlertaSucesso()\">\r\n                          <span aria-hidden=\"true\">&times;</span>\r\n                        </button>\r\n                      </div>\r\n                      <div *ngIf=\"mostrarAlertaFalha\" class=\"alert alert-danger\" role=\"alert\" style=\"margin-top: 10px;\">\r\n                        Insira apenas arquivos .pdf!\r\n                        <button  type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"fecharAlertaFalha()\">\r\n                          <span aria-hidden=\"true\">&times;</span>\r\n                        </button>\r\n                      </div>\r\n                      <div *ngIf=\"gif\" style=\"text-align: center;\">\r\n                        <img src=\"assets/img/loading_w.gif\" alt=\"Carregando\" width=\"5%\"/>\r\n                      </div>\r\n                      <span *ngIf=\"(files.length) == 0\">Nenhum arquivo selecionado. </span>\r\n                      <span *ngIf=\"(files.length) == 1\">1 arquivo selecionado.</span>\r\n                      <span *ngIf=\"(files.length) > 1\">{{files.length}} arquivos selecionados.</span>\r\n                    </div>\r\n                  </div>\r\n                  <button type=\"button\" [disabled]=\"(files.length) == 0 || (desativado)\" class=\"btn btn-block btn-success\" (click)=\"salvar()\">Enviar</button>\r\n                </form>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n</main>\r\n  \r\n\r\n");

/***/ }),

/***/ "RCyV":
/*!**********************************************************!*\
  !*** ./src/app/aviso-pop-up/aviso-pop-up.component.scss ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhdmlzby1wb3AtdXAuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.css */ "A3xY");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");





let AppComponent = class AppComponent {
    constructor(router) {
        this.router = router;
        this.title = 'Curso-Angular-REST';
        this.items = [{ label: 'Sair', icon: 'pi pi-fw pi-power-off', }];
    }
    ngOnInit() {
        if (localStorage.getItem('token') == null) {
            this.router.navigate(['login']);
        }
    }
    sair() {
        localStorage.clear();
        this.router.navigate(['login']);
    }
    esconderBarrar() {
        if (localStorage.getItem('token') !== null &&
            localStorage.getItem('token').toString().trim() !== null) {
            return false;
        }
        else {
            return true;
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], AppComponent);



/***/ }),

/***/ "T/fq":
/*!**************************************************************************!*\
  !*** ./src/app/requisitar-documentos/requisitar-documentos.component.ts ***!
  \**************************************************************************/
/*! exports provided: RequisitarDocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequisitarDocumentosComponent", function() { return RequisitarDocumentosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_requisitar_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./requisitar-documentos.component.html */ "n5A5");
/* harmony import */ var _requisitar_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./requisitar-documentos.component.scss */ "dnP0");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _service_empresa_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/empresa.service */ "pFYR");
/* harmony import */ var _service_mensagem_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/mensagem.service */ "02KH");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");








let usuarioL;
let RequisitarDocumentosComponent = class RequisitarDocumentosComponent {
    constructor(rest, fb, mensagemSerivce, usuarioService) {
        this.rest = rest;
        this.fb = fb;
        this.mensagemSerivce = mensagemSerivce;
        this.usuarioService = usuarioService;
        this.empresas = [];
    }
    ngOnInit() {
        usuarioL = JSON.parse(localStorage.getItem('usuario'));
        this.usuarioService.getByLogin(usuarioL.login).subscribe(data => this.setIdUsuario(data.id));
        this.receberEmpresas();
        this.requisicaoForm = this.fb.group({
            destinatario: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            documento1: false,
            documento2: false,
            mensagem: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
        });
    }
    receberEmpresas() {
        this.rest.receberEmpresas("", this.idUsuario).subscribe(data => {
            console.log(data);
            let empresas = [];
            data.forEach(function (item) {
                empresas.push(item);
            });
            this.empresas = empresas;
        });
    }
    enviarRequisicao(form) {
        console.log(form.value);
        this.mensagemSerivce.novaMensagem(this.idUsuario, form.value).subscribe(success => console.log("tudo certo"), error => console.log("falha"));
    }
    setIdUsuario(id) {
        this.idUsuario = id;
    }
};
RequisitarDocumentosComponent.ctorParameters = () => [
    { type: _service_empresa_service__WEBPACK_IMPORTED_MODULE_5__["EmpresaService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _service_mensagem_service__WEBPACK_IMPORTED_MODULE_6__["MensagemService"] },
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_7__["UsuarioService"] }
];
RequisitarDocumentosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-requisitar-documentos',
        template: _raw_loader_requisitar_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_requisitar_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_empresa_service__WEBPACK_IMPORTED_MODULE_5__["EmpresaService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _service_mensagem_service__WEBPACK_IMPORTED_MODULE_6__["MensagemService"], _service_usuario_service__WEBPACK_IMPORTED_MODULE_7__["UsuarioService"]])
], RequisitarDocumentosComponent);



/***/ }),

/***/ "TeEd":
/*!**************************************************************!*\
  !*** ./src/app/listar-empresas/listar-empresas.component.ts ***!
  \**************************************************************/
/*! exports provided: ListarEmpresasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarEmpresasComponent", function() { return ListarEmpresasComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_listar_empresas_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./listar-empresas.component.html */ "Edu+");
/* harmony import */ var _listar_empresas_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listar-empresas.component.scss */ "5hMc");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../confirmacao-pop-up/confirmacao-pop-up.component */ "9CqS");
/* harmony import */ var _service_empresa_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/empresa.service */ "pFYR");










let ListarEmpresasComponent = class ListarEmpresasComponent {
    constructor(rest, router, dialog) {
        this.rest = rest;
        this.router = router;
        this.dialog = dialog;
        this.campoPesquisa = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.empresas = [];
        this.retorno_empresa = [];
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.maxSize = 5;
        this.bigTotalItems = 675;
        this.bigCurrentPage = 1;
        this.numPages = 0;
        this.usuarioId = 0;
        this.currentPager = 1;
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        console.log(usuarioL);
        this.setUsuarioId(usuarioL.id);
        this.campoPesquisa.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(value => value.trim()), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["debounceTime"])(150), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["tap"])(value => console.log(value)), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])((value) => this.rest.receberEmpresas(value, this.usuarioId))).subscribe(data => {
            let empresas = [];
            data.forEach(function (item) {
                empresas.push(item);
            });
            this.empresas = empresas;
            this.setTotalItems(empresas.length);
            this.retorno_empresa = this.empresas.slice(0, 5);
            console.log(this.retorno_empresa);
        });
        this.receberEmpresas("");
        this.retorno_empresa = this.empresas.slice(0, 10);
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.retorno_empresa = this.empresas.slice(startItem, endItem);
    }
    receberEmpresas(busca) {
        this.rest.receberEmpresas(busca, this.usuarioId).subscribe(data => {
            let empresas = [];
            data.forEach(function (item) {
                empresas.push(item);
            });
            this.empresas = empresas;
            this.setTotalItems(empresas.length);
            this.retorno_empresa = this.empresas.slice(0, 5);
            console.log(this.retorno_empresa);
        });
    }
    editar(id) {
        this.router.navigate(['editar-empresa', id]);
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
    openDialog(empresa, id) {
        const dialogRef = this.dialog.open(_confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_8__["ConfirmacaoPopUpComponent"], {
            width: '350px',
            data: { nome: empresa.pjId.razao_social },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result !== null && result !== void 0 ? result : 1) {
                this.rest.apagarEmpresa(id).subscribe(success => {
                    console.log("sucesso");
                    this.receberEmpresas("");
                }, error => console.log("erro"));
            }
        });
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
};
ListarEmpresasComponent.ctorParameters = () => [
    { type: _service_empresa_service__WEBPACK_IMPORTED_MODULE_9__["EmpresaService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] }
];
ListarEmpresasComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-listar-empresas',
        template: _raw_loader_listar_empresas_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_listar_empresas_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_empresa_service__WEBPACK_IMPORTED_MODULE_9__["EmpresaService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"]])
], ListarEmpresasComponent);



/***/ }),

/***/ "TfGo":
/*!**************************************************!*\
  !*** ./src/app/service/login-service.service.ts ***!
  \**************************************************/
/*! exports provided: LoginServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginServiceService", function() { return LoginServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app-constants */ "EKKv");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./usuario.service */ "AxdJ");






let LoginServiceService = class LoginServiceService {
    constructor(http, router, usuarioService) {
        this.http = http;
        this.router = router;
        this.usuarioService = usuarioService;
    }
    ngOnInit() { }
    login(usuario) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseLogin, JSON.stringify(usuario)).subscribe(data => {
            var token = JSON.parse(JSON.stringify(data)).Authorization.split(' ')[1];
            localStorage.setItem("token", token);
            var str = atob(localStorage.getItem("token").split(".")[1]);
            this.usuarioService.getByLogin(usuario.login).subscribe(usuarioL => {
                localStorage.setItem('usuarioL', JSON.stringify(usuarioL));
                localStorage.setItem("user_role", usuarioL.authorities[0].nomeRole);
            });
            this.router.navigate(['dashboard']);
            localStorage.setItem('usuario', JSON.stringify(usuario));
        }, error => {
            console.error("Erro ao fazer login ");
            alert('Acesso Negado!');
        });
    }
};
LoginServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"] }
];
LoginServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"]])
], LoginServiceService);



/***/ }),

/***/ "UoEy":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/modal-aviso/modal-aviso.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">{{modalTitle}</h4>\r\n  <button type=\"button\" class=\"btn-close close pull-right\" aria-label=\"Close\" (click)=\"bsModalRef.hide()\">\r\n    <span aria-hidden=\"true\" class=\"visually-hidden\">&times;</span>\r\n  </button>\r\n</div>\r\n<div class=\"modal-body\">\r\n  <p>{{modalBody}}</p>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <button type=\"button\" class=\"btn btn-default\" (click)=\"bsModalRef.hide()\">{{modalCloseBtn}}</button>\r\n</div>\r\n");

/***/ }),

/***/ "VKVo":
/*!***************************************************!*\
  !*** ./src/app/dashboard/dashboard.component.css ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#msg:hover a{\r\n  cursor: pointer;\r\n  color: black;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRhc2hib2FyZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBZTtFQUNmLFlBQVk7QUFDZCIsImZpbGUiOiJkYXNoYm9hcmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtc2c6aG92ZXIgYXtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG59Il19 */");

/***/ }),

/***/ "VO7H":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tipo-documentos/cadastrar/cadastrar-tipos-documentos.component.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n<main class=\"main d-flex align-items-center mt-4\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"tipoDocumentoForm\">\r\n              <h1 *ngIf=\"!isUpdating; else elseTitulo\">Cadastrar Tipo de documento</h1>\r\n              <ng-template #elseTitulo>\r\n                <h1>Editar Tipo de documento</h1>\r\n              </ng-template>\r\n              <hr>\r\n\r\n              <div class=\"input-group mb-3\">\r\n\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\">Nome do documento</span>\r\n                </div>\r\n\r\n                <input \r\n                  type=\"text\" \r\n                  class=\"form-control\" \r\n                  name=\"nomeDocumento\" \r\n                  required\r\n                  [class.is-invalid]=\"tipoDocumentoForm.get('nomeDocumento').invalid \r\n                  && (tipoDocumentoForm.get('nomeDocumento').dirty \r\n                  || tipoDocumentoForm.get('nomeDocumento').touched)\" \r\n                  formControlName=\"nomeDocumento\"\r\n                >\r\n\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Nome do Documento é obrigatório.\r\n                </div>\r\n\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n\r\n                <div class=\"input-group col mb-3\">\r\n\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">Escritório</span>\r\n                  </div>\r\n\r\n                  <select \r\n                    class=\"form-control\" \r\n                    id=\"field_escritorioContabil\" \r\n                    data-cy=\"escritorioContabil\" \r\n                    name=\"escritorioContabil\"\r\n                    formControlName=\"escritorioContabil\"\r\n                    required\r\n                    [class.is-invalid]=\"tipoDocumentoForm.get('escritorioContabil').invalid \r\n                    && (tipoDocumentoForm.get('escritorioContabil').dirty \r\n                    || tipoDocumentoForm.get('escritorioContabil').touched)\" \r\n                    >\r\n                    <option [ngValue]=\"null\">Selecione o escritório</option>\r\n                    <option [ngValue]=\"escritorioOption.id === tipoDocumentoForm.get('escritorioContabil')!.value?.id ? \r\n                      tipoDocumentoForm.get('escritorioContabil')!.value : escritorioOption\"\r\n                      *ngFor=\"let escritorioOption of escritoriosSharedCollection; trackBy: trackTipoDocumentoById\">\r\n                      {{ escritorioOption.nomeFantasia }}\r\n                    </option>\r\n                  </select>\r\n\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Escritório é obrigatório.\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"row\">\r\n\r\n                <div class=\"input-group col mb-3\">\r\n\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\">Categoria</span>\r\n                  </div>\r\n\r\n                  <select \r\n                    class=\"form-control\" \r\n                    id=\"field_categoria\" \r\n                    data-cy=\"categoria\" \r\n                    name=\"categoria\"\r\n                    formControlName=\"categoria\"\r\n                    required\r\n                    [class.is-invalid]=\"tipoDocumentoForm.get('categoria').invalid \r\n                    && (tipoDocumentoForm.get('categoria').dirty \r\n                    || tipoDocumentoForm.get('categoria').touched)\" \r\n                    >\r\n                    <option [ngValue]=\"null\">Selecione a categoria</option>\r\n                    <option [ngValue]=\"categoriaOption.id === tipoDocumentoForm.get('categoria')!.value?.id ? \r\n                      tipoDocumentoForm.get('categoria')!.value : categoriaOption\"\r\n                      *ngFor=\"let categoriaOption of categoriasSharedCollection; trackBy: trackTipoDocumentoById\">\r\n                      {{ categoriaOption.nomeCategoria }}\r\n                    </option>\r\n                  </select>\r\n\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Categoria é obrigatório.\r\n                  </div>\r\n\r\n                </div>\r\n              </div>\r\n              <hr>\r\n\r\n              <button  \r\n              *ngIf=\"!isUpdating; else elseButtonSalvar\"\r\n              type=\"button\" \r\n              class=\"btn btn-block btn-success\" \r\n              (click)=\"save()\" \r\n              [disabled]=\"tipoDocumentoForm.invalid || isSaving\">\r\n                Cadastrar\r\n              </button>\r\n              <ng-template #elseButtonSalvar>\r\n                <button  \r\n                type=\"button\" \r\n                class=\"btn btn-block btn-success\" \r\n                (click)=\"save()\" \r\n                [disabled]=\"tipoDocumentoForm.invalid || isSaving\">\r\n                  Salvar\r\n                </button>\r\n              </ng-template>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n\r\n");

/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<router-outlet></router-outlet>\r\n");

/***/ }),

/***/ "WKHj":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastrar-cliente/cadastrar-cliente.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!--main class=\"main d-flex align-items-center mt-4\" *ngIf=\"url == '/cadastrar-escritorio-contabil'\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"clienteForm\">\r\n              <h1>Cadastrar Cliente</h1>\r\n              <p class=\"text-muted\">Crie o cliente</p>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"cnpj\" [class.is-invalid]=\"clienteForm.get('cnpj').invalid && (clienteForm.get('cnpj').dirty || clienteForm.get('cnpj').touched)\" placeholder=\"CNPJ\" formControlName=\"cnpj\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo CNPJ é obrigatório. Deve ter 14 dígitos.\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"razao_social\" placeholder=\"Razão Social da empresa\" formControlName=\"razao_social\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"nome_fantasia\" placeholder=\"Nome Fantasia da empresa\" formControlName=\"nome_fantasia\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"nome\" [class.is-invalid]=\"clienteForm.get('nome').invalid && (clienteForm.get('nome').dirty || clienteForm.get('nome').touched)\" placeholder=\"Nome do Solicitante\" formControlName=\"nome\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Nome é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"cpf\" placeholder=\"CPF do Solicitante\" formControlName=\"cpf\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"endereco\" placeholder=\"Endereço da empresa\" formControlName=\"endereco\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"numero_crc\" placeholder=\"Número do registro do CRC do Contador Responsável\" formControlName=\"numero_crc\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon\">@</i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"email\" placeholder=\"Email do usuário\" formControlName=\"email\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"telefone\" placeholder=\"Telefone do usuário\" formControlName=\"telefone\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"celular\" placeholder=\"Celular do usuário\" formControlName=\"celular\">\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"ativ_primar_cnae_\" placeholder=\"Ativide Primária CNAE\" formControlName=\"ativ_primar_cnae_\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"descricao_primar\" placeholder=\"Descrição CNAE\" formControlName=\"descricao_primar\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"ativ_secund_cnae_\" placeholder=\"Atividade Secundária CNAE\" formControlName=\"ativ_secund_cnae_\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"descricao_secund\" placeholder=\"Descrição CNAE\" formControlName=\"descricao_secund\" disabled>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                </div>\r\n                <input type=\"password\" class=\"form-control\" name=\"senha\" [class.is-invalid]=\"clienteForm.get('senha').invalid && (clienteForm.get('senha').dirty || clienteForm.get('senha').touched)\" placeholder=\"Senha\" formControlName=\"senha\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Senha é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                </div>\r\n                <input type=\"password\" class=\"form-control\" name=\"confirmar_senha\" placeholder=\"Confirmar Senha\" formControlName=\"confirmar_senha\" [class.is-invalid]=\"clienteForm.get('confirmar_senha').value != clienteForm.get('senha').value\">\r\n                <div class=\"invalid-feedback\">\r\n                  As senhas digitadas não coincidem\r\n                </div>\r\n              </div>\r\n              <div class=\"form-check mb-3\">\r\n                <input class=\"form-check-input\" type=\"checkbox\" value=\"aceito\" id=\"checkbox1\" formControlName=\"termo_condicoes\">\r\n                <label class=\"form-check-label\" for=\"checkbox1\">\r\n                  Aceito os Termos e as Condições.\r\n                </label>\r\n              </div>\r\n              <hr>\r\n              <h5 class=\"mb-3\">Já possui uma conta? <a routerLink=\"/login\">Faça o Login</a></h5>\r\n              <hr>\r\n              <button type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(clienteForm)\" [disabled]=\"clienteForm.invalid || clienteForm.get('confirmar_senha').value != clienteForm.get('senha').value || clienteForm.get('termo_condicoes').invalid\">Cadastrar</button>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main-->\r\n<div class=\"animated fadeIn mt-4\" *ngIf=\"url == '/listar-clientes' || url == '/dashboard'\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Lista de Clientes\r\n        </div>\r\n        <div class=\"card-body\">\r\n        <div class=\"row\">\r\n          <div class=\"col col-8\"> \r\n            <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" routerLink=\"/cadastrar-escritorio-contabil\">\r\n        </div>\r\n          <div class=\"col col-4\">\r\n            <input type=\"text\" id=\"myInput\" class=\"form-control\" placeholder=\"Busque pela razão social\" [formControl]=\"campoPesquisa\">\r\n\r\n          </div>\r\n        </div>\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>CNPJ</th>\r\n                <th>Razão Social</th>\r\n                <th>Nome Fantasia</th>\r\n                <th>Responsável</th>\r\n                <th>Contato</th>\r\n                <th></th>\r\n                <th></th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let cliente of retorno_clientes; let i = index\">\r\n                <th scope=\"row\">{{ i + 1 }}</th>\r\n                <td>{{ cliente.cnpj }}</td>\r\n                <td>{{ cliente.razaoSocial }}</td>\r\n                <td>{{ cliente.nomeFantasia }}</td>\r\n                <td>{{ cliente.responsavel.nome }}</td>\r\n                <td>{{ cliente.email }}</td>\r\n                <td><input class=\"btn btn-info\" type=\"button\" value=\"Editar\" (click)=\"editar(cliente.id)\"></td>\r\n                <td><input class=\"btn btn-danger ms-3\" type=\"button\" (click)=\"openDialog(cliente, cliente.id)\" value=\"Excluir\"></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n          <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n        </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</div>\r\n");

/***/ }),

/***/ "Y06A":
/*!**********************************************************************!*\
  !*** ./src/app/confirmacao-pop-up/confirmacao-pop-up.component.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb25maXJtYWNhby1wb3AtdXAuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "YPFc":
/*!******************************************************************!*\
  !*** ./src/app/editar-documento/editar-documento.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("main {\n  margin-top: 15%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGVkaXRhci1kb2N1bWVudG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFBO0FBQ0YiLCJmaWxlIjoiZWRpdGFyLWRvY3VtZW50by5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1haW57XHJcbiAgbWFyZ2luLXRvcDogMTUlO1xyXG59Il19 */");

/***/ }),

/***/ "YZOx":
/*!*************************************!*\
  !*** ./src/app/model/Escritorio.ts ***!
  \*************************************/
/*! exports provided: Escritorio */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Escritorio", function() { return Escritorio; });
class Escritorio {
}


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: appRouters, routes, options, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appRouters", function() { return appRouters; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "options", function() { return options; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "cUpR");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "omvX");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _coreui_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @coreui/angular */ "Iluq");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "FE24");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap/modal */ "LqlI");
/* harmony import */ var ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/pagination */ "6No5");
/* harmony import */ var ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-bootstrap/tabs */ "2ZVE");
/* harmony import */ var ngx_mask__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-mask */ "tmjD");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "aLe/");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _cadastrar_cliente_cadastrar_cliente_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./cadastrar-cliente/cadastrar-cliente.component */ "nLMO");
/* harmony import */ var _cadastrar_colaborador_empresa_cadastrar_colaborador_empresa_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./cadastrar-colaborador-empresa/cadastrar-colaborador-empresa.component */ "B9T5");
/* harmony import */ var _cadastrar_colaborador_escritorio_cadastrar_colaborador_escritorio_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./cadastrar-colaborador-escritorio/cadastrar-colaborador-escritorio.component */ "D9Ld");
/* harmony import */ var _cadastrar_empresa_cadastrar_empresa_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./cadastrar-empresa/cadastrar-empresa.component */ "5H8X");
/* harmony import */ var _cadastrar_escritorio_contabil_cadastrar_escritorio_contabil_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./cadastrar-escritorio-contabil/cadastrar-escritorio-contabil.component */ "hudU");
/* harmony import */ var _cadastrar_usuario_cadastrar_usuario_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./cadastrar-usuario/cadastrar-usuario.component */ "GV3p");
/* harmony import */ var _categoria_cadastrar_cadastrar_categoria_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./categoria/cadastrar/cadastrar-categoria.component */ "dvc5");
/* harmony import */ var _categoria_listar_listar_categoria_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./categoria/listar/listar-categoria.component */ "hvvW");
/* harmony import */ var _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./confirmacao-pop-up/confirmacao-pop-up.component */ "9CqS");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "QX6l");
/* harmony import */ var _editar_documento_editar_documento_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./editar-documento/editar-documento.component */ "lfrn");
/* harmony import */ var _form_debug_form_debug_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./form-debug/form-debug.component */ "9j8H");
/* harmony import */ var _form_documento_btn_drop_zone_directive__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./form-documento/btn-drop-zone.directive */ "C+bB");
/* harmony import */ var _form_documento_form_documento_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./form-documento/form-documento.component */ "HTR/");
/* harmony import */ var _home_doador_home_doador_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./home-doador/home-doador.component */ "g39S");
/* harmony import */ var _listar_documentos_listar_documentos_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./listar-documentos/listar-documentos.component */ "ZId1");
/* harmony import */ var _listar_empresas_listar_empresas_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./listar-empresas/listar-empresas.component */ "TeEd");
/* harmony import */ var _listar_mensagens_listar_mensagens_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./listar-mensagens/listar-mensagens.component */ "9TuI");
/* harmony import */ var _listar_usuarios_listar_usuarios_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./listar-usuarios/listar-usuarios.component */ "BZdJ");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./login/login.component */ "vtpD");
/* harmony import */ var _mensagem_mensagem_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./mensagem/mensagem.component */ "spqh");
/* harmony import */ var _requisitar_documentos_requisitar_documentos_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./requisitar-documentos/requisitar-documentos.component */ "T/fq");
/* harmony import */ var _service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./service/guardiao.guard */ "ZQi/");
/* harmony import */ var _service_header_interceptor__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./service/header-interceptor */ "mYhI");
/* harmony import */ var _tipo_documentos_cadastrar_cadastrar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./tipo-documentos/cadastrar/cadastrar-tipos-documentos.component */ "dalo");
/* harmony import */ var _tipo_documentos_listar_listar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./tipo-documentos/listar/listar-tipos-documentos.component */ "v7Ys");
/* harmony import */ var _aviso_pop_up_aviso_pop_up_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./aviso-pop-up/aviso-pop-up.component */ "zb5I");
/* harmony import */ var _modal_aviso_modal_aviso_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./modal-aviso/modal-aviso.component */ "KGs1");












































const DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};
const appRouters = [
    { path: 'home-doador', component: _home_doador_home_doador_component__WEBPACK_IMPORTED_MODULE_30__["HomeDoadorComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_35__["LoginComponent"] },
    { path: 'dashboard', component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_25__["DashboardComponent"] },
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    {
        path: '',
        component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_25__["DashboardComponent"],
        children: [
            { path: 'cadastrar-usuario', component: _cadastrar_usuario_cadastrar_usuario_component__WEBPACK_IMPORTED_MODULE_21__["CadastrarUsuarioComponent"] },
            { path: 'listar-usuarios', component: _listar_usuarios_listar_usuarios_component__WEBPACK_IMPORTED_MODULE_34__["ListarUsuariosComponent"] },
            { path: 'cadastrar-cliente', component: _cadastrar_cliente_cadastrar_cliente_component__WEBPACK_IMPORTED_MODULE_16__["CadastrarClienteComponent"] },
            { path: 'listar-clientes', component: _cadastrar_cliente_cadastrar_cliente_component__WEBPACK_IMPORTED_MODULE_16__["CadastrarClienteComponent"] },
            { path: 'cadastrar-empresa', component: _cadastrar_empresa_cadastrar_empresa_component__WEBPACK_IMPORTED_MODULE_19__["CadastrarEmpresaComponent"] },
            { path: 'listar-empresas', component: _listar_empresas_listar_empresas_component__WEBPACK_IMPORTED_MODULE_32__["ListarEmpresasComponent"] },
            { path: 'cadastrar-escritorio-contabil', component: _cadastrar_escritorio_contabil_cadastrar_escritorio_contabil_component__WEBPACK_IMPORTED_MODULE_20__["CadastrarEscritorioContabilComponent"] },
            { path: 'mensagem', component: _mensagem_mensagem_component__WEBPACK_IMPORTED_MODULE_36__["MensagemComponent"] },
            { path: 'form-documento', component: _form_documento_form_documento_component__WEBPACK_IMPORTED_MODULE_29__["FormDocumentoComponent"] },
            { path: 'requisitar-documento', component: _requisitar_documentos_requisitar_documentos_component__WEBPACK_IMPORTED_MODULE_37__["RequisitarDocumentosComponent"] },
            { path: 'editar-documento/:id', component: _editar_documento_editar_documento_component__WEBPACK_IMPORTED_MODULE_26__["EditarDocumentoComponent"] },
            { path: 'listar-mensagens/:id', component: _listar_mensagens_listar_mensagens_component__WEBPACK_IMPORTED_MODULE_33__["ListarMensagensComponent"] },
            { path: 'editar-usuario/:id', component: _cadastrar_usuario_cadastrar_usuario_component__WEBPACK_IMPORTED_MODULE_21__["CadastrarUsuarioComponent"] },
            { path: 'editar-empresa/:id', component: _cadastrar_empresa_cadastrar_empresa_component__WEBPACK_IMPORTED_MODULE_19__["CadastrarEmpresaComponent"] },
            { path: 'editar-escritorio-contabil/:id', component: _cadastrar_escritorio_contabil_cadastrar_escritorio_contabil_component__WEBPACK_IMPORTED_MODULE_20__["CadastrarEscritorioContabilComponent"] },
            { path: 'listar-tipos-documentos', component: _tipo_documentos_listar_listar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_41__["ListarTiposDocumentosComponent"], canActivate: [_service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__["GuardiaoGuard"]] },
            { path: 'cadastrar-tipos-documentos', component: _tipo_documentos_cadastrar_cadastrar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_40__["CadastrarTiposDocumentosComponent"], canActivate: [_service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__["GuardiaoGuard"]] },
            { path: 'editar-tipos-documentos/:id', component: _tipo_documentos_cadastrar_cadastrar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_40__["CadastrarTiposDocumentosComponent"], canActivate: [_service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__["GuardiaoGuard"]] },
            { path: 'cadastrar-categoria', component: _categoria_cadastrar_cadastrar_categoria_component__WEBPACK_IMPORTED_MODULE_22__["CadastrarCategoriaComponent"], canActivate: [_service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__["GuardiaoGuard"]] },
            { path: 'editar-categoria/:id', component: _categoria_cadastrar_cadastrar_categoria_component__WEBPACK_IMPORTED_MODULE_22__["CadastrarCategoriaComponent"], canActivate: [_service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__["GuardiaoGuard"]] },
            { path: 'listar-categoria', component: _categoria_listar_listar_categoria_component__WEBPACK_IMPORTED_MODULE_23__["ListarCategoriaComponent"], canActivate: [_service_guardiao_guard__WEBPACK_IMPORTED_MODULE_38__["GuardiaoGuard"]] },
        ]
    },
];
const routes = _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forRoot(appRouters);
const options = null;
let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_15__["AppComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_35__["LoginComponent"],
            _home_doador_home_doador_component__WEBPACK_IMPORTED_MODULE_30__["HomeDoadorComponent"],
            _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_25__["DashboardComponent"],
            _cadastrar_usuario_cadastrar_usuario_component__WEBPACK_IMPORTED_MODULE_21__["CadastrarUsuarioComponent"],
            _listar_usuarios_listar_usuarios_component__WEBPACK_IMPORTED_MODULE_34__["ListarUsuariosComponent"],
            _cadastrar_cliente_cadastrar_cliente_component__WEBPACK_IMPORTED_MODULE_16__["CadastrarClienteComponent"],
            _form_documento_form_documento_component__WEBPACK_IMPORTED_MODULE_29__["FormDocumentoComponent"],
            _form_documento_btn_drop_zone_directive__WEBPACK_IMPORTED_MODULE_28__["BtnDropZoneDirective"],
            _cadastrar_empresa_cadastrar_empresa_component__WEBPACK_IMPORTED_MODULE_19__["CadastrarEmpresaComponent"],
            _mensagem_mensagem_component__WEBPACK_IMPORTED_MODULE_36__["MensagemComponent"],
            _cadastrar_escritorio_contabil_cadastrar_escritorio_contabil_component__WEBPACK_IMPORTED_MODULE_20__["CadastrarEscritorioContabilComponent"],
            _form_debug_form_debug_component__WEBPACK_IMPORTED_MODULE_27__["FormDebugComponent"],
            _listar_documentos_listar_documentos_component__WEBPACK_IMPORTED_MODULE_31__["ListarDocumentosComponent"],
            _editar_documento_editar_documento_component__WEBPACK_IMPORTED_MODULE_26__["EditarDocumentoComponent"],
            _requisitar_documentos_requisitar_documentos_component__WEBPACK_IMPORTED_MODULE_37__["RequisitarDocumentosComponent"],
            _listar_mensagens_listar_mensagens_component__WEBPACK_IMPORTED_MODULE_33__["ListarMensagensComponent"],
            _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_24__["ConfirmacaoPopUpComponent"],
            _listar_empresas_listar_empresas_component__WEBPACK_IMPORTED_MODULE_32__["ListarEmpresasComponent"],
            _cadastrar_colaborador_escritorio_cadastrar_colaborador_escritorio_component__WEBPACK_IMPORTED_MODULE_18__["CadastrarColaboradorEscritorioComponent"],
            _cadastrar_colaborador_empresa_cadastrar_colaborador_empresa_component__WEBPACK_IMPORTED_MODULE_17__["CadastrarColaboradorEmpresaComponent"],
            _tipo_documentos_listar_listar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_41__["ListarTiposDocumentosComponent"],
            _tipo_documentos_cadastrar_cadastrar_tipos_documentos_component__WEBPACK_IMPORTED_MODULE_40__["CadastrarTiposDocumentosComponent"],
            _aviso_pop_up_aviso_pop_up_component__WEBPACK_IMPORTED_MODULE_42__["AvisoPopUpComponent"],
            _modal_aviso_modal_aviso_component__WEBPACK_IMPORTED_MODULE_43__["ModalAvisoComponent"],
            _categoria_listar_listar_categoria_component__WEBPACK_IMPORTED_MODULE_23__["ListarCategoriaComponent"],
            _categoria_cadastrar_cadastrar_categoria_component__WEBPACK_IMPORTED_MODULE_22__["CadastrarCategoriaComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClientModule"],
            routes,
            _service_header_interceptor__WEBPACK_IMPORTED_MODULE_39__["HttpInterceptorModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
            _coreui_angular__WEBPACK_IMPORTED_MODULE_8__["AppHeaderModule"],
            _coreui_angular__WEBPACK_IMPORTED_MODULE_8__["AppSidebarModule"],
            _coreui_angular__WEBPACK_IMPORTED_MODULE_8__["AppAsideModule"],
            ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_14__["PerfectScrollbarModule"],
            ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_9__["BsDropdownModule"].forRoot(),
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            ngx_bootstrap_pagination__WEBPACK_IMPORTED_MODULE_11__["PaginationModule"].forRoot(),
            ngx_bootstrap_tabs__WEBPACK_IMPORTED_MODULE_12__["TabsModule"].forRoot(),
            ngx_mask__WEBPACK_IMPORTED_MODULE_13__["NgxMaskModule"].forRoot(),
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
            ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_10__["ModalModule"].forRoot(),
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_15__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "ZId1":
/*!******************************************************************!*\
  !*** ./src/app/listar-documentos/listar-documentos.component.ts ***!
  \******************************************************************/
/*! exports provided: ListarDocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarDocumentosComponent", function() { return ListarDocumentosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_listar_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./listar-documentos.component.html */ "l3T6");
/* harmony import */ var _listar_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listar-documentos.component.scss */ "Ko81");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _service_documento_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/documento.service */ "C6Xi");






let ListarDocumentosComponent = class ListarDocumentosComponent {
    constructor(documentoService, router) {
        this.documentoService = documentoService;
        this.router = router;
        this.mostrarDocumentos = false;
        this.mostrar = "Mostrar Documentos +";
        this.documentos = [];
        this.retornoDocumentos = [];
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.maxSize = 5;
        this.bigTotalItems = 675;
        this.bigCurrentPage = 1;
        this.numPages = 0;
        this.currentPager = 1;
    }
    ngOnInit() {
        this.receberDocumentos();
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.retornoDocumentos = this.documentos.slice(startItem, endItem);
    }
    receberDocumentos() {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        this.documentoService.receberDocumentos(usuarioL.id).subscribe(data => {
            let documentos = [];
            data.forEach(function (item) {
                documentos.push(item);
            });
            this.documentos = documentos;
            this.setTotalItems(documentos.length);
            this.retornoDocumentos = this.documentos.slice(0, 5);
        });
    }
    mostrarDocClique() {
        if (this.mostrarDocumentos) {
            this.mostrarDocumentos = false;
            this.mostrar = "Mostrar Documentos +";
        }
        else {
            this.mostrarDocumentos = true;
            this.mostrar = "Esconder Documentos -";
        }
    }
    editar(id) {
        this.router.navigate(['editar-documento', id]);
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
};
ListarDocumentosComponent.ctorParameters = () => [
    { type: _service_documento_service__WEBPACK_IMPORTED_MODULE_5__["DocumentoService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
ListarDocumentosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-listar-documentos',
        template: _raw_loader_listar_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_listar_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_documento_service__WEBPACK_IMPORTED_MODULE_5__["DocumentoService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], ListarDocumentosComponent);



/***/ }),

/***/ "ZQi/":
/*!*******************************************!*\
  !*** ./src/app/service/guardiao.guard.ts ***!
  \*******************************************/
/*! exports provided: GuardiaoGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardiaoGuard", function() { return GuardiaoGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./usuario.service */ "AxdJ");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");





let GuardiaoGuard = class GuardiaoGuard {
    constructor(userServcice, router) {
        this.userServcice = userServcice;
        this.router = router;
    }
    findAuthorities(authority) {
        if (authority.nomeRole === 'ROLE_ADMIN' || authority.nomeRole === 'ROLE_ADMIN_ESCRITORIO') {
            return true;
        }
        return false;
    }
    canActivate(route, state) {
        // var str = atob(localStorage.getItem("token").split(".")[1]);
        // var verificaAdmin = str.indexOf("ROLE_ADMIN") > -1;
        // if(this.userServcice.userAutenticado() && verificaAdmin){
        // return true;
        //  }
        const userRole = localStorage.getItem('user_role');
        if ((userRole === 'ROLE_ADMIN' || userRole === 'ROLE_ADMIN_ESCRITORIO') && this.userServcice.userAutenticado()) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(true);
        }
        else {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(false);
        }
    }
};
GuardiaoGuard.ctorParameters = () => [
    { type: _usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
GuardiaoGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
], GuardiaoGuard);



/***/ }),

/***/ "aSMF":
/*!**************************************************************!*\
  !*** ./src/app/tipo-documentos/tipo-documentos-dto.model.ts ***!
  \**************************************************************/
/*! exports provided: TiposDocumentosDTO */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TiposDocumentosDTO", function() { return TiposDocumentosDTO; });
class TiposDocumentosDTO {
    constructor(nomeDocumento, escritorioContabilId, categoriaId) {
        this.nomeDocumento = nomeDocumento;
        this.escritorioContabilId = escritorioContabilId;
        this.categoriaId = categoriaId;
    }
}


/***/ }),

/***/ "aq7q":
/*!***************************************!*\
  !*** ./src/app/enum/NivelDeAcesso.ts ***!
  \***************************************/
/*! exports provided: NivelDeAcesso */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NivelDeAcesso", function() { return NivelDeAcesso; });
var NivelDeAcesso;
(function (NivelDeAcesso) {
    NivelDeAcesso["SOCIETARIA_COLABORADOR"] = "Societ\u00E1ria - colaborador";
    NivelDeAcesso["SOCIETARIA_GESTOR"] = "Societ\u00E1ria - gestor";
    NivelDeAcesso["CONTABIL_COLABORADOR"] = "Cont\u00E1bil - colaborador";
    NivelDeAcesso["CONTABIL_GESTOR"] = "Cont\u00E1bil - gestor";
    NivelDeAcesso["FISCAL_COLABORADOR"] = "Fiscal - colaborador";
    NivelDeAcesso["FISCAL_GESTOR"] = "Fiscal - gestor";
    NivelDeAcesso["DEPARTAMENTO_PESSOAL_COLABORADOR"] = "Departamento Pessoal - colaborador";
    NivelDeAcesso["DEPARTAMENTO_PESSOAL_GESTOR"] = "Departamento Pessoal - gestor";
})(NivelDeAcesso || (NivelDeAcesso = {}));


/***/ }),

/***/ "bmkQ":
/*!******************************************************************!*\
  !*** ./src/app/categoria/listar/listar-categoria.component.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsaXN0YXItY2F0ZWdvcmlhLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "c2Qq":
/*!*************************!*\
  !*** ./src/app/_nav.ts ***!
  \*************************/
/*! exports provided: navItemsEsc, navItemsEmp, navItems */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navItemsEsc", function() { return navItemsEsc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navItemsEmp", function() { return navItemsEmp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "navItems", function() { return navItems; });
const navItemsEsc = [
    {
        title: true,
        name: 'Cadastros'
    },
    {
        name: 'Usuário',
        url: '/listar-usuarios',
        icon: 'icon-user'
    },
    {
        name: 'Empresa',
        url: '/listar-empresas',
        icon: 'icon-briefcase'
    },
    {
        name: 'Tipo de documento',
        url: '/listar-tipos-documentos',
        icon: 'icon-briefcase',
    }
];
const navItemsEmp = [
    {
        title: true,
        name: 'Cadastros'
    },
    {
        name: 'Usuário',
        url: '/listar-usuarios',
        icon: 'icon-user'
    }
];
const navItems = [
    {
        title: true,
        name: 'Cadastros'
    },
    {
        name: 'Usuário',
        url: '/listar-usuarios',
        icon: 'icon-user'
    },
    {
        name: 'Escritório de contabilidade',
        url: '/listar-clientes',
        icon: 'icon-chart'
    },
    {
        name: 'Empresa',
        url: '/listar-empresas',
        icon: 'icon-briefcase'
    },
    {
        name: 'Tipo de documento',
        url: '/listar-tipos-documentos',
        icon: 'icon-briefcase',
    },
    {
        name: 'Categoria',
        url: '/listar-categoria',
        icon: 'icon-briefcase',
    }
];


/***/ }),

/***/ "cJpW":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editar-documento/editar-documento.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main class=\"main d-flex align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"documentoForm\">\r\n              <h1>Editar Documento</h1>\r\n              <!--div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('competencia').invalid && (documentoForm.get('competencia').dirty || documentoForm.get('competencia').touched)\">\r\n                  <span>O campo competência é obrigatório.</span>\r\n              </div>\r\n              <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('valorVencimento').invalid && (documentoForm.get('valorVencimento').dirty || documentoForm.get('valorVencimento').touched)\">\r\n                  <span>O campo valor de vencimento não pode ser vazio ou menor que 0.</span>\r\n              </div>\r\n              <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('categoria').invalid && (documentoForm.get('categoria').dirty || documentoForm.get('categoria').touched)\">\r\n                <span>O campo categoria é obrigatório.</span>\r\n              </div>\r\n              <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('statusEnvio').invalid && (documentoForm.get('statusEnvio').dirty || documentoForm.get('statusEnvio').touched)\">\r\n                <span>O campo status de envio é obrigatório.</span>\r\n              </div>\r\n              <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"documentoForm.get('usuarioId').invalid && (documentoForm.get('usuarioId').dirty || documentoForm.get('usuarioId').touched)\">\r\n                <span>O campo usuário é obrigatório.</span>\r\n              </div-->     \r\n              <div class=\"input-group mb-3\">\r\n                <input type=\"text\" class=\"form-control\" name=\"competencia\" formControlName=\"competencia\" placeholder=\"Competência\" [class.is-invalid]=\"documentoForm.get('competencia').invalid && (documentoForm.get('competencia').dirty || documentoForm.get('competencia').touched)\" required>\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Competência é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"number\" class=\"form-control\"  formControlName=\"valorVencimento\" name=\"valorVencimento\" placeholder=\"Valor de Vencimento\" value=\"{{0}}\" [class.is-invalid]=\"documentoForm.get('valorVencimento').invalid && (documentoForm.get('valorVencimento').dirty || documentoForm.get('valorVencimento').touched)\" required>\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Valor de Vencimento é obrigatório, deve conter apenas números e não pode ser menor que 0.\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group col-8 mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" formControlName=\"categoria\" name=\"categoria\" placeholder=\"Categoria\" [class.is-invalid]=\"documentoForm.get('categoria').invalid && (documentoForm.get('categoria').dirty || documentoForm.get('categoria').touched)\"  required>\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Categoria é obrigatório.\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!--div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\" formControlName=\"statusEnvio\" name=\"statusEnvio\">\r\n                    <option value='' disabled selected>Status de envio</option>\r\n                    <option *ngFor=\"let name of enumKeys\" [value]=\"name\"> {{status[name]}} </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\"  formControlName=\"usuarioId\" name=\"usuarioId\">\r\n                    <option value='' disabled selected>Usuario</option>\r\n                    <option *ngFor=\"let usuario of usuarios\" [ngValue]=\"usuario\"> {{usuario.nome}} </option>\r\n                  </select>\r\n                </div>\r\n              </div-->\r\n              \r\n              <button type=\"button\" class=\"btn btn-block btn-success\" (click) = \"editar(documentoForm)\" [disabled] = \"(documentoForm.invalid)\">Salvar Alterações</button>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n");

/***/ }),

/***/ "cWAq":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/aviso-pop-up/aviso-pop-up.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1 mat-dialog-title>Aviso</h1>\r\n<div mat-dialog-content>O usuário e a senha foi enviado para o e-mail cadastrado. Por favor, confira a caixa de entrada do seu e-mail.</div>\r\n<div mat-dialog-actions>\r\n  <div mat-button mat-dialog-close (click)=\"onNoClick()\">Fechar</div>\r\n</div>\r\n");

/***/ }),

/***/ "cc24":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastrar-usuario/cadastrar-usuario.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n<main class=\"main d-flex align-items-center mt-4\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"usuarioForm\">\r\n              <h1 *ngIf=\"(!editando)\">Cadastrar Usuário</h1>\r\n              <h1 *ngIf=\"(editando)\"> Editar Usuário </h1>\r\n              <p *ngIf=\"(!editando)\" class=\"text-muted\">Crie o usuário</p>\r\n              <div *ngIf=\"(!editando) && (admin)\" class=\"input-group mb-3\">\r\n                <div class=\"form-check col-6\">\r\n                  <input class=\"form-check-input\" type=\"radio\" name=\"flexRadioDefault\" id=\"flexRadioDefault1\" [checked]=\"(btn1)\" (change)=\"mudarValorBtn1()\">\r\n                  <label class=\"form-check-label\" for=\"flexRadioDefault1\" >\r\n                    Colaborador Empresa\r\n                  </label>\r\n                </div>\r\n                <div class=\"form-check col-6\">\r\n                  <input class=\"form-check-input\" type=\"radio\" name=\"flexRadioDefault\" id=\"flexRadioDefault2\" [checked]=\"(btn2)\" (change)=\"mudarValorBtn2()\">\r\n                  <label class=\"form-check-label\" for=\"flexRadioDefault2\" >\r\n                    Colaborador Escritório\r\n                  </label>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"(btn1) && (!editando) && (admin)\" class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select (change)=\"onKey1()\" class=\"form-control\" formControlName=\"escritorio\" [class.is-invalid]=\"usuarioForm.get('escritorio').touched && (btn1) && !(emp)\">\r\n                    <option value='' disabled selected>Empresa</option>\r\n                    <option *ngFor=\"let empresa of empresas\" [ngValue]=\"empresa.id\" mask=\"00.000.000/0000-00\"> {{empresa.pjId.cnpj}} </option>\r\n                  </select>\r\n                  <div class=\"invalid-feedback\">\r\n                      Selecione uma empresa\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"(btn2) && (!editando) && (admin)\" class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select (change)=\"onKey2()\" class=\"form-control\" formControlName=\"escritorio2\" [class.is-invalid]=\"usuarioForm.get('escritorio2').touched && (btn2) && !(esc)\">\r\n                    <option value='' disabled selected>Selecione o Escritório</option>\r\n                    <option *ngFor=\"let escritorio of escritorios\" [ngValue]=\"escritorio.id\" mask=\"00.000.000/0000-00\"> {{escritorio.cnpj}} </option>\r\n                  </select>\r\n                  <div class=\"invalid-feedback\">\r\n                    Selecione um escritório\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"nome\" [class.is-invalid]=\"usuarioForm.get('nome').invalid && (usuarioForm.get('nome').dirty || usuarioForm.get('nome').touched)\" placeholder=\"Nome Completo\" formControlName=\"nome\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Nome é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"rg\" placeholder=\"RG\" formControlName=\"rg\"  [class.is-invalid]=\"usuarioForm.get('rg').invalid && (usuarioForm.get('rg').dirty || usuarioForm.get('rg').touched)\" mask=\"0000000\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo RG é obrigatório. Deve ter 7 dígitos.\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group col mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cpf\" [class.is-invalid]=\"(usuarioForm.get('cpf').invalid && (usuarioForm.get('cpf').dirty || usuarioForm.get('cpf').touched)) || (usuarioForm.get('cpf').touched && !(cpf))\" mask=\"000.000.000-00\" placeholder=\"CPF\" formControlName=\"cpf\" (blur)=\"testaCPF($event.target.value)\">\r\n                  <div class=\"invalid-feedback\">\r\n                    CPF inválido\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\"><i class=\"icon-phone\"></i></span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"celular\" placeholder=\"Celular\" formControlName=\"celular\"  [class.is-invalid]=\"usuarioForm.get('celular').invalid && (usuarioForm.get('celular').dirty || usuarioForm.get('celular').touched)\" mask=\"(00) 0000-0000\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Celular é obrigatório.\r\n                </div>\r\n              </div>\r\n              <div class=\"input-group mb-3\">\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\">@</span>\r\n                </div>\r\n                <input type=\"text\" class=\"form-control\" name=\"email\" [class.is-invalid]=\"usuarioForm.get('email').invalid && (usuarioForm.get('email').dirty || usuarioForm.get('email').touched)\" placeholder=\"Email\" formControlName=\"email\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Email é obrigatório.\r\n                </div>\r\n              </div>\r\n\r\n              \r\n              <div formGroupName=\"endereco\" style=\"border: none;\">\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cep\" placeholder=\"CEP\" formControlName=\"cep\" (blur)=\"consultaCep($event.target.value)\" [class.is-invalid]=\"usuarioForm.get('endereco').get('cep').invalid && (usuarioForm.get('endereco').get('cep').dirty || usuarioForm.get('endereco').get('cep').touched)\"  mask=\"00000-000\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo CEP é obrigatório.\r\n                  </div>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div class=\"input-group col-8 mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" name=\"rua\" placeholder=\"Rua\" formControlName=\"rua\">\r\n                  </div>\r\n                  <div class=\"input-group col mb-3\">\r\n                    <input type=\"number\" class=\"form-control\" name=\"numero\" placeholder=\"Número\" formControlName=\"numero\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"bairro\" placeholder=\"Bairro\" formControlName=\"bairro\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"cidade\" placeholder=\"Cidade\" formControlName=\"cidade\">\r\n                </div>\r\n                <div class=\"input-group mb-3\">\r\n                  <input type=\"text\" class=\"form-control\" name=\"estado\" placeholder=\"Estado\" formControlName=\"estado\">\r\n                </div>\r\n                \r\n              </div>\r\n                \r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\" formControlName=\"nivelDeAcesso\"  [class.is-invalid]=\"usuarioForm.get('nivelDeAcesso').invalid && (usuarioForm.get('nivelDeAcesso').dirty || usuarioForm.get('nivelDeAcesso').touched)\">\r\n                    <option value='' disabled>Nível de Acesso</option>\r\n                    <option *ngFor=\"let nivel of niveis\" [value]=\"nivel\"> {{nivelDeAcesso[nivel]}} </option>\r\n                  </select>\r\n                  <div class=\"invalid-feedback\">\r\n                    Selecione um nível de acesso\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              \r\n\r\n\r\n                <!-- <div class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                  </div>\r\n                  <input type=\"text\" class=\"form-control\" name=\"login\" [class.is-invalid]=\"usuarioForm.get('login').invalid && (usuarioForm.get('login').dirty || usuarioForm.get('login').touched)\" placeholder=\"Login\" formControlName=\"login\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Login é obrigatório.\r\n                  </div>\r\n                </div>\r\n                <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" name=\"senha\" [class.is-invalid]=\"usuarioForm.get('senha').invalid && (usuarioForm.get('senha').dirty || usuarioForm.get('senha').touched)\" placeholder=\"Senha\" formControlName=\"senha\">\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Senha é obrigatório.\r\n                  </div>\r\n                </div>\r\n                <div *ngIf=\"(!editando)\" class=\"input-group mb-3\">\r\n                  <div class=\"input-group-prepend\">\r\n                    <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                  </div>\r\n                  <input type=\"password\" class=\"form-control\" name=\"confirmar_senha\" [class.is-invalid]=\"usuarioForm.get('confirmar_senha').value != usuarioForm.get('senha').value\" placeholder=\"Confirmar Senha\" formControlName=\"confirmar_senha\">\r\n                  <div class=\"invalid-feedback\">\r\n                    As senhas digitadas não coincidem\r\n                  </div>\r\n                </div> -->\r\n                \r\n                <!-- <button *ngIf=\"(!editando)\" type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(usuarioForm)\" [disabled]=\"usuarioForm.invalid || usuarioForm.get('confirmar_senha').value != usuarioForm.get('senha').value\">Cadastrar</button> -->\r\n                <button *ngIf=\"(!editando)\" type=\"button\" class=\"btn btn-block btn-success\" (click)=\"cadastrar(usuarioForm)\" [disabled]=\"(usuarioForm.invalid || (!(emp) && (btn1)) || (!(esc) && (btn2)) || !(cpf))\">Cadastrar</button>\r\n                <button *ngIf=\"(editando)\" type=\"button\" class=\"btn btn-block btn-success\" (click)=\"editar(usuarioForm)\" [disabled]=\"usuarioForm.invalid \">Editar</button>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n");

/***/ }),

/***/ "dAGO":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categoria/listar/listar-categoria.component.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"animated fadeIn mt-4\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Lista de Categorias\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col col-5\">\r\n              <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" routerLink=\"/cadastrar-categoria\">\r\n            </div>\r\n\r\n              <div class=\"col col-5\">\r\n                <input type=\"text\" id=\"myInput\" class=\"form-control\" placeholder=\"Busque pelo nome da categoria\"\r\n                [formControl]=\"categoriaPequisa\">\r\n              </div>\r\n              <div class=\"col\">\r\n                <button class=\"btn btn-info mb-2\" type=\"button\" (click)=\"pesquisarCategoria()\">Pesquisar</button>\r\n              </div>\r\n            \r\n          </div>\r\n\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>Nome da Categoria</th>\r\n                <!-- <th>Escritório</th> -->\r\n                <th></th>\r\n                <th></th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let categoria of categorias; let i = index\">\r\n                <th scope=\"row\">{{ i + 1 }}</th>\r\n                <td>{{ categoria.nomeCategoria}}</td>\r\n                <!-- <td>{{ tipoDocumento.escritorioContabil.nomeFantasia }}</td> -->\r\n                <td><input class=\"btn btn-info\" type=\"button\" value=\"Editar\" [routerLink]=\"['/editar-categoria', categoria.id]\"></td>\r\n                <td><input class=\"btn btn-danger ms-3\" type=\"button\" (click)=\"openDialog(categoria, categoria.id)\" value=\"Excluir\"></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n            <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>");

/***/ }),

/***/ "dalo":
/*!***********************************************************************************!*\
  !*** ./src/app/tipo-documentos/cadastrar/cadastrar-tipos-documentos.component.ts ***!
  \***********************************************************************************/
/*! exports provided: CadastrarTiposDocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarTiposDocumentosComponent", function() { return CadastrarTiposDocumentosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_tipos_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-tipos-documentos.component.html */ "VO7H");
/* harmony import */ var _cadastrar_tipos_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-tipos-documentos.component.scss */ "L9FF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _categoria_service_categoria_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../categoria/service/categoria.service */ "qS1r");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../service/escritorio-contabil.service */ "wjmZ");
/* harmony import */ var _service_tipos_documentos_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/tipos-documentos.service */ "Nv4Z");
/* harmony import */ var _tipo_documentos_dto_model__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../tipo-documentos-dto.model */ "aSMF");
/* harmony import */ var _tipo_documentos_model__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../tipo-documentos.model */ "IOBM");













let CadastrarTiposDocumentosComponent = class CadastrarTiposDocumentosComponent {
    constructor(fb, escritorioContabilService, tiposDocumentosService, categoriaService, route) {
        this.fb = fb;
        this.escritorioContabilService = escritorioContabilService;
        this.tiposDocumentosService = tiposDocumentosService;
        this.categoriaService = categoriaService;
        this.route = route;
        this.isUpdating = false;
        this.isSaving = false;
        this.escritoriosSharedCollection = [];
        this.categoriasSharedCollection = [];
        this.tipoDocumentoForm = this.fb.group({
            id: [],
            nomeDocumento: [],
            escritorioContabil: [],
            categoria: []
        });
    }
    ngOnInit() {
        this.escritorioContabilService.receberEscritorios('').subscribe((escritorios) => {
            this.escritoriosSharedCollection = escritorios;
        });
        this.categoriaService.findAll().subscribe((categorias) => {
            this.categoriasSharedCollection = categorias;
        });
        this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((params) => params['id']), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])(id => {
            if (id !== undefined) {
                return this.tiposDocumentosService.findById(id);
            }
            else {
                return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"]();
            }
        })).subscribe(tipoDocumento => {
            this.updateForm(tipoDocumento);
            tipoDocumento.id ? this.isUpdating = true : this.isUpdating = false;
        });
    }
    save() {
        this.isSaving = true;
        const tipoDocumento = this.createFromForm();
        const tipoDocumentoDTO = this.toDTO(tipoDocumento);
        if (tipoDocumento.id) {
            this.subscribeToSaveResponse(this.tiposDocumentosService.update(tipoDocumentoDTO, tipoDocumento.id));
        }
        else {
            this.subscribeToSaveResponse(this.tiposDocumentosService.create(tipoDocumentoDTO));
        }
    }
    previousState() {
        window.history.back();
    }
    subscribeToSaveResponse(result) {
        result.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => this.onSaveFinalize())).subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.previousState();
    }
    onSaveError() {
        window.scrollTo(0, 0);
    }
    onSaveFinalize() {
        this.isSaving = false;
        this.isUpdating = false;
    }
    trackTipoDocumentoById(index, item) {
        return item.id;
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new _tipo_documentos_model__WEBPACK_IMPORTED_MODULE_12__["TiposDocumentos"]()), { id: this.tipoDocumentoForm.get(['id']).value, nomeDocumento: this.tipoDocumentoForm.get(['nomeDocumento']).value, escritorioContabil: this.tipoDocumentoForm.get(['escritorioContabil']).value, categoria: this.tipoDocumentoForm.get(['categoria']).value });
    }
    toDTO(tipoDocumento) {
        return Object.assign(Object.assign({}, new _tipo_documentos_dto_model__WEBPACK_IMPORTED_MODULE_11__["TiposDocumentosDTO"]()), { nomeDocumento: tipoDocumento.nomeDocumento, escritorioContabilId: tipoDocumento.escritorioContabil.id, categoriaId: tipoDocumento.categoria.id });
    }
    updateForm(tipoDocumento) {
        this.tipoDocumentoForm.patchValue({
            id: tipoDocumento.id,
            nomeDocumento: tipoDocumento.nomeDocumento,
            escritorioContabil: tipoDocumento.escritorioContabil,
            categoria: tipoDocumento.categoria
        });
    }
};
CadastrarTiposDocumentosComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_9__["EscritorioContabilService"] },
    { type: _service_tipos_documentos_service__WEBPACK_IMPORTED_MODULE_10__["TiposDocumentosService"] },
    { type: _categoria_service_categoria_service__WEBPACK_IMPORTED_MODULE_8__["CategoriaService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] }
];
CadastrarTiposDocumentosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cadastrar-tipos-documentos',
        template: _raw_loader_cadastrar_tipos_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_tipos_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_9__["EscritorioContabilService"],
        _service_tipos_documentos_service__WEBPACK_IMPORTED_MODULE_10__["TiposDocumentosService"],
        _categoria_service_categoria_service__WEBPACK_IMPORTED_MODULE_8__["CategoriaService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
], CadastrarTiposDocumentosComponent);



/***/ }),

/***/ "dnP0":
/*!****************************************************************************!*\
  !*** ./src/app/requisitar-documentos/requisitar-documentos.component.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXF1aXNpdGFyLWRvY3VtZW50b3MuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "dvc5":
/*!**********************************************************************!*\
  !*** ./src/app/categoria/cadastrar/cadastrar-categoria.component.ts ***!
  \**********************************************************************/
/*! exports provided: CadastrarCategoriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarCategoriaComponent", function() { return CadastrarCategoriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_categoria_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-categoria.component.html */ "olFP");
/* harmony import */ var _cadastrar_categoria_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-categoria.component.scss */ "MbNM");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _service_categoria_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../service/categoria.service */ "qS1r");
/* harmony import */ var _categoria_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../categoria.model */ "3DPm");










let CadastrarCategoriaComponent = class CadastrarCategoriaComponent {
    constructor(fb, route, categoriaService) {
        this.fb = fb;
        this.route = route;
        this.categoriaService = categoriaService;
        this.isUpdating = false;
        this.isSaving = false;
        this.categoriaForm = this.fb.group({
            id: [],
            nomeCategoria: []
        });
    }
    ngOnInit() {
        this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((params) => params['id']), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])(id => {
            if (id !== undefined) {
                return this.categoriaService.findById(id);
            }
            else {
                return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"]();
            }
        })).subscribe(categoria => {
            this.updateForm(categoria);
            categoria.id ? this.isUpdating = true : this.isUpdating = false;
        });
    }
    save() {
        this.isSaving = true;
        const categoria = this.createFromForm();
        console.log(categoria);
        // const tipoDocumentoDTO: ITiposDocumentosDTO = this.toDTO(tipoDocumento);
        if (categoria.id) {
            this.subscribeToSaveResponse(this.categoriaService.update(categoria, categoria.id));
        }
        else {
            this.subscribeToSaveResponse(this.categoriaService.create(categoria));
        }
    }
    previousState() {
        window.history.back();
    }
    subscribeToSaveResponse(result) {
        result.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["finalize"])(() => this.onSaveFinalize())).subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
    }
    onSaveSuccess() {
        this.previousState();
    }
    onSaveError() {
        window.scrollTo(0, 0);
    }
    onSaveFinalize() {
        this.isSaving = false;
        this.isUpdating = false;
    }
    trackCategoriaById(index, item) {
        return item.id;
    }
    createFromForm() {
        return Object.assign(Object.assign({}, new _categoria_model__WEBPACK_IMPORTED_MODULE_9__["Categoria"]()), { id: this.categoriaForm.get(['id']).value, nomeCategoria: this.categoriaForm.get(['nomeCategoria']).value });
    }
    // protected toDTO(tipoDocumento: ICategoria): ITiposDocumentosDTO {
    //   return {
    //     ...new TiposDocumentosDTO(),
    //     nomeDocumento: tipoDocumento.nomeDocumento,
    //     escritorioContabilId: tipoDocumento.escritorioContabil.id,
    //   };
    // }
    updateForm(categoria) {
        this.categoriaForm.patchValue({
            id: categoria.id,
            nomeCategoria: categoria.nomeCategoria,
        });
    }
};
CadastrarCategoriaComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _service_categoria_service__WEBPACK_IMPORTED_MODULE_8__["CategoriaService"] }
];
CadastrarCategoriaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cadastrar-categoria',
        template: _raw_loader_cadastrar_categoria_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_categoria_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
        _service_categoria_service__WEBPACK_IMPORTED_MODULE_8__["CategoriaService"]])
], CadastrarCategoriaComponent);



/***/ }),

/***/ "g39S":
/*!******************************************************!*\
  !*** ./src/app/home-doador/home-doador.component.ts ***!
  \******************************************************/
/*! exports provided: HomeDoadorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeDoadorComponent", function() { return HomeDoadorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_home_doador_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./home-doador.component.html */ "p8QW");
/* harmony import */ var _home_doador_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home-doador.component.css */ "53o5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _service_candidato_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/candidato.service */ "3QCp");






let HomeDoadorComponent = class HomeDoadorComponent {
    constructor(router, candidatoService) {
        this.router = router;
        this.candidatoService = candidatoService;
    }
    ngOnInit() {
        this.candidatoService.getPorEstado().subscribe(data => {
            this.porEstado = data;
        });
        this.candidatoService.getImcList().subscribe(data => {
            this.imcList = data;
            for (let i = 0; i < this.imcList.length; i++) {
                this.imcList[i] == null ? this.imcList[i] = 0 : this.imcList[i];
                this.imcList[i] = this.imcList[i].toFixed(2);
            }
        });
        this.candidatoService.getObesosList().subscribe(data => {
            this.obesosList = data;
            for (let i = 0; i < this.obesosList.length; i++) {
                this.obesosList[i] = this.obesosList[i].toFixed(2);
            }
        });
        this.candidatoService.getMediaIdadeList().subscribe(data => {
            this.mediaIdadeList = data;
            for (let i = 0; i < this.mediaIdadeList.length; i++) {
                this.mediaIdadeList[i] == null ? this.mediaIdadeList[i] = 0 : this.mediaIdadeList[i];
                this.mediaIdadeList[i] = this.mediaIdadeList[i].toFixed(2);
            }
        });
        this.candidatoService.getQuantidadeDoadoresList().subscribe(data => {
            this.quantidadeDoadoresList = data;
        });
    }
    sair() {
        localStorage.clear();
        location.reload();
    }
};
HomeDoadorComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _service_candidato_service__WEBPACK_IMPORTED_MODULE_5__["CandidatoService"] }
];
HomeDoadorComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home-doador',
        template: _raw_loader_home_doador_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_home_doador_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _service_candidato_service__WEBPACK_IMPORTED_MODULE_5__["CandidatoService"]])
], HomeDoadorComponent);



/***/ }),

/***/ "grlU":
/*!**********************************!*\
  !*** ./src/app/model/Usuario.ts ***!
  \**********************************/
/*! exports provided: Usuario */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Usuario", function() { return Usuario; });
class Usuario {
}


/***/ }),

/***/ "hMR8":
/*!********************************************************************************************!*\
  !*** ./src/app/cadastrar-colaborador-empresa/cadastrar-colaborador-empresa.component.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItY29sYWJvcmFkb3ItZW1wcmVzYS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "hudU":
/*!******************************************************************************************!*\
  !*** ./src/app/cadastrar-escritorio-contabil/cadastrar-escritorio-contabil.component.ts ***!
  \******************************************************************************************/
/*! exports provided: CadastrarEscritorioContabilComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarEscritorioContabilComponent", function() { return CadastrarEscritorioContabilComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_escritorio_contabil_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-escritorio-contabil.component.html */ "ydG1");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../service/escritorio-contabil.service */ "wjmZ");









let CadastrarEscritorioContabilComponent = class CadastrarEscritorioContabilComponent {
    constructor(escritorioContabilService, fb, route, router, http) {
        this.escritorioContabilService = escritorioContabilService;
        this.fb = fb;
        this.route = route;
        this.router = router;
        this.http = http;
        this.editando = false;
    }
    ngOnInit() {
        this.route.params
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])((params) => params["id"]), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])((id) => {
            if (id !== undefined) {
                return this.escritorioContabilService.getById(id);
            }
            else {
                return new rxjs__WEBPACK_IMPORTED_MODULE_6__["Observable"]();
            }
        }))
            .subscribe((escritorio) => {
            this.updateForm(escritorio);
            this.setEditando();
        });
        const original = _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"].prototype.registerOnChange;
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["DefaultValueAccessor"].prototype.registerOnChange = function (fn) {
            return original.call(this, (value) => {
                const trimmed = typeof value === "string" ? value.trim() : value;
                return fn(trimmed);
            });
        };
        this.escritorioForm = this.fb.group({
            id: [null],
            cnpj: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            razaoSocial: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            nomeFantasia: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            endereco: this.fb.group({
                cep: ["", [
                        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required,
                        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(8),
                        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(8)
                    ]],
                rua: [""],
                bairro: [""],
                numero: [""],
                complemento: [""],
            }),
            contato: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
            codigoPrimariaCnae: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            atividadePrimariaCnae: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            codigoSecundariaCnae: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            atividadeSecundariaCnae: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            nomeResponsavel: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            cpf: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            crc: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            contatoResponsavel: [""],
            emailResponsavel: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]]
        });
    }
    get controls() {
        return this.escritorioForm.controls;
    }
    get endereco() {
        return this.escritorioForm.controls.endereco;
    }
    onSubmit(escritorio) {
        this.escritorioContabilService.criarFormulario(escritorio.value).subscribe((sucess) => {
            console.log("Enviado!");
            this.router.navigate(["dashboard"]);
        }, (error) => console.error("Deu ruim!"), () => console.log("Request completo"));
    }
    setEditando() {
        this.editando = true;
    }
    updateForm(escritorio) {
        console.log(escritorio);
        this.escritorioForm.patchValue({
            id: escritorio.id,
            cnpj: escritorio.cnpj,
            razaoSocial: escritorio.razaoSocial,
            nomeFantasia: escritorio.nomeFantasia,
            endereco: this.fb.group({
                rua: "",
                numero: "",
                bairro: "",
                cep: "",
                complemento: "",
            }),
            contato: escritorio.contato,
            email: escritorio.email,
            codigoPrimariaCnae: escritorio.codigoPrimariaCnae,
            atividadePrimariaCnae: escritorio.atividadePrimariaCnae,
            condigoSecundariaCnae: escritorio.codigoSecundariaCnae,
            atividadeSecundariaCnae: escritorio.atividadeSecundariaCnae,
            nomeResponsavel: escritorio.nomeResponsavel,
            cpf: escritorio.cpf,
            crc: escritorio.crc,
            contatoResponsavel: escritorio.contatoResponsavel,
            emailResponsavel: escritorio.emailResponsavel,
        });
    }
    editar(escritorio) {
        this.escritorioContabilService.editar(escritorio.value).subscribe((success) => {
            console.log("Deu certo");
            this.router.navigate(["dashboard"]);
        }, (error) => {
            console.log("Falhou");
            console.log(escritorio.value);
            console.log(error);
        });
    }
    cnpjKeyPressEvent(event) {
        if (!(this.escritorioForm.get("cnpj").value === "")) {
            let cnpj = event.target.value;
            cnpj = cnpj
                .replace(".", "")
                .replace(".", "")
                .replace("/", "")
                .replace("-", "");
            this.http.get("/wk/pessoajuridica/cnpj/" + cnpj).subscribe((success) => {
                console.log(success + " - nao tem no banco");
                if (success !== null) {
                    this.escritorioForm.get("cnpj").setErrors({ duplicado: true });
                }
            }, (error) => {
                console.log(error);
            });
            this.http.get("/receita" + cnpj).subscribe((success) => {
                const responseJson = JSON.parse(JSON.stringify(success));
                if (responseJson.message === "CNPJ inválido") {
                    return this.escritorioForm.get("cnpj").setErrors({ cnpjInvalido: true });
                }
                this.onSuccess(responseJson);
            }, (error) => {
                console.log(error);
            });
        }
    }
    cpfKeyPressEvent(event) {
        if (!(this.escritorioForm.get("cpf").value === "")) {
            let cpf = event.target.value;
            cpf = cpf.replace(".", "").replace(".", "").replace("-", "");
            this.http.get("/wk/usuario/cpf/" + cpf).subscribe((success) => {
                console.log(success + " - nao tem no banco");
                if (success !== null) {
                    this.escritorioForm.get("cpf").setErrors({ duplicado: true });
                }
            }, (error) => {
                console.log(error);
            });
        }
    }
    onSuccess(responseJson) {
        console.log(responseJson);
        this.escritorioForm.get(["razaoSocial"]).setValue(responseJson.nome);
        this.escritorioForm.get(["nomeFantasia"]).setValue(responseJson.fantasia);
        this.escritorioForm
            .get(["endereco"])
            .get(["cep"])
            .setValue(responseJson.cep);
        this.escritorioForm
            .get(["endereco"])
            .get(["rua"])
            .setValue(responseJson.logradouro);
        this.escritorioForm
            .get(["endereco"])
            .get(["bairro"])
            .setValue(responseJson.bairro);
        this.escritorioForm
            .get(["endereco"])
            .get(["numero"])
            .setValue(responseJson.numero);
        this.escritorioForm
            .get(["endereco"])
            .get(["complemento"])
            .setValue(responseJson.complemento);
        this.escritorioForm
            .get(["codigoPrimariaCnae"])
            .setValue(responseJson.atividade_principal[0].code);
        this.escritorioForm
            .get(["atividadePrimariaCnae"])
            .setValue(responseJson.atividade_principal[0].text);
        this.escritorioForm
            .get(["codigoSecundariaCnae"])
            .setValue(responseJson.atividades_secundarias[0].code);
        this.escritorioForm
            .get(["atividadeSecundariaCnae"])
            .setValue(responseJson.atividades_secundarias[0].text);
    }
    onError(response) {
        const responseJson = JSON.parse(JSON.stringify(response));
        alert("Erro ao consultar o CNPJ. Detalhes: " + responseJson.statusText);
    }
};
CadastrarEscritorioContabilComponent.ctorParameters = () => [
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_8__["EscritorioContabilService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CadastrarEscritorioContabilComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: "app-cadastrar-escritorio-contabil",
        template: _raw_loader_cadastrar_escritorio_contabil_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_8__["EscritorioContabilService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
], CadastrarEscritorioContabilComponent);



/***/ }),

/***/ "hvvW":
/*!****************************************************************!*\
  !*** ./src/app/categoria/listar/listar-categoria.component.ts ***!
  \****************************************************************/
/*! exports provided: ListarCategoriaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarCategoriaComponent", function() { return ListarCategoriaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_listar_categoria_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./listar-categoria.component.html */ "dAGO");
/* harmony import */ var _listar_categoria_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listar-categoria.component.scss */ "bmkQ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../confirmacao-pop-up/confirmacao-pop-up.component */ "9CqS");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/escritorio-contabil.service */ "wjmZ");
/* harmony import */ var _service_categoria_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/categoria.service */ "qS1r");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "s7LF");









let ListarCategoriaComponent = class ListarCategoriaComponent {
    constructor(dialog, categoriaService, escritorioContabilService, fb) {
        this.dialog = dialog;
        this.categoriaService = categoriaService;
        this.escritorioContabilService = escritorioContabilService;
        this.fb = fb;
        this.categorias = [];
        this.usuarioId = 0;
        this.categoriaPequisa = new _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormControl"]();
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        this.setUsuarioId(usuarioL.id);
        this.setRole(localStorage.getItem('user_role'));
        this.categoriaService.findAll().subscribe((categorias) => {
            this.categorias = categorias;
        });
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.categorias = this.categorias.slice(startItem, endItem);
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
    openDialog(tipoDocumento, id) {
        const dialogRef = this.dialog.open(_confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_5__["ConfirmacaoPopUpComponent"], {
            width: '350px',
            data: { nome: tipoDocumento.nomeDocumento },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result !== null && result !== void 0 ? result : 1) {
                this.categoriaService.delete(id).subscribe(success => {
                    this.updateTable('null');
                });
            }
        });
    }
    onSearchEscritorioChange(event) {
        this.updateTable(event.target.value);
    }
    updateTable(nomeCategoria) {
        this.categorias = [];
        if (nomeCategoria === null) {
            this.categoriaService.findAll().subscribe((categoria) => {
                categoria.forEach((categoria) => {
                    this.categorias.push(categoria);
                });
            });
        }
        else {
            this.categoriaService.findByName(nomeCategoria).subscribe((categorias) => {
                categorias.forEach((categoria) => {
                    this.categorias.push(categoria);
                });
            });
        }
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
    setRole(value) {
        this.role = value;
    }
    pesquisarCategoria() {
        const categoriaSearch = this.categoriaPequisa.value;
        if (categoriaSearch === '' || categoriaSearch === null || categoriaSearch === undefined) {
            this.updateTable(null);
        }
        else {
            this.updateTable(categoriaSearch);
        }
    }
};
ListarCategoriaComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
    { type: _service_categoria_service__WEBPACK_IMPORTED_MODULE_7__["CategoriaService"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_6__["EscritorioContabilService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"] }
];
ListarCategoriaComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-categoria',
        template: _raw_loader_listar_categoria_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_listar_categoria_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
        _service_categoria_service__WEBPACK_IMPORTED_MODULE_7__["CategoriaService"],
        _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_6__["EscritorioContabilService"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormBuilder"]])
], ListarCategoriaComponent);



/***/ }),

/***/ "in5m":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"app-body\">\r\n  <main class=\"main d-flex align-items-center\">\r\n    <div class=\"container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-8 mx-auto\">\r\n          <div class=\"card-group\">\r\n            <div class=\"card p-4\">\r\n              <div class=\"card-body\">\r\n                <form>\r\n                  <h1>Login</h1>\r\n                  <p class=\"text-muted\">Entre na sua conta</p>\r\n                  <div class=\"input-group mb-3\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"icon-user\"></i></span>\r\n                    </div>\r\n                    <input type=\"email\" class=\"form-control\" [(ngModel)]=\"user.login\" id=\"login\" name=\"login\" [ngModelOptions]=\"{standalone: true}\">\r\n                  </div>\r\n                  <div class=\"input-group mb-4\">\r\n                    <div class=\"input-group-prepend\">\r\n                      <span class=\"input-group-text\"><i class=\"icon-lock\"></i></span>\r\n                    </div>\r\n                    <input type=\"password\" class=\"form-control\" [(ngModel)]=\"user.senha\" id=\"senha\" [ngModelOptions]=\"{standalone: true}\" name=\"senha\">\r\n                  </div>\r\n                  <div class=\"row\">\r\n                    <div class=\"col-6\">\r\n                      <button type=\"button\" class=\"btn btn-info px-4 text-white\" (click)=\"logar()\">Login</button>\r\n                    </div>\r\n                    <div class=\"col-6 text-right\">\r\n                      <button type=\"button\" class=\"btn btn-link px-0\">Esqueceu a senha?</button>\r\n                    </div>\r\n                  </div>\r\n                </form>\r\n              </div>\r\n            </div>\r\n            <div class=\"card text-white bg-info py-5 d-md-down-none\" style=\"width:44%\">\r\n              <div class=\"card-body text-center\">\r\n                <div>\r\n                  <h2>Cadastre-se</h2>\r\n                  <button type=\"button\" class=\"btn btn-info active mt-3\">Se Cadastre Agora!</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </main>\r\n</div>\r\n");

/***/ }),

/***/ "l3T6":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/listar-documentos/listar-documentos.component.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-outline-dark mostrar\" (click)= \"mostrarDocClique()\"> {{mostrar}}</button>\r\n<div *ngIf=\"mostrarDocumentos\" class=\"animated fadeIn mt-4\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Lista de Documentos\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" routerLink=\"/form-documento\">\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>Competencia</th>\r\n                <th>Valor de Vencimento</th>\r\n                <th>Categoria</th>\r\n                <th>Empresa</th>\r\n                <th></th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let documento of retornoDocumentos; let i = index\">\r\n                <th scope=\"row\">{{i + 1}}</th>\r\n                <td>{{documento.competencia}}</td>\r\n                <td> {{documento.valorVencimento}} </td>\r\n                <td> {{documento.categoria}}</td>\r\n                <td> {{documento.empresa.pjId.razao_social}} </td>\r\n                <td><input class=\"btn btn-info\" type=\"button\" value=\"Editar\" (click)=\"editar(documento.id)\"></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n          <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n            <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</div>\r\n");

/***/ }),

/***/ "lfrn":
/*!****************************************************************!*\
  !*** ./src/app/editar-documento/editar-documento.component.ts ***!
  \****************************************************************/
/*! exports provided: EditarDocumentoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarDocumentoComponent", function() { return EditarDocumentoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_editar_documento_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./editar-documento.component.html */ "cJpW");
/* harmony import */ var _editar_documento_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./editar-documento.component.scss */ "YPFc");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _service_documento_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/documento.service */ "C6Xi");








let EditarDocumentoComponent = class EditarDocumentoComponent {
    constructor(route, service, fb, router) {
        this.route = route;
        this.service = service;
        this.fb = fb;
        this.router = router;
    }
    ngOnInit() {
        this.documentoForm = this.fb.group({
            id: [null],
            competencia: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            valorVencimento: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].min(0)],
            categoria: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
        });
        this.route.params.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((params) => params['id']), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(id => this.service.receberDocumento(id))).subscribe(documento => this.atualizarFormulario(documento));
    }
    atualizarFormulario(documento) {
        this.documentoForm.patchValue({
            id: documento.id,
            competencia: documento.competencia,
            valorVencimento: documento.valorVencimento,
            categoria: documento.categoria
        });
    }
    editar(documento) {
        this.service.editaDocumento(documento.value).subscribe(success => {
            console.log("Deu certo");
            this.router.navigate(['dashboard']);
        }, error => {
            console.log("Falhou");
            console.log(error);
        });
    }
};
EditarDocumentoComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _service_documento_service__WEBPACK_IMPORTED_MODULE_7__["DocumentoService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
EditarDocumentoComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-editar-documento',
        template: _raw_loader_editar_documento_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_editar_documento_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _service_documento_service__WEBPACK_IMPORTED_MODULE_7__["DocumentoService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
], EditarDocumentoComponent);



/***/ }),

/***/ "mYhI":
/*!***********************************************!*\
  !*** ./src/app/service/header-interceptor.ts ***!
  \***********************************************/
/*! exports provided: HeaderInterceptorService, HttpInterceptorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderInterceptorService", function() { return HeaderInterceptorService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpInterceptorModule", function() { return HttpInterceptorModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "IheW");



let HeaderInterceptorService = class HeaderInterceptorService {
    constructor() { }
    intercept(req, next) {
        if (localStorage.getItem('token') !== null) {
            const token = 'Bearer ' + localStorage.getItem('token');
            const tokenRequest = req.clone({
                headers: req.headers.set('Authorization', token)
            });
            return next.handle(tokenRequest);
        }
        else {
            return next.handle(req);
        }
    }
};
HeaderInterceptorService.ctorParameters = () => [];
HeaderInterceptorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])
], HeaderInterceptorService);

let HttpInterceptorModule = class HttpInterceptorModule {
};
HttpInterceptorModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        providers: [{
                provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"],
                useClass: HeaderInterceptorService,
                multi: true,
            },
        ],
    })
], HttpInterceptorModule);



/***/ }),

/***/ "n5A5":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/requisitar-documentos/requisitar-documentos.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main class=\"main d-flex align-items-center\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"requisicaoForm\">\r\n              <h1>Requisitar Documentos</h1>\r\n              <p class=\"text-muted\">Faça a requisição de um ou mais documentos para uma empresa</p> \r\n              <div class=\"row\">\r\n                <div class=\"input-group col mb-3\">\r\n                  <select class=\"form-control\" formControlName=\"destinatario\">\r\n                    <option value='' disabled selected>Empresa</option>\r\n                    <option *ngFor=\"let empresa of empresas\" [ngValue]=\"empresa.responsavel.id\"> {{empresa.pjId.razao_social}} </option>\r\n                  </select>\r\n                </div>\r\n              </div>\r\n\r\n              <label class=\"form-label\">Selecione os documentos desejados</label> \r\n\r\n                <div class=\"form-check col\">\r\n                  <input class=\"form-check-input\" type=\"checkbox\" value=\"Requisitar\" id=\"checkbox1\" formControlName=\"documento1\">\r\n                  <label class=\"form-check-label\" for=\"checkbox1\">\r\n                    Documento 1\r\n                  </label>\r\n                </div>\r\n\r\n                <div class=\"form-check mb-3 col\">\r\n                  <input class=\"form-check-input\" type=\"checkbox\" value=\"Requisitar\" id=\"checkbox2\" formControlName=\"documento2\">\r\n                  <label class=\"form-check-label\" for=\"checkbox2\"> Documento 2 </label>\r\n                </div>\r\n              \r\n              <div class=\"row\" style=\"margin-bottom: 10px;\">\r\n                <div class=\"col-md-12\">\r\n                    <textarea id=\"textarea-input\" name=\"textarea-input\" rows=\"9\" class=\"form-control\"\r\n                    placeholder=\"Escreva uma mensagem aqui\" formControlName=\"mensagem\"></textarea>\r\n                </div>\r\n              </div>\r\n              \r\n              <button type=\"button\" class=\"btn btn-block btn-success\" (click)=\"enviarRequisicao(requisicaoForm)\" [disabled]=\"(!requisicaoForm.get('documento1').value && !requisicaoForm.get('documento2').value) || requisicaoForm.get('destinatario').invalid || requisicaoForm.get('mensagem').invalid\"> Fazer Requisição</button>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>");

/***/ }),

/***/ "n7sk":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".app-body {\r\n  height: 100vh;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0FBQ2YiLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hcHAtYm9keSB7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "nLMO":
/*!******************************************************************!*\
  !*** ./src/app/cadastrar-cliente/cadastrar-cliente.component.ts ***!
  \******************************************************************/
/*! exports provided: CadastrarClienteComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastrarClienteComponent", function() { return CadastrarClienteComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cadastrar_cliente_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cadastrar-cliente.component.html */ "WKHj");
/* harmony import */ var _cadastrar_cliente_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cadastrar-cliente.component.scss */ "oBdX");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../confirmacao-pop-up/confirmacao-pop-up.component */ "9CqS");
/* harmony import */ var _model_Escritorio__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../model/Escritorio */ "YZOx");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../service/escritorio-contabil.service */ "wjmZ");











let CadastrarClienteComponent = class CadastrarClienteComponent {
    constructor(fb, rest, router, dialog) {
        this.fb = fb;
        this.rest = rest;
        this.router = router;
        this.dialog = dialog;
        this.campoPesquisa = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]();
        this.clientes = [];
        this.retorno_clientes = [];
        this.currentPage = 1;
        this.smallnumPages = 0;
        this.maxSize = 5;
        this.bigTotalItems = 675;
        this.bigCurrentPage = 1;
        this.numPages = 0;
        this.currentPager = 1;
        this.cliente = new _model_Escritorio__WEBPACK_IMPORTED_MODULE_9__["Escritorio"]();
    }
    ngOnInit() {
        this.campoPesquisa.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["map"])(value => value.trim()), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["debounceTime"])(150), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["distinctUntilChanged"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["tap"])(value => console.log(value)), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])((value) => this.rest.receberEscritorios(value))).subscribe(data => {
            let escritorios = [];
            data.forEach(function (item) {
                escritorios.push(item);
            });
            this.clientes = escritorios;
            this.setTotalItems(escritorios.length);
            this.retorno_clientes = this.clientes.slice(0, 5);
        });
        this.receberEscritorios("");
        this.clienteForm = this.fb.group({
            cnpj: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(14), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(14)]],
            razao_social: [""],
            nome_fantasia: [""],
            nome: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            cpf: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
            endereco: [""],
            numero_crc: [""],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
            telefone: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(10)]],
            celular: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].maxLength(11)]],
            ativ_primar_cnae_: [""],
            descricao_primar: [""],
            ativ_secund_cnae_: [""],
            descricao_secund: [""],
            senha: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            confirmar_senha: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]],
            termo_condicoes: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]]
        });
        this.retorno_clientes = this.clientes.slice(0, 10);
        this.url = window.location.pathname;
    }
    cadastrar(form) {
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.retorno_clientes = this.clientes.slice(startItem, endItem);
    }
    receberEscritorios(busca) {
        this.rest.receberEscritorios(busca).subscribe(data => {
            console.log(data);
            let escritorios = [];
            data.forEach(function (item) {
                escritorios.push(item);
            });
            this.clientes = escritorios;
            this.setTotalItems(escritorios.length);
            this.retorno_clientes = this.clientes.slice(0, 5);
        });
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
    editar(id) {
        this.router.navigate(['editar-escritorio-contabil', id]);
    }
    openDialog(cliente, id) {
        const dialogRef = this.dialog.open(_confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_8__["ConfirmacaoPopUpComponent"], {
            width: '350px',
            data: { nome: cliente.razaoSocial },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result !== null && result !== void 0 ? result : 1) {
                this.rest.delete(id).subscribe(success => {
                    console.log("sucesso");
                    this.receberEscritorios("");
                }, error => console.log("erro"));
            }
        });
    }
};
CadastrarClienteComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_10__["EscritorioContabilService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] }
];
CadastrarClienteComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cadastrar-cliente',
        template: _raw_loader_cadastrar_cliente_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cadastrar_cliente_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_10__["EscritorioContabilService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _angular_material_dialog__WEBPACK_IMPORTED_MODULE_5__["MatDialog"]])
], CadastrarClienteComponent);



/***/ }),

/***/ "oBdX":
/*!********************************************************************!*\
  !*** ./src/app/cadastrar-cliente/cadastrar-cliente.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItY2xpZW50ZS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "olFP":
/*!**************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categoria/cadastrar/cadastrar-categoria.component.html ***!
  \**************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n<main class=\"main d-flex align-items-center mt-4\">\r\n  <div class=\"container\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-8 mx-auto\">\r\n        <div class=\"card mx-4\">\r\n          <div class=\"card-body p-4\">\r\n            <form [formGroup]=\"categoriaForm\">\r\n              <h1 *ngIf=\"!isUpdating; else elseTitulo\">Cadastrar Categoria</h1>\r\n              <ng-template #elseTitulo>\r\n                <h1>Editar Categoria</h1>\r\n              </ng-template>\r\n              <hr>\r\n\r\n              <div class=\"input-group mb-3\">\r\n\r\n                <div class=\"input-group-prepend\">\r\n                  <span class=\"input-group-text\">Nome da categoria</span>\r\n                </div>\r\n\r\n                <input \r\n                  type=\"text\" \r\n                  class=\"form-control\" \r\n                  name=\"nomeCategoria\" \r\n                  [class.is-invalid]=\"categoriaForm.get('nomeCategoria').invalid \r\n                  && (categoriaForm.get('nomeCategoria').dirty \r\n                  || categoriaForm.get('nomeCategoria').touched)\" \r\n                  formControlName=\"nomeCategoria\"\r\n                >\r\n\r\n                <div class=\"invalid-feedback\">\r\n                  O campo Nome da Categoria é obrigatório.\r\n                </div>\r\n\r\n              </div>\r\n\r\n              <!-- <div class=\"row\">\r\n\r\n                <div class=\"input-group col mb-3\">\r\n\r\n                  <select \r\n                    class=\"form-control\" \r\n                    id=\"field_escritorioContabil\" \r\n                    data-cy=\"escritorioContabil\" \r\n                    name=\"escritorioContabil\"\r\n                    formControlName=\"escritorioContabil\"\r\n                    required\r\n                    [class.is-invalid]=\"tipoDocumentoForm.get('escritorioContabil').invalid \r\n                    && (tipoDocumentoForm.get('escritorioContabil').dirty \r\n                    || tipoDocumentoForm.get('escritorioContabil').touched)\" \r\n                    >\r\n                    <option [ngValue]=\"null\">Selecione o escritório</option>\r\n                    <option [ngValue]=\"escritorioOption.id === tipoDocumentoForm.get('escritorioContabil')!.value?.id ? \r\n                      tipoDocumentoForm.get('escritorioContabil')!.value : escritorioOption\"\r\n                      *ngFor=\"let escritorioOption of escritoriosSharedCollection; trackBy: trackTipoDocumentoById\">\r\n                      {{ escritorioOption.nomeFantasia }}\r\n                    </option>\r\n                  </select>\r\n\r\n                  <div class=\"invalid-feedback\">\r\n                    O campo Escritório é obrigatório.\r\n                  </div>\r\n\r\n                </div>\r\n              </div> -->\r\n              <hr>\r\n\r\n              <button  \r\n              *ngIf=\"!isUpdating; else elseButtonSalvar\"\r\n              type=\"button\" \r\n              class=\"btn btn-block btn-success\" \r\n              (click)=\"save()\" \r\n              [disabled]=\"categoriaForm.invalid || isSaving\">\r\n                Cadastrar\r\n              </button>\r\n              <ng-template #elseButtonSalvar>\r\n                <button  \r\n                type=\"button\" \r\n                class=\"btn btn-block btn-success\" \r\n                (click)=\"save()\" \r\n                [disabled]=\"categoriaForm.invalid || isSaving\">\r\n                  Salvar\r\n                </button>\r\n              </ng-template>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</main>\r\n\r\n");

/***/ }),

/***/ "ovI4":
/*!**********************************!*\
  !*** ./src/app/model/Empresa.ts ***!
  \**********************************/
/*! exports provided: Empresa */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Empresa", function() { return Empresa; });
class Empresa {
}


/***/ }),

/***/ "p8QW":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-doador/home-doador.component.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div align=\"right\"><button (click)=\"sair()\" style=\"background-color: crimson; color: wheat; border-radius: 55%;\">Exit</button></div>\r\n\r\n<div class=\"card\" style=\"background: white;\">\r\n    <fieldset legend=\"Doadores Estatísticas\">\r\n\r\n        <h3 style=\"text-align: center;\">Candidatos por Estado</h3>\r\n        <div align=\"center\">\r\n            <table class=\"verde\">\r\n                <tr>\r\n                    <th *ngFor=\"let p of porEstado\">{{ p.estado}}</th>\r\n                </tr>\r\n                <tr>\r\n\r\n                    <th *ngFor=\"let p of porEstado\">{{ p.quantidadeCandidatos}}</th>\r\n                </tr>\r\n            </table>\r\n        </div>\r\n\r\n        <br>\r\n        <br>\r\n\r\n        <h3 style=\"text-align: center;\">Média IMC</h3>\r\n        <div align=\"center\">\r\n            <table class=\"amarelo\">\r\n\r\n                <tr>\r\n                    <th>0 a 10 anos</th>\r\n                    <th>11 a 20 anos</th>\r\n                    <th>21 a 30 anos</th>\r\n                    <th>31 a 40 anos</th>\r\n                    <th>41 a 50 anos</th>\r\n                    <th>51 a 60 anos</th>\r\n                    <th>61 a 70 anos</th>\r\n                </tr>\r\n                <tr>\r\n                    <th *ngFor=\"let i of imcList\">{{ i }} </th>\r\n                </tr>\r\n\r\n            </table>\r\n        </div>\r\n\r\n        <br>\r\n        <br>\r\n\r\n        <h3 style=\"text-align: center;\">Percentual Obesidade</h3>\r\n        <div align=\"center\">\r\n            <table>\r\n                <tr>\r\n                    <th style=\"background-color: rgba(0, 132, 255, 0.514);\">Homem</th>\r\n                    <th style=\"background-color: pink;\">Mulher</th>\r\n                </tr>\r\n                <tr>\r\n                    <td *ngFor=\"let o of obesosList\">{{ o }}</td>\r\n                </tr>\r\n            </table>\r\n        </div>\r\n\r\n        <br>\r\n        <br>\r\n\r\n        <h3 style=\"text-align: center;\">Média Idade</h3>\r\n        <div align=\"center\">\r\n            <table class=\"roxo\">\r\n                <tr>\r\n                    <td>A+</td>\r\n                    <td>A-</td>\r\n                    <td>B+</td>\r\n                    <td>B-</td>\r\n                    <td>AB+</td>\r\n                    <td>AB-</td>\r\n                    <td>O+</td>\r\n                    <td>O-</td>\r\n                </tr>\r\n                <tr>\r\n                    <td *ngFor=\"let m of mediaIdadeList\">{{ m }}</td>\r\n                </tr>\r\n            </table>\r\n        </div>\r\n\r\n        <br>\r\n        <br>\r\n\r\n        <h3 style=\"text-align: center;\">Quantidade Doadores</h3>\r\n        <div align=\"center\">\r\n            <table class=\"vermelho\">\r\n                <tr>\r\n                    <td>A+</td>\r\n                    <td>A-</td>\r\n                    <td>B+</td>\r\n                    <td>B-</td>\r\n                    <td>AB+</td>\r\n                    <td>AB-</td>\r\n                    <td>O+</td>\r\n                    <td>O-</td>\r\n                </tr>\r\n                <tr>\r\n                    <td *ngFor=\"let q of quantidadeDoadoresList\">{{ q }}</td>\r\n                </tr>\r\n            </table>\r\n        </div>\r\n\r\n    </fieldset>\r\n</div>\r\n");

/***/ }),

/***/ "pFYR":
/*!********************************************!*\
  !*** ./src/app/service/empresa.service.ts ***!
  \********************************************/
/*! exports provided: EmpresaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmpresaService", function() { return EmpresaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app-constants */ "EKKv");




let EmpresaService = class EmpresaService {
    constructor(http) {
        this.http = http;
    }
    salvarEmpresa(empresa) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlEmpresaCliente + "salva", empresa);
    }
    getById(id) {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlEmpresaCliente + id);
    }
    editarEmpresa(empresa) {
        return this.http.put(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlEmpresaCliente + "editar/" + empresa.id, empresa);
    }
    apagarEmpresa(id) {
        return this.http.delete(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlEmpresaCliente + "apagar/" + id);
    }
    receberEmpresas(busca, id) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        params = params.set('busca', busca);
        params = params.set('id', id);
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlEmpresaCliente + "encontra", { params: params });
    }
};
EmpresaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
EmpresaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], EmpresaService);



/***/ }),

/***/ "qS1r":
/*!********************************************************!*\
  !*** ./src/app/categoria/service/categoria.service.ts ***!
  \********************************************************/
/*! exports provided: CategoriaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoriaService", function() { return CategoriaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app-constants */ "EKKv");




let CategoriaService = class CategoriaService {
    constructor(http) {
        this.http = http;
    }
    create(categoria) {
        return this.http.post(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCategoria}novo`, categoria);
    }
    update(categoria, id) {
        return this.http.put(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCategoria}update/${id}`, categoria);
    }
    delete(id) {
        return this.http.delete(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCategoria}delete/${id}`);
    }
    findById(id) {
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCategoria}find/id/${id}`);
    }
    findByName(nomeCategoria) {
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCategoria}find/name/${nomeCategoria}`);
    }
    findAll() {
        return this.http.get(`${_app_constants__WEBPACK_IMPORTED_MODULE_3__["AppConstants"].baseUrlCategoria}find/all`);
    }
};
CategoriaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
CategoriaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
], CategoriaService);



/***/ }),

/***/ "rryG":
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/listar-usuarios/listar-usuarios.component.html ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"animated fadeIn mt-4\">\r\n  <div class=\"row justify-content-center align-middle\">\r\n    <div class=\"col-lg-12\">\r\n      <div class=\"card\">\r\n        <div class=\"card-header\">\r\n          <i class=\"fa fa-align-justify\"></i> Lista de Usuários\r\n        </div>\r\n        <div class=\"card-body\">\r\n          <div class=\"row\">\r\n            <div class=\"col col-8\">\r\n             <input class=\"btn btn-info mb-2\" type=\"button\" value=\"Novo+\" style=\"margin-right: 5px;\" routerLink=\"/cadastrar-usuario\">\r\n            </div>\r\n            <div class=\"col col-4\">\r\n              <input type=\"text\" id=\"myInput\" class=\"form-control\" placeholder=\"Busque pelo nome\" [formControl]=\"campoPesquisa\">\r\n\r\n            </div>\r\n        </div>\r\n          <table class=\"table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th>Nome</th>\r\n                <th>RG</th>\r\n                <th>CPF</th>\r\n                <th>Celular</th>\r\n                <th>Email</th>\r\n                <th></th>\r\n                <th></th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr *ngFor=\"let usuario of retornoUsuarios; let i = index\">\r\n                <th scope=\"row\">{{ i + 1 }}</th>\r\n                <td>{{ usuario.nome }}</td>\r\n                <td>{{ usuario.rg }}</td>\r\n                <td>{{ usuario.cpf }}</td>\r\n                <td>{{ usuario.celular }}</td>\r\n                <td>{{ usuario.email }}</td>\r\n                <td><input class=\"btn btn-info\" type=\"button\" value=\"Editar\" (click)=\"editar(usuario.id)\"></td>\r\n                <td><input class=\"btn btn-danger ms-3\" type=\"button\" value=\"Excluir\" (click)=\"openDialog(usuario, usuario.id)\" ></td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        <div class=\"col-xs-12 col-12 d-sm-down-none\">\r\n          <pagination [totalItems]=\"totalItems\" [(ngModel)]=\"currentPage\" (pageChanged)=\"pageChanged($event)\" [itemsPerPage]=\"5\"></pagination>\r\n        </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n</div>\r\n\r\n");

/***/ }),

/***/ "spqh":
/*!************************************************!*\
  !*** ./src/app/mensagem/mensagem.component.ts ***!
  \************************************************/
/*! exports provided: MensagemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensagemComponent", function() { return MensagemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_mensagem_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./mensagem.component.html */ "4a6u");
/* harmony import */ var _mensagem_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mensagem.component.scss */ "9BWX");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "s7LF");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _service_empresa_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/empresa.service */ "pFYR");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/escritorio-contabil.service */ "wjmZ");
/* harmony import */ var _service_mensagem_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../service/mensagem.service */ "02KH");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");










let MensagemComponent = class MensagemComponent {
    constructor(rest, fb, usuarioService, empresaService, router, escritorioService) {
        this.rest = rest;
        this.fb = fb;
        this.usuarioService = usuarioService;
        this.empresaService = empresaService;
        this.router = router;
        this.escritorioService = escritorioService;
        this.empresas = [];
        this.escritorios = [];
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuario'));
        this.usuarioService.getByLogin(usuarioL.login).subscribe(data => this.setIdUsuario(data.id));
        this.receberEmpresas();
        this.receberEscritorios("");
        this.msgForm = this.fb.group({
            destinatario: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            mensagem: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            documento1: false,
            documento2: false,
        });
    }
    receberEmpresas() {
        this.empresaService.receberEmpresas("", this.idUsuario).subscribe(data => {
            console.log(data);
            let empresas = [];
            data.forEach(function (item) {
                empresas.push(item);
            });
            this.empresas = empresas;
        });
    }
    receberEscritorios(busca) {
        this.escritorioService.receberEscritorios(busca).subscribe(data => {
            console.log(data);
            let escritorios = [];
            data.forEach(function (item) {
                escritorios.push(item);
            });
            this.escritorios = escritorios;
        });
    }
    enviar(msg) {
        console.log(msg.value);
        this.rest.novaMensagem(this.idUsuario, msg.value).subscribe(success => { this.router.navigate(['listar-mensagens', this.idUsuario]); }, error => console.log("falha"));
    }
    setIdUsuario(id) {
        this.idUsuario = id;
    }
    voltar() {
        this.router.navigate(['listar-mensagens', this.idUsuario]);
    }
};
MensagemComponent.ctorParameters = () => [
    { type: _service_mensagem_service__WEBPACK_IMPORTED_MODULE_8__["MensagemService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_9__["UsuarioService"] },
    { type: _service_empresa_service__WEBPACK_IMPORTED_MODULE_6__["EmpresaService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_7__["EscritorioContabilService"] }
];
MensagemComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-mensagem',
        template: _raw_loader_mensagem_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_mensagem_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_mensagem_service__WEBPACK_IMPORTED_MODULE_8__["MensagemService"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _service_usuario_service__WEBPACK_IMPORTED_MODULE_9__["UsuarioService"], _service_empresa_service__WEBPACK_IMPORTED_MODULE_6__["EmpresaService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_7__["EscritorioContabilService"]])
], MensagemComponent);



/***/ }),

/***/ "tTeE":
/*!************************************!*\
  !*** ./src/app/model/Documento.ts ***!
  \************************************/
/*! exports provided: Documento */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Documento", function() { return Documento; });
class Documento {
}


/***/ }),

/***/ "tpOW":
/*!********************************************************************!*\
  !*** ./src/app/cadastrar-usuario/cadastrar-usuario.component.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYWRhc3RyYXItdXN1YXJpby5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "v7Ys":
/*!*****************************************************************************!*\
  !*** ./src/app/tipo-documentos/listar/listar-tipos-documentos.component.ts ***!
  \*****************************************************************************/
/*! exports provided: ListarTiposDocumentosComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListarTiposDocumentosComponent", function() { return ListarTiposDocumentosComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_listar_tipos_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./listar-tipos-documentos.component.html */ "FLUB");
/* harmony import */ var _listar_tipos_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./listar-tipos-documentos.component.scss */ "Dz3O");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");
/* harmony import */ var _confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../confirmacao-pop-up/confirmacao-pop-up.component */ "9CqS");
/* harmony import */ var _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/escritorio-contabil.service */ "wjmZ");
/* harmony import */ var _service_tipos_documentos_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../service/tipos-documentos.service */ "Nv4Z");








let ListarTiposDocumentosComponent = class ListarTiposDocumentosComponent {
    constructor(dialog, tiposDocumentosService, escritorioContabilService) {
        this.dialog = dialog;
        this.tiposDocumentosService = tiposDocumentosService;
        this.escritorioContabilService = escritorioContabilService;
        this.tiposDocumentos = [];
        this.escritorios = [];
        this.usuarioId = 0;
    }
    ngOnInit() {
        let usuarioL = JSON.parse(localStorage.getItem('usuarioL'));
        this.setUsuarioId(usuarioL.id);
        this.setRole(localStorage.getItem('user_role'));
        this.tiposDocumentosService.findAll(this.usuarioId).subscribe((tiposDocumentos) => {
            this.tiposDocumentos = tiposDocumentos;
        });
        this.escritorioContabilService.receberEscritorios('').subscribe((escritorios) => {
            this.escritorios = escritorios;
        });
    }
    pageChanged(event) {
        const startItem = (event.page - 1) * event.itemsPerPage;
        const endItem = event.page * event.itemsPerPage;
        this.tiposDocumentos = this.tiposDocumentos.slice(startItem, endItem);
    }
    setTotalItems(lenght) {
        this.totalItems = lenght;
    }
    openDialog(tipoDocumento, id) {
        const dialogRef = this.dialog.open(_confirmacao_pop_up_confirmacao_pop_up_component__WEBPACK_IMPORTED_MODULE_5__["ConfirmacaoPopUpComponent"], {
            width: '350px',
            data: { nome: tipoDocumento.nomeDocumento },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result !== null && result !== void 0 ? result : 1) {
                this.tiposDocumentosService.delete(id).subscribe(success => {
                    this.updateTable('null');
                });
            }
        });
    }
    onSearchEscritorioChange(event) {
        this.updateTable(event.target.value);
    }
    updateTable(idEscritorio) {
        this.tiposDocumentos = [];
        if (idEscritorio !== 'null') {
            this.tiposDocumentosService.findAllByEscritorioId(+idEscritorio).subscribe((tiposDocumentos) => {
                tiposDocumentos.forEach((tipoDocumento) => {
                    this.tiposDocumentos.push(tipoDocumento);
                });
            });
        }
        else {
            this.tiposDocumentosService.findAll(this.usuarioId).subscribe((tiposDocumentos) => {
                tiposDocumentos.forEach((tipoDocumento) => {
                    this.tiposDocumentos.push(tipoDocumento);
                });
            });
        }
    }
    setUsuarioId(id) {
        this.usuarioId = id;
    }
    setRole(value) {
        this.role = value;
    }
};
ListarTiposDocumentosComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
    { type: _service_tipos_documentos_service__WEBPACK_IMPORTED_MODULE_7__["TiposDocumentosService"] },
    { type: _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_6__["EscritorioContabilService"] }
];
ListarTiposDocumentosComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tipos-documentos',
        template: _raw_loader_listar_tipos_documentos_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_listar_tipos_documentos_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
        _service_tipos_documentos_service__WEBPACK_IMPORTED_MODULE_7__["TiposDocumentosService"],
        _service_escritorio_contabil_service__WEBPACK_IMPORTED_MODULE_6__["EscritorioContabilService"]])
], ListarTiposDocumentosComponent);



/***/ }),

/***/ "vtpD":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.component.html */ "in5m");
/* harmony import */ var _login_component_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.component.css */ "n7sk");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _service_login_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../service/login-service.service */ "TfGo");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var _service_usuario_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../service/usuario.service */ "AxdJ");







let LoginComponent = class LoginComponent {
    constructor(loginService, router, userService) {
        this.loginService = loginService;
        this.router = router;
        this.userService = userService;
        this.user = { login: '', senha: '' };
        this.radio = 0;
    }
    ngOnInit() {
        if (localStorage.getItem('token') !== null &&
            localStorage.getItem('token').toString().trim() !== null) {
            var str = atob(localStorage.getItem("token").split(".")[1]);
            this.router.navigate(['login']);
        }
    }
    logar() {
        this.loginService.login(this.user);
        //this.router.navigate(['dashboard']);
    }
    novo() {
        this.user = { login: '', senha: '' };
    }
    radio1clicado() {
        this.radio = 1;
    }
    radio2clicado() {
        this.radio = 2;
    }
};
LoginComponent.ctorParameters = () => [
    { type: _service_login_service_service__WEBPACK_IMPORTED_MODULE_4__["LoginServiceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _service_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"] }
];
LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_component_css__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_service_login_service_service__WEBPACK_IMPORTED_MODULE_4__["LoginServiceService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        _service_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"]])
], LoginComponent);



/***/ }),

/***/ "wjmZ":
/*!********************************************************!*\
  !*** ./src/app/service/escritorio-contabil.service.ts ***!
  \********************************************************/
/*! exports provided: EscritorioContabilService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EscritorioContabilService", function() { return EscritorioContabilService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "IheW");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "iInd");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _app_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../app-constants */ "EKKv");






const httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
let EscritorioContabilService = class EscritorioContabilService {
    constructor(http, router) {
        this.http = http;
        this.router = router;
    }
    criarFormulario(escritorio) {
        return this.http.post(_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].baseUrlEscritorioContabil + `registrar`, escritorio, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["take"])(1));
    }
    receberEscritorios(busca) {
        let params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpParams"]();
        params = params.set('busca', busca);
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].baseUrlEscritorioContabil + 'todos', { params: params });
    }
    getById(id) {
        return this.http.get(_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].baseUrlEscritorioContabil + id);
    }
    editar(escritorio) {
        return this.http.put(_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].baseUrlEscritorioContabil + "editar/" + escritorio.id, escritorio);
    }
    delete(id) {
        return this.http.delete(_app_constants__WEBPACK_IMPORTED_MODULE_5__["AppConstants"].baseUrlEscritorioContabil + "apagar/" + id);
    }
};
EscritorioContabilService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
EscritorioContabilService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: "root",
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
], EscritorioContabilService);



/***/ }),

/***/ "ydG1":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastrar-escritorio-contabil/cadastrar-escritorio-contabil.component.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<button type=\"button\" class=\"btn btn-light\" onclick=\"history.back()\" style=\"background-color: rgb(228, 229, 230);\"> <i\r\n    class=\"fa fa-arrow-left\"></i> Voltar</button>\r\n<div class=\"container\">\r\n  <div class=\"row justify-content-md-center\">\r\n    <div class=\"col-md-auto\">\r\n\r\n      <div class=\"card border-light\">\r\n\r\n        <div class=\"card-header bg-white\">\r\n          <h3 *ngIf=\"(!editando)\">Cadastrar Escritório Contabilidade</h3>\r\n          <h3 *ngIf=\"(editando)\"> Editar Escritório de Contabilidade</h3>\r\n          <p *ngIf=\"(!editando)\" class=\"card-title\">Leva alguns segundos para criar sua conta</p>\r\n        </div>\r\n\r\n        <form #f=\"ngForm\" class=\"form\" [formGroup]=\"escritorioForm\">\r\n\r\n          <div class=\"card-body \">\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label for=\"cnpj\">CNPJ</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.cnpj.invalid && (controls.cnpj.dirty || controls.cnpj.touched)}\"\r\n                type=\"text\" name=\"cnpj\" formControlName=\"cnpj\" id=\"cnpj\" mask=\"00.000.00/0000-000\"\r\n                aria-label=\"cnpj empresa\" (blur)=\"cnpjKeyPressEvent($event)\">\r\n              <div class=\"invalid-feedback\">\r\n                <div *ngIf=\"controls.cnpj.errors?.required\">O campo CNPJ é obrigatório.</div>\r\n                <div *ngIf=\"controls.cnpj.errors?.duplicado && controls.cnpj.dirty\">CNPJ já existe, por favor insira\r\n                  outro.</div>\r\n                <div *ngIf=\"controls.cnpj.errors?.cnpjInvalido && controls.cnpj.dirty\">CNPJ inválido, por favor insira\r\n                  outro.</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label for=\"razao-social\">Razão Social</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.razaoSocial.invalid && (controls.razaoSocial.dirty || controls.razaoSocial.touched)}\"\r\n                type=\"text\" name=\"razao-social\" formControlName=\"razaoSocial\" id=\"razaoSocial\"\r\n                aria-label=\"razao social empresa\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.razaoSocial.invalid && (controls.razaoSocial.dirty || controls.razaoSocial.touched)\">\r\n                <div *ngIf=\"controls.razaoSocial.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label for=\"nome-fantasia\">Nome Fantasia</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.nomeFantasia.invalid && (controls.nomeFantasia.dirty || controls.nomeFantasia.touched)}\"\r\n                type=\"text\" name=\"nome-fantasia\" formControlName=\"nomeFantasia\" id=\"nomeFantasia\"\r\n                aria-label=\"nome fantasia empresa\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.nomeFantasia.invalid && (controls.nomeFantasia.dirty || controls.nomeFantasia.touched)\">\r\n                <div *ngIf=\"controls.nomeFantasia.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div formGroupName=\"endereco\">\r\n              <div class=\"form-group mb-1\">\r\n                <label for=\"cep\">CEP</label>\r\n                <input class=\"form-control\" type=\"text\" name=\"cep\" id=\"cep\"\r\n                  [class.is-invalid]=\"endereco.get('cep').invalid && endereco.get('cep').dirty || endereco.get('cep').touched\"\r\n                  formControlName=\"cep\" mask=\"00000-000\" aria-label=\"cep\">\r\n                <div class=\"invalid-feedback\">\r\n                  O campo CEP é obrigatório. Deve ter 8 dígitos.\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group mb-1\">\r\n                <label for=\"rua\">Rua</label>\r\n                <input type=\"text\" class=\"form-control\" name=\"rua\" formControlName=\"rua\">\r\n              </div>\r\n              <div class=\"row\">\r\n                <div class=\"form-group col-8 mb-1\">\r\n                  <label for=\"bairro\">Bairro</label>\r\n                  <input type=\"text\" class=\"form-control\" name=\"bairro\" formControlName=\"bairro\">\r\n                </div>\r\n                <div class=\"form-group col mb-1\">\r\n                  <label for=\"numero\">Número</label>\r\n                  <input type=\"number\" class=\"form-control\" name=\"numero\" formControlName=\"numero\">\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group mb-1\">\r\n                <label for=\"complemento\">Complemento</label>\r\n                <input type=\"text\" class=\"form-control\" name=\"complemento\" formControlName=\"complemento\">\r\n              </div>\r\n            </div>\r\n            <div class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"contato\">Contato</label>\r\n              <input class=\"form-control\" type=\"text\" name=\"contato\" id=\"contato\" formControlName=\"contato\"\r\n              [ngClass]=\"{'is-invalid': controls.contato.invalid && (controls.contato.dirty || controls.contato.touched)}\"\r\n                mask=\"(00) 0 0000-0000\" showMaskTyped=\"true\" aria-label=\"contato escritório\">\r\n                <div class=\"invalid-feedback\">\r\n                  <div *ngIf=\"controls.contato.errors?.required\">Campo obrigatório</div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"email\">Email</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.email.invalid && (controls.email.dirty || controls.email.touched)}\"\r\n                type=\"text\" id=\"email \" name=\"email\" formControlName=\"email\" aria-label=\"email solicitante\" required>\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.email.invalid && (controls.email.dirty || controls.email.touched)\">\r\n                <div *ngIf=\"controls.email.errors?.required\">Campo Obrigatório</div>\r\n                <div *ngIf=\"controls.email.errors?.email\">Email Inválido</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"form-group col-8 mb-1\">\r\n                <label class=\"form-label\" for=\"atividadePrimariaCnae\">Atividade Primária Cnae</label>\r\n                <input class=\"form-control\"\r\n                  [ngClass]=\"{'is-invalid': controls.atividadePrimariaCnae.invalid && (controls.atividadePrimariaCnae.dirty || controls.atividadePrimariaCnae.touched)}\"\r\n                  type=\"text\" name=\"atividade-primaria-cnae\" id=\"atividadePrimariaCnae\"\r\n                  formControlName=\"atividadePrimariaCnae\" aria-label=\"atividade primaria cnae\">\r\n                <div class=\"invalid-feedback\"\r\n                  *ngIf=\"controls.atividadePrimariaCnae.invalid && (controls.atividadePrimariaCnae.dirty || controls.atividadePrimariaCnae.touched)\">\r\n                  <div *ngIf=\"controls.atividadePrimariaCnae.errors?.required\">Campo Obrigatório</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group col-4 mb-1\">\r\n                <label class=\"form-label\" for=\"codigoPrimariaCnae\">Código</label>\r\n                <input class=\"form-control\"\r\n                  [ngClass]=\"{'is-invalid': controls.codigoPrimariaCnae.invalid && (controls.codigoPrimariaCnae.dirty || controls.codigoPrimariaCnae.touched)}\"\r\n                  type=\"text\" name=\"codigo-primaria-cnae\" id=\"codigoPrimariaCnae\" formControlName=\"codigoPrimariaCnae\"\r\n                  aria-label=\"código primaria cnae\" mask=\"00.00-0/00\">\r\n                <div class=\"invalid-feedback\"\r\n                  *ngIf=\"controls.codigoPrimariaCnae.invalid && (controls.codigoPrimariaCnae.dirty || controls.codigoPrimariaCnae.touched)\">\r\n                  <div *ngIf=\"controls.codigoPrimariaCnae.errors?.required\">Campo Obrigatório</div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"row\">\r\n              <div class=\"form-group col-8 mb-1\">\r\n                <label class=\"form-label\" for=\"atividadeSecundariaCnae\">Atividade Secundária Cnae</label>\r\n                <input class=\"form-control\"\r\n                  [ngClass]=\"{'is-invalid': controls.atividadeSecundariaCnae.invalid && (controls.atividadeSecundariaCnae.dirty || controls.atividadeSecundariaCnae.touched)}\"\r\n                  type=\"text\" name=\"atividade-secundaria-cnae\" id=\"atividadeSecundariaCnae\"\r\n                  formControlName=\"atividadeSecundariaCnae\" aria-label=\"atividade secundaria cnae\">\r\n                <div class=\"invalid-feedback\"\r\n                  *ngIf=\"controls.atividadeSecundariaCnae.invalid && (controls.atividadeSecundariaCnae.dirty || controls.atividadeSecundariaCnae.touched)\">\r\n                  <div *ngIf=\"controls.atividadeSecundariaCnae.errors?.required\">Campo Obrigatório</div>\r\n                </div>\r\n              </div>\r\n              <div class=\"form-group col-4 mb-1\">\r\n                <label class=\"form-label\" for=\"codigoSecundariaCnae\">Código</label>\r\n                <input class=\"form-control\"\r\n                  [ngClass]=\"{'is-invalid': controls.codigoSecundariaCnae.invalid\r\n                                            && (controls.codigoSecundariaCnae.dirty || controls.codigoSecundariaCnae.touched)}\" type=\"text\"\r\n                  name=\"codigo-secundaria-cnae\" id=\"codigoSecundariaCnae\" formControlName=\"codigoSecundariaCnae\"\r\n                  aria-label=\"código secundaria cnae\" mask=\"00.00-0/00\">\r\n                <div class=\"invalid-feedback\"\r\n                  *ngIf=\"controls.codigoSecundariaCnae.invalid && (controls.codigoSecundariaCnae.dirty || controls.codigoSecundariaCnae.touched)\">\r\n                  <div *ngIf=\"controls.codigoSecundariaCnae.errors?.required\">Campo Obrigatório</div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n            <hr>\r\n            <h4 class=\"mb-4\">Dados do Contador Responsável</h4>\r\n\r\n            <div *ngIf=\"(!editando)\" class=\"form-group mb-1\">\r\n              <label for=\"nome-responsavel\">Nome Completo</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.nomeResponsavel.invalid && (controls.nomeResponsavel.dirty || controls.nomeResponsavel.touched)}\"\r\n                type=\"text\" name=\"nome-responsavel\" formControlName=\"nomeResponsavel\" id=\"nomeResponsavel\"\r\n                aria-label=\"nome responsavel\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.nomeResponsavel.invalid && (controls.nomeResponsavel.dirty || controls.nomeResponsavel.touched)\">\r\n                <div *ngIf=\"controls.nomeResponsavel.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div *ngIf=\"(!editando)\" class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"cpf\">CPF</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.cpf.invalid && (controls.cpf.dirty || controls.cpf.touched)}\"\r\n                type=\"text\" name=\"cpf\" id=\"cpf\" formControlName=\"cpf\" mask=\"000.000.000-00\"\r\n                aria-label=\"cpf responsavel\" (blur)=\"cpfKeyPressEvent($event)\">\r\n              <div class=\"invalid-feedback\">\r\n                <div *ngIf=\"controls.cpf.errors?.required\">O campo CPF é obrigatório.</div>\r\n                <div *ngIf=\"controls.cpf.errors?.duplicado && controls.cpf.dirty\">CPF já existe,\r\n                  por favor insira outro.</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"crc\">CRC</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.crc.invalid && (controls.crc.dirty || controls.crc.touched)}\"\r\n                type=\"text\" name=\"crc\" id=\"crc\" mask=\"SS-000000/S9\" formControlName=\"crc\" placeholder=\"XX-0000000/X0\"\r\n                aria-label=\"crc contador\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.crc.invalid && (controls.crc.dirty || controls.crc.touched)\">\r\n                <div *ngIf=\"controls.crc.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"contatoResponsavel\">Celular</label>\r\n              <input class=\"form-control\"\r\n                type=\"text\" name=\"contatoResponsavel\" id=\"contatoResponsavel\" formControlName=\"contatoResponsavel\" mask=\"(00) 0 0000-0000\"\r\n                showMaskTyped=\"true\" aria-label=\"contato do responsável\">\r\n            </div>\r\n\r\n            <div class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"emailResponsavel\">Email</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.emailResponsavel.invalid && (controls.emailResponsavel.dirty || controls.emailResponsavel.touched)}\"\r\n                type=\"text\" id=\"emailResponsavel\" name=\"emailResponsavel\" formControlName=\"emailResponsavel\" aria-label=\"email responsavel\" required>\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.emailResponsavel.invalid && (controls.emailResponsavel.dirty || controls.emailResponsavel.touched)\">\r\n                <div *ngIf=\"controls.emailResponsavel.errors?.required\">Campo Obrigatório</div>\r\n                <div *ngIf=\"controls.emailResponsavel.errors?.email\">Email Inválido</div>\r\n              </div>\r\n            </div>\r\n\r\n\r\n            <!--\r\n            <div *ngIf=\"(!editando)\" class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"login\">Login</label>\r\n              <input class=\"form-control\"\r\n                [ngClass]=\"{'is-invalid': controls.login.invalid && (controls.login.dirty || controls.login.touched)}\"\r\n                type=\"text\" name=\"login\" id=\"login\" formControlName=\"login\" aria-label=\"Login de usuário\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.login.invalid && (controls.login.dirty || controls.login.touched)\">\r\n                <div *ngIf=\"controls.login.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n            </div>\r\n\r\n            <div *ngIf=\"(!editando)\" class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"senha\">Senha</label>\r\n              <input class=\"form-control\" [ngClass]=\"{'is-invalid': controls.senha.invalid &&\r\n                                         (controls.senha.dirty || controls.senha.touched)}\"\r\n                type=\"password\" name=\"senha\" id=\"senha\" formControlName=\"senha\" aria-label=\"senha\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.senha.invalid && (controls.senha.dirty || controls.senha.touched)\">\r\n                <div *ngIf=\"controls.senha.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n            </div>\r\n\r\n            TODO: corrigir status dos erros para aplicar os css\r\n            <div *ngIf=\"(!editando)\" class=\"form-group mb-1\">\r\n              <label class=\"form-label\" for=\"senhaConfirmacao\">Confirmar Senha</label>\r\n              <input class=\"form-control\" [ngClass]=\"{'is-invalid': (controls.senhaConfirmacao.invalid ||\r\n                                          (controls.senhaConfirmacao.valid && escritorioForm.errors?.isNoMath)) &&\r\n                                          (controls.senhaConfirmacao.dirty || controls.senhaConfirmacao.touched)}\"\r\n                type=\"password\" name=\"senha-confirmacao\" id=\"senhaConfirmacao\" formControlName=\"senhaConfirmacao\"\r\n                aria-label=\"senha\">\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"controls.senhaConfirmacao.invalid && (controls.senhaConfirmacao.dirty || controls.senhaConfirmacao.touched)\">\r\n                <div *ngIf=\"controls.senha.errors?.required\">Campo Obrigatório</div>\r\n              </div>\r\n              <div class=\"invalid-feedback\"\r\n                *ngIf=\"(controls.senhaConfirmacao.invalid || controls.senhaConfirmacao.valid) && (controls.senhaConfirmacao.dirty || controls.senhaConfirmacao.touched)\">\r\n                <div *ngIf=\"escritorioForm.errors?.isNoMath\">Senha não confirmada</div>\r\n              </div>\r\n            </div>\r\n            -->\r\n\r\n\r\n          </div>\r\n\r\n          <div class=\" card-footer bg-light\">\r\n            <button *ngIf=\"(!editando)\" class=\"btn btn-block btn-success\" [disabled]=\"!(escritorioForm.valid)\"\r\n              (click)=\"onSubmit(escritorioForm)\">Cadastrar</button>\r\n            <button *ngIf=\"(editando)\" class=\"btn btn-block btn-success\" [disabled]=\"!(escritorioForm.valid)\"\r\n              (click)=\"editar(escritorioForm)\">Editar</button>\r\n          </div>\r\n\r\n        </form>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "wAiw");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zb5I":
/*!********************************************************!*\
  !*** ./src/app/aviso-pop-up/aviso-pop-up.component.ts ***!
  \********************************************************/
/*! exports provided: AvisoPopUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvisoPopUpComponent", function() { return AvisoPopUpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_aviso_pop_up_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./aviso-pop-up.component.html */ "cWAq");
/* harmony import */ var _aviso_pop_up_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./aviso-pop-up.component.scss */ "RCyV");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "8Y7J");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ "0IaG");





let AvisoPopUpComponent = class AvisoPopUpComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    onNoClick() {
        this.dialogRef.close();
    }
};
AvisoPopUpComponent.ctorParameters = () => [
    { type: _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"] }
];
AvisoPopUpComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-aviso-pop-up',
        template: _raw_loader_aviso_pop_up_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_aviso_pop_up_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    }),
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"]])
], AvisoPopUpComponent);



/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map