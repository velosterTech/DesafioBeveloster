package br.com.wk.dto;

import br.com.wk.model.Documento;

public class DocumentoDTO {
	private String nome;
	private String cnpjEscritorio;


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Documento transformaParaObjeto() {
		Documento documento = new Documento();
		documento.setNome(this.nome);
		return documento;
	}

	public String getCnpjEscritorio() {
		return cnpjEscritorio;
	}

	public void setCnpjEscritorio(String cnpjEscritorio) {
		this.cnpjEscritorio = cnpjEscritorio;
	}
}
