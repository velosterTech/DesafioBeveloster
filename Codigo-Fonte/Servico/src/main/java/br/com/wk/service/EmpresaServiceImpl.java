package br.com.wk.service;

import br.com.wk.dto.EmpresaDTO;
import br.com.wk.exception.EntidadeNaoEncontradaException;
import br.com.wk.model.RegimeTributario;
import br.com.wk.model.*;
import br.com.wk.repository.EmpresaRepository;
import br.com.wk.repository.RegimeTributarioRepository;
import br.com.wk.repository.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired
	private EmpresaRepository empresaRepository;

//	@Autowired
//	private PessoaJuridicaService pessoajuridicaService;
//
//	@Autowired
//	private EscritorioContabilRepository escritorioContabilRepository;

	@Autowired
	UsuarioRepository usuarioRepo;

	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	EscritorioService escritorioService;

	@Autowired
	RegimeTributarioRepository regimeTributarioRepo;

	@Override
	public Empresa save(EmpresaDTO empresaDTO) {

		Empresa empresa = empresaDTO.transformToEmpresa();
	
		empresa.setEscritorio(escritorioService.findByCnpj(empresaDTO.getEscritorio()));
		
		RegimeTributario regimeTributario = new RegimeTributario();
		regimeTributario.setNome(empresaDTO.getRegimeTributario());
		regimeTributarioRepo.save(regimeTributario);
		empresa.setRegimeTributario(regimeTributario);
		Empresa empresaSalva = empresaRepository.save(empresa);
//		usuarioRepository.insereAcessoRoleAdminEmpresa(usuario.getId());
//		usuarioRepository.insereEmpresa(usuario.getId(), empresaSalva.getId());
		
		UsuarioEmpresa usuario = empresaDTO.transformToUsuarioAdm();
		usuario.getEmpresas().add(empresaSalva);
		Long nCpf = usuarioRepo.numeroDeCpfs(usuario.getCpf());
		if(nCpf >= 1){
			usuario.setLogin(usuario.getLogin() + (nCpf + 1));
		}
		
		Usuario usuarioSalvo = usuarioRepo.save(usuario);
		usuarioRepo.insereAcessoRoleAdminEmpresa(usuarioSalvo.getId());
		usuarioRepo.insereAcessoRoleUserEmpresa(usuarioSalvo.getId());
		return empresaSalva;
	}

	@Override
	public List<Empresa> findAllLike(String busca, Boolean status, Long id) {

		Usuario usuario = usuarioRepo.findById(id).get();
		List<Empresa> empresaResult = new ArrayList<>();
		
		if (status) {
			if(!usuario.getEscritorios().isEmpty()) { // UsuarioAdminEscritorio
				usuario.getEscritorios().stream()
						.forEach(e -> empresaResult.addAll(empresaRepository.findAllLikeEscritorioTrue(busca, e)));
			} else if (!usuario.getEmpresas().isEmpty()){ // UsuarioAdminEmpresa
				empresaResult.addAll(empresaRepository.findAllLikeUsuariosTrue(id, busca));
			} else { // UsuarioAdmin
				empresaResult.addAll(empresaRepository.findAllLikeTrue(busca));
			}

		} else {
			if(!usuario.getEscritorios().isEmpty()){
				usuario.getEscritorios().stream()
						.forEach(e -> empresaResult.addAll(empresaRepository.findAllLikeEscritorio(busca, e)));
			} else if (!usuario.getEmpresas().isEmpty()){
				empresaResult.addAll(empresaRepository.findAllLikeUsuarios(id, busca));
			} else {
				empresaResult.addAll(empresaRepository.findAllLike(busca));
			}
		}
		return empresaResult;
	}

	@Override
	public Empresa findById(Long id) {
		return empresaRepository.findById(id).orElse(null);
	}

	@Override
	public Empresa findByCnpj(String cnpj) {
		return empresaRepository.findByCNPJ(cnpj).orElse(null);
	}

//	@Override
//	public void delete(Long id) {
//		Empresa empresa = findById(id);
////		empresa.setStatus(false);
//		empresaRepository.delete(empresa);
//	}
	
	@Override
	public void ativarDesativar(Long id) {
		System.out.println(id);
		Empresa empresa = empresaRepository.findById(id)
				.orElseThrow(() -> {
					throw new EntidadeNaoEncontradaException("Empresa do ID [ " + id + " ] não encontrada");
					});
		System.out.println(empresa.getId());
		System.out.println(empresa.getStatus());
		empresa.setStatus(!empresa.getStatus());
		if(!empresa.getStatus()){
			for(Usuario usuario: empresa.getUsuarios()){
				if(!(usuario.getEmpresas().size() > 1)){
					usuario.setStatus(false);
					usuarioRepo.save(usuario);
				}
			}
		}
		
		empresaRepository.save(empresa);
	}

	@Override
	public Empresa update(EmpresaDTO empresaDto) {
		Empresa empresaAtualizada = empresaDto.transformToEmpresa();
		
		Escritorio escritorio = escritorioService.findByCnpj(empresaDto.getEscritorio());
		System.out.println(escritorio.toString());
		empresaAtualizada.setEscritorio(escritorio);
		
		Empresa empresaAlvo = findById(empresaDto.getId());
		
//		RegimeTributario regimeTributario = regimeTributarioRepo.findByNome(empresaDto.getRegimeTributario());
//		System.out.println(regimeTributario);
		
		empresaAtualizada.setRegimeTributario(empresaAlvo.getRegimeTributario());
		
		//empresaAtualizada.setUsuarios(empresaAlvo.getUsuarios());
		
		BeanUtils.copyProperties(empresaAtualizada, empresaAlvo);
		
		return empresaRepository.save(empresaAlvo);

	}

	@Override
	public Empresa findByInscricaoEstadual(String inscricao, Escritorio escritorio) {
		return empresaRepository.findByInscricaoEstadual(inscricao, escritorio).orElse(null);
	}

	@Override
	public Empresa findByCnpjEscritorio(String cnpj, Escritorio escritorio) {
		return empresaRepository.findByCNPJEscritorio(cnpj, escritorio).orElse(null);
	}

	@Override
	public List<Empresa> findAllByEscritorioId(Long id) {
		return empresaRepository.findByEscritorioId(id);
	}


//
//	@Override
//	public Empresa findByUsuario(Usuario usuario) {
//		return empresaRepository.findByUsuario(usuario);
//	}

//	@Override
//	public List<Empresa> findAllLikeEscritorio(String busca, EscritorioContabil escritorioId){
//		return empresaclienteRepository.findAllLikeEscritorio(busca, escritorioId);
//	}

//
//	@Override
//	public List<EmpresaCliente> findAll(Long id, String busca) {
//		Usuario usuario = usuarioRepository.findById(id).get();
//		if (usuario.getRoles().get(0).getNomeRole().equals("ROLE_ADMIN")) {
//			return empresaclienteRepository.findAllLike(busca);
//		} else if (usuario.getRoles().get(0).getNomeRole().equals("ROLE_ADMIN_ESCRITORIO")) {
//			return empresaclienteRepository.findAllLikeEscritorio(busca, usuario.getEscritorios().get(0));
//		}
//		return empresaclienteRepository.findAll();
//
//	}

}