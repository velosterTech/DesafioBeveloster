//package br.com.wk.controller;
//
//import java.util.List;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import br.com.wk.model.Categoria;
//import br.com.wk.service.CategoriaService;
//
//@RestController
//@RequestMapping(value = "/categoria")
//public class CategoriaController {
//
//	@Autowired
//	CategoriaService categoriaService;
//	
//	@PostMapping("/novo")
//	public ResponseEntity<Categoria> createCategoria(@RequestBody Categoria categoria){
//		Categoria result = categoriaService.save(categoria);		
//		return new ResponseEntity<Categoria>(result, HttpStatus.CREATED);
//	}
//	
//	@PutMapping("/update/{id}")
//	public ResponseEntity<Categoria> updateCategoria(@RequestBody Categoria categoria, @PathVariable("id") Long idCategoria){
//		Categoria result = categoriaService.save(categoria);		
//		return new ResponseEntity<Categoria>(result, HttpStatus.CREATED);
//	}
//	
//	@DeleteMapping("/delete/{id}")
//	public ResponseEntity<Void> deleteCategoria(@PathVariable("id") Long idCategoria){
//		categoriaService.delete(idCategoria);		
//		return new ResponseEntity<Void>(HttpStatus.OK);
//	}
//	
//	@GetMapping("/find/all")
//	public ResponseEntity<List<Categoria>> findAll(){	
//		return new ResponseEntity<List<Categoria>>(categoriaService.findAll(), HttpStatus.OK);
//	}
//	
//	@GetMapping("/find/id/{id}")
//	public ResponseEntity<Categoria> findById(@PathVariable("id") Long id){	
//		return new ResponseEntity<Categoria>(categoriaService.findById(id).get(), HttpStatus.OK);
//	}
//	
//	@GetMapping("/find/name/{nomeCategoria}")
//	public ResponseEntity<List<Categoria>> findByName(@PathVariable("nomeCategoria") String nomeCategoria){	
//		return new ResponseEntity<List<Categoria>>(categoriaService.findByName(nomeCategoria), HttpStatus.OK);
//	}
//}
