package br.com.wk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.wk.dto.DepartamentoDTO;
import br.com.wk.model.Departamento;
import br.com.wk.model.Escritorio;
import br.com.wk.repository.DepartamentoRepository;

@Service
public class DepartamentoServiceImpl implements DepartamentoService {

    @Autowired
    DepartamentoRepository departamentoRepository;
    @Autowired
    EscritorioService escritorioService;

    @Override
    public Departamento add(DepartamentoDTO departamentoDTO) {
        System.out.println(departamentoDTO.getNome() + " " + departamentoDTO.getCnpj());
        Departamento departamento = departamentoDTO.transformaParaObjeto();
        Escritorio escritorio = escritorioService.findByCnpj(departamentoDTO.getCnpj());
        departamento.setEscritorio(escritorio);
        return departamentoRepository.save(departamento);
    }
    
}
