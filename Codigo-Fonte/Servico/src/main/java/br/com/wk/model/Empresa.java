package br.com.wk.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.wk.enums.NaturezaJuridica;

@Entity
@Table(name="saffo_empresa")
public class Empresa {
	
	@Id
	@Column(name="saffo_empresa_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGeneratorEmpresa")
    @SequenceGenerator(name = "seqGeneratorEmpresa", sequenceName="SAFFO_EMPRESA_SEQ", allocationSize=1)
	private Long id;
	
	@Column(name="saffo_empresa_cnpj")
	private String cnpj;
	
	@Column(name="saffo_empresa_razao_social")
	private String razaoSocial;
	
	@Column(name="saffo_empresa_nome_fantasia")
	private String nomeFantasia;	
	
	@Column(name="saffo_empresa_inscricao_estadual")
	private String inscricaoEstadual;
	
	@Column(name="saffo_empresa_cep")
	private String cep;
	
	@Column(name="saffo_empresa_logradouro")
	private String logradouro;
	
	@Column(name="saffo_empresa_bairro")
	private String bairro;
	
	@Column(name="saffo_empresa_numero")
	private String numero;
	
	@Column(name="saffo_empresa_complemento")
	private String complemento;
	
	@Column(name="saffo_empresa_contato")
	private String contato;	
	
	@Column(name="saffo_empresa_email")
	private String email;
	
	@Column(name="saffo_empresa_atvd_primaria")
	private String atvdPrimaria;
	
	@Column(name="saffo_empresa_cod_cnae_primaria")
	private String codCnaePrimaria;
	
	@Column(name="saffo_empresa_atvd_secundaria")
	private String atvdSecundaria;
	
	@Column(name="saffo_empresa_cod_cnae_secundaria")
	private String codCnaeSecundaria;
	
	@Column(name = "saffo_empresa_status")
    private Boolean status = true;
	
	@Column(name="saffo_empresa_nat_juridica")
	@Enumerated(EnumType.STRING)
	private NaturezaJuridica naturezaJuridica;	
	
	@ManyToOne
	@JoinColumn(name="saffo_rgm_trib_id")	
	private RegimeTributario regimeTributario;
	
	@ManyToOne
	@JoinColumn(name="saffo_escritorio_id")
	private Escritorio escritorio;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
    		name = "saffo_tarefa_empresa",
    		joinColumns = @JoinColumn(name= "saffo_empresa_id"),
    		inverseJoinColumns = @JoinColumn(name="saffo_tarefa_id")
    )
	private Set<Tarefa> tarefas = new HashSet<>();
	
	@ManyToMany(mappedBy = "empresas")
    private Set<Usuario> usuarios = new HashSet<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAtvdPrimaria() {
		return atvdPrimaria;
	}

	public void setAtvdPrimaria(String atvdPrimaria) {
		this.atvdPrimaria = atvdPrimaria;
	}

	public String getCodCnaePrimaria() {
		return codCnaePrimaria;
	}

	public void setCodCnaePrimaria(String codCnaePrimaria) {
		this.codCnaePrimaria = codCnaePrimaria;
	}

	public String getAtvdSecundaria() {
		return atvdSecundaria;
	}

	public void setAtvdSecundaria(String atvdSecundaria) {
		this.atvdSecundaria = atvdSecundaria;
	}

	public String getCodCnaeSecundaria() {
		return codCnaeSecundaria;
	}

	public void setCodCnaeSecundaria(String codCnaeSecundaria) {
		this.codCnaeSecundaria = codCnaeSecundaria;
	}

	public NaturezaJuridica getNaturezaJuridica() {
		return naturezaJuridica;
	}

	public void setNaturezaJuridica(NaturezaJuridica naturezaJuridica) {
		this.naturezaJuridica = naturezaJuridica;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public RegimeTributario getRegimeTributario() {
		return regimeTributario;
	}

	public void setRegimeTributario(RegimeTributario regimeTributario) {
		this.regimeTributario = regimeTributario;
	}

	public Escritorio getEscritorio() {
		return escritorio;
	}

	public void setEscritorio(Escritorio escritorio) {
		this.escritorio = escritorio;
	}

	@JsonIgnore
	public Set<Tarefa> getTarefas() {
		return tarefas;
	}

	public void setTarefas(Set<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}


	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@JsonIgnore
	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	@Override
	public int hashCode() {
		return Objects.hash(atvdPrimaria, atvdSecundaria, bairro, cep, cnpj, codCnaePrimaria, codCnaeSecundaria,
				complemento, contato, email, escritorio, id, inscricaoEstadual, logradouro, naturezaJuridica,
				nomeFantasia, numero, razaoSocial, regimeTributario, status, tarefas, usuarios);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Empresa))
			return false;
		Empresa other = (Empresa) obj;
		return Objects.equals(atvdPrimaria, other.atvdPrimaria) && Objects.equals(atvdSecundaria, other.atvdSecundaria)
				&& Objects.equals(bairro, other.bairro) && Objects.equals(cep, other.cep)
				&& Objects.equals(cnpj, other.cnpj) && Objects.equals(codCnaePrimaria, other.codCnaePrimaria)
				&& Objects.equals(codCnaeSecundaria, other.codCnaeSecundaria)
				&& Objects.equals(complemento, other.complemento) && Objects.equals(contato, other.contato)
				&& Objects.equals(email, other.email) && Objects.equals(escritorio, other.escritorio)
				&& Objects.equals(id, other.id) && Objects.equals(inscricaoEstadual, other.inscricaoEstadual)
				&& Objects.equals(logradouro, other.logradouro) && naturezaJuridica == other.naturezaJuridica
				&& Objects.equals(nomeFantasia, other.nomeFantasia) && Objects.equals(numero, other.numero)
				&& Objects.equals(razaoSocial, other.razaoSocial)
				&& Objects.equals(regimeTributario, other.regimeTributario) && Objects.equals(status, other.status)
				&& Objects.equals(tarefas, other.tarefas) && Objects.equals(usuarios, other.usuarios);
	}

	
	
}
	