package br.com.wk.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "saffo_regra")
public class Regra {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGeneratorRegra")
    @SequenceGenerator(name = "seqGeneratorRegra", sequenceName="SAFFO_REGRA_SEQ", allocationSize=1)
    @Column(name = "saffo_regra_id")
    private Long id;

    @Column(name = "saffo_regra_nome")
    private String nome;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "saffo_configuracao_id")
    private Configuracao configuracao;

    

    public Regra(String nome, Configuracao configuracao) {
        this.nome = nome;
        this.configuracao = configuracao;
    }

    public Regra() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonIgnore
    public Configuracao getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Regra other = (Regra) obj;
        if (configuracao == null) {
            if (other.configuracao != null)
                return false;
        } else if (!configuracao.equals(other.configuracao))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (nome == null) {
            if (other.nome != null)
                return false;
        } else if (!nome.equals(other.nome))
            return false;
        return true;
    }
    
    
}
