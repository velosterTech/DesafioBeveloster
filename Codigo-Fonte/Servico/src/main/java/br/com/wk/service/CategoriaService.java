//package br.com.wk.service;
//
//import java.util.List;
//import java.util.Optional;
//
//import br.com.wk.model.Categoria;
//
//public interface CategoriaService {
//	List<Categoria> findAll();
//	List<Categoria> findByName(String name);
//	Optional<Categoria> findById(Long id);
//	Categoria save(Categoria categoria);
//	void delete(Long id);
//}
