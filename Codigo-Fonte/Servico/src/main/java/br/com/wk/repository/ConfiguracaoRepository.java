package br.com.wk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.wk.model.Configuracao;
import br.com.wk.model.Escritorio;

@Repository
public interface ConfiguracaoRepository extends JpaRepository<Configuracao, Long> {
    @Query(value = "Select c from Configuracao c where c.documento.escritorio =?1 AND lower(c.documento.nome) LIKE lower(concat(?2,'%')) AND c.status = true")
    List<Configuracao> findAllTrueByEscritorio(Escritorio escritorio, String busca);

    @Query(value = "Select c from Configuracao c where c.documento.escritorio =?1 AND lower(c.documento.nome) LIKE lower(concat(?2,'%'))")
    List<Configuracao> findAllByEscritorio(Escritorio escritorio, String busca);

    @Query(value = "Select c from Configuracao c where lower(c.documento.nome) LIKE lower(concat(?1,'%'))")
    List<Configuracao> findAllLike(String busca);
}
