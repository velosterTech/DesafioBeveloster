//package br.com.wk.service;
//
//import java.util.List;
//
//import br.com.wk.dto.TipoDocumentoDTO;
//import br.com.wk.model.TipoDocumento;
//
//public interface TipoDocumentoService {
//	
//	public TipoDocumento findDocumentoById(Long id);
//	public TipoDocumento createDocumento(TipoDocumentoDTO tipoDocumento);
//	public List<TipoDocumento> findAllTipoDocumentosByEscritorio(Long idEscritorio);
//	public List<TipoDocumento> findAllTipoDocumentos(Long id);
//	public void deleteById(Long id);
//	public TipoDocumento updateDocumento(TipoDocumentoDTO tipoDocumento, Long id);
//	public List<TipoDocumento> findAllByUsuarioEscritorio(Long userId);
//	public List<TipoDocumento> findAllByCategoria(Long id, Long categoriaId);
//}
