package br.com.wk.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="saffo_regime_tributario")
public class RegimeTributario {
	
	@Column(name="saffo_rgm_trib_id")
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGeneratorRegimeTributario")
    @SequenceGenerator(name = "seqGeneratorRegimeTributario", sequenceName="SAFFO_RGM_TRIB_SEQ", allocationSize=1)	
	private Long Id;
	
	@Column(name="saffo_rgm_trib_nome")
	private String nome;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Id, nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof RegimeTributario))
			return false;
		RegimeTributario other = (RegimeTributario) obj;
		return Objects.equals(Id, other.Id) && Objects.equals(nome, other.nome);
	}

	
}
	