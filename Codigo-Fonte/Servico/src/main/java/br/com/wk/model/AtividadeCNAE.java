//package br.com.wk.model;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//
//@Entity 
//@Table (name="atividade_cnae")
//public class AtividadeCNAE {
//	
//	@Id
//	private long id;
//	
//	@OneToMany (mappedBy="atividadePrimaria")
//	private List<Escritorio> escritoriosAtividadePrimaria = new ArrayList<Escritorio>();
//	
//	@OneToMany (mappedBy="atividadeSecundaria")
//	private List<Escritorio> escritoriosAtividadeSecundaria = new ArrayList<Escritorio>();
//	
//	@Column (name="nome_atividade")
//	private String nomeAtividade;
//	
//	private String descricao;
//
//	public long getId() {
//		return id;
//	}
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getNomeAtividade() {
//		return nomeAtividade;
//	}
//
//	public void setNomeAtividade(String nomeAtividade) {
//		this.nomeAtividade = nomeAtividade;
//	}
//
//	public String getDescricao() {
//		return descricao;
//	}
//
//	public void setDescricao(String descricao) {
//		this.descricao = descricao;
//	}
//	
//	
//	public List<Escritorio> getEscritoriosAtividadePrimaria() {
//		return escritoriosAtividadePrimaria;
//	}
//	
//	public void setEscritoriosAtividadePrimaria(List<Escritorio> escritoriosAtividadePrimaria) {
//		this.escritoriosAtividadePrimaria = escritoriosAtividadePrimaria;
//	}
//
//	public List<Escritorio> getEscritoriosAtividadeSecundaria() {
//		return escritoriosAtividadeSecundaria;
//	}
//
//	public void setEscritoriosAtividadeSecundaria(List<Escritorio> escritoriosAtividadeSecundaria) {
//		this.escritoriosAtividadeSecundaria = escritoriosAtividadeSecundaria;
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(descricao, escritoriosAtividadePrimaria, escritoriosAtividadeSecundaria, id, nomeAtividade);
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!(obj instanceof AtividadeCNAE))
//			return false;
//		AtividadeCNAE other = (AtividadeCNAE) obj;
//		return Objects.equals(descricao, other.descricao)
//				&& Objects.equals(escritoriosAtividadePrimaria, other.escritoriosAtividadePrimaria)
//				&& Objects.equals(escritoriosAtividadeSecundaria, other.escritoriosAtividadeSecundaria)
//				&& id == other.id && Objects.equals(nomeAtividade, other.nomeAtividade);
//	}
//	
//	
//}
