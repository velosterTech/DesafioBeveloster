
//package br.com.wk.controller;
//
//import java.util.List;
//import org.springframework.data.domain.Sort;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.wk.dto.MensagemDTO;
//import br.com.wk.model.Categoria;
//import br.com.wk.model.EmpresaCliente;
//import br.com.wk.model.Mensagem;
//import br.com.wk.model.TipoDocumento;
//import br.com.wk.model.Usuario;
//import br.com.wk.repository.EscritorioContabilRepository;
//import br.com.wk.repository.MensagemRepository;
//import br.com.wk.repository.UsuarioRepository;
//import br.com.wk.service.CategoriaService;
//import br.com.wk.service.EmpresaClienteService;
//import br.com.wk.service.EscritorioService;
//import br.com.wk.service.MensagemService;
//import br.com.wk.service.PessoaJuridicaService;
//import br.com.wk.service.TipoDocumentoService;
//
//@RestController
//@RequestMapping(value="/mensagem")
//public class MensagemController {
//    @Autowired
//    private MensagemRepository mensagemRepository;
//    @Autowired 
//    private CategoriaService categoriaService;
//    @Autowired
//    private TipoDocumentoService tipoDocumentoService;
//    @Autowired
//    private UsuarioRepository usuarioRepository;
//    @Autowired
//    private MensagemService mensagemService;
//
//    @GetMapping(value = "/todas")
//    public ResponseEntity<List<Mensagem>> getAll(){
//        return new ResponseEntity<List<Mensagem>>((List<Mensagem>)mensagemRepository.findAll(), HttpStatus.OK);
//    }
//
//    @GetMapping(value = "/todas/{id}")
//    public ResponseEntity<List<Mensagem>> getAllSpec(@PathVariable long id){
//        return new ResponseEntity<List<Mensagem>>(mensagemService.findAll(id), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/nova/{id}")
//    public ResponseEntity<Mensagem> novaMsg(@PathVariable Long id, @RequestBody Mensagem mensagem){
//        return new ResponseEntity<Mensagem>(mensagemRepository.save(mensagemService.novMensagem(id, mensagem)), HttpStatus.OK);
//    }
//
//    @PostMapping(value = "/nova/solicitacao/{id}")
//    public ResponseEntity<Mensagem> novaSolicitacao(@PathVariable Long id, @RequestBody Mensagem mensagem, @RequestParam Long categoriaId,@RequestParam Long tipoDocumentoId){
//        return new ResponseEntity<Mensagem>(mensagemService.novaSolicitacao(id, mensagem, categoriaId, tipoDocumentoId), HttpStatus.OK);
//    }
//
//    @PutMapping(value = "/atualizaStatus/{id}")
//    public ResponseEntity<Mensagem> atualizaStatus(@PathVariable long id){
//        return new ResponseEntity<Mensagem>(mensagemService.atualizaStatus(id), HttpStatus.OK);
//    }
//}