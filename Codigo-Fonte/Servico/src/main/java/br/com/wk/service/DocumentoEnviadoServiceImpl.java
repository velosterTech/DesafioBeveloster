package br.com.wk.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.wk.model.DocumentoEnviado;
import br.com.wk.repository.DocumentoEnviadoRepository;

@Service
public class DocumentoEnviadoServiceImpl implements DocumentoEnviadoService{
    @Autowired
    DocumentoEnviadoRepository documentoEnviadoRepository;

    @Override
    public DocumentoEnviado add(DocumentoEnviado documentoEnviado) {
        return documentoEnviadoRepository.save(documentoEnviado);
    }

    @Override
    public List<DocumentoEnviado> findAllByEmpresa(Long id) {
        return documentoEnviadoRepository.findAllEmpresa(id);
    }


}
