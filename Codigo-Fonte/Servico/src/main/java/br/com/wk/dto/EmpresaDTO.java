package br.com.wk.dto;

import java.util.HashSet;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.wk.enums.NaturezaJuridica;
import br.com.wk.enums.NivelDeAcessoDoSistema;
import br.com.wk.enums.RegimeTributarioEnum;
import br.com.wk.model.Empresa;
import br.com.wk.model.Perfil;
import br.com.wk.model.Usuario;
import br.com.wk.model.UsuarioEmpresa;

public class EmpresaDTO {
	
	private Long id;
	private String escritorio;
	private String cnpj;
	private String razaoSocial;
	private String nomeFantasia;
	private String inscricaoEstadual;
	private String cep;
	private String logradouro;
	private String bairro;
	private String numero;
	private String complemento;
	private String contato;
	private String email;
	private String codCnaePrimaria;
	private String atvdPrimaria;
	private String codCnaeSecundaria;
	private String atvdSecundaria;
	private String regimeTributario;
	private NaturezaJuridica naturezaJuridica;

	private String nomeSolicitante;
	private String cpf;
	private String emailSolicitante;
	private String celular;

	public EmpresaDTO() {
	}

	public EmpresaDTO(Long id, String escritorio, String cnpj, String razaoSocial, String nomeFantasia,
			String inscricaoEstadual, String cep, String logradouro, String bairro, String numero, String complemento,
			String contato, String email, String codCnaePrimaria, String atvdPrimaria, String codCnaeSecundaria,
			String atvdSecundaria, String regimeTributario, NaturezaJuridica naturezaJuridica, String nomeSolicitante,
			String cpf, String emailSolicitante, String celular) {
		this.id = id;
		this.escritorio = escritorio;
		this.cnpj = cnpj;
		this.razaoSocial = razaoSocial;
		this.nomeFantasia = nomeFantasia;
		this.inscricaoEstadual = inscricaoEstadual;
		this.cep = cep;
		this.logradouro = logradouro;
		this.bairro = bairro;
		this.numero = numero;
		this.complemento = complemento;
		this.contato = contato;
		this.email = email;
		this.codCnaePrimaria = codCnaePrimaria;
		this.atvdPrimaria = atvdPrimaria;
		this.codCnaeSecundaria = codCnaeSecundaria;
		this.atvdSecundaria = atvdSecundaria;
		this.regimeTributario = regimeTributario;
		this.naturezaJuridica = naturezaJuridica;
		this.nomeSolicitante = nomeSolicitante;
		this.cpf = cpf;
		this.emailSolicitante = emailSolicitante;
		this.celular = celular;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEscritorio() {
		return escritorio;
	}

	public void setEscritorio(String escritorio) {
		this.escritorio = escritorio;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCodCnaePrimaria() {
		return codCnaePrimaria;
	}

	public void setCodCnaePrimaria(String codCnaePrimaria) {
		this.codCnaePrimaria = codCnaePrimaria;
	}

	public String getAtvdPrimaria() {
		return atvdPrimaria;
	}

	public void setAtvdPrimaria(String atvdPrimaria) {
		this.atvdPrimaria = atvdPrimaria;
	}

	public String getCodCnaeSecundaria() {
		return codCnaeSecundaria;
	}

	public void setCodCnaeSecundaria(String codCnaeSecundaria) {
		this.codCnaeSecundaria = codCnaeSecundaria;
	}

	public String getAtvdSecundaria() {
		return atvdSecundaria;
	}

	public void setAtvdSecundaria(String atvdSecundaria) {
		this.atvdSecundaria = atvdSecundaria;
	}

	public String getRegimeTributario() {
		return regimeTributario;
	}

	public void setRegimeTributario(String regimeTributario) {
		this.regimeTributario = regimeTributario;
	}

	public NaturezaJuridica getNaturezaJuridica() {
		return naturezaJuridica;
	}

	public void setNaturezaJuridica(NaturezaJuridica naturezaJuridica) {
		this.naturezaJuridica = naturezaJuridica;
	}

	public String getNomeSolicitante() {
		return nomeSolicitante;
	}

	public void setNomeSolicitante(String nomeSolicitante) {
		this.nomeSolicitante = nomeSolicitante;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmailSolicitante() {
		return emailSolicitante;
	}

	public void setEmailSolicitante(String emailSolicitante) {
		this.emailSolicitante = emailSolicitante;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public Empresa transformToEmpresa() {
		Empresa empresa = new Empresa();
		empresa.setId(this.id);
		empresa.setCnpj(this.cnpj);
		empresa.setRazaoSocial(this.razaoSocial);
		empresa.setNomeFantasia(this.nomeFantasia);
		empresa.setInscricaoEstadual(this.inscricaoEstadual);
		empresa.setCep(this.cep);
		empresa.setLogradouro(this.logradouro);
		empresa.setBairro(this.bairro);
		empresa.setNumero(this.numero);
		empresa.setComplemento(this.complemento);
		empresa.setContato(this.contato);
		empresa.setEmail(this.email);
		empresa.setAtvdPrimaria(this.atvdPrimaria);
		empresa.setCodCnaePrimaria(this.codCnaePrimaria);
		empresa.setAtvdSecundaria(this.atvdSecundaria);
		empresa.setCodCnaeSecundaria(this.codCnaeSecundaria);
		empresa.setNaturezaJuridica(this.naturezaJuridica);
		empresa.setUsuarios(new HashSet<Usuario>());
		return empresa;
	}

	public UsuarioEmpresa transformToUsuarioAdm() {

		UsuarioEmpresa usuario = new UsuarioEmpresa();
		usuario.setCpf(cpf);
		String senhacriptografada = new BCryptPasswordEncoder().encode(usuario.getCpf());
		usuario.setNome(nomeSolicitante);
		usuario.setLogin(cpf);
		usuario.setSenha(senhacriptografada);
		usuario.setStatus(true); // logica para verificar o status
		usuario.setEmail(emailSolicitante);
		usuario.setContato(celular);
		// usuario.getPerfis().add(new Perfil(NivelDeAcessoDoSistema.ROLE_ADMIN_EMPRESA));
		// usuario.getPerfis().add(new Perfil(NivelDeAcessoDoSistema.ROLE_COLABORADOR_EMPRESA));

		return usuario;
	}

}
