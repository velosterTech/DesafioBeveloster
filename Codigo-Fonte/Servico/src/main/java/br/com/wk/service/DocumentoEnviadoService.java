package br.com.wk.service;

import java.util.List;

import br.com.wk.model.DocumentoEnviado;

public interface DocumentoEnviadoService {
    DocumentoEnviado add(DocumentoEnviado documentoEnviado);
    List<DocumentoEnviado> findAllByEmpresa(Long id);
}
