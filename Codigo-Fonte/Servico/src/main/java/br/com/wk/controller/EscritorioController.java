package br.com.wk.controller;

import br.com.wk.dto.EscritorioDTO;
import br.com.wk.exception.MailSenderException;
import br.com.wk.model.Departamento;
import br.com.wk.model.Escritorio;
import br.com.wk.service.EscritorioService;
import br.com.wk.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/escritorio")
public class EscritorioController {

	@Autowired
	private EscritorioService escritorioService;

	@Autowired
	private MailService mailService;

	@PostMapping(value = "/registrar", produces = "application/json")
	public ResponseEntity<Escritorio> create(@RequestBody EscritorioDTO escritorioDTO) {
		Escritorio escritorioSalvo = escritorioService.save(escritorioDTO);
		try {
			mailService.sendMailWPassword(new String[]{escritorioDTO.getEmail(), escritorioDTO.getEmailResponsavel()}, "Acesso ao sistema SAFFO", escritorioDTO.getCpfSolicitante(), escritorioDTO.getCpfSolicitante());
		} catch (MailSenderException e) {
			e.printStackTrace();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(escritorioSalvo);
	}

	@GetMapping(value = "/todos")
	public ResponseEntity<List<Escritorio>> findAll(@RequestParam String busca, @RequestParam Boolean status, @RequestParam Long id) {
		return ResponseEntity.status(HttpStatus.OK).body(escritorioService.findAll(busca, status, id));
	}
	
	@GetMapping(value= "/cnpj/{cnpj}")
	public ResponseEntity<Escritorio> findByCnpj(@PathVariable String cnpj){
		return ResponseEntity.status(HttpStatus.OK).body(escritorioService.findByCnpj(cnpj));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Escritorio> findById(@PathVariable Long id) {
		return ResponseEntity.status(HttpStatus.OK).body(escritorioService.findById(id));
	}

	@PutMapping(value = "/editar/{id}")
	public ResponseEntity<Escritorio> update(@PathVariable long id, @RequestBody EscritorioDTO escritorioDTO) {
		return ResponseEntity.status(HttpStatus.OK).body(escritorioService.update(escritorioDTO, id));
	}

	@PutMapping("/status")
	public ResponseEntity<?> ativarDesativarStatus(@RequestBody Long id) {
		escritorioService.ativardesativar(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@GetMapping("/departamento")
	public ResponseEntity<List<Departamento>> findDepartamentos(@RequestParam String cnpj){
		return new ResponseEntity<List<Departamento>>(escritorioService.findDepartamentos(cnpj), HttpStatus.OK);
	}

}
