//package br.com.wk.service;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import br.com.wk.model.Categoria;
//import br.com.wk.repository.CategoriaRepository;
//
//@Service
//public class CategoriaServiceImpl implements CategoriaService{
//
//	@Autowired
//	CategoriaRepository categoriaRepository;
//	
//	@Override
//	public List<Categoria> findAll() {
//		// TODO Auto-generated method stub
//		return categoriaRepository.findAll();
//	}
//
//	@Override
//	public List<Categoria> findByName(String name) {
//		// TODO Auto-generated method stub
//		return categoriaRepository.findByName(name);
//	}
//
//	@Override
//	public Categoria save(Categoria categoria) {
//		// TODO Auto-generated method stub
//		return categoriaRepository.save(categoria);
//	}
//
//	@Override
//	public void delete(Long id) {
//		// TODO Auto-generated method stub
//		categoriaRepository.deleteById(id);
//	}
//
//	@Override
//	public Optional<Categoria> findById(Long id) {
//		// TODO Auto-generated method stub
//		return categoriaRepository.findById(id);
//	}
//	
//}
