package br.com.wk.service;

import java.util.List;
import java.util.Set;

import br.com.wk.dto.UsuarioDTO;
import br.com.wk.model.Usuario;

public interface UsuarioService {
	
	public List<Usuario> findAllFilter(boolean status);
	public Set<Usuario> findAll(Long id, Boolean status, String search);
	public void ativardesativar(Long id);
	public List<Usuario> findByCPF(String cpf);
	public Usuario findByCRC(String crc);
	public Usuario findById(Long id);
	public Usuario save(String cnpj,  UsuarioDTO usuarioDTO);
	public Usuario uptade(Long id, UsuarioDTO usuarioDTO, String cnpj);
	
}
