//package br.com.wk.controller;
//
//import java.net.URI;
//import java.net.URISyntaxException;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.wk.model.Certificado;
//import br.com.wk.service.CertificadoService;
//
//@RestController
//@RequestMapping(value = "/certificado")
//public class CertificadoController {
//	
//	@Autowired
//	protected CertificadoService certificadoService;
//	
//	@PostMapping("/save")
//	public ResponseEntity<Certificado> saveCertificado(@RequestBody Certificado certificado) throws URISyntaxException{
//		if(certificado.getId() != null) {
//			return ResponseEntity
//		        .badRequest()
//		        .body(null);
//		}
//		Certificado result = certificadoService.save(certificado);
//		return ResponseEntity
//	        .created(new URI("/empresacliente"))
//	        .body(result);
//	}
//	
//	@PostMapping("/save/{id}")
//	public ResponseEntity<Certificado> updateCertificado(@PathVariable("id") Long id, 
//			@RequestBody Certificado certificado) throws URISyntaxException{
//		if(certificado.getId() == null) {
//			return ResponseEntity
//		        .badRequest()
//		        .body(null);
//		}
//		Certificado result = certificadoService.save(certificado);
//		return ResponseEntity
//		        .created(new URI("/empresacliente"))
//		        .body(result);
//	}
//	
//	@DeleteMapping("/delete/{id}")
//	public ResponseEntity<Void> deleteCertificado(@PathVariable("id") Long id) throws URISyntaxException{
//		certificadoService.delete(id);
//		return ResponseEntity
//		        .ok()
//		        .body(null);
//	}
//}
