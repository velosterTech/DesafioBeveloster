package br.com.wk.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.wk.model.Regra;

@Repository
public interface RegraRepository extends JpaRepository<Regra, Long> {
    @Transactional
	@Modifying
    @Query(nativeQuery = true, value = "delete from saffo_regra where saffo_configuracao_id =?1")
    void deleteRegra(Long idConfiguracao);

}
