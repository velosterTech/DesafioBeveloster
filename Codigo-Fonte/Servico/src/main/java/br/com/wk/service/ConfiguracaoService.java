package br.com.wk.service;

import java.util.List;

import br.com.wk.dto.ConfiguracaoDTO;
import br.com.wk.model.Configuracao;
import br.com.wk.model.Escritorio;

public interface ConfiguracaoService {
    List<Configuracao> findAll(Long id, String busca);
    Configuracao findById(Long id);
    List<String> verificaRegras(ConfiguracaoDTO configuracaoDTO);
    Configuracao add(ConfiguracaoDTO configuracaoDTO);
    List<Configuracao> findAllByEscritorio(Escritorio escritorio);
    void ativarDesativaStatus(Long id);
    List<String> update(ConfiguracaoDTO configuracaoDTO);

}
