package br.com.wk.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import br.com.wk.dto.DocumentoDTO;
import br.com.wk.dto.DocumentoWebDTO;
import br.com.wk.model.Documento;

public interface DocumentoService {

	//	public Documento update(Long id, Documento documento);
//	public ResponseEntity<Void> addDocumento(Long id, MultipartFile[] files);
//	public void deleteDocumento(Documento documento);
//	public List<Documento> findAll(Long id);
	public Documento add(Documento documento);
	public Documento findById(long id);
	public Documento salvar(DocumentoDTO documentoDTO);
	public List<Documento> findAll(String cnpj);
	public List<DocumentoWebDTO> ocr(String cnpj, MultipartFile[] files);
	public void enviarDocumento(DocumentoWebDTO documentoDTO);
}