package br.com.wk.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@DiscriminatorValue(value = "USUARIO_ESCRITORIO")
public class UsuarioEscritorio extends Usuario{

	private static final long serialVersionUID = 1L;

	@Column(name = "saffo_usuario_cpf")
    private String cpf;

    @Column(name = "saffo_usuario_rg")
    private String rg;

    @Column(name = "saffo_usuario_crc")
    private String crc;
    
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "saffo_departamento_id")
    private Departamento departamento; 
    
    @Column(name = "saffo_usuario_contato")
    private String contato;
    
    @Column(name = "saffo_usuario_email")
    private String email;
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCrc() {
        return crc;
    }

    public void setCrc(String crc) {
        this.crc = crc;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;

    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }



}

