package br.com.wk.service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.wk.dto.ConfiguracaoDTO;
import br.com.wk.model.Configuracao;
import br.com.wk.model.Documento;
import br.com.wk.model.Escritorio;
import br.com.wk.model.Regra;
import br.com.wk.model.Usuario;
import br.com.wk.repository.ConfiguracaoRepository;
import br.com.wk.repository.DocumentoRepository;
import br.com.wk.repository.RegraRepository;
import br.com.wk.utils.Upload;

@Service
public class ConfiguracaoServiceImpl implements ConfiguracaoService{
	
    @Autowired
    private ConfiguracaoRepository configuracaoRepository;
    @Autowired 
    private DocumentoService documentoService;
    @Autowired
    private RegraRepository regraRepository;
    @Autowired
    private UsuarioService usuarioService;

    @Override
    public List<Configuracao> findAll(Long id, String busca) {
        Usuario usuario = usuarioService.findById(id);
		List<Configuracao> configuracaoResult = new ArrayList<>();
		if (!usuario.getEscritorios().isEmpty()) {
			usuario.getEscritorios().forEach(e -> configuracaoResult.addAll(configuracaoRepository.findAllByEscritorio(e , busca)));
		} else {
			configuracaoResult.addAll(configuracaoRepository.findAllLike(busca));
        }
        return configuracaoResult;
    }

    @Override
    public Configuracao findById(Long id) {
        Optional<Configuracao> configuracao = configuracaoRepository.findById(id);
		if (configuracao.isPresent()) {
			return configuracao.get();
		} 
        return null;
    }

    @Override
    public List<String> verificaRegras(ConfiguracaoDTO configuracaoDTO) {
        File file = Upload.base64ToPdf(configuracaoDTO.getFile(), "./test.pdf");
        String conteudo = Upload.ocr(file);
        List<String> regrasNaoEncontradas = new ArrayList<>();
        for(String s: configuracaoDTO.getRegras()){
            if(!conteudo.contains(s)){
                regrasNaoEncontradas.add(s);
            }
        }
        if(regrasNaoEncontradas.size() == 0){
           this.add(configuracaoDTO);
        } 
        file.delete();
        return regrasNaoEncontradas;
        

    }

    @Override
    public Configuracao add(ConfiguracaoDTO configuracaoDTO) {
        Configuracao configuracao = configuracaoDTO.transformaParaObjeto();
        Documento documento = documentoService.salvar(configuracaoDTO.transformToDocumentoDTO());
        configuracao.setDocumento(documento);
        configuracaoRepository.save(configuracao);
        for(String s: configuracaoDTO.getRegras()){
            Regra regra = regraRepository.save(new Regra(s, configuracao));
            configuracao.getRegras().add(regra);
        }
        return configuracaoRepository.save(configuracao);
    }

    @Override
    public List<Configuracao> findAllByEscritorio(Escritorio escritorio) {
        return configuracaoRepository.findAllTrueByEscritorio(escritorio, "");
    }

    @Override
    public void ativarDesativaStatus(Long id) {
        Configuracao configuracao = configuracaoRepository.findById(id).get();
        configuracao.setStatus(!configuracao.getStatus());
        configuracaoRepository.save(configuracao);
    }

    @Override
    public List<String> update(ConfiguracaoDTO configuracaoDTO) {
        File file = Upload.base64ToPdf(configuracaoDTO.getFile(), "./test.pdf");
        String conteudo = Upload.ocr(file);
        List<String> regrasNaoEncontradas = new ArrayList<>();
        for(String s: configuracaoDTO.getRegras()){
            if(!conteudo.contains(s)){
                regrasNaoEncontradas.add(s);
            }
        }
        if(regrasNaoEncontradas.size() == 0){
           this.atualizaConfiguracao(configuracaoDTO);
        } 
        file.delete();
        return regrasNaoEncontradas;
    }

    private Configuracao atualizaConfiguracao(ConfiguracaoDTO configuracaoDTO){
        Documento documento = documentoService.findById(configuracaoDTO.getDocumentoId());
        documento.setNome(configuracaoDTO.getDocumento());
        documentoService.add(documento);
        Configuracao configuracao = this.findById(configuracaoDTO.getId());
        configuracao.setConcluir(configuracao.getConcluir());
        configuracao.setData(configuracao.getData());
        configuracao.setDocumento(documento);
        if(configuracao.getRegras().size() > 0){
            regraRepository.deleteRegra(configuracaoDTO.getId());
        }
        for(String s: configuracaoDTO.getRegras()){
            Regra regra = regraRepository.save(new Regra(s, configuracao));
            configuracao.getRegras().add(regra);
        }
        return configuracaoRepository.save(configuracao);
    }
    
}
