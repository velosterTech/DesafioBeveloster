package br.com.wk.dto;

import java.io.File;

import javax.mail.Multipart;

import org.springframework.web.multipart.MultipartFile;

import br.com.wk.enums.Concluir;
import br.com.wk.model.Configuracao;

public class ConfiguracaoDTO {
    private Long id;
    private Long documentoId;
    private String[] regras;
    private String data;
    private Concluir concluir;
    private String file;
    private String documento;
    private String cnpjEscritorio;
    
    public String[] getRegras() {
        return regras;
    }
    public void setRegras(String[] regras) {
        this.regras = regras;
    }
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
    public Concluir getConcluir() {
        return concluir;
    }
    public void setConcluir(Concluir concluir) {
        this.concluir = concluir;
    }

    public Configuracao transformaParaObjeto(){
        Configuracao configuracao = new Configuracao();
        configuracao.setConcluir(concluir);
        configuracao.setData(data);
        configuracao.setStatus(true);

        return configuracao;
    }
    public String getFile() {
        return file;
    }
    public void setFile(String file) {
        this.file = file;
    }
    public String getDocumento() {
        return documento;
    }
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    public String getCnpjEscritorio() {
        return cnpjEscritorio;
    }
    public void setCnpjEscritorio(String cnpjEscritorio) {
        this.cnpjEscritorio = cnpjEscritorio;
    }
  
    public DocumentoDTO transformToDocumentoDTO(){
        DocumentoDTO documentoDTO = new DocumentoDTO();
        documentoDTO.setCnpjEscritorio(cnpjEscritorio);
        documentoDTO.setNome(documento);
        return documentoDTO;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getDocumentoId() {
        return documentoId;
    }
    public void setDocumentoId(Long documentoId) {
        this.documentoId = documentoId;
    }
   

}
