//package br.com.wk.service;
//
//import java.util.List;
//
//import br.com.wk.dto.EscritorioContabilDTO;
//import br.com.wk.model.EscritorioContabil;
//
//public interface EscritorioContabilService {
//	List<Long> findAllIdByUsuarioId(Long userId);
//	EscritorioContabil save(EscritorioContabilDTO escritorioContabil);
//	EscritorioContabil update(Long id, EscritorioContabilDTO escritorioContabil);
//	void delete(Long id);
//}
