package br.com.wk.dto;
import java.io.Serializable;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.wk.enums.NivelDeAcessoDoSistema;
import br.com.wk.model.Escritorio;
import br.com.wk.model.Perfil;
import br.com.wk.model.UsuarioEscritorio;


public class EscritorioDTO implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cnpj;
    private String razaoSocial;
    private String nomeFantasia;
    private String nomeSolicitante;
    private String cpfSolicitante;
    private String endereco;
    private String crc;
    private String email;
    private String telefoneFixo;
    private String telefoneCelular;
    private String emailResponsavel;
    private Boolean status;
	

	public Escritorio transformaParaObjeto(){
        Escritorio escritorio = new Escritorio();
        escritorio.setStatus(this.status);
        escritorio.setCnpj(this.cnpj);
        escritorio.setRazaoSocial(this.razaoSocial);
        escritorio.setEmail(this.email);
        escritorio.setNomeFantasia(this.nomeFantasia);
        escritorio.setEndereco(this.endereco);
        escritorio.setContato(this.telefoneFixo);
	    return escritorio;
	}




    public Boolean getStatus() {
        return status;
    }




    public void setStatus(Boolean status) {
        this.status = status;
    }




    public String getRazaoSocial() {
        return razaoSocial;
    }


    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }


    public String getNomeFantasia() {
        return nomeFantasia;
    }


    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }


    public String getNomeSolicitante() {
        return nomeSolicitante;
    }


    public void setNomeSolicitante(String nomeSolicitante) {
        this.nomeSolicitante = nomeSolicitante;
    }




    public String getEndereco() {
        return endereco;
    }


    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }


    public String getCrc() {
        return crc;
    }


    public void setCrc(String crc) {
        this.crc = crc;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }



    public String getEmailResponsavel() {
        return emailResponsavel;
    }


    public void setEmailResponsavel(String emailResponsavel) {
        this.emailResponsavel = emailResponsavel;
    }




    public String getCnpj() {
        return cnpj;
    }




    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }




    public String getTelefoneFixo() {
        return telefoneFixo;
    }




    public void setTelefoneFixo(String telefoneFixo) {
        this.telefoneFixo = telefoneFixo;
    }




    public String getTelefoneCelular() {
        return telefoneCelular;
    }




    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public String getCpfSolicitante() {
        return cpfSolicitante;
    }

    public void setCpfSolicitante(String cpfSolicitante) {
        this.cpfSolicitante = cpfSolicitante;
    }
    
    public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	public UsuarioEscritorio transformaParaObjetoUser(){
        		UsuarioEscritorio responsavel = new UsuarioEscritorio();
                responsavel.setStatus(true);
                responsavel.setLogin(this.cpfSolicitante);
                responsavel.setNome(this.nomeSolicitante);
                responsavel.setCpf(this.cpfSolicitante);
                responsavel.setEmail(this.emailResponsavel);
                responsavel.setCrc(this.crc);
                responsavel.setContato(this.telefoneCelular);
                String senhacriptografada = new BCryptPasswordEncoder().encode(responsavel.getCpf());
                responsavel.setSenha(senhacriptografada);
                // responsavel.getPerfis().add(new Perfil(NivelDeAcessoDoSistema.ROLE_ADMIN_ESCRITORIO));
                // responsavel.getPerfis().add(new Perfil(NivelDeAcessoDoSistema.ROLE_COLABORADOR_ESCRITORIO));
        		return responsavel;
        	}




	
	
	

}