
//package br.com.wk.controller;
//
//import java.util.List;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.wk.dto.EscritorioContabilDTO;
//import br.com.wk.exception.MailSenderException;
//import br.com.wk.model.EscritorioContabil;
//import br.com.wk.repository.EscritorioContabilRepository;
//import br.com.wk.service.EscritorioContabilService;
//import br.com.wk.service.MailService;
//
//@RestController
//@RequestMapping(value = "/escritorio-contabil", produces = "application/json")
//public class EscritorioContabilController {
//	
//	@Autowired
//	private EscritorioContabilRepository escritorioContabilRepository;
//	@Autowired 
//	private EscritorioContabilService escritorioContabilService;
//	@Autowired
//	private MailService mailService;
//	
//	@PostMapping(value = "/registrar")
//	public ResponseEntity<EscritorioContabil> addEscritorio(@Valid @RequestBody EscritorioContabilDTO escritorioContabil) throws MailSenderException{
//		EscritorioContabil escritorioSalvo = escritorioContabilService.save(escritorioContabil);		
//		
//		mailService.sendMailWPassword(escritorioContabil.getEmail(), "Acesso ao sistema SAFFO", Long.toString(escritorioContabil.getCpfSolicitante()), Long.toString(escritorioContabil.getCpfSolicitante()));
//		
//		return new ResponseEntity<EscritorioContabil>(escritorioSalvo, HttpStatus.OK);
//	}
//
//	@GetMapping(value = "/todos")
//	public ResponseEntity<List<EscritorioContabil>> getAll(@RequestParam String busca){
//		return new ResponseEntity<List<EscritorioContabil>>(escritorioContabilRepository.findAllLike(busca), HttpStatus.OK);
//	}
//
//	@GetMapping(value = "/{id}")
//	public ResponseEntity<EscritorioContabil> getById(@PathVariable long id){
//		return new ResponseEntity<EscritorioContabil>(escritorioContabilRepository.findById(id).get(), HttpStatus.OK);
//	}
//
//	@PutMapping(value = "/editar/{id}")
//	public ResponseEntity<EscritorioContabil> editar(@PathVariable long id, @RequestBody EscritorioContabilDTO escritorioContabil){
//		return new ResponseEntity<EscritorioContabil>(escritorioContabilService.update(id, escritorioContabil), HttpStatus.OK);
//	}
//	@DeleteMapping(value = "apagar/{id}")
//	public ResponseEntity<Void> apagar(@PathVariable long id){
//		escritorioContabilService.delete(id);
//		return new ResponseEntity<Void>(HttpStatus.OK);
//	}
//}