package br.com.wk.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dados {

	public static String identificaCNPJ(String text) {
		Pattern pattern = Pattern.compile("\\d{2}[.]\\d{3}[.]\\d{3}[/]\\d{4}[-]\\d{2}");
		String cnpj = "";
		Matcher matcher = pattern.matcher(text);

		while (matcher.find()) {
			cnpj = matcher.group();
		}
		cnpj = cnpj.replaceAll("[^\\d]+", "");
		return cnpj;
	}

	public static String identificaTitulo(String text) {
		String txt = "";
		int linhas = 1;
		for (int i = 0; i < text.length(); i++) {
			if (linhas >= 3) {
				break;
			}

			txt = txt + text.charAt(i);

			if (text.charAt(i) == '\n') {
				linhas += 1;
			}
		}
		return txt;
	}

	public static String identificaRazao(String text) {
		Pattern pattern = Pattern.compile("\\d{2}[.]\\d{3}[.]\\d{3}[/]\\d{4}[-]\\d{2}");
		Matcher matcher = pattern.matcher(text);
		String txt = "";
		Boolean matches = false;
		int i = 0;
		while (matcher.find()) {
			i = matcher.end();
			matches = true;
			break;
		}
		if (matches) {
			while (true) {
				if (text.charAt(i) == '\n') {
					break;
				}
				txt = txt + text.charAt(i);
				i++;
			}
			txt = txt.replaceAll("[^A-Z\\s]+", "");
			return txt.trim().replaceAll("\\s+", " ");
		}
		return null;
	}

	public static String identificaValor(String text) {
        Pattern pattern = Pattern.compile("[,]\\d{2}");
        Matcher matcher = pattern.matcher(text);
        Boolean matches = false;
        int i = 0;
        int j = 0;
        int fim = 0;
        while (matcher.find()) {
            i = matcher.end();
            fim = matcher.end();
            matches = true;
        }
        if (matches) {
            StringBuilder valor = new StringBuilder("");
            while (true) {
                if (text.charAt(i-1) != ',' && !Character.isDigit(text.charAt(i-1)) && text.charAt(i-1) != '.') {
                    j = i-1;
                    break;
                }
                i--;
            }
            for(int in = j; in < fim; in ++){
                valor.append(text.charAt(in));
            }
            String valorEncontrado = valor.toString().replaceAll("\\.", "");
            return valorEncontrado.replaceAll("\\,", "\\.");
        }
        return null;
    }

	public static String identificaCodigo(String text) {
		Pattern pattern = Pattern.compile("Código Denominação Principal Multa Juros Total\n");
		Matcher matcher = pattern.matcher(text);
		Boolean matches = false;
		int i = 0;
		while (matcher.find()) {
			i = matcher.end();
			matches = true;
			break;
		}
		if (matches) {
			String codigo = "";
			while (true) {
				if (text.charAt(i) == ' ') {
					break;
				}
				codigo = codigo + text.charAt(i);
				i++;

			}
			return codigo;
		}
		return null;
	}

	public static String identificaCompetencia(String text) {
		Pattern pattern = Pattern.compile("\\d{2}[/]\\d{2}[/]\\d{4}");
		Matcher matcher = pattern.matcher(text);
		String competencia = "";
		while (matcher.find()) {
			competencia = matcher.group();
			break;
		}
		return competencia;
	}

	public static String identificaInscricao(String text) {
		Pattern pattern = Pattern.compile("\\d{2}[.]\\d{3}[.]\\d{3}[-]\\d");
		Matcher matcher = pattern.matcher(text);
		String inscricao = "";
		while (matcher.find()) {
			inscricao = matcher.group();
			break;
		}
		return inscricao.replaceAll("[^\\d]+", "");
	}

	public static Double identificaValorIcms(String text) {
		Pattern pattern = Pattern.compile("\\d{2}[.]\\d{3}[.]\\d{3}[/]\\d{4}[-]\\d{2}");
		Matcher matcher = pattern.matcher(text);
		Double valor = 0.0;
		while (matcher.find()) {

			int i = matcher.end();
			while (!Character.isDigit(text.charAt(i))) {
				i++;
			}

			String num = "";
			while (text.charAt(i) != ' ') {
				num = num + text.charAt(i);
				i++;
			}
			valor += Double.parseDouble(num.replaceAll("\\,", "\\."));
			;
		}
		if (valor != 0.0) {
			return (Math.round(valor * 100.0) / 100.0);
		}
		return null;
	}

}
