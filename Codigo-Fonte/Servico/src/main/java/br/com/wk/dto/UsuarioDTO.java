package br.com.wk.dto;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.wk.model.Departamento;
import br.com.wk.model.UsuarioEmpresa;
import br.com.wk.model.UsuarioEscritorio;

public class UsuarioDTO {
    private String nome;

    private String cpf;

    private String rg;

    private String crc;
    
   private long departamento; 
    
    private String contato;
    
    private String email;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCrc() {
        return crc;
    }

    public void setCrc(String crc) {
        this.crc = crc;
    }

  

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UsuarioEmpresa transformaParaUsuarioEmpresa(){
        UsuarioEmpresa usuario = new UsuarioEmpresa();
        usuario.setNome(this.nome);
        usuario.setCpf(this.cpf);
        usuario.setEmail(this.email);
        usuario.setLogin(this.cpf);
        usuario.setRg(this.rg);
        usuario.setContato(this.contato);
        String senhacriptografada = new BCryptPasswordEncoder().encode(this.cpf);
		usuario.setSenha(senhacriptografada);
        usuario.setStatus(true);
        
        return usuario;
    }
    public UsuarioEscritorio transformaParaUsuarioEscritorio(){
        UsuarioEscritorio usuario = new UsuarioEscritorio();
        usuario.setNome(this.nome);
        usuario.setCpf(this.cpf);
        usuario.setEmail(this.email);
        usuario.setLogin(this.cpf);
        usuario.setRg(this.rg);
        usuario.setContato(this.contato);
        String senhacriptografada = new BCryptPasswordEncoder().encode(this.cpf);
		usuario.setSenha(senhacriptografada);
        usuario.setStatus(true);
        return usuario;
    }

    public long getDepartamento() {
        return departamento;
    }

    public void setDepartamento(long departamento) {
        this.departamento = departamento;
    }
}
