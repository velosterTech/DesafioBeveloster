//package br.com.wk.repository;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.stereotype.Repository;
//import br.com.wk.model.PessoaJuridica;
//
//@Repository
//public interface PessoaJuridicaRepository  extends JpaRepository <PessoaJuridica, Long>{
//	@Query("select u from PessoaJuridica u where u.cnpj =?1")
//	PessoaJuridica findPJByCNPJ (String cnpj);
//
//	@Query("select u from PessoaJuridica u where u.razao_social =?1")
//	PessoaJuridica findByRazao (String razao_social);
//	
//	@Query("select u from PessoaJuridica u where u.inscricao_estadual =?1")
//	PessoaJuridica findPJByInscricaoEstadual (Long inscricaoEstadual);
//
//}
