//package br.com.wk.model;
//
//import java.io.Serializable;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToOne;
//
//@Entity
//public class EscritorioContabil implements Serializable {
//	
//	
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE)
//	Long id;
//	
//	@Column(name = "cnpj", nullable = false, unique = true)
//	private Long cnpj;
//	private String razaoSocial;
//	private String nomeFantasia;
//	private String crc;
//	private String email;
//	private Long telefoneFixo;
//	private Long telefoneCelular;
//	private String codigoPrimariaCnae;
//	private String atividadePrimariaCnae;
//	private String codigoSecundariaCnae;
//	private String atividadeSecundariaCnae;
//	
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name= "id_endereco")
//	private Endereco endereco;
//
//	@ManyToOne
//	@JoinColumn(name="id_responsavel")
//	private Usuario responsavel;
//
//	private Boolean status;
//
//	
//	public EscritorioContabil() {
//		
//	}
//
//	public Boolean getStatus() {
//		return status;
//	}
//
//	public void setStatus(Boolean status) {
//		this.status = status;
//	}
//
//	public Usuario getResponsavel(){
//		return this.responsavel;
//	}
//
//	public void setResponsavel(Usuario responsavel){
//		this.responsavel = responsavel;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public Long getCnpj() {
//		return cnpj;
//	}
//
//	public void setCnpj(Long cnpj) {
//		this.cnpj = cnpj;
//	}
//
//	public String getRazaoSocial() {
//		return razaoSocial;
//	}
//
//	public void setRazaoSocial(String razaoSocial) {
//		this.razaoSocial = razaoSocial;
//	}
//
//	public String getNomeFantasia() {
//		return nomeFantasia;
//	}
//
//	public void setNomeFantasia(String nomeFantasia) {
//		this.nomeFantasia = nomeFantasia;
//	}
//
//	
//	public Endereco getEndereco() {
//		return endereco;
//	}
//
//	public void setEndereco(Endereco endereco) {
//		this.endereco = endereco;
//	}
//
//	public String getCrc() {
//		return crc;
//	}
//
//	public void setCrc(String crc) {
//		this.crc = crc;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public Long getTelefoneFixo() {
//		return telefoneFixo;
//	}
//
//	public void setTelefoneFixo(Long telefoneFixo) {
//		this.telefoneFixo = telefoneFixo;
//	}
//
//	public Long getTelefoneCelular() {
//		return telefoneCelular;
//	}
//
//	public void setTelefoneCelular(Long telefoneCelular) {
//		this.telefoneCelular = telefoneCelular;
//	}
//
//	public String getCodigoPrimariaCnae() {
//		return codigoPrimariaCnae;
//	}
//
//	public void setCodigoPrimariaCnae(String codigoPrimariaCnae) {
//		this.codigoPrimariaCnae = codigoPrimariaCnae;
//	}
//
//	public String getAtividadePrimariaCnae() {
//		return atividadePrimariaCnae;
//	}
//
//	public void setAtividadePrimariaCnae(String atividadePrimariaCnae) {
//		this.atividadePrimariaCnae = atividadePrimariaCnae;
//	}
//
//	public String getCodigoSecundariaCnae() {
//		return codigoSecundariaCnae;
//	}
//
//	public void setCodigoSecundariaCnae(String codigoSecundariaCnae) {
//		this.codigoSecundariaCnae = codigoSecundariaCnae;
//	}
//
//	public String getAtividadeSecundariaCnae() {
//		return atividadeSecundariaCnae;
//	}
//
//	public void setAtividadeSecundariaCnae(String atividadeSecundariaCnae) {
//		this.atividadeSecundariaCnae = atividadeSecundariaCnae;
//	}
//
//
//	
//}
//
