package br.com.wk.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;

public class MensagemWhatsapp {
	
	private String conteudo;
	private Set<String> contatos = new HashSet<>();

	public Set<String> getContatos() {
		return contatos;
	}

	public void setContatos(Set<String> contatos) {
		this.contatos = contatos;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

}
