//package br.com.wk.service;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import br.com.wk.dto.EscritorioContabilDTO;
//import br.com.wk.model.EscritorioContabil;
//import br.com.wk.model.Usuario;
//import br.com.wk.repository.EscritorioContabilRepository;
//import br.com.wk.repository.UsuarioRepository;
//
//@Service
//public class EscritorioContabilServiceImpl implements EscritorioContabilService{
//
//	@Autowired
//	EscritorioContabilRepository escritorioContabilRepository;
//	@Autowired
//	UsuarioRepository usuarioRepository;
//	
//	@Override
//	public List<Long> findAllIdByUsuarioId(Long userId) {
//		// TODO Auto-generated method stub
//		return escritorioContabilRepository.findAllIdByUsuarioId(userId);
//	}
//
//	@Override
//	public EscritorioContabil save(EscritorioContabilDTO escritorioContabil) {
//		Usuario responsavel = escritorioContabil.transformaParaObjetoUser();
//		usuarioRepository.save(responsavel);
//		usuarioRepository.insereAcessoRoleAdminEscritorio(responsavel.getId());
//		EscritorioContabil escritorio = escritorioContabil.transformaParaObjeto();
//		escritorio.setResponsavel(responsavel);
//		EscritorioContabil escritorioSalvo = escritorioContabilRepository.save(escritorio);
//		usuarioRepository.insereEscritorio(responsavel.getId(), escritorioSalvo.getId());
//		return escritorioSalvo;
//	}
//
//	@Override
//	public EscritorioContabil update(Long id, EscritorioContabilDTO escritorioContabil) {
//		EscritorioContabil escritorio = escritorioContabil.transformaParaObjeto();
//		escritorio.setId(id);
//		return escritorioContabilRepository.save(escritorio);
//	}
//
//	@Override
//	public void delete(Long id) {
//		EscritorioContabil escritorioContabil = escritorioContabilRepository.findById(id).get();
//		escritorioContabil.setStatus(false);
//		escritorioContabilRepository.save(escritorioContabil);		
//	}
//
//}
