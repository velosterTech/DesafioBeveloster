package br.com.wk.service;

import br.com.wk.dto.DocumentoDTO;
import br.com.wk.dto.DocumentoWebDTO;
import br.com.wk.exception.MailSenderException;
import br.com.wk.model.*;
import br.com.wk.repository.DocumentoRepository;
import br.com.wk.repository.UsuarioRepository;
import br.com.wk.utils.Dados;
import br.com.wk.utils.EncryptPDF;
import br.com.wk.utils.OcrCallable;
import br.com.wk.utils.Upload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import br.com.wk.model.Configuracao;
import br.com.wk.model.Documento;
import br.com.wk.model.Empresa;
import br.com.wk.model.Escritorio;
import br.com.wk.model.MensagemWhatsapp;

@Service
public class DocumentoServiceImpl implements DocumentoService {

	@Autowired
	EscritorioService escritorioService;

	@Autowired
	DocumentoRepository documentoRepo;

	@Autowired
	ConfiguracaoService configuracaoService;

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private MailService mailService;

	@Autowired
	private DocumentoEnviadoService documentoEnviadoService;

//	@Autowired
//	private WhatsappBotService whatsappService;

	@Override
	public Documento salvar(DocumentoDTO documentoDTO) {
		Documento documento = documentoDTO.transformaParaObjeto();
		documento.setEscritorio(escritorioService.findByCnpj(documentoDTO.getCnpjEscritorio()));
		return documentoRepo.save(documento);
	}

	@Override
	public List<Documento> findAll(String cnpj) {
		Escritorio escritorio = escritorioService.findByCnpj(cnpj);
		return documentoRepo.findAllByEscritorio(escritorio);
	}

	@Override
	public Documento findById(long id) {
		return documentoRepo.findById(id).get();
	}

	private void deletarArquivos(List<File> files) {
		for (File file : files) {
			file.delete();
		}
	}

	@Override
	public List<DocumentoWebDTO> ocr(String cnpj, MultipartFile[] files) {
		List<Configuracao> configuracoes = new ArrayList<>();
		Escritorio escritorio = escritorioService.findByCnpj(cnpj);
		configuracoes.addAll(configuracaoService.findAllByEscritorio(escritorio));
		if (configuracoes.size() == 0) {
			return null;
		}

		ExecutorService executor = Executors.newFixedThreadPool(10);
		List<Future<DocumentoWebDTO>> list = new ArrayList<Future<DocumentoWebDTO>>();
		List<File> filesUsed = new ArrayList<>();
		List<DocumentoWebDTO> documentos = new ArrayList<DocumentoWebDTO>();
		for (MultipartFile file : files) {
			File convFile = null;
			try {
				convFile = Upload.converterMultipartFileParaFile(file);
				filesUsed.add(convFile);
				OcrCallable callable = new OcrCallable();
				callable.setFile(convFile);
				callable.setConfiguracoes(configuracoes);
				Future<DocumentoWebDTO> future = executor.submit(callable);
				list.add(future);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		for (Future<DocumentoWebDTO> future : list) {
			try {
				DocumentoWebDTO documento = future.get();
				if (documento.getDocumento() != null) {
					documento = this.salvarDocumento(documento, escritorio);
				}
				documentos.add(documento);
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}

		executor.shutdown();
		this.deletarArquivos(filesUsed);

		return documentos;
	}

	private DocumentoWebDTO salvarDocumento(DocumentoWebDTO documento, Escritorio escritorio) {
		documento.setCheck(false);
		documento.setEnviado(false);
		documento.setEscritorio(escritorio);
		String valor = Dados.identificaValor(documento.getFileOcr());
		String vencimento = Dados.identificaCompetencia(documento.getFileOcr());
		if(!valor.equals(null) && !valor.equals("")){
			documento.setValor(Double.parseDouble(valor));
		}
		documento.setVencimento(vencimento);
		String cnpj = Dados.identificaCNPJ(documento.getFileOcr());
		Empresa empresa = empresaService.findByCnpjEscritorio(cnpj, escritorio);
		if (empresa == null) {
			String inscricao = Dados.identificaInscricao(documento.getFileOcr());
			if (!inscricao.equals(null) && !inscricao.equals("")) {
				Empresa empresa2 = empresaService.findByInscricaoEstadual(inscricao, escritorio);
				if (empresa2 == null) {
					documento.setEmpresa(null);
				} else {
					documento.setEmpresa(empresa2);
				}
			} else {
				documento.setEmpresa(null);
			}

		} else {
			documento.setEmpresa(empresa);
		}
		return documento;
	}

	@Override
	public void enviarDocumento(DocumentoWebDTO documentoDTO) {

		File file = Upload.base64ToPdf(documentoDTO.getFileBase64(), documentoDTO.getFileName());

		String mensagem = "Prezado cliente " +  documentoDTO.getEmpresa().getRazaoSocial() + ", \n" +
				"Você está recebendo o documento " + documentoDTO.getDocumento().getNome() + " com vencimento " + documentoDTO.getVencimento() + ".";

		File fileEncrypted = EncryptPDF.encriptarPdf(file, documentoDTO.getEmpresa().getCnpj());
		documentoEnviadoService.add(documentoDTO.transformaParaObjeto());
		try {
			mailService.sendMailWithAttachment(documentoDTO.getEmpresa().getEmail(), "Recebimento de Documento - SAFFO", fileEncrypted, mensagem);
			
		} catch (MailSenderException e) {

			e.printStackTrace();
		}

		MensagemWhatsapp mensagemWhatsapp = new MensagemWhatsapp();

		mensagemWhatsapp.setConteudo(mensagem);
		mensagemWhatsapp.getContatos().add(documentoDTO.getEmpresa().getContato());

//		whatsappService.receberMensagem(mensagemWhatsapp);

		file.delete();


	}

	@Override
	public Documento add(Documento documento) {
		return documentoRepo.save(documento);
	}
}