//package br.com.wk.model;
//
//import java.io.Serializable;
//import java.time.LocalDateTime;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "certificado")
//public class Certificado implements Serializable{
//	
//	private static final long serialVersionUID = 1L;
//	
//	@Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
//    @SequenceGenerator(name = "sequenceGenerator")
//    @Column(name = "id")
//	private Long id;
//	
//	@Column(name = "path")
//	private String path;
//	
//	@Column(name = "expiration")
//	private LocalDateTime expiration;
//	
//	@Column(name = "issued_to")
//	private String issuedTo;
//	
//	@Column(name = "issued_by")
//	private String issuedBy;
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getPath() {
//		return path;
//	}
//
//	public void setPath(String path) {
//		this.path = path;
//	}
//
//	public LocalDateTime getExpiration() {
//		return expiration;
//	}
//
//	public void setExpiration(LocalDateTime expiration) {
//		this.expiration = expiration;
//	}
//
//	public String getIssuedTo() {
//		return issuedTo;
//	}
//
//	public void setIssuedTo(String issuedTo) {
//		this.issuedTo = issuedTo;
//	}
//
//	public String getIssuedBy() {
//		return issuedBy;
//	}
//
//	public void setIssuedBy(String issuedBy) {
//		this.issuedBy = issuedBy;
//	}
//	
//	@Override
//    public boolean equals(Object o) {
//        if (this == o) {
//            return true;
//        }
//        if (!(o instanceof Certificado)) {
//            return false;
//        }
//        return id != null && id.equals(((Certificado) o).id);
//    }
//
//    @Override
//    public int hashCode() {
//        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
//        return getClass().hashCode();
//    }
//}
