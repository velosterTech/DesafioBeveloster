package br.com.wk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.wk.model.DocumentoEnviado;
import br.com.wk.model.Escritorio;

@Repository
public interface DocumentoEnviadoRepository extends JpaRepository<DocumentoEnviado, Long>{
    @Query(value = "Select de from DocumentoEnviado de where de.empresa.id =?1")
    List<DocumentoEnviado> findAllEmpresa(Long empresaId);

    @Query(value = "Select de from DocumentoEnviado de where de.escritorio =?1")
    List<DocumentoEnviado> findAllEscritorio(Escritorio escritorio);
}
