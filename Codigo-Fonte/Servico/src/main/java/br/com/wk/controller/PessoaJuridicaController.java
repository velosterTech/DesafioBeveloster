//package br.com.wk.controller;
//
//import java.util.List;
//import java.util.Optional;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import br.com.wk.model.PessoaJuridica;
//
//import br.com.wk.service.PessoaJuridicaService;
//
//@RestController
//@RequestMapping(value = "/pessoajuridica")
//public class PessoaJuridicaController {
//	@Autowired
//	private PessoaJuridicaService pessoajuridicaService;
//
//	@PostMapping(value = "/salva")
//	public ResponseEntity<PessoaJuridica> create(@RequestBody PessoaJuridica pessoajuridica) {
//		return ResponseEntity.status(HttpStatus.CREATED).body(pessoajuridicaService.save(pessoajuridica));
//	}
//
//	@GetMapping(value = "encontra")
//	public ResponseEntity<List<PessoaJuridica>> findAll() {
//		return ResponseEntity.status(HttpStatus.OK).body(pessoajuridicaService.findAll());
//	}
//
//	@GetMapping("/{id}")
//	public ResponseEntity<Optional<PessoaJuridica>> findById(@PathVariable Long id) {
//		return ResponseEntity.status(HttpStatus.OK).body(pessoajuridicaService.findById(id));
//	}
//
//	@PutMapping
//	public ResponseEntity<PessoaJuridica> update(@RequestBody PessoaJuridica pessoajuridica) {
//		return ResponseEntity.status(HttpStatus.OK).body(pessoajuridicaService.update(pessoajuridica));
//	}
//
//	@DeleteMapping("/{id}")
//	public ResponseEntity<?> delete(@PathVariable Long id) {
//		pessoajuridicaService.deleteById(id);
//		return ResponseEntity.status(HttpStatus.OK).build();
//	}
//
//	@GetMapping("/cnpj/{cnpj}")
//	public ResponseEntity<PessoaJuridica> findByCNPJ(@PathVariable String cnpj) {
//		return ResponseEntity.status(HttpStatus.OK).body(pessoajuridicaService.findByCNPJ(cnpj));
//	}
//	
//	@GetMapping("/inscricao-estadual/{inscricao}")
//	public ResponseEntity<PessoaJuridica> findbyIncricaoEstadual(@PathVariable Long inscricao) {
//		return ResponseEntity.status(HttpStatus.OK).body(pessoajuridicaService.findByInscricaoEstadual(inscricao));
//	}
//
//}
