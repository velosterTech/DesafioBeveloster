package br.com.wk.utils;

import com.itextpdf.kernel.pdf.*;

import java.io.File;

public class EncryptPDF {

	private static final String OWNER_PASSWORD = "*Veloster2022.";
	private static String USER_PASSWORD;

	public static File encriptarPdf(File file, String cnpj) {
		try {
			USER_PASSWORD = cnpj.substring(0,4);
			return EncryptPdf().manipulatePdf(file);

		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return null;
	}

	private static EncryptPDF EncryptPdf() {

		return new EncryptPDF();
	}

	protected File manipulatePdf(File file) throws Exception {
		File fileEncrypt = new File("encrypted-" + file.getName());
		PdfDocument pdfDoc = new PdfDocument(new PdfReader(file.getAbsolutePath()),
				new PdfWriter(fileEncrypt.getAbsolutePath(),
						new WriterProperties().setStandardEncryption(
								USER_PASSWORD.getBytes(),
								OWNER_PASSWORD.getBytes(), 
								EncryptionConstants.ALLOW_PRINTING,
								EncryptionConstants.ENCRYPTION_AES_128 | EncryptionConstants.DO_NOT_ENCRYPT_METADATA)));
		pdfDoc.close();
		return fileEncrypt;
	}
}
