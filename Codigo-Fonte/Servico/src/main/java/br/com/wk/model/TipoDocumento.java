//package br.com.wk.model;
//
//import java.io.Serializable;
//
//import java.util.Objects;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.ForeignKey;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToOne;
//import javax.persistence.SequenceGenerator;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//import br.com.wk.dto.TipoDocumentoDTO;
//
//@JsonIgnoreProperties(value = {"escritorio"})
//@Entity
//public class TipoDocumento implements Serializable{
//
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tipo_documento_sequence")
//	@SequenceGenerator(name="tipo_documento_sequence", sequenceName="tipo_documento_id_seq", allocationSize = 1)
//	private Long id;
//	
//	@Column(name = "nome_documento")
//	private String nomeDocumento;
//	
//	@ManyToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "id_escritorio")
//	private EscritorioContabil escritorioContabil;
//	
//	@OneToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "categoria_id", referencedColumnName = "id", foreignKey = @ForeignKey(foreignKeyDefinition = "tipo_documento__categoria__fk"))
//	private Categoria categoria;
//
//	public TipoDocumento() {
//		
//	}
//	
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public String getNomeDocumento() {
//		return nomeDocumento;
//	}
//
//	public void setNomeDocumento(String nomeDocumento) {
//		this.nomeDocumento = nomeDocumento;
//	}
//	
//	public EscritorioContabil getEscritorioContabil() {
//		return escritorioContabil;
//	}
//
//	public void setEscritorioContabil(EscritorioContabil escritorioContabil) {
//		this.escritorioContabil = escritorioContabil;
//	}
//	
//	public Categoria getCategoria() {
//		return categoria;
//	}
//
//	public void setCategoria(Categoria categoria) {
//		this.categoria = categoria;
//	}
//
//	public TipoDocumentoDTO transformToDTO() {
//		TipoDocumentoDTO dto = new TipoDocumentoDTO();
//		dto.setNomeDocumento(this.getNomeDocumento());
//		dto.setEscritorioContabilId(this.getEscritorioContabil().getId());
//		return dto;
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(escritorioContabil, id, nomeDocumento);
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		TipoDocumento other = (TipoDocumento) obj;
//		return Objects.equals(escritorioContabil, other.escritorioContabil) && Objects.equals(id, other.id)
//				&& Objects.equals(nomeDocumento, other.nomeDocumento);
//	}
//
//	
//	
//	
//	
//	
//}
