 package br.com.wk.repository;

 import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.wk.model.Documento;
import br.com.wk.model.Escritorio;

 @Repository
 public interface DocumentoRepository extends JpaRepository<Documento, Long>{
	@Query(value = "SELECT d FROM Documento d where d.escritorio=?1")
    List<Documento> findAllByEscritorio(Escritorio escritorio);
//    @Query(value = "SELECT u FROM Documento u")
//    List<Documento> findAllOrganizado(Sort sort);

 }
