//package br.com.wk.controller;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import br.com.wk.dto.TipoDocumentoDTO;
//import br.com.wk.model.TipoDocumento;
//import br.com.wk.model.Usuario;
//import br.com.wk.repository.UsuarioRepository;
//import br.com.wk.service.TipoDocumentoService;
//
//@RestController
//@RequestMapping(value = "/tipodocumentos")
//public class TipoDocumentoController {
//
//	@Autowired
//	TipoDocumentoService tipoDocService;
//
//	@Autowired
//	UsuarioRepository usuarioRepository;
//
//	@GetMapping("/find/{id}")
//	public ResponseEntity<TipoDocumento> findDocumentoById(@PathVariable Long id) {
//		
//		TipoDocumento documento = tipoDocService.findDocumentoById(id);
//		return new ResponseEntity<TipoDocumento>(documento, HttpStatus.OK);
//	}
//
//	@PostMapping("/novo")
//	public ResponseEntity<TipoDocumento> createDocumento(@RequestBody TipoDocumentoDTO tipoDocumento){
//		TipoDocumento documento = tipoDocService.createDocumento(tipoDocumento);		
//		return new ResponseEntity<TipoDocumento>(documento, HttpStatus.CREATED);
//	}
//	
//	@PutMapping("/editar/{id}")
//	public ResponseEntity<TipoDocumento> updateDocumento(@RequestBody TipoDocumentoDTO tipoDocumento,
//			@PathVariable("id") Long id){
//		TipoDocumento documento = tipoDocService.updateDocumento(tipoDocumento, id);		
//		return new ResponseEntity<TipoDocumento>(documento, HttpStatus.CREATED);
//	}
//	
//	@GetMapping("/find/escritorio/{id}")
//	public ResponseEntity<List<TipoDocumento>> findAllDocumentosByEscritorio(@PathVariable Long id) {
//		
//		List<TipoDocumento> documentos = tipoDocService.findAllTipoDocumentosByEscritorio(id);
//		return new ResponseEntity<List<TipoDocumento>>(documentos, HttpStatus.OK);
//	}
//	
//	@GetMapping("/find/all")
//	public ResponseEntity<List<TipoDocumento>> findAllTipoDocumentos(@RequestParam("id") Long id){
//		return new ResponseEntity<List<TipoDocumento>>(tipoDocService.findAllTipoDocumentos(id), HttpStatus.OK);
//	}
//
//	@GetMapping("/find/all/categoria")
//	public ResponseEntity<List<TipoDocumento>> findAllByCategoria(@RequestParam("id") Long id, @RequestParam("categoriaId") Long categoriaId){
//		return new ResponseEntity<List<TipoDocumento>>(tipoDocService.findAllByCategoria(id, categoriaId), HttpStatus.OK);
//	}
//	
//	@DeleteMapping("/delete/{id}")
//	public ResponseEntity<Void> deleteById(@PathVariable Long id){
//		tipoDocService.deleteById(id);
//		return new ResponseEntity<Void>(HttpStatus.OK);		
//	}
//
//}
