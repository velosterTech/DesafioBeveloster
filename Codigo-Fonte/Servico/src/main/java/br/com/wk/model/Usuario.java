package br.com.wk.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "saffo_usuario")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "saffo_usuario_tipo")
@DiscriminatorValue(value = "USUARIO_BEVELOSTER")
public class Usuario implements UserDetails{	

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "saffo_usuario_id")
    private Long id; 
	
    @Column(name = "saffo_usuario_nome")
    private String nome;

    @Column(name = "saffo_usuario_login")
    private String login;

    @Column(name = "saffo_usuario_senha")
    private String senha;

    @Column(name = "saffo_usuario_token")
    private String token;

    @Column(name = "saffo_usuario_status")
    private Boolean status;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
    		name = "saffo_usuario_perfil",
    		joinColumns = @JoinColumn(name= "saffo_usuario_id"),
    		inverseJoinColumns = @JoinColumn(name="saffo_perfil_id")
    )
    private Set<Perfil> perfis = new HashSet<>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
    		name = "saffo_usuario_escritorio",
    		joinColumns = @JoinColumn(name= "saffo_usuario_id"),
    		inverseJoinColumns = @JoinColumn(name="saffo_escritorio_id")
    )    
    private Set<Escritorio> escritorios = new HashSet<>();
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
    		name = "saffo_usuario_empresa",
    		joinColumns = @JoinColumn(name= "saffo_usuario_id"),
    		inverseJoinColumns = @JoinColumn(name="saffo_empresa_id")
    )    
    private Set<Empresa> empresas = new HashSet<>();
	      
    
    public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public Set<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(Set<Perfil> perfis) {
		this.perfis = perfis;
	}
	

	public Set<Escritorio> getEscritorios() {
		return escritorios;
	}

	public void setEscritorios(Set<Escritorio> escritorios) {
		this.escritorios = escritorios;
	}

	public Set<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(Set<Empresa> empresas) {
		this.empresas = empresas;
	}

	@Override
    public Set<Perfil> getAuthorities() {
        
        return this.perfis;
    }

    @Override
    public String getPassword() {
        
        return this.senha;
    }

    @Override
    public String getUsername() {
        return this.login;
    }

	@JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

	@JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        
        return true;
    }
 

    @Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Usuario))
			return false;
		Usuario other = (Usuario) obj;
		return Objects.equals(id, other.id);
	}

	public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nome=" + nome + "]";
	}

    
    
}

