package br.com.wk.service;

import java.util.List;
import java.util.Optional;

import br.com.wk.dto.EscritorioDTO;
import br.com.wk.model.Departamento;
import br.com.wk.model.Escritorio;



public interface EscritorioService {
		
	
	    List<Escritorio> findAll(String busca, Boolean status, Long id);

	    Escritorio findById(Long id);

	    Escritorio update (EscritorioDTO escritorioDTO, Long id);

	    void deleteById(Long id);

	    Escritorio save(EscritorioDTO escritorioDTO);
	    
	    public void ativardesativar(Long id);
	    
	    Escritorio findByCnpj(String cnpj);
	    
		List<Departamento> findDepartamentos(String cnpj);

}

