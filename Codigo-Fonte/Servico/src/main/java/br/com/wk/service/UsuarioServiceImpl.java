package br.com.wk.service;

import br.com.wk.dto.UsuarioDTO;
import br.com.wk.enums.NivelDeAcessoDoSistema;
import br.com.wk.model.*;
import br.com.wk.repository.DepartamentoRepository;
import br.com.wk.repository.EmpresaRepository;
import br.com.wk.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	EntityManager entityManager;

	@Autowired
	EmpresaService empresaService;

	@Autowired
	EscritorioService escritorioService;

	@Autowired
	EmpresaRepository empresaRepository;

	@Autowired
	DepartamentoRepository departamentoRepository;

	/**
	 * public List<Usuario> findAllFilter(boolean status) { Session session =
	 * entityManager.unwrap(Session.class); Filter filter =
	 * session.enableFilter("deletarUsuario"); filter.setParameter("status", false);
	 * List<Usuario> users = (List<Usuario>) usuarioRepository.findAll();
	 * session.disableFilter("deletarUsuario"); return users; }
	 **/
	@Override
	public List<Usuario> findAllFilter(boolean status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Usuario> findAll(Long id, Boolean status, String search) {

		Usuario usuario = usuarioRepository.findById(id).get();
		Set<Perfil> perfis = usuario.getPerfis();
		Set<Usuario> usuarioResult = new HashSet<>();
		
		if (status) {
			if(!usuario.getEscritorios().isEmpty()){
				perfis.stream().forEach(p -> usuario.getEscritorios().stream().forEach(e -> usuarioResult.addAll(usuarioRepository.findAllLikeEscRoleTrue(p.getNome(), e.getId(), search))));
			} else if (!usuario.getEmpresas().isEmpty()){
				perfis.stream().forEach(p -> usuario.getEmpresas().stream().forEach(e -> usuarioResult.addAll(usuarioRepository.findAllLikeEmpRoleTrue(p.getNome(), e.getId(), search))));
			} else {
				perfis.stream().forEach(p ->  usuarioResult.addAll(usuarioRepository.findAllLikeAdmRole(p.getNome(), search)));
			}

		} else {
			if(!usuario.getEscritorios().isEmpty()){
				perfis.stream().forEach(p -> usuario.getEscritorios().stream().forEach(e -> usuarioResult.addAll(usuarioRepository.findAllLikeEscRole(p.getNome(), e.getId(), search))));
			} else if (!usuario.getEmpresas().isEmpty()){
				perfis.stream().forEach(p -> usuario.getEmpresas().stream().forEach(e -> usuarioResult.addAll(usuarioRepository.findAllLikeEmpRole(p.getNome(), e.getId(), search))));
			} else {
				perfis.stream().forEach(p -> usuarioResult.addAll(usuarioRepository.findAllLikeAdmRole(p.getNome(), search)));
			}
		}
		
		//usuarioResult.stream().forEach(System.out::println);
		Boolean isAdmin = perfis.stream().anyMatch(e -> e.getNome() == NivelDeAcessoDoSistema.ROLE_ADMIN);
		
		if (!isAdmin) {
			
			usuarioResult.stream()
					.flatMap(u -> u.getPerfis().stream())
					.filter(p -> p.getNome().equals(NivelDeAcessoDoSistema.ROLE_ADMIN))
					.findAny()
					.ifPresent(perfilAdm -> usuarioResult
							.removeIf(u -> u.getPerfis().contains(perfilAdm)));
		}
		
		//usuarioResult.stream().forEach(System.out::println);
		
		return usuarioResult;
		
	}

	@Override
	public List<Usuario> findByCPF(String cpf) {
		return usuarioRepository.findByCpf(cpf);
	}

	@Override
	public void ativardesativar(Long id) {
		Usuario usuario = usuarioRepository.findById(id).get();
		usuario.setStatus(!usuario.getStatus());
		usuarioRepository.save(usuario);
	}

	@Override
	public Usuario save(String cnpj, UsuarioDTO usuarioDTO) {

		if (escritorioService.findByCnpj(cnpj) != null) {
			UsuarioEscritorio usuario = usuarioDTO.transformaParaUsuarioEscritorio();
			Escritorio escritorio = escritorioService.findByCnpj(cnpj);
			usuario.getEscritorios().add(escritorio);
			usuario.setDepartamento(departamentoRepository.findById(usuarioDTO.getDepartamento()).get());
			Usuario usuarioSalvo = usuarioRepository.save(usuario);
			usuarioRepository.insereAcessoRoleUserEscritorio(usuarioSalvo.getId());
			// usuarioRepository.insereAcessoRoleUser(usuarioSalvo.getId());
			return usuarioSalvo;

		} else {
			UsuarioEmpresa usuario = usuarioDTO.transformaParaUsuarioEmpresa();
			Empresa empresa = empresaService.findByCnpj(cnpj);
			usuario.getEmpresas().add(empresa);
			Usuario usuarioSalvo = usuarioRepository.save(usuario);
			usuarioRepository.insereAcessoRoleUserEmpresa(usuarioSalvo.getId());
			// usuarioRepository.insereAcessoRoleUser(usuarioSalvo.getId());
			return usuarioSalvo;
		}

	}

	public Usuario uptade(Long id, UsuarioDTO usuarioDTO, String cnpj) {
		Usuario usuarioAntigo = usuarioRepository.findById(id).get();
		if (usuarioAntigo.getEscritorios().size() > 0) {
			UsuarioEscritorio usuario = usuarioDTO.transformaParaUsuarioEscritorio();
			usuario.setId(id);
			usuario.setPerfis(usuarioAntigo.getPerfis());
			usuario.setEscritorios(usuarioAntigo.getEscritorios());
			usuario.setCrc(((UsuarioEscritorio) usuarioRepository.findById(id).get()).getCrc());
			if (usuarioDTO.getDepartamento() != 0) {
				usuario.setDepartamento(departamentoRepository.findById(usuarioDTO.getDepartamento()).get());
			}

			return usuarioRepository.save(usuario);
		} else {
			UsuarioEmpresa usuario = usuarioDTO.transformaParaUsuarioEmpresa();
			usuario.setId(id);
			usuario.setPerfis(usuarioAntigo.getPerfis());
			usuario.setEmpresas(usuarioAntigo.getEmpresas());
			return usuarioRepository.save(usuario);
		}

	}

	@Override
	public Usuario findById(Long id) {
		return usuarioRepository.findById(id).orElse(null);

	}
	
	@Override
	public Usuario findByCRC(String crc) {
		return usuarioRepository.findByCRC(crc).orElse(null);

	}

}
