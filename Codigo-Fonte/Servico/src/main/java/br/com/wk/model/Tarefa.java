package br.com.wk.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="saffo_tarefa")
public class Tarefa {
	
	@Column(name= "saffo_tarefa_id", nullable = false)
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGeneratorTarefa")
    @SequenceGenerator(name = "seqGeneratorTarefa", sequenceName="SAFFO_TAREFA_SEQ", allocationSize=1)
	private Long id;
	
	@Column(name="saffo_tarefa_nome")
	private String nome;
	
	@Column(name= "saffo_tarefa_data")
	private LocalDate date;
	
	@Column(name= "saffo_tarefa_prazo_final_legal")
	private String prazoFinalLegal;
	
	@Column(name= "saffo_tarefa_meta")
	private String meta;
	
	@Column(name= "saffo_tarefa_competencia")
	private String competencia;
	
	@Column(name= "saffo_tarefa_in_passivo_multa")
	private Character inPassivoMulta;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="saffo_frequencia_id", nullable = false)
	private Frequencia frequencia;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="saffo_regime_id", nullable = false)
	private Regime regime;
	
	@ManyToMany(mappedBy = "tarefas",fetch = FetchType.LAZY)
	private Set<Empresa> empresas = new HashSet<>();
	
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name= "saffo_departamento_id")
	private Departamento departamento;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getPrazoFinalLegal() {
		return prazoFinalLegal;
	}

	public void setPrazoFinalLegal(String prazoFinalLegal) {
		this.prazoFinalLegal = prazoFinalLegal;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public String getCompetencia() {
		return competencia;
	}

	public void setCompetencia(String competencia) {
		this.competencia = competencia;
	}

	public Character getInPassivoMulta() {
		return inPassivoMulta;
	}

	public void setInPassivoMulta(Character inPassivoMulta) {
		this.inPassivoMulta = inPassivoMulta;
	}

	public Frequencia getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(Frequencia frequencia) {
		this.frequencia = frequencia;
	}

	public Regime getRegime() {
		return regime;
	}

	public void setRegime(Regime regime) {
		this.regime = regime;
	}

	public Set<Empresa> getEmpresas() {
		return empresas;
	}

	public void setEmpresas(Set<Empresa> empresas) {
		this.empresas = empresas;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	@Override
	public int hashCode() {
		return Objects.hash(competencia, date, departamento, empresas, frequencia, id, inPassivoMulta, meta, nome,
				prazoFinalLegal, regime);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Tarefa))
			return false;
		Tarefa other = (Tarefa) obj;
		return Objects.equals(competencia, other.competencia) && Objects.equals(date, other.date)
				&& Objects.equals(departamento, other.departamento) && Objects.equals(empresas, other.empresas)
				&& Objects.equals(frequencia, other.frequencia) && Objects.equals(id, other.id)
				&& Objects.equals(inPassivoMulta, other.inPassivoMulta) && Objects.equals(meta, other.meta)
				&& Objects.equals(nome, other.nome) && Objects.equals(prazoFinalLegal, other.prazoFinalLegal)
				&& Objects.equals(regime, other.regime);
	}

	
}