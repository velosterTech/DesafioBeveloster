package br.com.wk;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@EnableAsync
@SpringBootApplication
@EnableTransactionManagement
@EnableWebMvc
@RestController
@EnableAutoConfiguration
@EnableCaching
public class WkApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(WkApplication.class, args);
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		
		registry.addMapping("/usuario/**")
		.allowedMethods("*")
		.allowedOrigins("*");
		
	}
	
//	 @Bean
//     public WebDriver webDriver() {
//	if(System.getProperty("os.name").toLowerCase().contains("windows")){
//	 	System.setProperty("webdriver.chrome.driver","C:\\Projetos\\Beveloster\\DesafioBeveloster\\Codigo-Fonte\\Servico\\ChromeDriver\\chromedriver.exe");
//	} else {
//		System.setProperty("webdriver.chrome.driver","/opt/saffo/Codigo-Fonte/Servico/ChromeDriver/chromedriver");
//	}
//         ChromeOptions options = new ChromeOptions();
//         options.setHeadless(false);
//         WebDriver webDriver = new ChromeDriver(options);
//         webDriver.get("https://web.whatsapp.com/");
//         return webDriver;
//     }
	
}

