package br.com.wk.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.wk.enums.NivelDeAcessoDoSistema;
import br.com.wk.model.Usuario;
import br.com.wk.model.UsuarioEmpresa;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long> {

	@Query("select u from Usuario u where u.login =?1 AND u.status = true")
	Usuario findUserByLogin (String login);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "update saffo_usuario set saffo_usuario_token = ?1 where saffo_usuario_login=?2")
	void atualizaTokenUser(String token, String login);
	
	@Query("select u from Usuario u where u.crc =?1")
	Optional<Usuario> findByCRC (String crc);

	// @Transactional
	// @Modifying
	// @Query(nativeQuery = true, value = "update saffo_usuario set saffo_usuario_status = false where saffo_escritorio_id=?1")
	// void desativaUsuariosEscritorio(Long escritorioId);

	// @Transactional
	// @Modifying
	// @Query(nativeQuery = true, value = "update saffo_usuario set saffo_usuario_status = false where saffo_empresa_id=?1")
	// void desativaUsuariosEmpresa(Long empresaId);



	// @Query(value = "SELECT constraint_name from information_schema.constraint_column_usage  where table_name = 'usuarios_role' and column_name = 'role_id' and constraint_name <> 'unique_role_user';", nativeQuery = true)
	// String consultaConstraintRole();

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "insert into saffo_usuario_perfil (saffo_usuario_id, saffo_perfil_id) values(?1, (select saffo_perfil_id from saffo_perfil where saffo_perfil_nome = 'ROLE_COLABORADOR_ESCRITORIO')); ") 
	void insereAcessoRoleUserEscritorio(Long idUser);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "insert into saffo_usuario_perfil (saffo_usuario_id, saffo_perfil_id) values(?1, (select saffo_perfil_id from saffo_perfil where saffo_perfil_nome = 'ROLE_COLABORADOR_EMPRESA')); ") 
	void insereAcessoRoleUserEmpresa(Long idUser);

	// // @Transactional
	// // @Modifying
	// // @Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_ADMIN')); ") 
	// // void insereAcessoRoleAdmin(Long idUser);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "insert into saffo_usuario_perfil (saffo_usuario_id, saffo_perfil_id) values(?1, (select saffo_perfil_id from saffo_perfil where saffo_perfil_nome = 'ROLE_ADMIN_EMPRESA'));")
	void insereAcessoRoleAdminEmpresa(Long idUser);

	@Transactional
	@Modifying
	@Query(nativeQuery = true, value = "insert into saffo_usuario_perfil (saffo_usuario_id, saffo_perfil_id) values(?1, (select saffo_perfil_id from saffo_perfil where saffo_perfil_nome = 'ROLE_ADMIN_ESCRITORIO'));")
	void insereAcessoRoleAdminEscritorio(Long idUser);

	@Query(nativeQuery = true, value = "SELECT count(saffo_usuario_cpf) from saffo_usuario where saffo_usuario_cpf like ?1%")
	Long numeroDeCpfs(String cpf);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_escritorio (usuario_id, empresa_id) values(?1, ?2);")
//	void insereEscritorio(Long idUser, Long escritorioId);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_empresa (usuario_id, empresa_id) values(?1, ?2);")
//	void insereEmpresa(Long idUser, Long empresaId);
//
	// @Query("SELECT u FROM Usuario u WHERE u.nome LIKE ?1%" )// AND u.status='true'
	// List<UsuarioEmpresa> findAllLike(String busca);

	// @Query("SELECT u FROM Usuario u WHERE u.nome LIKE ?1% AND u.status = 'true'")
	// List<UsuarioEmpresa> findAllLikeTrue(String busca);

	// @Query("SELECT distinct u FROM Usuario u join u.empresas e WHERE u.nome LIKE ?1% AND e.id =?2 AND u.status='true'")
	// List<UsuarioEmpresa> findAllLikeEmpresaTrue(String busca, Long empresaId);

	// @Query("SELECT distinct u FROM Usuario u join u.empresas e WHERE u.nome LIKE ?1% AND e.id =?2")
	// List<UsuarioEmpresa> findAllLikeEmpresa(String busca, Long empresaId);

	// @Query("SELECT distinct u FROM Usuario u join u.escritorios e WHERE u.nome LIKE ?1% AND e.id =?2")
	// List<UsuarioEmpresa> findAllLikeEscritorio(String busca, Long escritorioId);
	
	@Query("SELECT distinct u FROM Usuario u join u.perfis p join u.escritorios e WHERE p.nome =?1 AND e.id =?2 AND lower(u.nome) LIKE lower(concat(?3,'%'))")
	List<Usuario> findAllLikeEscRole(NivelDeAcessoDoSistema busca, Long escritorioId, String search);
	
	@Query("SELECT distinct u FROM Usuario u join u.perfis p join u.escritorios e WHERE p.nome =?1 AND e.id =?2 AND lower(u.nome) LIKE lower(concat(?3,'%')) AND u.status = true")
	List<Usuario> findAllLikeEscRoleTrue(NivelDeAcessoDoSistema busca, Long escritorioId, String search);

	@Query("SELECT distinct u FROM Usuario u join u.perfis p join u.empresas e WHERE p.nome =?1 AND e.id =?2 AND lower(u.nome) LIKE lower(concat(?3,'%'))")
	List<Usuario> findAllLikeEmpRole(NivelDeAcessoDoSistema busca, Long empresaId, String search);
	
	@Query("SELECT distinct u FROM Usuario u join u.perfis p join u.empresas e WHERE p.nome =?1 AND e.id =?2 AND lower(u.nome) LIKE lower(concat(?3,'%')) AND u.status = true")
	List<Usuario> findAllLikeEmpRoleTrue(NivelDeAcessoDoSistema busca, Long empresaId, String search);

	@Query("SELECT distinct u FROM Usuario u join u.perfis p WHERE p.nome =?1 AND lower(u.nome) LIKE lower(concat(?2,'%'))")
	List<Usuario> findAllLikeAdmRole(NivelDeAcessoDoSistema busca, String search);
	
	@Query("SELECT distinct u FROM Usuario u join u.perfis p WHERE p.nome =?1 AND lower(u.nome) LIKE lower(concat(?2,'%')) AND u.status = true")
	List<Usuario> findAllLikeAdmRoleTrue(NivelDeAcessoDoSistema busca, String search);
	
//
//	@Query("SELECT distinct u FROM Usuario u join u.empresas e WHERE e.id =?1")
//	List<EmpresaCliente> findUsuarioEmpresa(Long usuarioId);
//
//	@Query("SELECT distinct u FROM Usuario u join u.escritorios e WHERE e.id =?1")
//	List<EscritorioContabil> findUsuarioEscritorio(Long usuarioId);
//	
	@Query("select u from Usuario u where u.cpf =?1")
	List<Usuario> findByCpf(String cpf);
//	
//	@Transactional
//	@Modifying
// 	@Query(nativeQuery = true, value = "DELETE FROM usuarios_role WHERE usuario_id=?1")
//	void deleteRelationship(Long idUsuario);
//
//	/*
//	
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_SOCIETARIA_COLABORADOR_EMPRESA'));")
//	void insereAcessoRoleSocColEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_SOCIETARIA_COLABORADOR_ESCRITORIO'));")
//	void insereAcessoRoleSocColEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_SOCIETARIA_GESTOR_EMPRESA'));")
//	void insereAcessoRoleSocGesEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_SOCIETARIA_GESTOR_ESCRITORIO'));")
//	void insereAcessoRoleSocGesEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_CONTABIL_COLABORADOR_EMPRESA'));")
//	void insereAcessoRoleConColEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_CONTABIL_COLABORADOR_ESCRITORIO'));")
//	void insereAcessoRoleConColEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_CONTABIL_GESTOR_EMPRESA'));")
//	void insereAcessoRoleConGesEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_CONTABIL_GESTOR_ESCRITORIO'));")
//	void insereAcessoRoleConGesEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_FISCAL_COLABORADOR_EMPRESA'));")
//	void insereAcessoRoleFisColEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_FISCAL_COLABORADOR_ESCRITORIO'));")
//	void insereAcessoRoleFisColEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_FISCAL_GESTOR_EMPRESA'));")
//	void insereAcessoRoleFisGesEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_FISCAL_GESTOR_ESCRITORIO'));")
//	void insereAcessoRoleFisGesEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_DEPARTAMENTO_PESSOAL_COLABORADOR_EMPRESA'));")
//	void insereAcessoRoleDepColEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_DEPARTAMENTO_PESSOAL_COLABORADOR_ESCRITORIO'));")
//	void insereAcessoRoleDepColEscritorio(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_DEPARTAMENTO_PESSOAL_GESTOR_EMPRESA'));")
//	void insereAcessoRoleDepGesEmpresa(Long idUser);
//
//	@Transactional
//	@Modifying
//	@Query(nativeQuery = true, value = "insert into usuarios_role (usuario_id, role_id) values(?1, (select id from role where nome_role = 'ROLE_DEPARTAMENTO_PESSOAL_GESTOR_ESCRITORIO'));")
//	void insereAcessoRoleDepGesEscritorio(Long idUser);
//	*/
}
