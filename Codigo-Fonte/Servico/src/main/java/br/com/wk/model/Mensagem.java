//package br.com.wk.model;
//
//import java.io.Serializable;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//
//@Entity
//public class Mensagem implements Serializable{
//	private static final long serialVersionUID = 1L;
//	
//	@Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
//	private Long id;
//	
//	@Column (name="txt_mensagem")
//	private String txtMensagem;
//	
//	
//	@ManyToOne
//	@JoinColumn (name="usuario_remetente")
//	private Usuario usuarioRemetente;
//	
//	
//	@ManyToOne
//	@JoinColumn (name="usuario_destino")
//	private Usuario usuarioDestino;
//
//	private Boolean status;
//	
//	public Long getId() {
//		return id;
//	}
//
//
//	public Boolean getStatus() {
//		return status;
//	}
//
//
//	public void setStatus(Boolean status) {
//		this.status = status;
//	}
//
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//
//	public String getTxtMensagem() {
//		return txtMensagem;
//	}
//
//
//	public void setTxtMensagem(String txtMensagem) {
//		this.txtMensagem = txtMensagem;
//	}
//
//
//	public Usuario getUsuarioRemetente() {
//		return usuarioRemetente;
//	}
//
//
//	public void setUsuarioRemetente(Usuario usuarioRemetente) {
//		this.usuarioRemetente = usuarioRemetente;
//	}
//
//
//	public Usuario getUsuarioDestino() {
//		return this.usuarioDestino;
//	}
//
//
//	public void setUsuarioDestino(Usuario usuarioDestino) {
//		this.usuarioDestino = usuarioDestino;
//	}
//}
