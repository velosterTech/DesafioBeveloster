package br.com.wk.exception;

public class MailSenderException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String exception;
	
	public MailSenderException(String exception) {
		this.exception = exception;
	}
	
	@Override
	public String getMessage() {
		return "Ocorreu um erro ao enviar o email. Detalhes: " + exception;
	}

}
