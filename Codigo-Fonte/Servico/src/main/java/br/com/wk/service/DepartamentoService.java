package br.com.wk.service;

import br.com.wk.dto.DepartamentoDTO;
import br.com.wk.model.Departamento;

public interface DepartamentoService {
    public Departamento add(DepartamentoDTO departamentoDTO); 
}
