//package br.com.wk.model;
//
//import java.io.Serializable;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Entity;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.OneToOne;
//import javax.persistence.Table;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//@Entity
//@Table (name="empresa_cliente")
//public class EmpresaCliente implements Serializable{
//	
//	private static final long serialVersionUID = 1L;
//	
//	@GeneratedValue (strategy = GenerationType.AUTO)
//	@Id
//	private Long id;
//	
//	@ManyToOne (cascade = CascadeType.ALL)
//	@JoinColumn (name="escritorio_id")
//	private EscritorioContabil escritorioId;
//	
//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn (name="pj_id", nullable = false)
//	
//	private PessoaJuridica pjId;
//	
//	@ManyToOne
//	@JoinColumn(name="id_responsavel")
//	private Usuario responsavel;
//	
//	@OneToOne(fetch = FetchType.EAGER)
//	@JoinColumn(name = "certificado_id")
//	private Certificado certificado;
//	
//	private Boolean status;
//
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "endereco_id")
//	private Endereco endereco;
//	
//	public EmpresaCliente() {
//		super();
//	}
//	
//
//	public Boolean getStatus() {
//		return status;
//	}
//
//
//	public void setStatus(Boolean status) {
//		this.status = status;
//	}
//
//
//	public EmpresaCliente(Long id, EscritorioContabil escritorioId, PessoaJuridica pjId) {
//		super();
//		this.id = id;
//		this.escritorioId = escritorioId;
//		this.pjId = pjId;
//	}
//
//	public Usuario getResponsavel(){
//		return this.responsavel;
//	}
//	
//	public void setResponsavel(Usuario responsavel){
//		this.responsavel = responsavel;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public EscritorioContabil getEscritorioId() {
//		return escritorioId;
//	}
//
//	public void setEscritorioId(EscritorioContabil escritorioId) {
//		this.escritorioId = escritorioId;
//	}
//
//	public PessoaJuridica getPjId() {
//		return pjId;
//	}
//
//	public void setPjId(PessoaJuridica pjId) {
//		this.pjId = pjId;
//	}
//
//
//	public Certificado getCertificado() {
//		return certificado;
//	}
//
//
//	public void setCertificado(Certificado certificado) {
//		this.certificado = certificado;
//	}
//
//
//	public Endereco getEndereco() {
//		return endereco;
//	}
//
//
//	public void setEndereco(Endereco endereco) {
//		this.endereco = endereco;
//	}
//	
//	
//}
