package br.com.wk.controller;

import br.com.wk.dto.EmpresaDTO;
import br.com.wk.exception.MailSenderException;
import br.com.wk.model.Empresa;
import br.com.wk.repository.UsuarioRepository;
import br.com.wk.service.EmpresaService;
import br.com.wk.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/empresa")
public class EmpresaController {

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	UsuarioRepository usuarioRepository;

	@Autowired
	MailService mailService;

	@PostMapping(value = "/salva")
	public ResponseEntity<Empresa> create(@RequestBody EmpresaDTO empresa) throws MailSenderException {
		Empresa empresaSalva = empresaService.save(empresa);
		mailService.sendMailWPassword(
				new String[]{empresa.getEmail(), empresa.getEmailSolicitante()},
				"Acesso ao sistema SAFFO",
				empresa.getCpf(),
				empresa.getCpf()
		);
		return ResponseEntity.status(HttpStatus.CREATED).body(empresaSalva);
	}

	@GetMapping("/todos")
	public ResponseEntity<List<Empresa>> findAll(@RequestParam Long id, @RequestParam String busca, @RequestParam Boolean status) {

		return ResponseEntity.status(HttpStatus.OK).body(empresaService.findAllLike(busca, status, id));
	}

	@GetMapping("/{id}")
	public ResponseEntity<Empresa> findById(@PathVariable Long id) {
		
		return ResponseEntity.status(HttpStatus.OK).body(empresaService.findById(id));
	}
//
//	@GetMapping(value = "/admin/{id}")
//	public ResponseEntity<Empresa> findByAdmin(@PathVariable Long id) {
//		Usuario usuario = usuarioRepository.findById(id).get();
//		return ResponseEntity.status(HttpStatus.OK).body(usuario.getEmpresas().get(0));
//	}

	@PutMapping(value = "/editar")
	public ResponseEntity<Empresa> update(@RequestBody EmpresaDTO empresa) {
		
		return ResponseEntity.status(HttpStatus.OK).body(empresaService.update(empresa));
	}

//	@DeleteMapping(value = "/apagar/{id}")
//	public ResponseEntity<?> delete(@PathVariable Long id) {
//		empresaService.delete(id);
//		return ResponseEntity.status(HttpStatus.OK).build();
//	}
	
	@PutMapping("/status")
	public ResponseEntity<?> ativarDesativarStatus(@RequestBody Long id) throws MailSenderException {
		empresaService.ativarDesativar(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@GetMapping(value= "/cnpj/{cnpj}")
	public ResponseEntity<Empresa> findByCnpj(@PathVariable String cnpj){
		return ResponseEntity.status(HttpStatus.OK).body(empresaService.findByCnpj(cnpj));
	}

	@GetMapping(value= "/escritorio/{id}")
	public ResponseEntity<List<Empresa>> findAllByCnpj(@PathVariable Long id){
		return ResponseEntity.status(HttpStatus.OK).body(empresaService.findAllByEscritorioId(id));
	}
	
}
