//package br.com.wk.repository;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import br.com.wk.model.Categoria;
//
//@Repository
//public interface CategoriaRepository extends JpaRepository<Categoria, Long>{
//	
//	@Query("select c from Categoria c where c.nomeCategoria LIKE CONCAT('%', :nomeCategoria, '%')")
//	List<Categoria> findByName(@Param("nomeCategoria") String nomeCategoria);
//	
//}
