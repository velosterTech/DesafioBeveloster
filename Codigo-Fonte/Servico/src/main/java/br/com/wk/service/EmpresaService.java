package br.com.wk.service;

import java.util.List;

import br.com.wk.dto.EmpresaDTO;
import br.com.wk.model.Empresa;
import br.com.wk.model.Escritorio;

public interface EmpresaService {

//    List<Empresa> findAll(Long id, String busca);

	List<Empresa> findAllLike(String busca, Boolean status, Long id);

//    List<Empresa> findAllLikeEscritorio(String busca, EscritorioContabil escritorioId);

    Empresa findById(Long id);
    
//    void delete(Long id);

    Empresa save(EmpresaDTO empresaDTO);

    Empresa findByCnpj(String cnpj);

    Empresa findByCnpjEscritorio(String cnpj, Escritorio escritorio);

    List<Empresa> findAllByEscritorioId(Long id);

    Empresa update(EmpresaDTO empresa);
    
    public void ativarDesativar(Long id);

    Empresa findByInscricaoEstadual(String inscricao, Escritorio escritorio);

//    Empresa findByUsuario(Usuario usuario);

    
}
