package br.com.wk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.security.core.GrantedAuthority;

import br.com.wk.enums.NivelDeAcessoDoSistema;

@Entity
@Table(name = "saffo_perfil")
public class Perfil implements GrantedAuthority{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "saffo_perfil_id")
    private Long id;

    @Column(name="saffo_perfil_nome")
	@Enumerated(EnumType.STRING)
	private NivelDeAcessoDoSistema nome;
    
	public Perfil() {
	}	

	public Perfil(NivelDeAcessoDoSistema nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NivelDeAcessoDoSistema getNome() {
		return this.nome;
	}

	public void setNome(NivelDeAcessoDoSistema nome) {
		this.nome = nome;
	}

	@Override
	public String getAuthority() {
		
		return this.nome.toString();
	}

	@Override
	public String toString() {
		return "Perfil [id=" + id + ", nome=" + nome + "]";
	}
	
	
}
