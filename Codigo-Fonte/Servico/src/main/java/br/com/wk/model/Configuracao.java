package br.com.wk.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.wk.enums.Concluir;

@Entity
@Table(name = "saffo_configuracao")
public class Configuracao {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGeneratorConfiguracao")
    @SequenceGenerator(name = "seqGeneratorConfiguracao", sequenceName="SAFFO_CONFIGURACAO_SEQ", allocationSize=1)
    @Column(name= "saffo_configuracao_id")
    private Long id;

    @Column(name="saffo_configuracao_data")
    private String data;

    @Column(name="saffo_configuracao_concluir")
	@Enumerated(EnumType.STRING)
    private Concluir concluir;
    
    @OneToMany(mappedBy = "configuracao")
    private Set<Regra> regras = new HashSet<>();

    @OneToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "saffo_documento_id")
    private Documento documento;

    @Column(name = "saffo_configuracao_status")
    private Boolean status;
    
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Concluir getConcluir() {
        return concluir;
    }

    public void setConcluir(Concluir concluir) {
        this.concluir = concluir;
    }

    public Set<Regra> getRegras() {
        return regras;
    }

    public void setRegras(Set<Regra> regras) {
        this.regras = regras;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Configuracao other = (Configuracao) obj;
        if (concluir != other.concluir)
            return false;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (regras == null) {
            if (other.regras != null)
                return false;
        } else if (!regras.equals(other.regras))
            return false;
        return true;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

}
