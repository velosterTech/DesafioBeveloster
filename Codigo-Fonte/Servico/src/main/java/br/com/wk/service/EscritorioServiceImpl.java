
package br.com.wk.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.wk.dto.EscritorioDTO;
import br.com.wk.exception.EntidadeNaoEncontradaException;
import br.com.wk.model.Departamento;
import br.com.wk.model.Empresa;
import br.com.wk.model.Escritorio;
import br.com.wk.model.Usuario;
import br.com.wk.model.UsuarioEscritorio;
import br.com.wk.repository.EmpresaRepository;
import br.com.wk.repository.EscritorioRepository;
import br.com.wk.repository.UsuarioRepository;

@Service

public class EscritorioServiceImpl implements EscritorioService {

	@Autowired
	private EscritorioRepository escritorioRepository;
	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired
	private EmpresaRepository empresaRepository;

	@Override
	public Escritorio save(EscritorioDTO escritorioDTO) {
		Escritorio escritorio = escritorioDTO.transformaParaObjeto();
		Escritorio escritorioSalvo = escritorioRepository.save(escritorio);
		UsuarioEscritorio responsavel = escritorioDTO.transformaParaObjetoUser();
		responsavel.getEscritorios().add(escritorioSalvo);
		Long nCpf = usuarioRepository.numeroDeCpfs(responsavel.getCpf());
		if(nCpf >= 1){

			responsavel.setLogin(responsavel.getLogin() + (nCpf + 1));
		}
		Usuario usuarioSalvo = usuarioRepository.save(responsavel);
		usuarioRepository.insereAcessoRoleAdminEscritorio(usuarioSalvo.getId());
		usuarioRepository.insereAcessoRoleUserEscritorio(usuarioSalvo.getId());
		return escritorioSalvo;
	}

	@Override
	public List<Escritorio> findAll(String busca, Boolean status, Long id) {
		Usuario usuario = usuarioRepository.findById(id).get();
		List<Escritorio> escritorioResult = new ArrayList<>();

		if (status) {
			if (!usuario.getEscritorios().isEmpty()) {
				escritorioResult.addAll(escritorioRepository.findAllLikeUsuariosTrue(id, busca));
			} else {
				escritorioResult.addAll(escritorioRepository.findAllLikeTrue(busca));
			}

		} else {
			if (!usuario.getEscritorios().isEmpty()) {
				escritorioResult.addAll(escritorioRepository.findAllLikeUsuarios(id, busca));
			} else {
				escritorioResult.addAll(escritorioRepository.findAllLike(busca));
			}
		}
		return escritorioResult;
	}

	@Override
	public Escritorio findById(Long id) {
		Optional<Escritorio> escritorio = escritorioRepository.findById(id);
		if (escritorio.isPresent()) {
			return escritorio.get();
		} else {
			throw new EntidadeNaoEncontradaException("Escritório com id [ " + id + " ] não encontrado");
		}

	}

	@Override
	public Escritorio update(EscritorioDTO escritorioDTO, Long id) {
		Escritorio escritorio = escritorioDTO.transformaParaObjeto();
		escritorio.setId(id);
		return escritorioRepository.save(escritorio);
	}

	@Override
	public void deleteById(Long id) {
		escritorioRepository.deleteById(id);
	}

	@Override
	public void ativardesativar(Long id) {
		Escritorio escritorio = escritorioRepository.findById(id).get();
		escritorio.setStatus(!escritorio.getStatus());
		if (!escritorio.getStatus()) {
			for (Usuario usuarioEsc : escritorio.getUsuarios()) {
				if (!(usuarioEsc.getEscritorios().size() > 1)) {
					usuarioEsc.setStatus(false);
					usuarioRepository.save(usuarioEsc);
				}
			}
			for (Empresa empresa : empresaRepository.findByEscritorio(escritorio)) {
				empresa.setStatus(escritorio.getStatus());
				for (Usuario usuarioEmp : empresa.getUsuarios()) {
					if (!(usuarioEmp.getEmpresas().size() > 1)) {
						usuarioEmp.setStatus(false);
						usuarioRepository.save(usuarioEmp);
					}
				}
			}
		}
		escritorioRepository.save(escritorio);
	}

	@Override
	public Escritorio findByCnpj(String cnpj) {
		return escritorioRepository.findByCNPJ(cnpj);
	}

	@Override
	public List<Departamento> findDepartamentos(String cnpj) {
		Escritorio escritorio = escritorioRepository.findByCNPJ(cnpj);
		return escritorioRepository.findDepartamentos(escritorio.getId());
	}
}