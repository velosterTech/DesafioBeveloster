package br.com.wk.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.stereotype.Component;

import br.com.wk.dto.DocumentoWebDTO;
import br.com.wk.model.Configuracao;
import br.com.wk.model.Regra;

@Component
public class OcrCallable implements Callable<DocumentoWebDTO> {

    private File file;
    private List<Configuracao> configuracoes = new ArrayList<>();

    public void setFile(File file) {
        this.file = file;
    }

    public void setConfiguracoes(List<Configuracao> configuracoes) {
        this.configuracoes = configuracoes;
    }

    @Override
    public DocumentoWebDTO call() throws Exception {
        
    	DocumentoWebDTO documento = new DocumentoWebDTO();
        documento.setFileName(file.getName());
        String text = Upload.ocr(this.file);

        for(Configuracao configuracao: this.configuracoes){
            int contador = 0;
            for(Regra regra: configuracao.getRegras()){
                if(text.contains(regra.getNome())){
                    contador++;
                }
            }
            if(contador == configuracao.getRegras().size()){
                documento.setDocumento(configuracao.getDocumento());
                documento.setFileOcr(text);
                documento.setFileBase64(Upload.encodeFileToBase64(file));
                break;
            }
        }
        return documento;
    }

}

