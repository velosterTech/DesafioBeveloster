
package br.com.wk.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.wk.model.Departamento;
import br.com.wk.model.Escritorio;

@Repository
public interface EscritorioRepository  extends JpaRepository <Escritorio, Long>{
	
    @Query("SELECT u FROM Escritorio u WHERE lower(u.razaoSocial) LIKE lower(concat(?1,'%'))")// AND u.status='true'
	List<Escritorio> findAllLike(String busca);

    @Query("SELECT u FROM Escritorio u WHERE lower(u.razaoSocial) LIKE  lower(concat(?1,'%')) AND u.status = 'true'")
	List<Escritorio> findAllLikeTrue(String busca);
	
    @Query("select e from Escritorio e where e.cnpj =?1")
	Escritorio findByCNPJ(String cnpj);
	
    @Query(value = "select d from Departamento d where d.escritorio.id =?1")
    List<Departamento> findDepartamentos(Long id);

    @Query(value = "select e from Escritorio e join e.usuarios u WHERE u.id =?1 AND lower(e.razaoSocial) LIKE lower(concat(?2,'%'))")
    List<Escritorio> findAllLikeUsuarios(Long id, String busca);

    @Query(value = "select e from Escritorio e join e.usuarios u WHERE u.id =?1 AND lower(e.razaoSocial) LIKE lower(concat(?2,'%')) AND e.status = 'true'")
    List<Escritorio> findAllLikeUsuariosTrue(Long id, String busca);


}
