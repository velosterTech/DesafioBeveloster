//package br.com.wk.dto;
//
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
//import br.com.wk.model.Endereco;
//import br.com.wk.model.EscritorioContabil;
//import br.com.wk.model.Usuario;
//
//public class EscritorioContabilDTO {
//    
//  private Long id;
//  private Long cnpj;
//	private String razaoSocial;
//	private String nomeFantasia;
//	private String nomeSolicitante;
//	private Long cpfSolicitante;
//	private Endereco endereco;
//	private String crc;
//	private String email;
//	private Long telefoneFixo;
//	private Long telefoneCelular;
//	private String codigoPrimariaCnae;
//	private String atividadePrimariaCnae;
//	private String codigoSecundariaCnae;
//	private String atividadeSecundariaCnae;
//  private String login;
//	private String senha;  
//	private String senhaConfirmada;
//	private String emailResponsavel;
//	
//
//
//	public Long getId() {
//		return id;
//	}
//
//	public String getEmailResponsavel() {
//		return emailResponsavel;
//	}
//
//	public void setEmailResponsavel(String emailResponsavel) {
//		this.emailResponsavel = emailResponsavel;
//	}
//
//	public String getLogin() {
//        return login;
//    }
//
//    public void setLogin(String login) {
//        this.login = login;
//    }
//
//    public void setId(Long id) {
//		this.id = id;
//	}
//
//	public Long getCnpj() {
//		return cnpj;
//	}
//
//	public void setCnpj(Long cnpj) {
//		this.cnpj = cnpj;
//	}
//
//	public String getRazaoSocial() {
//		return razaoSocial;
//	}
//
//	public void setRazaoSocial(String razaoSocial) {
//		this.razaoSocial = razaoSocial;
//	}
//
//	public String getNomeFantasia() {
//		return nomeFantasia;
//	}
//
//	public void setNomeFantasia(String nomeFantasia) {
//		this.nomeFantasia = nomeFantasia;
//	}
//
//	public String getNomeSolicitante() {
//		return nomeSolicitante;
//	}
//
//	public void setNomeSolicitante(String nomeSolicitante) {
//		this.nomeSolicitante = nomeSolicitante;
//	}
//
//	public Long getCpfSolicitante() {
//		return cpfSolicitante;
//	}
//
//	public void setCpfSolicitante(Long cpfSolicitante) {
//		this.cpfSolicitante = cpfSolicitante;
//	}
//
//	public Endereco getEndereco() {
//		return endereco;
//	}
//
//	public void setEndereco(Endereco endereco) {
//		this.endereco = endereco;
//	}
//
//	public String getCrc() {
//		return crc;
//	}
//
//	public void setCrc(String crc) {
//		this.crc = crc;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public Long getTelefoneFixo() {
//		return telefoneFixo;
//	}
//
//	public void setTelefoneFixo(Long telefoneFixo) {
//		this.telefoneFixo = telefoneFixo;
//	}
//
//	public Long getTelefoneCelular() {
//		return telefoneCelular;
//	}
//
//	public void setTelefoneCelular(Long telefoneCelular) {
//		this.telefoneCelular = telefoneCelular;
//	}
//
//	public String getCodigoPrimariaCnae() {
//		return codigoPrimariaCnae;
//	}
//
//	public void setCodigoPrimariaCnae(String codigoPrimariaCnae) {
//		this.codigoPrimariaCnae = codigoPrimariaCnae;
//	}
//
//	public String getAtividadePrimariaCnae() {
//		return atividadePrimariaCnae;
//	}
//
//	public void setAtividadePrimariaCnae(String atividadePrimariaCnae) {
//		this.atividadePrimariaCnae = atividadePrimariaCnae;
//	}
//
//	public String getCodigoSecundariaCnae() {
//		return codigoSecundariaCnae;
//	}
//
//	public void setCodigoSecundariaCnae(String codigoSecundariaCnae) {
//		this.codigoSecundariaCnae = codigoSecundariaCnae;
//	}
//
//	public String getAtividadeSecundariaCnae() {
//		return atividadeSecundariaCnae;
//	}
//
//	public void setAtividadeSecundariaCnae(String atividadeSecundariaCnae) {
//		this.atividadeSecundariaCnae = atividadeSecundariaCnae;
//	}
//
//	public String getSenha() {
//		return senha;
//	}
//
//	public void setSenha(String senha) {
//		this.senha = senha;
//	}
//
//	public String getSenhaConfirmada() {
//		return senhaConfirmada;
//	}
//
//	public void setSenhaConfirmada(String senhaConfirmada) {
//		this.senhaConfirmada = senhaConfirmada;
//	}
//	
//	public EscritorioContabil transformaParaObjeto(){
//		EscritorioContabil escritorio = new EscritorioContabil();
//			escritorio.setAtividadePrimariaCnae(this.atividadePrimariaCnae);
//			escritorio.setCnpj(this.cnpj);
//			escritorio.setRazaoSocial(this.razaoSocial);
//			escritorio.setNomeFantasia(this.nomeFantasia);		
//			escritorio.setCrc(this.crc);			
//			escritorio.setEmail(this.email);
//			escritorio.setTelefoneCelular(this.telefoneCelular);
//			escritorio.setTelefoneFixo(this.telefoneFixo);
//			escritorio.setAtividadePrimariaCnae(this.atividadePrimariaCnae);
//			escritorio.setAtividadeSecundariaCnae(this.atividadeSecundariaCnae);
//			escritorio.setCodigoPrimariaCnae(this.codigoPrimariaCnae);
//			escritorio.setCodigoSecundariaCnae(this.codigoSecundariaCnae);
//			escritorio.setEndereco(this.endereco);
//			escritorio.setStatus(true);
//		return escritorio;
//	}
//
//	public Usuario transformaParaObjetoUser(){
//		Usuario responsavel = new Usuario();
//			responsavel.setStatus(true);
//			responsavel.setLogin(Long.toString(this.cpfSolicitante));
//			responsavel.setNome(this.nomeSolicitante);
//			responsavel.setEmail(this.email);
//			responsavel.setCpf(Long.toString(this.cpfSolicitante));
//			responsavel.setEmail(this.emailResponsavel);
//			String senhacriptografada = new BCryptPasswordEncoder().encode(responsavel.getCpf());
//			responsavel.setSenha(senhacriptografada);
//		return responsavel;
//	}
//}
