package br.com.wk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.wk.dto.ConfiguracaoDTO;
import br.com.wk.model.Configuracao;
import br.com.wk.service.ConfiguracaoService;
import io.swagger.models.Response;

@RestController
@RequestMapping(value = "/configuracao")
public class ConfiguracaoController {
    @Autowired
    private ConfiguracaoService configuracaoService;

    @PostMapping(value = "/salvar")
    public ResponseEntity<List<String>> salvarConfiguracao(@RequestBody ConfiguracaoDTO configuracaoDTO){
        return new ResponseEntity<List<String>>(configuracaoService.verificaRegras(configuracaoDTO), HttpStatus.OK);
    }

    @GetMapping(value = "/todos")
    public ResponseEntity<List<Configuracao>> getAll(@RequestParam Long id, @RequestParam String busca){
        return new ResponseEntity<List<Configuracao>>(configuracaoService.findAll(id, busca), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Configuracao> findById(@PathVariable Long id){
        return new ResponseEntity<Configuracao>(configuracaoService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> ativaDesativaStatus(@PathVariable Long id){
        configuracaoService.ativarDesativaStatus(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<List<String>> updateConfiguracao(@RequestBody ConfiguracaoDTO configuracaoDTO){
        return new ResponseEntity<List<String>>(configuracaoService.update(configuracaoDTO), HttpStatus.OK);
    }
}
