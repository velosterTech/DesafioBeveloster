
//package br.com.wk.service;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import br.com.wk.dto.TipoDocumentoDTO;
//import br.com.wk.exception.EntidadeNaoEncontradaException;
//import br.com.wk.model.Categoria;
//import br.com.wk.model.Escritorio;
//import br.com.wk.model.EscritorioContabil;
//import br.com.wk.model.TipoDocumento;
//import br.com.wk.model.Usuario;
//import br.com.wk.repository.EscritorioContabilRepository;
//import br.com.wk.repository.TipoDocumentoRepository;
//import br.com.wk.repository.UsuarioRepository;
//
//@Service
//public class TipoDocumentoServiceImpl implements TipoDocumentoService{
//
//	@Autowired
//	TipoDocumentoRepository tipoDocumentoRepository;
//	
//	@Autowired
//	EscritorioService escritorioService;
//	
//	@Autowired
//	EscritorioContabilRepository escritorioContabilRepository;
//	
//	@Autowired
//	CategoriaService categoriaService;
//
//	@Autowired
//	UsuarioService usuarioService;
//
//	@Autowired
//	UsuarioRepository usuarioRepository;
//	
//	@Override
//	public TipoDocumento findDocumentoById(Long id) {
//		
//		Optional<TipoDocumento> documento = tipoDocumentoRepository.findById(id);
//		if (documento.isPresent()) {
//			return documento.get();
//		} else {
//			throw new EntidadeNaoEncontradaException("tipo de documento não encontrado");
//		}
//	}
//
//	@Override
//	public TipoDocumento createDocumento(TipoDocumentoDTO tipoDocDTO) {
//		
//		Optional<EscritorioContabil> escritorioContabilOpt = escritorioContabilRepository.findById(tipoDocDTO.getEscritorioContabilId());
//		Optional<Categoria> categoriaOptional = categoriaService.findById(tipoDocDTO.getCategoriaId());
//		TipoDocumento tipoDocumento = tipoDocDTO.transformToObject();
//		tipoDocumento.setEscritorioContabil(escritorioContabilOpt.get());
//		tipoDocumento.setCategoria(categoriaOptional.get());
//		return tipoDocumentoRepository.save(tipoDocumento);
//	}
//
//	@Override
//	public List<TipoDocumento> findAllTipoDocumentosByEscritorio(Long idEscritorio) {
//
//		Optional<EscritorioContabil> escritorioContabilOpt = escritorioContabilRepository.findById(idEscritorio);
//		return tipoDocumentoRepository.findAllByEscritorio(escritorioContabilOpt.get().getId());
//	
//	}
//	
//	@Override
//	public List<TipoDocumento> findAllTipoDocumentos(Long id){
//		Usuario usuario = usuarioRepository.findById(id).get();
//		List<TipoDocumento> tipoDocumentosDTO = new ArrayList<>();
//    	if(usuario.getRoles().get(0).getNomeRole().equals("ROLE_ADMIN")){
//			tipoDocumentosDTO = tipoDocumentoRepository.findAll();
//		    return tipoDocumentosDTO;
//		} else if(usuario.getRoles().get(0).getNomeRole().equals("ROLE_ADMIN_ESCRITORIO")){
//			Long escritorioId = usuario.getEscritorios().get(0).getId();
//			tipoDocumentosDTO = tipoDocumentoRepository.findAllByEscritorio(escritorioId);
//			return tipoDocumentosDTO;
//		}
//		return tipoDocumentosDTO;
//	}
//	
//	
//	@Override
//	public void deleteById(Long id){
//		
//		TipoDocumento tipoDocumento = findDocumentoById(id);
//		tipoDocumentoRepository.delete(tipoDocumento);		
//	}
//
//	@Override
//	public TipoDocumento updateDocumento(TipoDocumentoDTO tipoDocumentoDTO, Long id) {
//		TipoDocumento tipoDocumento = tipoDocumentoDTO.transformToObject();
//		Optional<Categoria> categoriaOptional = categoriaService.findById(tipoDocumentoDTO.getCategoriaId());
//		tipoDocumento.setEscritorioContabil(escritorioContabilRepository.findOneById(tipoDocumentoDTO.getEscritorioContabilId()));
//		tipoDocumento.setCategoria(categoriaOptional.get());
//		tipoDocumento.setId(id);
//		tipoDocumentoRepository.save(tipoDocumento);
//		return null;
//	}
//
//	@Override
//	public List<TipoDocumento> findAllByUsuarioEscritorio(Long userId) {
//		// TODO Auto-generated method stub
//		return tipoDocumentoRepository.findAllByUsuarioEscritorio(userId);
//	}
//
//	@Override
//	public List<TipoDocumento> findAllByCategoria(Long id, Long categoriaId) {
//		Usuario usuario = usuarioService.findById(id);
//		if(usuario.getRoles().get(0).getNomeRole().equals("ROLE_ADMIN")){
//		    return tipoDocumentoRepository.findAllByCategoria(categoriaId);
//		} else{
//			Long escritorioId = usuario.getEscritorios().get(0).getId();
//			return tipoDocumentoRepository.findAllByEscritorioAndCategoria(escritorioId, categoriaId);
//		}
//	}
//
//}