package br.com.wk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.wk.dto.DepartamentoDTO;
import br.com.wk.model.Departamento;
import br.com.wk.service.DepartamentoService;

@RestController
@RequestMapping(value = "/departamento")
public class DepartamentoController {

    @Autowired
	private DepartamentoService departamentoService;
	
	@PostMapping(value = "/salvar")
	public ResponseEntity<Departamento> salvar(@RequestBody DepartamentoDTO departamentoDTO) {
		return new ResponseEntity<Departamento>(departamentoService.add(departamentoDTO), HttpStatus.OK);
	}
    
}
