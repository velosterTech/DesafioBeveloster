
//package br.com.wk.model;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//import javax.persistence.FetchType;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.OneToMany;
//import javax.persistence.OneToOne;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//
//import br.com.wk.dto.EscritorioDTO;
//import br.com.wk.enums.NaturezaJuridica;
//import br.com.wk.enums.RegimeTributario;
//
//@Entity
//
//@Table(name="pessoa_juridica")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "contratos", "escritoriosPj"})
//public class PessoaJuridica {
//
//	@GeneratedValue (strategy = GenerationType.AUTO)
//	@Id
//	private long id;
//	
//	@OneToMany (mappedBy="pessoaJuridica")
//	private List<Escritorio> escritoriosPj = new ArrayList<Escritorio>();
//	
//	@OneToMany (mappedBy="pjId")
//	private List<EmpresaCliente> contratos = new ArrayList<EmpresaCliente>();
//
//	private String nome_completo;
//
//	private String razao_social;
//	
//	private String celular;
//	
//	private String email;
//	
//	@Column(name = "cnpj", nullable = false, unique = true)
//	private String cnpj;
//
//	@Column(name= "inscricao_estadual", nullable = false, unique = true)
//	private long inscricao_estadual;
//
//	@Column(name = "regime_tributario")
////	@Enumerated(EnumType.STRING)
//	private RegimeTributario regimeTributario;
//
//	@Column(name = "natureza_juridica")
////	@Enumerated(EnumType.STRING)
//	private NaturezaJuridica naturezaJuridica;
//
//	private String nome_fantasia;
//	
//	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name = "endereco_id", referencedColumnName = "id")
////	@JsonIgnoreProperties(value = {"escritorioId"}, allowSetters = true)
//	private Endereco endereco;
//	
//	public NaturezaJuridica getNaturezaJuridica(){
//		return this.naturezaJuridica;
//	}
//
//	public String getNome_fantasia() {
//		return nome_fantasia;
//	}
//
//	public void setNome_fantasia(String nome_fantasia) {
//		this.nome_fantasia = nome_fantasia;
//	}
//
//	public void setNaturezaJuridica(NaturezaJuridica naturezaJuridica){
//		this.naturezaJuridica = naturezaJuridica;
//	}
//
//	public RegimeTributario getRegimeTributario(){
//		return this.regimeTributario;
//	}
//
//	public void setRegimeTributario(RegimeTributario regimeTributario){
//		this.regimeTributario = regimeTributario;
//	}
//
//	public long getId() {
//		return id;
//	}
//
//	public long getInscricao_estadual() {
//		return inscricao_estadual;
//	}
//
//	public void setInscricao_estadual(long inscricao_estadual) {
//		this.inscricao_estadual = inscricao_estadual;
//	}
//
//	public PessoaJuridica() {
//		super();
//	}
//
//	public PessoaJuridica(long id, List<EmpresaCliente> contratos, String nome_completo,
//			String razao_social, String celular, String email, String cnpj) {
//		super();
//		this.id = id;
//		//this.escritoriosPj = escritoriosPj;
//		this.contratos = contratos;
//		this.nome_completo = nome_completo;
//		this.razao_social = razao_social;
//		this.celular = celular;
//		this.email = email;
//		this.cnpj = cnpj;
//	}
//
//
//
//	public String getNome_completo() {
//		return nome_completo;
//	}
//
//	public void setNome_completo(String nome_completo) {
//		this.nome_completo = nome_completo;
//	}
//
//
//
//	public void setId(long id) {
//		this.id = id;
//	}
//
//	public String getRazao_social() {
//		return razao_social;
//	}
//
//	public void setRazao_social(String razao_social) {
//		this.razao_social = razao_social;
//	}
//
//	public String getCelular() {
//		return celular;
//	}
//
//	public void setCelular(String celular) {
//		this.celular = celular;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	public String getCnpj() {
//		return cnpj;
//	}
//
//	public void setCnpj(String cnpj) {
//		this.cnpj = cnpj;
//	}
//	
//	
//	public List<Escritorio> getEscritoriosPj() {
//		return escritoriosPj;
//	}
//
////	public void setEscritoriosPj(List<Escritorio> escritoriosPj) {
////		this.escritoriosPj = escritoriosPj;
////	}
//
//	public List<EmpresaCliente> getContratos() {
//		return contratos;
//	}
//
//	public void setContratos(List<EmpresaCliente> contratos) {
//		this.contratos = contratos;
//	}
//	
//	public Endereco getEndereco() {
//		return endereco;
//	}
//
//	public void setEndereco(Endereco endereco) {
//		this.endereco = endereco;
//	}
//
//	@Override
//	public int hashCode() {
//		return Objects.hash(celular, cnpj, contratos, email, endereco, escritoriosPj, id, inscricao_estadual,
//				naturezaJuridica, nome_completo, razao_social, regimeTributario);
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		PessoaJuridica other = (PessoaJuridica) obj;
//		return Objects.equals(celular, other.celular) && Objects.equals(cnpj, other.cnpj)
//				&& Objects.equals(contratos, other.contratos) && Objects.equals(email, other.email)
//				&& Objects.equals(endereco, other.endereco) && Objects.equals(escritoriosPj, other.escritoriosPj)
//				&& id == other.id && inscricao_estadual == other.inscricao_estadual
//				&& naturezaJuridica == other.naturezaJuridica && Objects.equals(nome_completo, other.nome_completo)
//				&& Objects.equals(razao_social, other.razao_social) && regimeTributario == other.regimeTributario;
//	}
//
//	
//
//	
//
//
//
//	
//}