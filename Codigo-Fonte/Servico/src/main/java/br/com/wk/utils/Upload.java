package br.com.wk.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

import org.springframework.web.multipart.MultipartFile;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class Upload {
	public static String ocr(File file) {
		Tesseract tesseract = new Tesseract();
		String text;

		try {
			tesseract.setDatapath("tessdata");
			tesseract.setLanguage("por");
			text = tesseract.doOCR(file);
		} catch (TesseractException e) {
			text = e.getMessage();
		}

		return text;
	}

	public static File converterMultipartFileParaFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public static File base64ToPdf(String b64, String filename) {
		File file = new File("test.pdf");

		try (FileOutputStream fos = new FileOutputStream(file);) {
			// To be short I use a corrupted PDF string, so make sure to use a valid one if
			// you want to preview the PDF file
			// String b64 =
			// "JVBERi0xLjUKJYCBgoMKMSAwIG9iago8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvRmlyc3QgMTQxL04gMjAvTGVuZ3==";
			byte[] decoder = Base64.getDecoder().decode(b64);

			fos.write(decoder);
			fos.close();
			return file;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public static String encodeFileToBase64(File file) {
		try {
			byte[] fileContent = Files.readAllBytes(file.toPath());
			return Base64.getEncoder().encodeToString(fileContent);
		} catch (IOException e) {
			return null;
		}
	}
}
