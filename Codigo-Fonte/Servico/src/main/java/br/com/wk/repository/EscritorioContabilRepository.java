//package br.com.wk.repository;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//import org.springframework.stereotype.Repository;
//
//import br.com.wk.model.EscritorioContabil;
//
//@Repository
//public interface EscritorioContabilRepository extends JpaRepository<EscritorioContabil, Long>{
//    @Query("select u from EscritorioContabil u where u.razaoSocial =?1")
//	EscritorioContabil findByRazao (String razao_social);
//
//    @Query("SELECT u FROM EscritorioContabil u WHERE u.razaoSocial LIKE ?1% AND u.status='true'")
//	List<EscritorioContabil> findAllLike(String busca);
//    
//    @Query("select e from EscritorioContabil e where e.id = :id")
//    EscritorioContabil findOneById(@Param("id") Long id);
//    
//    @Query("select e.id from EscritorioContabil e where e.responsavel.id = :userId")
//    List<Long> findAllIdByUsuarioId(@Param("userId") Long userId);
//}
//
