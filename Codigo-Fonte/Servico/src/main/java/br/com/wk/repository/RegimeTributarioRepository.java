package br.com.wk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.wk.model.RegimeTributario;

@Repository
public interface RegimeTributarioRepository extends JpaRepository<RegimeTributario, Long>{

	@Query("Select r from RegimeTributario r where r.nome =?1")
	public RegimeTributario findByNome(String nome); 
}
