//package br.com.wk.service;
//
//import java.util.List;
//import java.util.Optional;
//
//
//import br.com.wk.model.PessoaJuridica;
//
//public interface PessoaJuridicaService {
//
//    List<PessoaJuridica> findAll();
//
//    Optional<PessoaJuridica> findById(Long id);
//
//    PessoaJuridica update (PessoaJuridica pessoajuridica);
//
//    void deleteById(Long id);
//
//	PessoaJuridica save(PessoaJuridica pessoajuridica);
//
//    PessoaJuridica findByCNPJ(String cnpj);
//
//    PessoaJuridica findByRazao(String razao_social);
//
//    PessoaJuridica findByInscricaoEstadual(Long inscricao);
//
//	
//}
