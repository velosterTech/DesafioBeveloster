//package br.com.wk.service;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Sort;
//import org.springframework.stereotype.Service;
//
//import br.com.wk.model.Categoria;
//import br.com.wk.model.Mensagem;
//import br.com.wk.model.TipoDocumento;
//import br.com.wk.model.Usuario;
//import br.com.wk.repository.MensagemRepository;
//import br.com.wk.repository.UsuarioRepository;
//
//@Service
//public class MensagemServiceImpl implements MensagemService {
//
//    @Autowired
//    private MensagemRepository mensagemRepository;
//    @Autowired 
//    private CategoriaService categoriaService;
//    @Autowired
//    private TipoDocumentoService tipoDocumentoService;
//    @Autowired
//    private UsuarioRepository usuarioRepository;
//
//    @Override
//    public List<Mensagem> findAll(Long id) {
//        Usuario usuario = usuarioRepository.findById(id).get();
//        if(usuario.getLogin().equals("admin")){
//            return (List<Mensagem>)mensagemRepository.findAll(); 
//        }
//        return mensagemRepository.findAllSpec(usuario, Sort.by("id").descending());
//    }
//
//    @Override
//    public Mensagem novMensagem(Long id, Mensagem mensagem) {
//        mensagem.setUsuarioRemetente(usuarioRepository.findById(id).get());
//        mensagem.setStatus(false);
//        return mensagemRepository.save(mensagem);
//    }
//
//    @Override
//    public Mensagem novaSolicitacao(Long id, Mensagem mensagem, Long categoriaId, Long tipoDocumentoId) {
//        mensagem.setUsuarioRemetente(usuarioRepository.findById(id).get());
//        mensagem.setStatus(false);
//        Categoria categoria = categoriaService.findById(categoriaId).get();
//        TipoDocumento tipoDocumento = tipoDocumentoService.findDocumentoById(tipoDocumentoId);
//        mensagem.setTxtMensagem(mensagem.getTxtMensagem() + " Categoria: " + categoria.getNomeCategoria() + "; Documento: " + tipoDocumento.getNomeDocumento());
//        return mensagemRepository.save(mensagem);
//    }
//
//    @Override
//    public Mensagem atualizaStatus(Long id) {
//        Mensagem mensagem = mensagemRepository.findById(id).get();
//        mensagem.setStatus(true);
//        return mensagemRepository.save(mensagem);
//    }
//
//    
//    
//}
