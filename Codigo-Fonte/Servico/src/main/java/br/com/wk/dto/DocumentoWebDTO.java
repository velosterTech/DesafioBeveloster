package br.com.wk.dto;

import br.com.wk.model.Documento;
import br.com.wk.model.DocumentoEnviado;
import br.com.wk.model.Empresa;
import br.com.wk.model.Escritorio;

public class DocumentoWebDTO {
    private Boolean check;
    private Double valor;
    private String vencimento;
    private Empresa empresa;
    private Documento documento;
    private String fileBase64;
    private String fileOcr;
    private String fileName;
    private Boolean enviado;
    private Escritorio escritorio;

    public Escritorio getEscritorio() {
        return escritorio;
    }
    public void setEscritorio(Escritorio escritorio) {
        this.escritorio = escritorio;
    }
    public Boolean getEnviado() {
        return enviado;
    }
    public void setEnviado(Boolean enviado) {
        this.enviado = enviado;
    }
    public Boolean getCheck() {
        return check;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public void setCheck(Boolean check) {
        this.check = check;
    }
    public Double getValor() {
        return valor;
    }
    public void setValor(Double valor) {
        this.valor = valor;
    }
    public String getVencimento() {
        return vencimento;
    }
    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }
    public Empresa getEmpresa() {
        return empresa;
    }
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    public Documento getDocumento() {
        return documento;
    }
    public void setDocumento(Documento documento) {
        this.documento = documento;
    }
    public String getFileBase64() {
        return fileBase64;
    }
    public void setFileBase64(String fileBase64) {
        this.fileBase64 = fileBase64;
    }
    public String getFileOcr() {
        return fileOcr;
    }
    public void setFileOcr(String fileOcr) {
        this.fileOcr = fileOcr;
    }

    public DocumentoEnviado transformaParaObjeto(){
        DocumentoEnviado documentoEnviado = new DocumentoEnviado(valor, vencimento, empresa, documento, escritorio);
        return documentoEnviado;
    }
    
}
