package br.com.wk.service;

import br.com.wk.exception.MailSenderException;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class MailService {
	
	private final JavaMailSender javaMailSender;
	
	private final MessageSource messageSource;
	
	public MailService(JavaMailSender javaMailSender, MessageSource messageSource) {
		this.javaMailSender = javaMailSender;
		this.messageSource = messageSource;
	}
	
	@Async
	public String sendMailWPassword(String[] mailTo, String mailSubject, String password, String login) throws MailSenderException {
		try {
			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);
			helper.setFrom("velostertech2022@gmail.com");
			helper.setTo(mailTo);
			helper.setSubject(mailSubject);
			helper.setText("<p>Seu cadastro no sistema SAFFO foi confirmado com sucesso!</p>"
					+ "<p>Acesse o sistema: <a href=\"http://206.189.6.222\">Sistema SAFFO</a></p>"
					+ "<br /><p>Seus dados para acesso são:</p>"
					+ "<p>Login: <strong>" + login + "</strong></p>"
					+ "<p>Senha: <strong>" + password + "</strong></p>"
					+ "<br /><p>Atenciosamente,</p>"
					+ "<p>Equipe de desenvolvimento.</p>", true);
			javaMailSender.send(message);
			return "OK";
		} catch(Exception e) {
			throw new MailSenderException(e.getMessage());
		}
	}
	
	public String sendMailWithAttachment(String mailTo, String mailSubject, File file, String mensagem) throws MailSenderException {
		try {
			System.out.println("testeATTACHMENT");
			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message,true);
			helper.setFrom("velostertech2022@gmail.com");
			helper.setTo(mailTo);
			helper.setSubject(mailSubject);
			helper.setText(mensagem, true);
			FileSystemResource fileEmail = new FileSystemResource(file);
			helper.addAttachment(file.getName(), fileEmail);
			javaMailSender.send(message);
			file.delete();			
			return "OK";
			
		} catch(Exception e) {
			throw new MailSenderException(e.getMessage());
		}
	}
	
	@Async
	public String sendMailUpdateWPassword(String mailTo, String mailSubject, String password, String login) throws MailSenderException {
		try {
			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);
			helper.setFrom("velostertech2022@gmail.com");
			helper.setTo(mailTo);
			helper.setSubject(mailSubject);
			helper.setText("<p>Seu cadastro no sistema SAFFO foi atualizado com sucesso!</p>"
					+ "<p>Acesse o sistema: <a href=\"http://3.93.147.126\">Sistema SAFFO</a></p>"
					+ "<br /><p>Seus dados para acesso são:</p>"
					+ "<p>Login: <strong>" + login + "</strong></p>"
					+ "<p>Senha: <strong>" + password + "</strong></p>"
					+ "<br /><p>Atenciosamente,</p>"
					+ "<p>Equipe de desenvolvimento.</p>", true);
			javaMailSender.send(message);
			return "OK";
		}catch(Exception e) {
			throw new MailSenderException(e.getMessage());
		}
	}
}
