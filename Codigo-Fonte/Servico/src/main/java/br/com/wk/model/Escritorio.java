package br.com.wk.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "saffo_escritorio")
public class Escritorio {
	
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "saffo_escritorio_id")
    private Long id;

    @Column(name = "saffo_escritorio_cnpj")
    private String cnpj;

    @Column(name = "saffo_escritorio_razao_social")
    private String razaoSocial;

    @Column(name = "saffo_escritorio_nome_fantasia")
    private String nomeFantasia;

    @Column(name = "saffo_escritorio_contato")
    private String contato;

    @Column(name = "saffo_escritorio_email")
    private String email;

    @Column(name = "saffo_escritorio_endereco")
    private String endereco;

    @Column(name = "saffo_escritorio_status")
    private Boolean status;
    
    @ManyToMany(mappedBy = "escritorios")
    private Set<Usuario> usuarios = new HashSet<>();
    
    @OneToMany(mappedBy = "escritorio")
    private Set<Empresa> empresas = new HashSet<>();
    
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return this.cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return this.razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getNomeFantasia() {
        return this.nomeFantasia;
    }

    public void setNomeFantasia(String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public String getContato() {
        return this.contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
    
    @JsonIgnore
    public Set<Usuario> getUsuarios() {
		return usuarios;
	}
    
    
	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Escritorio))
			return false;
		Escritorio other = (Escritorio) obj;
		return Objects.equals(id, other.id);
	}

    @JsonIgnore
    public Set<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(Set<Empresa> empresas) {
        this.empresas = empresas;
    }

	


	
    
    
}
