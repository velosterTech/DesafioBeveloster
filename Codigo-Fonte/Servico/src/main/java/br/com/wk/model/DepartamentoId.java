package br.com.wk.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Embeddable
public class DepartamentoId implements Serializable{
    
    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "saffo_departamento_id", unique = true)
    private Long departamentoId;
    
    @Column(name = "saffo_escritorio_saffo_escritorio_id")
    private Long escritorioId;
    
    public DepartamentoId() {
    }
    
    public DepartamentoId(Long departamentoId, Long escritorioId) {
        this.departamentoId = departamentoId;
        this.escritorioId = escritorioId;
    }
    public Long getDepartamentoId() {
        return departamentoId;
    }
    public void setDepartamentoId(Long departamentoId) {
        this.departamentoId = departamentoId;
    }
    public Long getEscritorioId() {
        return escritorioId;
    }
    public void setEscritorioId(Long escritorioId) {
        this.escritorioId = escritorioId;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((departamentoId == null) ? 0 : departamentoId.hashCode());
        result = prime * result + ((escritorioId == null) ? 0 : escritorioId.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DepartamentoId other = (DepartamentoId) obj;
        if (departamentoId == null) {
            if (other.departamentoId != null)
                return false;
        } else if (!departamentoId.equals(other.departamentoId))
            return false;
        if (escritorioId == null) {
            if (other.escritorioId != null)
                return false;
        } else if (!escritorioId.equals(other.escritorioId))
            return false;
        return true;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
