package br.com.wk.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.wk.dto.DocumentoDTO;
import br.com.wk.dto.DocumentoWebDTO;
import br.com.wk.model.Documento;
import br.com.wk.model.DocumentoEnviado;
import br.com.wk.service.DocumentoEnviadoService;
import br.com.wk.service.DocumentoService;

@RestController
@RequestMapping(value = "/documento")
public class DocumentoController {

	@Autowired
	private DocumentoService documentoService;
	@Autowired
	private DocumentoEnviadoService documentoEnviadoService;

	@PostMapping(value = "/salvar")
	public ResponseEntity<Documento> salvar(@RequestBody DocumentoDTO documentoDTO) {
		return new ResponseEntity<Documento>(documentoService.salvar(documentoDTO), HttpStatus.OK);
	}

	@GetMapping(value = "/todos/cnpj")
	public ResponseEntity<List<Documento>> getAll(@RequestParam String cnpj){
		return new ResponseEntity<List<Documento>>(documentoService.findAll(cnpj), HttpStatus.OK);
	}

	@PostMapping(value = "/arquivos/{cnpj}")
	public ResponseEntity<List<DocumentoWebDTO>> recebeUpload(@PathVariable String cnpj, @RequestParam MultipartFile[] files) {
		return new ResponseEntity<List<DocumentoWebDTO>>(documentoService.ocr(cnpj, files), HttpStatus.OK);
	}

	@PostMapping(value = "/enviar-documento")
	public ResponseEntity<Void> enviarDocumento(@RequestBody DocumentoWebDTO documentoDTO) {
		documentoService.enviarDocumento(documentoDTO);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@GetMapping(value = "/documentos-enviados")
	public ResponseEntity<List<DocumentoEnviado>> getAllDocumentosEnviados(@RequestParam Long id){
		return new ResponseEntity<List<DocumentoEnviado>>(documentoEnviadoService.findAllByEmpresa(id), HttpStatus.OK);
	}
//
//	@GetMapping(value = "/todos")
//	public ResponseEntity<List<Documento>> getAllDocumentos(@RequestParam Long id){
//		return new ResponseEntity<List<Documento>>(documentoService.findAll(id), HttpStatus.OK);
//	}
//
//	@GetMapping(value = "/{id}")
//	public ResponseEntity<Documento> encontraDocumento(@PathVariable Long id){
//		return new ResponseEntity<Documento>(documentoService.findById(id), HttpStatus.OK);
//	}
//
//	@PutMapping(value = "/editar/{id}")
//	public ResponseEntity<Documento> editaDocumento(@PathVariable Long id, @RequestBody Documento documento){
//		return new ResponseEntity<Documento>(documentoService.update(id, documento),HttpStatus.OK);
//	}

}