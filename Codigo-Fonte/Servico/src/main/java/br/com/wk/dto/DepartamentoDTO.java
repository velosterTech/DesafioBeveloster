package br.com.wk.dto;

import br.com.wk.model.Departamento;

public class DepartamentoDTO {
    private String nome;
    private String cnpj;
    

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Departamento transformaParaObjeto(){
        Departamento departamento = new Departamento();
        departamento.setNome(this.nome);
        return departamento;
    }
}
