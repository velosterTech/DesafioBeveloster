
//package br.com.wk.repository;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
//
//import br.com.wk.model.Escritorio;
//import br.com.wk.model.EscritorioContabil;
//import br.com.wk.model.TipoDocumento;
//
//public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, Long> {
//	
//	@Query("select td from TipoDocumento td where td.escritorioContabil.id = :escritorioId")
//	List<TipoDocumento> findAllByEscritorio(@Param("escritorioId") Long escritorioId);
//	
//	@Query("select td from TipoDocumento td where td.escritorioContabil.responsavel.id = :userId")
//	List<TipoDocumento> findAllByUsuarioEscritorio(@Param("userId") Long userId);
//
//	@Query("select td from TipoDocumento td where td.escritorioContabil.id = :escritorioId and td.categoria.id =:categoriaId")
//	List<TipoDocumento> findAllByEscritorioAndCategoria(@Param("escritorioId") Long escritorioId, @Param("categoriaId") Long categoriaId);
//
//	@Query("select td from TipoDocumento td where td.categoria.id =:categoriaId")
//	List<TipoDocumento> findAllByCategoria(@Param("categoriaId") Long categoriaId);
//}
