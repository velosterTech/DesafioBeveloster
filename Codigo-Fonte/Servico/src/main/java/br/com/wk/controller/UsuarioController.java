package br.com.wk.controller;

import br.com.wk.dto.UsuarioDTO;
import br.com.wk.exception.MailSenderException;
import br.com.wk.model.Usuario;
import br.com.wk.repository.UsuarioRepository;
import br.com.wk.service.MailService;
import br.com.wk.service.UserDetailsServiceImpl;
import br.com.wk.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UserDetailsServiceImpl implementacaoUserDetailsService;
	
	@Autowired
	private MailService mailService;
	
//	@PostMapping(value = "/admin", produces = "application/json")
//	public ResponseEntity<Usuario> cadastrarAdmin( @Valid @RequestBody Usuario usuario) {
//		String senhacriptografada = new BCryptPasswordEncoder().encode(usuario.getSenha());
//		usuario.setSenha(senhacriptografada);
//		Usuario usuarioSalvo = usuarioRepository.save(usuario);
////		usuario.setStatus(true);
//		implementacaoUserDetailsService.insereAcessoPadraoAdmin(usuarioSalvo.getId());
//		return new ResponseEntity<Usuario>(usuarioSalvo, HttpStatus.OK);
//	} 

	@PostMapping(value = "/cadastrar", produces = "application/json")
	public ResponseEntity<Usuario> cadastrarUsuario(@RequestParam String cnpj, @Valid @RequestBody UsuarioDTO
	 usuarioDTO) throws MailSenderException {
		Usuario usuarioSalvo = usuarioService.save(cnpj, usuarioDTO);		
		mailService.sendMailWPassword(new String[]{usuarioDTO.getEmail()}, "Acesso ao sistema SAFFO", usuarioDTO.getCpf(), usuarioDTO.getCpf());
		return new ResponseEntity<Usuario>(usuarioSalvo, HttpStatus.OK);
	}

	
	@PutMapping(value = "/status")
		public ResponseEntity<?> ativarDesativarStatus(@RequestBody Long id){
		usuarioService.ativardesativar(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	
	@PutMapping (value = "/update/{id}", produces = "application/json")
	public  Usuario updateUsuario(@PathVariable Long id, @RequestBody UsuarioDTO usuarioDTO, @RequestParam String cnpj) throws MailSenderException{
		Usuario usuarioAtual = usuarioService.uptade(id, usuarioDTO, cnpj);
	    mailService.sendMailUpdateWPassword(usuarioDTO.getEmail(), "Atualização de acesso ao sistema SAFFO", usuarioDTO.getCpf(), usuarioDTO.getCpf());
		return usuarioAtual;
	}  

	@GetMapping(value="/todos")
	public ResponseEntity<Set<Usuario>> getUsuarios(@RequestParam("id") Long id, @RequestParam("status") Boolean status, @RequestParam("search") String search){
		return new ResponseEntity<Set<Usuario>>((Set<Usuario>)usuarioService.findAll(id, status, search),HttpStatus.OK);
	}


	@GetMapping(value="/buscaLogin")
	public ResponseEntity<Usuario> getUsuarioByLogin(@RequestParam("login") String login){
		return new ResponseEntity<Usuario>(usuarioRepository.findUserByLogin(login), HttpStatus.OK);
	} 

	@GetMapping(value="/{id}")
	public ResponseEntity<Usuario> buscaUsuario(@PathVariable long id){
		return new ResponseEntity<Usuario>(usuarioRepository.findById(id).get(), HttpStatus.OK);
	}
	
	@GetMapping(value="/cpf/{cpf}") // TODO: Tentar passar atraves do DTO (não expor o cpf) - Danillo
	public ResponseEntity<List<Usuario>> findUsuarioByCPF(@PathVariable String cpf){
		return new ResponseEntity<List<Usuario>>(usuarioService.findByCPF(cpf), HttpStatus.OK);
	}
	
	@GetMapping(value="/crc/{crc}") // TODO: Tentar passar atraves do DTO (não expor o cpf) - Danillo
	public ResponseEntity<Usuario> findByCRC(@PathVariable String crc){
		return new ResponseEntity<Usuario>(usuarioService.findByCRC(crc), HttpStatus.OK);
	}

}
