package br.com.wk.repository;

import br.com.wk.enums.NivelDeAcessoDoSistema;
import br.com.wk.model.Empresa;
import br.com.wk.model.Escritorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EmpresaRepository  extends JpaRepository <Empresa, Long> {
	
    @Query("select e from Empresa e where e.cnpj =?1")
	Optional<Empresa> findByCNPJ (String cnpj);

    @Query("SELECT e FROM Empresa e WHERE lower(e.razaoSocial) LIKE lower(concat(?1,'%'))")// AND u.status='true'
	List<Empresa> findAllLike(String busca);

    @Query("SELECT e FROM Empresa e WHERE lower(e.razaoSocial) LIKE lower(concat(?1,'%')) AND e.status = 'true'")
	List<Empresa> findAllLikeTrue(String busca);

    @Query("SELECT e from Empresa e WHERE e.escritorio =?1")
    List<Empresa> findByEscritorio(Escritorio escritorio);

    @Query("SELECT e from Empresa e WHERE e.escritorio.id =?1")
    List<Empresa> findByEscritorioId(Long id);

    @Query("SELECT e FROM Empresa e WHERE lower(e.razaoSocial) LIKE lower(concat(?1,'%')) AND e.escritorio=?2")
	List<Empresa> findAllLikeEscritorio(String busca, Escritorio escritorio);

    @Query("SELECT e FROM Empresa e WHERE lower(e.razaoSocial) LIKE lower(concat(?1,'%')) AND e.escritorio=?2 AND e.status = 'true'")
	List<Empresa> findAllLikeEscritorioTrue(String busca, Escritorio escritorio);

    @Query("Select e FROM Empresa e where e.inscricaoEstadual=?1 and e.escritorio =?2")
    Optional<Empresa> findByInscricaoEstadual(String inscricao, Escritorio escritorio);

    @Query("SELECT distinct e FROM Empresa e join e.usuarios u join u.escritorios esc join u.perfis p WHERE p.nome =?1 AND esc.id =?2 AND e.razaoSocial LIKE ?3% AND u.status = true")
    List<Empresa> findAllLikeEscRoleTrue(NivelDeAcessoDoSistema roleAdminEscritorio, Long id, String busca);

    @Query("SELECT distinct e FROM Empresa e join e.usuarios u join u.perfis p WHERE p.nome =?1 AND e.razaoSocial LIKE ?2% AND u.status = true")
    List<Empresa> findAllLikeEmpRoleTrue(NivelDeAcessoDoSistema roleAdminEscritorio, String busca);
    
    @Query(value = "select e from Empresa e join e.usuarios u WHERE u.id =?1 AND lower(e.razaoSocial) LIKE lower(concat(?2,'%'))")
    List<Empresa> findAllLikeUsuarios(Long id, String busca);

    @Query(value = "select e from Empresa e join e.usuarios u WHERE u.id =?1 AND lower(e.razaoSocial) LIKE lower(concat(?2,'%'))  AND e.status = 'true'")
    List<Empresa> findAllLikeUsuariosTrue(Long id, String busca);

    @Query("select e from Empresa e where e.cnpj =?1 and e.escritorio =?2")
	Optional<Empresa> findByCNPJEscritorio (String cnpj, Escritorio escritorio);
    
}

