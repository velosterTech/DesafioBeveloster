//package br.com.wk.repository;
//
//import java.util.List;
//
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.domain.Sort;
//import br.com.wk.model.EmpresaCliente;
//import br.com.wk.model.Mensagem;
//import br.com.wk.model.Usuario;
//
//public interface MensagemRepository extends CrudRepository<Mensagem, Long>{
//    @Query(value = "SELECT u FROM Mensagem u where u.usuarioDestino =?1")
//    List<Mensagem> findAllSpec(Usuario usuario, Sort sort);
//}
