package br.com.wk.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="saffo_frequencia")
public class Frequencia {

	@Column(name= "saffo_frequencia_id")
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqGeneratorFrequencia")
    @SequenceGenerator(name = "seqGeneratorFrequencia", sequenceName="SAFFO_FREQUENCIA_SEQ", allocationSize=1)		
	private Long id;
	
	@Column(name= "saffo_frequencia_nome")
	private String nome;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, nome);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Frequencia))
			return false;
		Frequencia other = (Frequencia) obj;
		return Objects.equals(id, other.id) && Objects.equals(nome, other.nome);
	}
	
	
}
