
package br.com.wk.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue(value = "USUARIO_EMPRESA")
public class UsuarioEmpresa extends Usuario{

	private static final long serialVersionUID = 1L;

	@Column(name = "saffo_usuario_cpf")
    private String cpf;

    @Column(name = "saffo_usuario_rg")
    private String rg;
    
    @Column(name = "saffo_usuario_contato")
    private String contato;
    
    @Column(name = "saffo_usuario_email")
    private String email;
    
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }
    
    

}

