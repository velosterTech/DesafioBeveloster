package br.com.wk.enums;

public enum NaturezaJuridica {
    MEI, EI, LTDA, SA, SLU;
}
