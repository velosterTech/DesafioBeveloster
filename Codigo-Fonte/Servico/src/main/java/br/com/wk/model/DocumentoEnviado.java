package br.com.wk.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "saffo_documento_enviado")
public class DocumentoEnviado {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "saffo_documento_enviado_id")
    private Long id;

    @Column(name = "saffo_documento_enviado_valor")
    private Double valor;

    @Column(name = "saffo_documento_enviado_vencimento")
    private String vencimento;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "saffo_empresa_id")
    private Empresa empresa;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "saffo_documento_id")
    private Documento documento;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "saffo_escritorio_id")
    private Escritorio escritorio;

    public DocumentoEnviado(Double valor, String vencimento, Empresa empresa, Documento documento,
            Escritorio escritorio) {
        this.valor = valor;
        this.vencimento = vencimento;
        this.empresa = empresa;
        this.documento = documento;
        this.escritorio = escritorio;
    }

    public DocumentoEnviado() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Escritorio getEscritorio() {
        return escritorio;
    }

    public void setEscritorio(Escritorio escritorio) {
        this.escritorio = escritorio;
    }
}
