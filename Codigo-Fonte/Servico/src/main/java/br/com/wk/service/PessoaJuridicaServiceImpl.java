
//package br.com.wk.service;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import br.com.wk.model.Escritorio;
//import br.com.wk.model.PessoaJuridica;
//import br.com.wk.repository.PessoaJuridicaRepository;
//
//@Service
//public class PessoaJuridicaServiceImpl implements PessoaJuridicaService {
//	@Autowired
//	private PessoaJuridicaRepository pessoajuridicaRepository;
//
//	@Override
//	public PessoaJuridica save(PessoaJuridica pessoajuridica) {
//		Escritorio escritorio = new Escritorio();
//		escritorio.setPessoaJuridica(pessoajuridica);
//		return pessoajuridicaRepository.save(pessoajuridica);
//	}
//
//	@Override
//	public List<PessoaJuridica> findAll() {
//
//		return pessoajuridicaRepository.findAll();
//	}
//
//	@Override
//	public Optional<PessoaJuridica> findById(Long id) {
//
//		return pessoajuridicaRepository.findById(id);
//	}
//
//	@Override
//	public PessoaJuridica update(PessoaJuridica pessoajuridica) {
//
//		return pessoajuridicaRepository.save(pessoajuridica);
//	}
//
//	@Override
//	public void deleteById(Long id) {
//		pessoajuridicaRepository.deleteById(id);
//	}
//
//	@Override
//	public PessoaJuridica findByCNPJ(String cnpj) {
//
//		return pessoajuridicaRepository.findPJByCNPJ(cnpj);
//
//	}
//
//	@Override
//	public PessoaJuridica findByRazao(String razao_social) {
//		return pessoajuridicaRepository.findByRazao(razao_social);
//	}
//
//	@Override
//	public PessoaJuridica findByInscricaoEstadual(Long inscricao) {
//		return pessoajuridicaRepository.findPJByInscricaoEstadual(inscricao);
//	}
//}
